<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
//        'css/bootstrap.min.css',
        'css/site.css',
        'css/font-awesome-4.7.0/css/font-awesome.css',
        'css/jquery.fancybox.min.css',
//        'css/jquery.fancybox.css',
    ];
    public $js = [
        'js/bootstrap-filestyle.min.js',
        'js/jquery.fancybox.min.js',
        'js/main.js',
//        'js/jquery.fancybox-1.2.1.pack.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
