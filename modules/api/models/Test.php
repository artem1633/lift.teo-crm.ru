<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05.05.2020
 * Time: 14:58
 */

namespace app\modules\api\models;


use app\models\Settings;
use app\models\Users;
use yii\base\Model;
use yii\helpers\Url;

/**
 * // * @property string $request_name Имя запроса (Напр. get-mechanism-type)
 * @property array $methods АПИ Методы
 * @property string $method АПИ Метод
 * @property string $access_token Токен доступа
 * @property string $response JSON Ответ на АПИ запрос
 * @property int as_array Ответ на АПИ запрос выводится в виде массива
 * @property Users $identity
 */
class Test extends Model
{
    public $identity;
//    public $request_name;
    public $methods;
    public $method;
    public $access_token;
    public $component_id;
    public $pagination_page_size;
    public $pagination_page_num;
    public $brand_id;
    public $group_id;
    public $type_id;
    public $response;
    public $as_array;


    public function init()
    {
        parent::init();
        $this->identity = \Yii::$app->user->identity;
        $this->methods = $this->getMethods();
        $this->access_token = Settings::getValueByKey('access_token');
        $this->as_array = 1;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['request_name', 'method', 'access_token', 'response'], 'string'],
            [
                ['component_id', 'pagination_page_size', 'pagination_page_num', 'brand_id', 'group_id', 'type_id', 'as_array'],
                'integer'
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'request_name' => 'Наименование запроса',
            'methods' => 'Доступные методы',
            'method' => 'Метод',
            'access_token' => 'Токен доступа',
            'as_array' => 'Ответ вывести как массив',
        ];
    }

    /**
     * Список методов
     * @return array
     */
    public function getMethods()
    {
        return [
            'get-nomenclature' => 'Номенкатура специалиста (get-nomenclature)',
            'get-brand' => 'Бренды (get-brand)',
            'get-group' => 'Группы (get-group)',
            'get-part-type' => 'Типы (get-part-type)',
            'get-mechanism-type' => 'Тип механизма (get-mechanism-type)'
        ];
    }

    /**
     * Отправка АПИ запроса
     */
    public function sendRequest()
    {
        $params = [
            'access_token' => $this->access_token,
        ];

        switch ($this->method) {
            case 'get-nomenclature':
                $params['component_id'] = $this->component_id;
                $params['pagination_page_size'] = $this->pagination_page_size;
                $params['pagination_page_num'] = $this->pagination_page_num;
                break;
            case 'get-brand':
                $params['brand_id'] = $this->brand_id;
                break;
            case 'get-group':
                $params['group_id'] = $this->group_id;
                break;
            case 'get-part-type' || 'get-mechanism-type':
                $params['type_id'] = $this->type_id;
                break;
            default:
                return false;
        }

        $url = Url::to(['/api/v1/' . $this->method . '?' . http_build_query($params)], true);

        \Yii::info($url, 'test');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $this->response = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($this->response, true);

        \Yii::info($result, 'test');

        if ($result['success'] == 'false'){
            $this->response = $result['error'];
        } else {
            if ($this->as_array){
                $this->response = $result;
            }
        }
    }


}