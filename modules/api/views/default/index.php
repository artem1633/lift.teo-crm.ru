<?php

use app\models\Users;
use yii\helpers\Html;

$this->title = 'АПИ';
echo Html::a('Тестирование АПИ', ['/api/test'], [
        'class' => 'btn btn-info pull-right',
    'style' => 'text-decoration: none;'
])
?>
<html xmlns:v="urn:schemas-microsoft-com:vml"
      xmlns:o="urn:schemas-microsoft-com:office:office"
      xmlns:w="urn:schemas-microsoft-com:office:word"
      xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
      xmlns="http://www.w3.org/TR/REC-html40">

<head>
    <meta http-equiv=Content-Type content="text/html; charset=windows-1251">
    <meta name=ProgId content=Word.Document>
    <meta name=Generator content="Microsoft Word 15">
    <meta name=Originator content="Microsoft Word 15">
    <link rel=File-List href="Описание%20АПИ.files/filelist.xml">
    <!--[if gte mso 9]>
    <xml>
        <o:DocumentProperties>
            <o:Author>user</o:Author>
            <o:LastAuthor>user</o:LastAuthor>
            <o:Revision>2</o:Revision>
            <o:TotalTime>131</o:TotalTime>
            <o:Created>2019-05-31T09:32:00Z</o:Created>
            <o:LastSaved>2019-05-31T09:32:00Z</o:LastSaved>
            <o:Pages>7</o:Pages>
            <o:Words>797</o:Words>
            <o:Characters>4548</o:Characters>
            <o:Lines>37</o:Lines>
            <o:Paragraphs>10</o:Paragraphs>
            <o:CharactersWithSpaces>5335</o:CharactersWithSpaces>
            <o:Version>16.00</o:Version>
        </o:DocumentProperties>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
        </o:OfficeDocumentSettings>
    </xml><![endif]-->
    <link rel=dataStoreItem href="Описание%20АПИ.files/item0001.xml"
          target="Описание%20АПИ.files/props002.xml">
    <link rel=themeData href="Описание%20АПИ.files/themedata.thmx">
    <link rel=colorSchemeMapping href="Описание%20АПИ.files/colorschememapping.xml">
    <!--[if gte mso 9]>
    <xml>
        <w:WordDocument>
            <w:TrackMoves>false</w:TrackMoves>
            <w:TrackFormatting/>
            <w:PunctuationKerning/>
            <w:ValidateAgainstSchemas/>
            <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
            <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
            <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
            <w:DoNotPromoteQF/>
            <w:LidThemeOther>RU</w:LidThemeOther>
            <w:LidThemeAsian>X-NONE</w:LidThemeAsian>
            <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>
            <w:Compatibility>
                <w:BreakWrappedTables/>
                <w:SnapToGridInCell/>
                <w:WrapTextWithPunct/>
                <w:UseAsianBreakRules/>
                <w:DontGrowAutofit/>
                <w:SplitPgBreakAndParaMark/>
                <w:EnableOpenTypeKerning/>
                <w:DontFlipMirrorIndents/>
                <w:OverrideTableStyleHps/>
            </w:Compatibility>
            <m:mathPr>
                <m:mathFont m:val="Cambria Math"/>
                <m:brkBin m:val="before"/>
                <m:brkBinSub m:val="&#45;-"/>
                <m:smallFrac m:val="off"/>
                <m:dispDef/>
                <m:lMargin m:val="0"/>
                <m:rMargin m:val="0"/>
                <m:defJc m:val="centerGroup"/>
                <m:wrapIndent m:val="1440"/>
                <m:intLim m:val="subSup"/>
                <m:naryLim m:val="undOvr"/>
            </m:mathPr>
        </w:WordDocument>
    </xml><![endif]--><!--[if gte mso 9]>
    <xml>
        <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="false"
                        DefSemiHidden="false" DefQFormat="false" DefPriority="99"
                        LatentStyleCount="371">
            <w:LsdException Locked="false" Priority="0" QFormat="true" Name="Normal"/>
            <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 1"/>
            <w:LsdException Locked="false" Priority="9" SemiHidden="true"
                            UnhideWhenUsed="true" QFormat="true" Name="heading 2"/>
            <w:LsdException Locked="false" Priority="9" SemiHidden="true"
                            UnhideWhenUsed="true" QFormat="true" Name="heading 3"/>
            <w:LsdException Locked="false" Priority="9" SemiHidden="true"
                            UnhideWhenUsed="true" QFormat="true" Name="heading 4"/>
            <w:LsdException Locked="false" Priority="9" SemiHidden="true"
                            UnhideWhenUsed="true" QFormat="true" Name="heading 5"/>
            <w:LsdException Locked="false" Priority="9" SemiHidden="true"
                            UnhideWhenUsed="true" QFormat="true" Name="heading 6"/>
            <w:LsdException Locked="false" Priority="9" SemiHidden="true"
                            UnhideWhenUsed="true" QFormat="true" Name="heading 7"/>
            <w:LsdException Locked="false" Priority="9" SemiHidden="true"
                            UnhideWhenUsed="true" QFormat="true" Name="heading 8"/>
            <w:LsdException Locked="false" Priority="9" SemiHidden="true"
                            UnhideWhenUsed="true" QFormat="true" Name="heading 9"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="index 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="index 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="index 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="index 4"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="index 5"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="index 6"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="index 7"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="index 8"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="index 9"/>
            <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                            UnhideWhenUsed="true" Name="toc 1"/>
            <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                            UnhideWhenUsed="true" Name="toc 2"/>
            <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                            UnhideWhenUsed="true" Name="toc 3"/>
            <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                            UnhideWhenUsed="true" Name="toc 4"/>
            <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                            UnhideWhenUsed="true" Name="toc 5"/>
            <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                            UnhideWhenUsed="true" Name="toc 6"/>
            <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                            UnhideWhenUsed="true" Name="toc 7"/>
            <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                            UnhideWhenUsed="true" Name="toc 8"/>
            <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                            UnhideWhenUsed="true" Name="toc 9"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Normal Indent"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="footnote text"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="annotation text"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="header"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="footer"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="index heading"/>
            <w:LsdException Locked="false" Priority="35" SemiHidden="true"
                            UnhideWhenUsed="true" QFormat="true" Name="caption"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="table of figures"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="envelope address"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="envelope return"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="footnote reference"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="annotation reference"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="line number"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="page number"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="endnote reference"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="endnote text"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="table of authorities"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="macro"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="toa heading"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Bullet"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Number"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List 4"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List 5"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Bullet 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Bullet 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Bullet 4"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Bullet 5"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Number 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Number 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Number 4"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Number 5"/>
            <w:LsdException Locked="false" Priority="10" QFormat="true" Name="Title"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Closing"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Signature"/>
            <w:LsdException Locked="false" Priority="1" SemiHidden="true"
                            UnhideWhenUsed="true" Name="Default Paragraph Font"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Body Text"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Body Text Indent"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Continue"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Continue 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Continue 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Continue 4"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Continue 5"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Message Header"/>
            <w:LsdException Locked="false" Priority="11" QFormat="true" Name="Subtitle"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Salutation"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Date"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Body Text First Indent"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Body Text First Indent 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Note Heading"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Body Text 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Body Text 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Body Text Indent 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Body Text Indent 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Block Text"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Hyperlink"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="FollowedHyperlink"/>
            <w:LsdException Locked="false" Priority="22" QFormat="true" Name="Strong"/>
            <w:LsdException Locked="false" Priority="20" QFormat="true" Name="Emphasis"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Document Map"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Plain Text"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="E-mail Signature"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Top of Form"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Bottom of Form"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Normal (Web)"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Acronym"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Address"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Cite"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Code"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Definition"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Keyboard"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Preformatted"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Sample"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Typewriter"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Variable"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Normal Table"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="annotation subject"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="No List"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Outline List 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Outline List 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Outline List 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Simple 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Simple 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Simple 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Classic 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Classic 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Classic 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Classic 4"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Colorful 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Colorful 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Colorful 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Columns 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Columns 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Columns 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Columns 4"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Columns 5"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Grid 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Grid 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Grid 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Grid 4"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Grid 5"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Grid 6"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Grid 7"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Grid 8"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table List 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table List 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table List 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table List 4"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table List 5"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table List 6"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table List 7"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table List 8"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table 3D effects 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table 3D effects 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table 3D effects 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Contemporary"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Elegant"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Professional"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Subtle 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Subtle 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Web 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Web 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Web 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Balloon Text"/>
            <w:LsdException Locked="false" Priority="39" Name="Table Grid"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Theme"/>
            <w:LsdException Locked="false" SemiHidden="true" Name="Placeholder Text"/>
            <w:LsdException Locked="false" Priority="1" QFormat="true" Name="No Spacing"/>
            <w:LsdException Locked="false" Priority="60" Name="Light Shading"/>
            <w:LsdException Locked="false" Priority="61" Name="Light List"/>
            <w:LsdException Locked="false" Priority="62" Name="Light Grid"/>
            <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1"/>
            <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2"/>
            <w:LsdException Locked="false" Priority="65" Name="Medium List 1"/>
            <w:LsdException Locked="false" Priority="66" Name="Medium List 2"/>
            <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1"/>
            <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2"/>
            <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3"/>
            <w:LsdException Locked="false" Priority="70" Name="Dark List"/>
            <w:LsdException Locked="false" Priority="71" Name="Colorful Shading"/>
            <w:LsdException Locked="false" Priority="72" Name="Colorful List"/>
            <w:LsdException Locked="false" Priority="73" Name="Colorful Grid"/>
            <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 1"/>
            <w:LsdException Locked="false" Priority="61" Name="Light List Accent 1"/>
            <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 1"/>
            <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 1"/>
            <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 1"/>
            <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 1"/>
            <w:LsdException Locked="false" SemiHidden="true" Name="Revision"/>
            <w:LsdException Locked="false" Priority="34" QFormat="true"
                            Name="List Paragraph"/>
            <w:LsdException Locked="false" Priority="29" QFormat="true" Name="Quote"/>
            <w:LsdException Locked="false" Priority="30" QFormat="true"
                            Name="Intense Quote"/>
            <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 1"/>
            <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 1"/>
            <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 1"/>
            <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 1"/>
            <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 1"/>
            <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 1"/>
            <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 1"/>
            <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 1"/>
            <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 2"/>
            <w:LsdException Locked="false" Priority="61" Name="Light List Accent 2"/>
            <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 2"/>
            <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 2"/>
            <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 2"/>
            <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 2"/>
            <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 2"/>
            <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 2"/>
            <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 2"/>
            <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 2"/>
            <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 2"/>
            <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 2"/>
            <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 2"/>
            <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 2"/>
            <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 3"/>
            <w:LsdException Locked="false" Priority="61" Name="Light List Accent 3"/>
            <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 3"/>
            <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 3"/>
            <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 3"/>
            <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 3"/>
            <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 3"/>
            <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 3"/>
            <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 3"/>
            <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 3"/>
            <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 3"/>
            <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 3"/>
            <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 3"/>
            <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 3"/>
            <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 4"/>
            <w:LsdException Locked="false" Priority="61" Name="Light List Accent 4"/>
            <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 4"/>
            <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 4"/>
            <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 4"/>
            <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 4"/>
            <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 4"/>
            <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 4"/>
            <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 4"/>
            <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 4"/>
            <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 4"/>
            <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 4"/>
            <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 4"/>
            <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 4"/>
            <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 5"/>
            <w:LsdException Locked="false" Priority="61" Name="Light List Accent 5"/>
            <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 5"/>
            <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 5"/>
            <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 5"/>
            <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 5"/>
            <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 5"/>
            <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 5"/>
            <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 5"/>
            <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 5"/>
            <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 5"/>
            <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 5"/>
            <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 5"/>
            <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 5"/>
            <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 6"/>
            <w:LsdException Locked="false" Priority="61" Name="Light List Accent 6"/>
            <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 6"/>
            <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 6"/>
            <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 6"/>
            <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 6"/>
            <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 6"/>
            <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 6"/>
            <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 6"/>
            <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 6"/>
            <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 6"/>
            <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 6"/>
            <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 6"/>
            <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 6"/>
            <w:LsdException Locked="false" Priority="19" QFormat="true"
                            Name="Subtle Emphasis"/>
            <w:LsdException Locked="false" Priority="21" QFormat="true"
                            Name="Intense Emphasis"/>
            <w:LsdException Locked="false" Priority="31" QFormat="true"
                            Name="Subtle Reference"/>
            <w:LsdException Locked="false" Priority="32" QFormat="true"
                            Name="Intense Reference"/>
            <w:LsdException Locked="false" Priority="33" QFormat="true" Name="Book Title"/>
            <w:LsdException Locked="false" Priority="37" SemiHidden="true"
                            UnhideWhenUsed="true" Name="Bibliography"/>
            <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                            UnhideWhenUsed="true" QFormat="true" Name="TOC Heading"/>
            <w:LsdException Locked="false" Priority="41" Name="Plain Table 1"/>
            <w:LsdException Locked="false" Priority="42" Name="Plain Table 2"/>
            <w:LsdException Locked="false" Priority="43" Name="Plain Table 3"/>
            <w:LsdException Locked="false" Priority="44" Name="Plain Table 4"/>
            <w:LsdException Locked="false" Priority="45" Name="Plain Table 5"/>
            <w:LsdException Locked="false" Priority="40" Name="Grid Table Light"/>
            <w:LsdException Locked="false" Priority="46" Name="Grid Table 1 Light"/>
            <w:LsdException Locked="false" Priority="47" Name="Grid Table 2"/>
            <w:LsdException Locked="false" Priority="48" Name="Grid Table 3"/>
            <w:LsdException Locked="false" Priority="49" Name="Grid Table 4"/>
            <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark"/>
            <w:LsdException Locked="false" Priority="51" Name="Grid Table 6 Colorful"/>
            <w:LsdException Locked="false" Priority="52" Name="Grid Table 7 Colorful"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="Grid Table 1 Light Accent 1"/>
            <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 1"/>
            <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 1"/>
            <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 1"/>
            <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 1"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="Grid Table 6 Colorful Accent 1"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="Grid Table 7 Colorful Accent 1"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="Grid Table 1 Light Accent 2"/>
            <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 2"/>
            <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 2"/>
            <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 2"/>
            <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 2"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="Grid Table 6 Colorful Accent 2"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="Grid Table 7 Colorful Accent 2"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="Grid Table 1 Light Accent 3"/>
            <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 3"/>
            <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 3"/>
            <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 3"/>
            <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 3"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="Grid Table 6 Colorful Accent 3"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="Grid Table 7 Colorful Accent 3"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="Grid Table 1 Light Accent 4"/>
            <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 4"/>
            <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 4"/>
            <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 4"/>
            <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 4"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="Grid Table 6 Colorful Accent 4"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="Grid Table 7 Colorful Accent 4"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="Grid Table 1 Light Accent 5"/>
            <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 5"/>
            <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 5"/>
            <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 5"/>
            <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 5"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="Grid Table 6 Colorful Accent 5"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="Grid Table 7 Colorful Accent 5"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="Grid Table 1 Light Accent 6"/>
            <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 6"/>
            <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 6"/>
            <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 6"/>
            <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 6"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="Grid Table 6 Colorful Accent 6"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="Grid Table 7 Colorful Accent 6"/>
            <w:LsdException Locked="false" Priority="46" Name="List Table 1 Light"/>
            <w:LsdException Locked="false" Priority="47" Name="List Table 2"/>
            <w:LsdException Locked="false" Priority="48" Name="List Table 3"/>
            <w:LsdException Locked="false" Priority="49" Name="List Table 4"/>
            <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark"/>
            <w:LsdException Locked="false" Priority="51" Name="List Table 6 Colorful"/>
            <w:LsdException Locked="false" Priority="52" Name="List Table 7 Colorful"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="List Table 1 Light Accent 1"/>
            <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 1"/>
            <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 1"/>
            <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 1"/>
            <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 1"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="List Table 6 Colorful Accent 1"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="List Table 7 Colorful Accent 1"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="List Table 1 Light Accent 2"/>
            <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 2"/>
            <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 2"/>
            <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 2"/>
            <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 2"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="List Table 6 Colorful Accent 2"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="List Table 7 Colorful Accent 2"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="List Table 1 Light Accent 3"/>
            <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 3"/>
            <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 3"/>
            <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 3"/>
            <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 3"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="List Table 6 Colorful Accent 3"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="List Table 7 Colorful Accent 3"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="List Table 1 Light Accent 4"/>
            <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 4"/>
            <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 4"/>
            <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 4"/>
            <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 4"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="List Table 6 Colorful Accent 4"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="List Table 7 Colorful Accent 4"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="List Table 1 Light Accent 5"/>
            <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 5"/>
            <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 5"/>
            <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 5"/>
            <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 5"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="List Table 6 Colorful Accent 5"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="List Table 7 Colorful Accent 5"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="List Table 1 Light Accent 6"/>
            <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 6"/>
            <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 6"/>
            <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 6"/>
            <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 6"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="List Table 6 Colorful Accent 6"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="List Table 7 Colorful Accent 6"/>
        </w:LatentStyles>
    </xml><![endif]-->
    <style>
        <!--
        /* Font Definitions */
        @font-face {
            font-family: "Cambria Math";
            panose-1: 2 4 5 3 5 4 6 3 2 4;
            mso-font-charset: 1;
            mso-generic-font-family: roman;
            mso-font-pitch: variable;
            mso-font-signature: 0 0 0 0 0 0;
        }

        @font-face {
            font-family: "Calibri Light";
            panose-1: 2 15 3 2 2 2 4 3 2 4;
            mso-font-charset: 204;
            mso-generic-font-family: swiss;
            mso-font-pitch: variable;
            mso-font-signature: -536859905 -1073732485 9 0 511 0;
        }

        @font-face {
            font-family: Calibri;
            panose-1: 2 15 5 2 2 2 4 3 2 4;
            mso-font-charset: 204;
            mso-generic-font-family: swiss;
            mso-font-pitch: variable;
            mso-font-signature: -536859905 -1073732485 9 0 511 0;
        }

        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal {
            mso-style-unhide: no;
            mso-style-qformat: yes;
            mso-style-parent: "";
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 8.0pt;
            margin-left: 0cm;
            line-height: 107%;
            mso-pagination: widow-orphan;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: Calibri;
            mso-fareast-theme-font: minor-latin;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
            mso-fareast-language: EN-US;
        }

        h1 {
            mso-style-priority: 9;
            mso-style-unhide: no;
            mso-style-qformat: yes;
            mso-style-link: "Заголовок 1 Знак";
            mso-style-next: Обычный;
            margin-top: 12.0pt;
            margin-right: 0cm;
            margin-bottom: 0cm;
            margin-left: 0cm;
            margin-bottom: .0001pt;
            line-height: 107%;
            mso-pagination: widow-orphan lines-together;
            page-break-after: avoid;
            mso-outline-level: 1;
            font-size: 16.0pt;
            font-family: "Calibri Light", sans-serif;
            mso-ascii-font-family: "Calibri Light";
            mso-ascii-theme-font: major-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: major-fareast;
            mso-hansi-font-family: "Calibri Light";
            mso-hansi-theme-font: major-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: major-bidi;
            color: #2E74B5;
            mso-themecolor: accent1;
            mso-themeshade: 191;
            mso-font-kerning: 0pt;
            mso-fareast-language: EN-US;
            font-weight: normal;
        }

        h3 {
            mso-style-priority: 9;
            mso-style-unhide: no;
            mso-style-qformat: yes;
            mso-style-link: "Заголовок 3 Знак";
            mso-margin-top-alt: auto;
            margin-right: 0cm;
            mso-margin-bottom-alt: auto;
            margin-left: 0cm;
            mso-pagination: widow-orphan;
            mso-outline-level: 3;
            font-size: 13.5pt;
            font-family: "Times New Roman", serif;
            mso-fareast-font-family: "Times New Roman";
            font-weight: bold;
        }

        p.MsoToc1, li.MsoToc1, div.MsoToc1 {
            mso-style-update: auto;
            mso-style-priority: 39;
            mso-style-next: Обычный;
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 5.0pt;
            margin-left: 0cm;
            line-height: 107%;
            mso-pagination: widow-orphan;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: Calibri;
            mso-fareast-theme-font: minor-latin;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
            mso-fareast-language: EN-US;
        }

        p.MsoToc2, li.MsoToc2, div.MsoToc2 {
            mso-style-update: auto;
            mso-style-priority: 39;
            mso-style-next: Обычный;
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 5.0pt;
            margin-left: 11.0pt;
            line-height: 107%;
            mso-pagination: widow-orphan;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
        }

        p.MsoToc3, li.MsoToc3, div.MsoToc3 {
            mso-style-update: auto;
            mso-style-priority: 39;
            mso-style-next: Обычный;
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 5.0pt;
            margin-left: 22.0pt;
            line-height: 107%;
            mso-pagination: widow-orphan;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: Calibri;
            mso-fareast-theme-font: minor-latin;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
            mso-fareast-language: EN-US;
        }

        p.MsoToc4, li.MsoToc4, div.MsoToc4 {
            mso-style-update: auto;
            mso-style-priority: 39;
            mso-style-next: Обычный;
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 5.0pt;
            margin-left: 33.0pt;
            line-height: 107%;
            mso-pagination: widow-orphan;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
        }

        p.MsoToc5, li.MsoToc5, div.MsoToc5 {
            mso-style-update: auto;
            mso-style-priority: 39;
            mso-style-next: Обычный;
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 5.0pt;
            margin-left: 44.0pt;
            line-height: 107%;
            mso-pagination: widow-orphan;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
        }

        p.MsoToc6, li.MsoToc6, div.MsoToc6 {
            mso-style-update: auto;
            mso-style-priority: 39;
            mso-style-next: Обычный;
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 5.0pt;
            margin-left: 55.0pt;
            line-height: 107%;
            mso-pagination: widow-orphan;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
        }

        p.MsoToc7, li.MsoToc7, div.MsoToc7 {
            mso-style-update: auto;
            mso-style-priority: 39;
            mso-style-next: Обычный;
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 5.0pt;
            margin-left: 66.0pt;
            line-height: 107%;
            mso-pagination: widow-orphan;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
        }

        p.MsoToc8, li.MsoToc8, div.MsoToc8 {
            mso-style-update: auto;
            mso-style-priority: 39;
            mso-style-next: Обычный;
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 5.0pt;
            margin-left: 77.0pt;
            line-height: 107%;
            mso-pagination: widow-orphan;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
        }

        p.MsoToc9, li.MsoToc9, div.MsoToc9 {
            mso-style-update: auto;
            mso-style-priority: 39;
            mso-style-next: Обычный;
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 5.0pt;
            margin-left: 88.0pt;
            line-height: 107%;
            mso-pagination: widow-orphan;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: minor-fareast;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
        }

        p.MsoHeader, li.MsoHeader, div.MsoHeader {
            mso-style-priority: 99;
            mso-style-link: "Верхний колонтитул Знак";
            margin: 0cm;
            margin-bottom: .0001pt;
            mso-pagination: widow-orphan;
            tab-stops: center 233.85pt right 467.75pt;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: Calibri;
            mso-fareast-theme-font: minor-latin;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
            mso-fareast-language: EN-US;
        }

        p.MsoFooter, li.MsoFooter, div.MsoFooter {
            mso-style-priority: 99;
            mso-style-link: "Нижний колонтитул Знак";
            margin: 0cm;
            margin-bottom: .0001pt;
            mso-pagination: widow-orphan;
            tab-stops: center 233.85pt right 467.75pt;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: Calibri;
            mso-fareast-theme-font: minor-latin;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
            mso-fareast-language: EN-US;
        }

        a:link, span.MsoHyperlink {
            mso-style-priority: 99;
            color: blue;
            text-decoration: underline;
            text-underline: single;
        }

        a:visited, span.MsoHyperlinkFollowed {
            mso-style-noshow: yes;
            mso-style-priority: 99;
            color: #954F72;
            mso-themecolor: followedhyperlink;
            text-decoration: underline;
            text-underline: single;
        }

        p {
            mso-style-noshow: yes;
            mso-style-priority: 99;
            mso-margin-top-alt: auto;
            margin-right: 0cm;
            mso-margin-bottom-alt: auto;
            margin-left: 0cm;
            mso-pagination: widow-orphan;
            font-size: 12.0pt;
            font-family: "Times New Roman", serif;
            mso-fareast-font-family: "Times New Roman";
        }

        p.MsoTocHeading, li.MsoTocHeading, div.MsoTocHeading {
            mso-style-priority: 39;
            mso-style-qformat: yes;
            mso-style-parent: "Заголовок 1";
            mso-style-next: Обычный;
            margin-top: 12.0pt;
            margin-right: 0cm;
            margin-bottom: 0cm;
            margin-left: 0cm;
            margin-bottom: .0001pt;
            line-height: 107%;
            mso-pagination: widow-orphan lines-together;
            page-break-after: avoid;
            font-size: 16.0pt;
            font-family: "Calibri Light", sans-serif;
            mso-ascii-font-family: "Calibri Light";
            mso-ascii-theme-font: major-latin;
            mso-fareast-font-family: "Times New Roman";
            mso-fareast-theme-font: major-fareast;
            mso-hansi-font-family: "Calibri Light";
            mso-hansi-theme-font: major-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: major-bidi;
            color: #2E74B5;
            mso-themecolor: accent1;
            mso-themeshade: 191;
        }

        span

        .3
        {
            mso-style-name: "Заголовок 3 Знак"
        ;
            mso-style-priority: 9
        ;
            mso-style-unhide: no
        ;
            mso-style-locked: yes
        ;
            mso-style-link: "Заголовок 3"
        ;
            mso-ansi-font-size: 13.5pt
        ;
            mso-bidi-font-size: 13.5pt
        ;
            font-family: "Times New Roman", serif
        ;
            mso-ascii-font-family: "Times New Roman"
        ;
            mso-fareast-font-family: "Times New Roman"
        ;
            mso-hansi-font-family: "Times New Roman"
        ;
            mso-bidi-font-family: "Times New Roman"
        ;
            mso-fareast-language: RU
        ;
            font-weight: bold
        ;
        }
        span.a {
            mso-style-name: "Верхний колонтитул Знак";
            mso-style-priority: 99;
            mso-style-unhide: no;
            mso-style-locked: yes;
            mso-style-link: "Верхний колонтитул";
        }

        span.a0 {
            mso-style-name: "Нижний колонтитул Знак";
            mso-style-priority: 99;
            mso-style-unhide: no;
            mso-style-locked: yes;
            mso-style-link: "Нижний колонтитул";
        }

        span

        .1
        {
            mso-style-name: "Заголовок 1 Знак"
        ;
            mso-style-priority: 9
        ;
            mso-style-unhide: no
        ;
            mso-style-locked: yes
        ;
            mso-style-link: "Заголовок 1"
        ;
            mso-ansi-font-size: 16.0pt
        ;
            mso-bidi-font-size: 16.0pt
        ;
            font-family: "Calibri Light", sans-serif
        ;
            mso-ascii-font-family: "Calibri Light"
        ;
            mso-ascii-theme-font: major-latin
        ;
            mso-fareast-font-family: "Times New Roman"
        ;
            mso-fareast-theme-font: major-fareast
        ;
            mso-hansi-font-family: "Calibri Light"
        ;
            mso-hansi-theme-font: major-latin
        ;
            mso-bidi-font-family: "Times New Roman"
        ;
            mso-bidi-theme-font: major-bidi
        ;
            color: #2E74B5
        ;
            mso-themecolor: accent1
        ;
            mso-themeshade: 191
        ;
        }
        .MsoChpDefault {
            mso-style-type: export-only;
            mso-default-props: yes;
            font-family: "Calibri", sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: Calibri;
            mso-fareast-theme-font: minor-latin;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
            mso-fareast-language: EN-US;
        }

        .MsoPapDefault {
            mso-style-type: export-only;
            margin-bottom: 8.0pt;
            line-height: 107%;
        }

        /* Page Definitions */
        @page WordSection1 {
            size: 595.3pt 841.9pt;
            margin: 2.0cm 42.5pt 2.0cm 3.0cm;
            mso-header-margin: 35.4pt;
            mso-footer-margin: 35.4pt;
            mso-paper-source: 0;
        }

        div.WordSection1 {
            page: WordSection1;
        }

        -->
    </style>
    <!--[if gte mso 10]>
    <style>
        /* Style Definitions */
        table.MsoNormalTable {
            mso-style-name: "Обычная таблица";
            mso-tstyle-rowband-size: 0;
            mso-tstyle-colband-size: 0;
            mso-style-noshow: yes;
            mso-style-priority: 99;
            mso-style-parent: "";
            mso-padding-alt: 0cm 5.4pt 0cm 5.4pt;
            mso-para-margin-top: 0cm;
            mso-para-margin-right: 0cm;
            mso-para-margin-bottom: 8.0pt;
            mso-para-margin-left: 0cm;
            line-height: 107%;
            mso-pagination: widow-orphan;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
            mso-fareast-language: EN-US;
        }
    </style>
    <![endif]--><!--[if gte mso 9]>
    <xml>
        <o:shapedefaults v:ext="edit" spidmax="1026"/>
    </xml><![endif]--><!--[if gte mso 9]>
    <xml>
        <o:shapelayout v:ext="edit">
            <o:idmap v:ext="edit" data="1"/>
        </o:shapelayout>
    </xml><![endif]-->
</head>

<body lang=RU link=blue vlink="#954F72" style='tab-interval:35.4pt'>

<div class=WordSection1>

    <p class=MsoToc1 style='tab-stops:right dotted 467.25pt'><span
                style='font-size:13.5pt;line-height:107%;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'>Оглавление:<o:p></o:p></span></p>

    <p class=MsoToc1 style='tab-stops:right dotted 467.25pt'><!--[if supportFields]><span
                lang=EN-US style='font-size:13.5pt;line-height:107%;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US;
mso-fareast-language:RU'><span style='mso-element:field-begin'></span><span
                style='mso-spacerun:yes'> </span>TOC \o &quot;1-1&quot; \h \z \u <span
                style='mso-element:field-separator'></span></span><![endif]--><a
                href="#_Toc10197923"><span style='mso-fareast-font-family:"Times New Roman";
mso-fareast-language:RU;mso-no-proof:yes'>Общее описание</span><span
                    style='color:windowtext;display:none;mso-hide:screen;mso-no-proof:yes;
text-decoration:none;text-underline:none'><span style='mso-tab-count:1 dotted'>. </span></span><!--[if supportFields]>
            <span
                    style='color:windowtext;display:none;mso-hide:screen;mso-no-proof:yes;
text-decoration:none;text-underline:none'><span style='mso-element:field-begin'></span>
PAGEREF _Toc10197923 \h <span style='mso-element:field-separator'></span></span><![endif]--><span
                    style='color:windowtext;display:none;mso-hide:screen;mso-no-proof:yes;
text-decoration:none;text-underline:none'>2<!--[if gte mso 9]>
                <xml>
                    <w:data>
                        08D0C9EA79F9BACE118C8200AA004BA90B02000000080000000D0000005F0054006F006300310030003100390037003900320033000000
                    </w:data>
                </xml><![endif]--></span><!--[if supportFields]><span style='color:windowtext;
display:none;mso-hide:screen;mso-no-proof:yes;text-decoration:none;text-underline:
none'><span style='mso-element:field-end'></span></span><![endif]--></a><span
                style='mso-fareast-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-fareast-language:RU;mso-no-proof:yes'><o:p></o:p></span></p>

    <p class=MsoToc1 style='tab-stops:right dotted 467.25pt'><a href="#_Toc10197924"><span
                    lang=EN-US style='mso-fareast-font-family:"Times New Roman";mso-ansi-language:
EN-US;mso-fareast-language:RU;mso-no-proof:yes'>get</span><span
                    style='mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU;
mso-no-proof:yes'>-</span><span lang=EN-US style='mso-fareast-font-family:"Times New Roman";
mso-ansi-language:EN-US;mso-fareast-language:RU;mso-no-proof:yes'>nomenclature</span><span
                    style='color:windowtext;display:none;mso-hide:screen;mso-no-proof:yes;
text-decoration:none;text-underline:none'><span style='mso-tab-count:1 dotted'>. </span></span><!--[if supportFields]>
            <span
                    style='color:windowtext;display:none;mso-hide:screen;mso-no-proof:yes;
text-decoration:none;text-underline:none'><span style='mso-element:field-begin'></span>
PAGEREF _Toc10197924 \h <span style='mso-element:field-separator'></span></span><![endif]--><span
                    style='color:windowtext;display:none;mso-hide:screen;mso-no-proof:yes;
text-decoration:none;text-underline:none'>3<!--[if gte mso 9]>
                <xml>
                    <w:data>
                        08D0C9EA79F9BACE118C8200AA004BA90B02000000080000000D0000005F0054006F006300310030003100390037003900320034000000
                    </w:data>
                </xml><![endif]--></span><!--[if supportFields]><span style='color:windowtext;
display:none;mso-hide:screen;mso-no-proof:yes;text-decoration:none;text-underline:
none'><span style='mso-element:field-end'></span></span><![endif]--></a><span
                style='mso-fareast-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-fareast-language:RU;mso-no-proof:yes'><o:p></o:p></span></p>

    <p class=MsoToc1 style='tab-stops:right dotted 467.25pt'><a href="#_Toc10197925"><span
                    lang=EN-US style='mso-fareast-font-family:"Times New Roman";mso-ansi-language:
EN-US;mso-fareast-language:RU;mso-no-proof:yes'>get</span><span
                    style='mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU;
mso-no-proof:yes'>-</span><span lang=EN-US style='mso-fareast-font-family:"Times New Roman";
mso-ansi-language:EN-US;mso-fareast-language:RU;mso-no-proof:yes'>brand</span><span
                    style='color:windowtext;display:none;mso-hide:screen;mso-no-proof:yes;
text-decoration:none;text-underline:none'><span style='mso-tab-count:1 dotted'>. </span></span><!--[if supportFields]>
            <span
                    style='color:windowtext;display:none;mso-hide:screen;mso-no-proof:yes;
text-decoration:none;text-underline:none'><span style='mso-element:field-begin'></span>
PAGEREF _Toc10197925 \h <span style='mso-element:field-separator'></span></span><![endif]--><span
                    style='color:windowtext;display:none;mso-hide:screen;mso-no-proof:yes;
text-decoration:none;text-underline:none'>4<!--[if gte mso 9]>
                <xml>
                    <w:data>
                        08D0C9EA79F9BACE118C8200AA004BA90B02000000080000000D0000005F0054006F006300310030003100390037003900320035000000
                    </w:data>
                </xml><![endif]--></span><!--[if supportFields]><span style='color:windowtext;
display:none;mso-hide:screen;mso-no-proof:yes;text-decoration:none;text-underline:
none'><span style='mso-element:field-end'></span></span><![endif]--></a><span
                style='mso-fareast-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-fareast-language:RU;mso-no-proof:yes'><o:p></o:p></span></p>

    <p class=MsoToc1 style='tab-stops:right dotted 467.25pt'><a href="#_Toc10197926"><span
                    lang=EN-US style='mso-fareast-font-family:"Times New Roman";mso-ansi-language:
EN-US;mso-fareast-language:RU;mso-no-proof:yes'>get</span><span
                    style='mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU;
mso-no-proof:yes'>-</span><span lang=EN-US style='mso-fareast-font-family:"Times New Roman";
mso-ansi-language:EN-US;mso-fareast-language:RU;mso-no-proof:yes'>group</span><span
                    style='color:windowtext;display:none;mso-hide:screen;mso-no-proof:yes;
text-decoration:none;text-underline:none'><span style='mso-tab-count:1 dotted'>. </span></span><!--[if supportFields]>
            <span
                    style='color:windowtext;display:none;mso-hide:screen;mso-no-proof:yes;
text-decoration:none;text-underline:none'><span style='mso-element:field-begin'></span>
PAGEREF _Toc10197926 \h <span style='mso-element:field-separator'></span></span><![endif]--><span
                    style='color:windowtext;display:none;mso-hide:screen;mso-no-proof:yes;
text-decoration:none;text-underline:none'>5<!--[if gte mso 9]>
                <xml>
                    <w:data>
                        08D0C9EA79F9BACE118C8200AA004BA90B02000000080000000D0000005F0054006F006300310030003100390037003900320036000000
                    </w:data>
                </xml><![endif]--></span><!--[if supportFields]><span style='color:windowtext;
display:none;mso-hide:screen;mso-no-proof:yes;text-decoration:none;text-underline:
none'><span style='mso-element:field-end'></span></span><![endif]--></a><span
                style='mso-fareast-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-fareast-language:RU;mso-no-proof:yes'><o:p></o:p></span></p>

    <p class=MsoToc1 style='tab-stops:right dotted 467.25pt'><a href="#_Toc10197927"><span
                    lang=EN-US style='mso-fareast-font-family:"Times New Roman";mso-ansi-language:
EN-US;mso-fareast-language:RU;mso-no-proof:yes'>get</span><span
                    style='mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU;
mso-no-proof:yes'>-</span><span lang=EN-US style='mso-fareast-font-family:"Times New Roman";
mso-ansi-language:EN-US;mso-fareast-language:RU;mso-no-proof:yes'>part-type</span><span
                    style='color:windowtext;display:none;mso-hide:screen;mso-no-proof:yes;
text-decoration:none;text-underline:none'><span style='mso-tab-count:1 dotted'>. </span></span><!--[if supportFields]>
            <span
                    style='color:windowtext;display:none;mso-hide:screen;mso-no-proof:yes;
text-decoration:none;text-underline:none'><span style='mso-element:field-begin'></span>
PAGEREF _Toc10197927 \h <span style='mso-element:field-separator'></span></span><![endif]--><span
                    style='color:windowtext;display:none;mso-hide:screen;mso-no-proof:yes;
text-decoration:none;text-underline:none'>6<!--[if gte mso 9]>
                <xml>
                    <w:data>
                        08D0C9EA79F9BACE118C8200AA004BA90B02000000080000000D0000005F0054006F006300310030003100390037003900320037000000
                    </w:data>
                </xml><![endif]--></span><!--[if supportFields]><span style='color:windowtext;
display:none;mso-hide:screen;mso-no-proof:yes;text-decoration:none;text-underline:
none'><span style='mso-element:field-end'></span></span><![endif]--></a><span
                style='mso-fareast-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-fareast-language:RU;mso-no-proof:yes'><o:p></o:p></span></p>

    <p class=MsoToc1 style='tab-stops:right dotted 467.25pt'><a href="#_Toc10197928"><span
                    lang=EN-US style='mso-fareast-font-family:"Times New Roman";mso-ansi-language:
EN-US;mso-fareast-language:RU;mso-no-proof:yes'>get</span><span
                    style='mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU;
mso-no-proof:yes'>-</span><span lang=EN-US style='mso-fareast-font-family:"Times New Roman";
mso-ansi-language:EN-US;mso-fareast-language:RU;mso-no-proof:yes'>mechanism</span><span
                    style='mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU;
mso-no-proof:yes'>-</span><span lang=EN-US style='mso-fareast-font-family:"Times New Roman";
mso-ansi-language:EN-US;mso-fareast-language:RU;mso-no-proof:yes'>type</span><span
                    style='color:windowtext;display:none;mso-hide:screen;mso-no-proof:yes;
text-decoration:none;text-underline:none'><span style='mso-tab-count:1 dotted'>. </span></span><!--[if supportFields]>
            <span
                    style='color:windowtext;display:none;mso-hide:screen;mso-no-proof:yes;
text-decoration:none;text-underline:none'><span style='mso-element:field-begin'></span>
PAGEREF _Toc10197928 \h <span style='mso-element:field-separator'></span></span><![endif]--><span
                    style='color:windowtext;display:none;mso-hide:screen;mso-no-proof:yes;
text-decoration:none;text-underline:none'>7<!--[if gte mso 9]>
                <xml>
                    <w:data>
                        08D0C9EA79F9BACE118C8200AA004BA90B02000000080000000D0000005F0054006F006300310030003100390037003900320038000000
                    </w:data>
                </xml><![endif]--></span><!--[if supportFields]><span style='color:windowtext;
display:none;mso-hide:screen;mso-no-proof:yes;text-decoration:none;text-underline:
none'><span style='mso-element:field-end'></span></span><![endif]--></a><span
                style='mso-fareast-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-fareast-language:RU;mso-no-proof:yes'><o:p></o:p></span></p>

    <p class=MsoNormal><!--[if supportFields]><span lang=EN-US style='font-size:
13.5pt;line-height:107%;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:EN-US;mso-fareast-language:
RU'><span style='mso-element:field-end'></span></span><![endif]--><span
                lang=EN-US style='font-size:13.5pt;line-height:107%;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US;
mso-fareast-language:RU'><o:p>&nbsp;</o:p></span></p>

    <span lang=EN-US style='font-size:13.5pt;line-height:107%;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US;
mso-fareast-language:RU;mso-bidi-language:AR-SA'><br clear=all
                                                     style='mso-special-character:line-break;page-break-before:always'>
</span>

    <p class=MsoNormal><span lang=EN-US style='font-size:13.5pt;line-height:107%;
font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-ansi-language:EN-US;mso-fareast-language:RU'><o:p>&nbsp;</o:p></span></p>

    <h1><a name="_Toc9947249"></a><a name="_Toc10197923"></a><a name="_Toc10016200"><span
                    style='mso-bookmark:_Toc10197923'><span style='mso-bookmark:_Toc9947249'><span
                            style='font-size:24.0pt;line-height:107%;mso-fareast-font-family:"Times New Roman";
mso-fareast-language:RU'>Общее описание</span></span></span></a><span
                style='mso-bookmark:_Toc9947249'><span style='font-size:24.0pt;line-height:
107%;mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'><o:p></o:p></span></span></h1>

    <p class=MsoNormal style='text-indent:35.4pt'><span style='mso-bookmark:_Toc9947249'><span
                    style='mso-fareast-language:RU'>Все методы возвращают данные в формате </span></span><span
                style='mso-bookmark:_Toc9947249'><span lang=EN-US style='mso-ansi-language:
EN-US;mso-fareast-language:RU'>JSON</span></span><span style='mso-bookmark:
_Toc9947249'><span style='mso-fareast-language:RU'>. <o:p></o:p></span></span></p>

    <?php if (Users::isAdmin()): ?>
        <p class=MsoNormal style='text-indent:35.4pt'><span style='mso-bookmark:_Toc9947249'><span
                        style='mso-fareast-language:RU'>Токен доступа к АПИ: <b><?= \app\models\Settings::getValueByKey('access_token') ?></b><span style='mso-bookmark:
_Toc9947249'><span style='mso-fareast-language:RU'> <o:p></o:p></span></span></p>
        <p></p>
    <?php endif; ?>

    <p class=MsoNormal><span style='mso-bookmark:_Toc9947249'><span
                    style='mso-fareast-language:RU'><o:p>&nbsp;</o:p></span></span></p>

    <span style='font-size:24.0pt;line-height:107%;font-family:"Calibri",sans-serif;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:"Times New Roman";
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:"Times New Roman";
mso-bidi-theme-font:minor-bidi;mso-ansi-language:RU;mso-fareast-language:RU;
mso-bidi-language:AR-SA'><br clear=all style='mso-special-character:line-break;
page-break-before:always'>
</span>

    <p class=MsoNormal><span style='mso-bookmark:_Toc9947249'><span
                    style='font-size:24.0pt;line-height:107%;font-family:"Calibri Light",sans-serif;
mso-ascii-theme-font:major-latin;mso-fareast-font-family:"Times New Roman";
mso-hansi-theme-font:major-latin;mso-bidi-font-family:"Times New Roman";
mso-bidi-theme-font:major-bidi;color:#2E74B5;mso-themecolor:accent1;mso-themeshade:
191;mso-fareast-language:RU'><o:p>&nbsp;</o:p></span></span></p>

    <span style='mso-bookmark:_Toc9947249'></span>

    <h1><a name="_Toc10197924"><span lang=EN-US style='font-size:24.0pt;line-height:
107%;mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;
mso-fareast-language:RU'>get</span></a><span style='mso-bookmark:_Toc10197924'><span
                    style='font-size:24.0pt;line-height:107%;mso-fareast-font-family:"Times New Roman";
mso-fareast-language:RU'>-</span></span><span style='mso-bookmark:_Toc10197924'><span
                    lang=EN-US style='font-size:24.0pt;line-height:107%;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>nomenclature</span></span><span
                style='mso-bookmark:_Toc10197924'></span><span style='font-size:24.0pt;
line-height:107%;mso-fareast-font-family:"Times New Roman";mso-fareast-language:
RU'><o:p></o:p></span></h1>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'>Описание:&nbsp;<b>Возвращает номенклатуру специалиста</b><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'>Метод:&nbsp;<b>GET</b><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'>Адрес:&nbsp;</span><a href="http://liftgo.pro/api/v1/get-nomenclature"> <span
                    style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>http://liftgo.pro/api/v1/</span><span
                    lang=EN-US style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>get</span><span style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>-</span><span
                    lang=EN-US style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>nomenclature</span><span style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'><o:p></o:p></span></a></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal;mso-outline-level:3'><b><span style='font-size:13.5pt;
font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:RU'>Принимает<o:p></o:p></span></b></p>

    <table class=MsoNormalTable border=1 cellpadding=0 width=623 style='width:466.95pt;
 mso-cellspacing:1.5pt;mso-yfti-tbllook:1184'>
        <thead>
        <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
            <td style='padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
   margin-bottom:0cm;margin-left:3.6pt;margin-bottom:.0001pt;text-align:center;
   line-height:normal'><b><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>Имя
   параметра<o:p></o:p></span></b></p>
            </td>
            <td width=80 style='width:60.35pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
   margin-bottom:0cm;margin-left:3.6pt;margin-bottom:.0001pt;text-align:center;
   line-height:normal'><b><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>Тип<o:p></o:p></span></b></p>
            </td>
            <td width=337 style='width:252.45pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
   margin-bottom:0cm;margin-left:3.6pt;margin-bottom:.0001pt;text-align:center;
   line-height:normal'><b><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>Описание<o:p></o:p></span></b></p>
            </td>
            <td width=110 valign=top style='width:82.35pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
   margin-bottom:0cm;margin-left:3.6pt;margin-bottom:.0001pt;text-align:center;
   line-height:normal'><b><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>Обязательное<o:p></o:p></span></b></p>
            </td>
        </tr>
        <tr style='mso-yfti-irow:1;height:20.0pt'>
            <td style='padding:.75pt .75pt .75pt .75pt;height:20.0pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;
   mso-fareast-language:RU;mso-bidi-font-weight:bold'>access_token<o:p></o:p></span></p>
            </td>
            <td width=80 style='width:60.35pt;padding:.75pt .75pt .75pt .75pt;
   height:20.0pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
   margin-bottom:0cm;margin-left:3.6pt;margin-bottom:.0001pt;text-align:center;
   line-height:normal'><span lang=EN-US style='font-size:12.0pt;font-family:
   "Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-ansi-language:EN-US;mso-fareast-language:RU;mso-bidi-font-weight:bold'>string<o:p></o:p></span></p>
            </td>
            <td width=337 style='width:252.45pt;padding:.75pt .75pt .75pt .75pt;
   height:20.0pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-fareast-language:RU;mso-bidi-font-weight:bold'>Токен
   доступа<o:p></o:p></span></p>
            </td>
            <td width=110 valign=top style='width:82.35pt;padding:.75pt .75pt .75pt .75pt;
   height:20.0pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-fareast-language:RU;mso-bidi-font-weight:bold'>Да<o:p></o:p></span></p>
            </td>
        </tr>
        <tr style='mso-yfti-irow:2;height:22.4pt'>
            <td style='padding:.75pt .75pt .75pt .75pt;height:22.4pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;
   mso-fareast-language:RU;mso-bidi-font-weight:bold'>component_id<o:p></o:p></span></p>
            </td>
            <td width=80 style='width:60.35pt;padding:.75pt .75pt .75pt .75pt;
   height:22.4pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
   margin-bottom:0cm;margin-left:3.6pt;margin-bottom:.0001pt;text-align:center;
   line-height:normal'><span lang=EN-US style='font-size:12.0pt;font-family:
   "Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-ansi-language:EN-US;mso-fareast-language:RU;mso-bidi-font-weight:bold'>integer<o:p></o:p></span></p>
            </td>
            <td width=337 style='width:252.45pt;padding:.75pt .75pt .75pt .75pt;
   height:22.4pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;
   mso-fareast-language:RU;mso-bidi-font-weight:bold'>ID </span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-fareast-language:RU;mso-bidi-font-weight:bold'>детали <o:p></o:p></span></p>
            </td>
            <td width=110 valign=top style='width:82.35pt;padding:.75pt .75pt .75pt .75pt;
   height:22.4pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-fareast-language:RU;mso-bidi-font-weight:bold'>Нет<o:p></o:p></span></p>
            </td>
        </tr>
        <tr style='mso-yfti-irow:3;height:18.4pt'>
            <td style='padding:.75pt .75pt .75pt .75pt;height:18.4pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;
   mso-fareast-language:RU;mso-bidi-font-weight:bold'>pagination_page_size<o:p></o:p></span></p>
            </td>
            <td width=80 style='width:60.35pt;padding:.75pt .75pt .75pt .75pt;
   height:18.4pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
   margin-bottom:0cm;margin-left:3.6pt;margin-bottom:.0001pt;text-align:center;
   line-height:normal'><span lang=EN-US style='font-size:12.0pt;font-family:
   "Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-ansi-language:EN-US;mso-fareast-language:RU;mso-bidi-font-weight:bold'>integer<o:p></o:p></span></p>
            </td>
            <td width=337 style='width:252.45pt;padding:.75pt .75pt .75pt .75pt;
   height:18.4pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-fareast-language:RU;mso-bidi-font-weight:bold'>Количество
   возвращаемых строк на страницу. Игнорируется при наличии </span><span
                            lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;
   mso-fareast-language:RU;mso-bidi-font-weight:bold'>component</span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-fareast-language:RU;mso-bidi-font-weight:bold'>_</span><span
                            lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;
   mso-fareast-language:RU;mso-bidi-font-weight:bold'>id</span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-fareast-language:RU;mso-bidi-font-weight:bold'><o:p></o:p></span></p>
            </td>
            <td width=110 valign=top style='width:82.35pt;padding:.75pt .75pt .75pt .75pt;
   height:18.4pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-fareast-language:RU;mso-bidi-font-weight:bold'>Нет<o:p></o:p></span></p>
            </td>
        </tr>
        <tr style='mso-yfti-irow:4;mso-yfti-lastrow:yes;height:18.4pt'>
            <td style='padding:.75pt .75pt .75pt .75pt;height:18.4pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;
   mso-fareast-language:RU;mso-bidi-font-weight:bold'>pagination</span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-fareast-language:RU;mso-bidi-font-weight:bold'>_</span><span
                            lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;
   mso-fareast-language:RU;mso-bidi-font-weight:bold'>page</span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-fareast-language:RU;mso-bidi-font-weight:bold'>_</span><span
                            lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;
   mso-fareast-language:RU;mso-bidi-font-weight:bold'>num</span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-fareast-language:RU;mso-bidi-font-weight:bold'><o:p></o:p></span></p>
            </td>
            <td width=80 style='width:60.35pt;padding:.75pt .75pt .75pt .75pt;
   height:18.4pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
   margin-bottom:0cm;margin-left:3.6pt;margin-bottom:.0001pt;text-align:center;
   line-height:normal'><span lang=EN-US style='font-size:12.0pt;font-family:
   "Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-ansi-language:EN-US;mso-fareast-language:RU;mso-bidi-font-weight:bold'>integer</span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-fareast-language:RU;mso-bidi-font-weight:bold'><o:p></o:p></span></p>
            </td>
            <td width=337 style='width:252.45pt;padding:.75pt .75pt .75pt .75pt;
   height:18.4pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-fareast-language:RU;mso-bidi-font-weight:bold'>Номер
   страницы. Игнорируется при отсутствии </span><span lang=EN-US
                                                      style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU;
   mso-bidi-font-weight:bold'>pagination</span><span style='font-size:12.0pt;
   font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-fareast-language:RU;mso-bidi-font-weight:bold'>_</span><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU;
   mso-bidi-font-weight:bold'>page</span><span style='font-size:12.0pt;
   font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-fareast-language:RU;mso-bidi-font-weight:bold'>_</span><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU;
   mso-bidi-font-weight:bold'>size</span><span style='font-size:12.0pt;
   font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-fareast-language:RU;mso-bidi-font-weight:bold'>. По умолчанию == 1<o:p></o:p></span></p>
            </td>
            <td width=110 valign=top style='width:82.35pt;padding:.75pt .75pt .75pt .75pt;
   height:18.4pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-fareast-language:RU;mso-bidi-font-weight:bold'>Нет<o:p></o:p></span></p>
            </td>
        </tr>
        </thead>
    </table>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal;mso-outline-level:3'><b><span style='font-size:13.5pt;
font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:RU'>Возвращает&nbsp;<u>Массив значений</u><o:p></o:p></span></b></p>

    <table class=MsoNormalTable border=1 cellpadding=0 style='mso-cellspacing:1.5pt;
 mso-yfti-tbllook:1184'>
        <thead>
        <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
            <td width=118 style='width:88.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
   text-align:center;line-height:normal'><b><span style='font-size:12.0pt;
   font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-fareast-language:RU'>Имя параметра<o:p></o:p></span></b></p>
            </td>
            <td width=85 style='width:63.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
   text-align:center;line-height:normal'><b><span style='font-size:12.0pt;
   font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-fareast-language:RU'>Тип<o:p></o:p></span></b></p>
            </td>
            <td width=225 style='width:168.6pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
   text-align:center;line-height:normal'><b><span style='font-size:12.0pt;
   font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-fareast-language:RU'>Описание<o:p></o:p></span></b></p>
            </td>
            <td width=185 valign=top style='width:139.05pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
   text-align:center;line-height:normal'><b><span style='font-size:12.0pt;
   font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-fareast-language:RU'>Прим.<o:p></o:p></span></b></p>
            </td>
        </tr>
        </thead>
        <tr style='mso-yfti-irow:1'>
            <td width=118 style='width:88.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>success<o:p></o:p></span></p>
            </td>
            <td width=85 style='width:63.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>string</span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'><o:p></o:p></span></p>
            </td>
            <td width=225 style='width:168.6pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>true – </span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>успешно</span><span lang=EN-US
                                                                 style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>; false -</span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'> ошибка<o:p></o:p></span></p>
            </td>
            <td width=185 valign=top style='width:139.05pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'><o:p>&nbsp;</o:p></span></p>
            </td>
        </tr>
        <tr style='mso-yfti-irow:2'>
            <td width=118 style='width:88.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>error<o:p></o:p></span></p>
            </td>
            <td width=85 style='width:63.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>string<o:p></o:p></span></p>
            </td>
            <td width=225 style='width:168.6pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>Описание ошибки<o:p></o:p></span></p>
            </td>
            <td width=185 valign=top style='width:139.05pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>Если </span><span lang=EN-US
                                                               style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>success</span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'> == </span><span lang=EN-US
                                                              style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>true</span><span
                            lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
  mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'> </span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>параметр не возвращается<o:p></o:p></span></p>
            </td>
        </tr>
        <tr style='mso-yfti-irow:3;mso-yfti-lastrow:yes'>
            <td width=118 style='width:88.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>data<o:p></o:p></span></p>
            </td>
            <td width=85 style='width:63.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>array<o:p></o:p></span></p>
            </td>
            <td width=225 style='width:168.6pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>Массив запрошенных данных<o:p></o:p></span></p>
            </td>
            <td width=185 valign=top style='width:139.05pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>Если </span><span lang=EN-US
                                                               style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>success</span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>==</span><span lang=EN-US
                                                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>false</span><span
                            lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
  mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'> </span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>параметр не возвращается<o:p></o:p></span></p>
            </td>
        </tr>
    </table>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'><o:p>&nbsp;</o:p></span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'>Пример запроса:<o:p></o:p></span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'><br>
</span><a
                href="http://liftgo.pro/api/v1/get-nomenclature?access_token=Gk6jUhvjmgPuf&amp;component_id=185"><span
                    lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>http</span><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>://</span><span
                    lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>liftgo</span><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>.</span><span
                    lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>pro</span><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>/</span><span
                    lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>api</span><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>/</span><span
                    lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>v</span><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>1/</span><span
                    lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>get</span><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>-</span><span
                    lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>nomenclature</span><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>?</span><span
                    lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>access</span><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>_</span><span
                    lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>token</span><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>=</span><span
                    lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>Gk</span><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>6</span><span
                    lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>jUhvjmgPuf</span><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>&amp;</span><span
                    lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>component</span><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>_</span><span
                    lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>id</span><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>=185</span></a><span
                style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-fareast-language:RU'><o:p></o:p></span></p>

    <p class=MsoNormal><span style='font-size:13.5pt;line-height:107%;font-family:
"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";color:black;
mso-fareast-language:RU'><o:p>&nbsp;</o:p></span></p>

    <p class=MsoNormal><span style='font-size:13.5pt;line-height:107%;font-family:
"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";color:black;
mso-fareast-language:RU'>Ответ</span><span lang=EN-US style='font-size:13.5pt;
line-height:107%;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:EN-US;mso-fareast-language:
RU'>:<o:p></o:p></span></p>

    <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;line-height:107%;
font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-ansi-language:EN-US;mso-fareast-language:RU'>{&quot;success&quot;:&quot;true&quot;,&quot;data&quot;:[{&quot;id&quot;:185,&quot;name&quot;:&quot;\t</span><span
                style='font-size:10.0pt;line-height:107%;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'>Колесо</span><span style='font-size:10.0pt;line-height:107%;font-family:
"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";color:black;
mso-ansi-language:EN-US;mso-fareast-language:RU'> </span><span
                style='font-size:10.0pt;line-height:107%;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'>привода</span><span style='font-size:10.0pt;line-height:107%;font-family:
"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";color:black;
mso-ansi-language:EN-US;mso-fareast-language:RU'> </span><span
                style='font-size:10.0pt;line-height:107%;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'>поручня</span><span style='font-size:10.0pt;line-height:107%;font-family:
"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";color:black;
mso-ansi-language:EN-US;mso-fareast-language:RU'> </span><span
                style='font-size:10.0pt;line-height:107%;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'>эскалатора</span><span lang=EN-US style='font-size:10.0pt;line-height:107%;
font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-ansi-language:EN-US;mso-fareast-language:RU'> SJEC D=587</span><span
                style='font-size:10.0pt;line-height:107%;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'>мм</span><span lang=EN-US style='font-size:10.0pt;line-height:107%;
font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-ansi-language:EN-US;mso-fareast-language:RU'> (</span><span
                style='font-size:10.0pt;line-height:107%;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'>крепление</span><span style='font-size:10.0pt;line-height:107%;font-family:
"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";color:black;
mso-ansi-language:EN-US;mso-fareast-language:RU'> </span><span
                style='font-size:10.0pt;line-height:107%;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'>М</span><span lang=EN-US style='font-size:10.0pt;line-height:107%;
font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-ansi-language:EN-US;mso-fareast-language:RU'>10)&quot;,&quot;vendor_code&quot;:&quot;&quot;,&quot;own_vendor_code&quot;:&quot;&quot;,&quot;brand_id&quot;:2,&quot;group_id&quot;:1,&quot;weight&quot;:null,&quot;type_of_parts_id&quot;:null,&quot;type_of_mechanism_id&quot;:null,&quot;description&quot;:&quot;&quot;,&quot;main_photo&quot;:&quot;/images/nomenclature/main_photo/1554292724AZBuM.jpg&quot;,&quot;additional_photos&quot;:null,&quot;created_by&quot;:1,&quot;nomenclature_type&quot;:&quot;tech_specialist&quot;,&quot;created_date&quot;:&quot;2019-04-03
14:58:44&quot;,&quot;updated_date&quot;:&quot;2019-04-03 14:58:44&quot;}]}<o:p></o:p></span></p>

    <span style='font-size:16.0pt;line-height:107%;font-family:"Calibri Light",sans-serif;
mso-ascii-theme-font:major-latin;mso-fareast-font-family:"Times New Roman";
mso-hansi-theme-font:major-latin;mso-bidi-font-family:"Times New Roman";
mso-bidi-theme-font:major-bidi;color:#2E74B5;mso-themecolor:accent1;mso-themeshade:
191;mso-ansi-language:RU;mso-fareast-language:RU;mso-bidi-language:AR-SA'><br
                clear=all style='page-break-before:always'>
</span>

    <h1><a name="_Toc10197925"><span lang=EN-US style='font-size:24.0pt;line-height:
107%;mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;
mso-fareast-language:RU'>get</span></a><span style='mso-bookmark:_Toc10197925'><span
                    style='font-size:24.0pt;line-height:107%;mso-fareast-font-family:"Times New Roman";
mso-fareast-language:RU'>-</span></span><span style='mso-bookmark:_Toc10197925'><span
                    lang=EN-US style='font-size:24.0pt;line-height:107%;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>brand</span></span><span
                style='mso-bookmark:_Toc10197925'></span><span style='font-size:24.0pt;
line-height:107%;mso-fareast-font-family:"Times New Roman";mso-fareast-language:
RU'><o:p></o:p></span></h1>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'>Описание:&nbsp;<b>Возвращает список брэндов</b><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'>Метод:&nbsp;<b>GET</b><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'>Адрес:</span><span lang=EN-US style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US;
mso-fareast-language:RU'>&nbsp;</span><a
                href="http://liftgo.pro/api/v1/get-brands"><span lang=EN-US style='font-size:
13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
mso-ansi-language:EN-US;mso-fareast-language:RU'>http</span><span
                    style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>://</span><span lang=EN-US
                                                           style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>liftgo</span><span
                    style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>.</span><span lang=EN-US
                                                         style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>pro</span><span
                    style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>/</span><span lang=EN-US
                                                         style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>api</span><span
                    style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>/</span><span lang=EN-US
                                                         style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>v</span><span
                    style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>1/</span><span lang=EN-US
                                                          style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>get</span><span
                    style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>-brands</span></a><span
                style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-fareast-language:RU'><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal;mso-outline-level:3'><b><span style='font-size:13.5pt;
font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:RU'>Принимает<o:p></o:p></span></b></p>

    <table class=MsoNormalTable border=1 cellpadding=0 width=623 style='width:466.95pt;
 mso-cellspacing:1.5pt;mso-yfti-tbllook:1184'>
        <thead>
        <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
            <td style='padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
   margin-bottom:0cm;margin-left:3.6pt;margin-bottom:.0001pt;text-align:center;
   line-height:normal'><b><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>Имя
   параметра<o:p></o:p></span></b></p>
            </td>
            <td width=80 style='width:59.8pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
   margin-bottom:0cm;margin-left:3.6pt;margin-bottom:.0001pt;text-align:center;
   line-height:normal'><b><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>Тип<o:p></o:p></span></b></p>
            </td>
            <td width=330 style='width:247.35pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
   margin-bottom:0cm;margin-left:3.6pt;margin-bottom:.0001pt;text-align:center;
   line-height:normal'><b><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>Описание<o:p></o:p></span></b></p>
            </td>
            <td width=110 valign=top style='width:82.35pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
   margin-bottom:0cm;margin-left:3.6pt;margin-bottom:.0001pt;text-align:center;
   line-height:normal'><b><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>Обязательное<o:p></o:p></span></b></p>
            </td>
        </tr>
        <tr style='mso-yfti-irow:1;height:20.0pt'>
            <td style='padding:.75pt .75pt .75pt .75pt;height:20.0pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;
   mso-fareast-language:RU;mso-bidi-font-weight:bold'>access_token<o:p></o:p></span></p>
            </td>
            <td width=80 style='width:59.8pt;padding:.75pt .75pt .75pt .75pt;height:
   20.0pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
   margin-bottom:0cm;margin-left:3.6pt;margin-bottom:.0001pt;text-align:center;
   line-height:normal'><span lang=EN-US style='font-size:12.0pt;font-family:
   "Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-ansi-language:EN-US;mso-fareast-language:RU;mso-bidi-font-weight:bold'>string<o:p></o:p></span></p>
            </td>
            <td width=330 style='width:247.35pt;padding:.75pt .75pt .75pt .75pt;
   height:20.0pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-fareast-language:RU;mso-bidi-font-weight:bold'>Токен
   доступа<o:p></o:p></span></p>
            </td>
            <td width=110 valign=top style='width:82.35pt;padding:.75pt .75pt .75pt .75pt;
   height:20.0pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-fareast-language:RU;mso-bidi-font-weight:bold'>Да<o:p></o:p></span></p>
            </td>
        </tr>
        <tr style='mso-yfti-irow:2;mso-yfti-lastrow:yes;height:22.4pt'>
            <td style='padding:.75pt .75pt .75pt .75pt;height:22.4pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;
   mso-fareast-language:RU;mso-bidi-font-weight:bold'>brand_id<o:p></o:p></span></p>
            </td>
            <td width=80 style='width:59.8pt;padding:.75pt .75pt .75pt .75pt;height:
   22.4pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
   margin-bottom:0cm;margin-left:3.6pt;margin-bottom:.0001pt;text-align:center;
   line-height:normal'><span lang=EN-US style='font-size:12.0pt;font-family:
   "Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-ansi-language:EN-US;mso-fareast-language:RU;mso-bidi-font-weight:bold'>integer<o:p></o:p></span></p>
            </td>
            <td width=330 style='width:247.35pt;padding:.75pt .75pt .75pt .75pt;
   height:22.4pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;
   mso-fareast-language:RU;mso-bidi-font-weight:bold'>ID </span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-fareast-language:RU;mso-bidi-font-weight:bold'>Бренда<o:p></o:p></span></p>
            </td>
            <td width=110 valign=top style='width:82.35pt;padding:.75pt .75pt .75pt .75pt;
   height:22.4pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-fareast-language:RU;mso-bidi-font-weight:bold'>Нет<o:p></o:p></span></p>
            </td>
        </tr>
        </thead>
    </table>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal;mso-outline-level:3'><b><span style='font-size:13.5pt;
font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:RU'>Возвращает&nbsp;<u>Массив значений</u><o:p></o:p></span></b></p>

    <table class=MsoNormalTable border=1 cellpadding=0 style='mso-cellspacing:1.5pt;
 mso-yfti-tbllook:1184'>
        <thead>
        <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
            <td width=118 style='width:88.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
   text-align:center;line-height:normal'><b><span style='font-size:12.0pt;
   font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-fareast-language:RU'>Имя параметра<o:p></o:p></span></b></p>
            </td>
            <td width=85 style='width:63.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
   text-align:center;line-height:normal'><b><span style='font-size:12.0pt;
   font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-fareast-language:RU'>Тип<o:p></o:p></span></b></p>
            </td>
            <td width=225 style='width:168.6pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
   text-align:center;line-height:normal'><b><span style='font-size:12.0pt;
   font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-fareast-language:RU'>Описание<o:p></o:p></span></b></p>
            </td>
            <td width=185 valign=top style='width:139.05pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
   text-align:center;line-height:normal'><b><span style='font-size:12.0pt;
   font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-fareast-language:RU'>Прим.<o:p></o:p></span></b></p>
            </td>
        </tr>
        </thead>
        <tr style='mso-yfti-irow:1'>
            <td width=118 style='width:88.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>success<o:p></o:p></span></p>
            </td>
            <td width=85 style='width:63.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>string</span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'><o:p></o:p></span></p>
            </td>
            <td width=225 style='width:168.6pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>true – </span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>успешно</span><span lang=EN-US
                                                                 style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>; false -</span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'> ошибка<o:p></o:p></span></p>
            </td>
            <td width=185 valign=top style='width:139.05pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'><o:p>&nbsp;</o:p></span></p>
            </td>
        </tr>
        <tr style='mso-yfti-irow:2'>
            <td width=118 style='width:88.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>error<o:p></o:p></span></p>
            </td>
            <td width=85 style='width:63.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>string<o:p></o:p></span></p>
            </td>
            <td width=225 style='width:168.6pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>Описание ошибки<o:p></o:p></span></p>
            </td>
            <td width=185 valign=top style='width:139.05pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>Если </span><span lang=EN-US
                                                               style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>success</span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'> == </span><span lang=EN-US
                                                              style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>true</span><span
                            lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
  mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'> </span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>параметр не возвращается<o:p></o:p></span></p>
            </td>
        </tr>
        <tr style='mso-yfti-irow:3;mso-yfti-lastrow:yes'>
            <td width=118 style='width:88.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>data<o:p></o:p></span></p>
            </td>
            <td width=85 style='width:63.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>array<o:p></o:p></span></p>
            </td>
            <td width=225 style='width:168.6pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>Массив запрошенных данных<o:p></o:p></span></p>
            </td>
            <td width=185 valign=top style='width:139.05pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>Если </span><span lang=EN-US
                                                               style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>success</span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>==</span><span lang=EN-US
                                                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>false</span><span
                            lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
  mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'> </span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>параметр не возвращается<o:p></o:p></span></p>
            </td>
        </tr>
    </table>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'><o:p>&nbsp;</o:p></span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'>Пример запроса:<o:p></o:p></span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'><br>
</span><a
                href="http://liftgo.pro/api/v1/get-brand?access_token=Gk6jUhvjmgP2alHp1gTwV&amp;brand_id=6"><span
                    lang=EN-US style='font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>http</span><span
                    style='font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
mso-fareast-language:RU'>://</span><span lang=EN-US style='font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>liftgo</span><span style='font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>.</span><span lang=EN-US
                                                         style='font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
mso-ansi-language:EN-US;mso-fareast-language:RU'>pro</span><span
                    style='font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
mso-fareast-language:RU'>/</span><span lang=EN-US style='font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>api</span><span style='font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>/</span><span lang=EN-US
                                                         style='font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
mso-ansi-language:EN-US;mso-fareast-language:RU'>v</span><span
                    style='font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
mso-fareast-language:RU'>1/</span><span lang=EN-US style='font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>get</span><span style='font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>-</span><span lang=EN-US
                                                         style='font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
mso-ansi-language:EN-US;mso-fareast-language:RU'>brand</span><span
                    style='font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
mso-fareast-language:RU'>?</span><span lang=EN-US style='font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>access</span><span style='font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>_</span><span lang=EN-US
                                                         style='font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
mso-ansi-language:EN-US;mso-fareast-language:RU'>token</span><span
                    style='font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
mso-fareast-language:RU'>=</span><span lang=EN-US style='font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>Gk</span><span style='font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>6</span><span lang=EN-US
                                                         style='font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
mso-ansi-language:EN-US;mso-fareast-language:RU'>jUhvjmgP</span><span
                    style='font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
mso-fareast-language:RU'>2</span><span lang=EN-US style='font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>alHp</span><span style='font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>1</span><span lang=EN-US
                                                         style='font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
mso-ansi-language:EN-US;mso-fareast-language:RU'>gTwV</span><span
                    style='font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
mso-fareast-language:RU'>&amp;</span><span lang=EN-US style='font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>brand</span><span style='font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>_</span><span lang=EN-US
                                                         style='font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
mso-ansi-language:EN-US;mso-fareast-language:RU'>id</span><span
                    style='font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
mso-fareast-language:RU'>=6</span></a><span style='font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'><o:p></o:p></span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-fareast-language:RU'><o:p>&nbsp;</o:p></span></p>

    <p class=MsoNormal><span style='font-size:13.5pt;line-height:107%;font-family:
"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";color:black;
mso-fareast-language:RU'>Ответ</span><span lang=EN-US style='font-size:13.5pt;
line-height:107%;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:EN-US;mso-fareast-language:
RU'>:<o:p></o:p></span></p>

    <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;line-height:107%;
font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-ansi-language:EN-US;mso-fareast-language:RU'>{&quot;success&quot;:&quot;true&quot;,&quot;data&quot;:[{&quot;id&quot;:6,&quot;name&quot;:&quot;Hyundai&quot;}]}<o:p></o:p></span>
    </p>

    <span lang=EN-US style='font-size:11.0pt;line-height:107%;font-family:"Calibri",sans-serif;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:"Times New Roman";
mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US;mso-fareast-language:
RU;mso-bidi-language:AR-SA'><br clear=all style='mso-special-character:line-break;
page-break-before:always'>
</span>

    <p class=MsoNormal><span lang=EN-US style='mso-ansi-language:EN-US;mso-fareast-language:
RU'><o:p>&nbsp;</o:p></span></p>

    <h1><a name="_Toc10197926"><span lang=EN-US style='font-size:24.0pt;line-height:
107%;mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;
mso-fareast-language:RU'>get</span></a><span style='mso-bookmark:_Toc10197926'><span
                    style='font-size:24.0pt;line-height:107%;mso-fareast-font-family:"Times New Roman";
mso-fareast-language:RU'>-</span></span><span style='mso-bookmark:_Toc10197926'><span
                    lang=EN-US style='font-size:24.0pt;line-height:107%;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>group</span></span><span
                style='mso-bookmark:_Toc10197926'></span><span style='font-size:24.0pt;
line-height:107%;mso-fareast-font-family:"Times New Roman";mso-fareast-language:
RU'><o:p></o:p></span></h1>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'>Описание:&nbsp;<b>Возвращает список групп запчасти/детали</b><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'>Метод:&nbsp;<b>GET</b><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'>Адрес:</span><span lang=EN-US style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US;
mso-fareast-language:RU'>&nbsp;</span><a
                href="http://liftgo.pro/api/v1/get-group"><span lang=EN-US style='font-size:
13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
mso-ansi-language:EN-US;mso-fareast-language:RU'>http</span><span
                    style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>://</span><span lang=EN-US
                                                           style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>liftgo</span><span
                    style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>.</span><span lang=EN-US
                                                         style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>pro</span><span
                    style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>/</span><span lang=EN-US
                                                         style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>api</span><span
                    style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>/</span><span lang=EN-US
                                                         style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>v</span><span
                    style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>1/</span><span lang=EN-US
                                                          style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>get</span><span
                    style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>-</span><span lang=EN-US
                                                         style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>group</span></a><span
                style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-fareast-language:RU'><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal;mso-outline-level:3'><b><span style='font-size:13.5pt;
font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:RU'>Принимает<o:p></o:p></span></b></p>

    <table class=MsoNormalTable border=1 cellpadding=0 width=623 style='width:466.95pt;
 mso-cellspacing:1.5pt;mso-yfti-tbllook:1184'>
        <thead>
        <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
            <td style='padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
   margin-bottom:0cm;margin-left:3.6pt;margin-bottom:.0001pt;text-align:center;
   line-height:normal'><b><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>Имя
   параметра<o:p></o:p></span></b></p>
            </td>
            <td width=80 style='width:59.8pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
   margin-bottom:0cm;margin-left:3.6pt;margin-bottom:.0001pt;text-align:center;
   line-height:normal'><b><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>Тип<o:p></o:p></span></b></p>
            </td>
            <td width=330 style='width:247.35pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
   margin-bottom:0cm;margin-left:3.6pt;margin-bottom:.0001pt;text-align:center;
   line-height:normal'><b><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>Описание<o:p></o:p></span></b></p>
            </td>
            <td width=110 valign=top style='width:82.35pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
   margin-bottom:0cm;margin-left:3.6pt;margin-bottom:.0001pt;text-align:center;
   line-height:normal'><b><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>Обязательное<o:p></o:p></span></b></p>
            </td>
        </tr>
        <tr style='mso-yfti-irow:1;height:20.0pt'>
            <td style='padding:.75pt .75pt .75pt .75pt;height:20.0pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;
   mso-fareast-language:RU;mso-bidi-font-weight:bold'>access_token<o:p></o:p></span></p>
            </td>
            <td width=80 style='width:59.8pt;padding:.75pt .75pt .75pt .75pt;height:
   20.0pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
   margin-bottom:0cm;margin-left:3.6pt;margin-bottom:.0001pt;text-align:center;
   line-height:normal'><span lang=EN-US style='font-size:12.0pt;font-family:
   "Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-ansi-language:EN-US;mso-fareast-language:RU;mso-bidi-font-weight:bold'>string<o:p></o:p></span></p>
            </td>
            <td width=330 style='width:247.35pt;padding:.75pt .75pt .75pt .75pt;
   height:20.0pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-fareast-language:RU;mso-bidi-font-weight:bold'>Токен
   доступа<o:p></o:p></span></p>
            </td>
            <td width=110 valign=top style='width:82.35pt;padding:.75pt .75pt .75pt .75pt;
   height:20.0pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-fareast-language:RU;mso-bidi-font-weight:bold'>Да<o:p></o:p></span></p>
            </td>
        </tr>
        <tr style='mso-yfti-irow:2;mso-yfti-lastrow:yes;height:22.4pt'>
            <td style='padding:.75pt .75pt .75pt .75pt;height:22.4pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;
   mso-fareast-language:RU;mso-bidi-font-weight:bold'>group_id<o:p></o:p></span></p>
            </td>
            <td width=80 style='width:59.8pt;padding:.75pt .75pt .75pt .75pt;height:
   22.4pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
   margin-bottom:0cm;margin-left:3.6pt;margin-bottom:.0001pt;text-align:center;
   line-height:normal'><span lang=EN-US style='font-size:12.0pt;font-family:
   "Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-ansi-language:EN-US;mso-fareast-language:RU;mso-bidi-font-weight:bold'>integer<o:p></o:p></span></p>
            </td>
            <td width=330 style='width:247.35pt;padding:.75pt .75pt .75pt .75pt;
   height:22.4pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;
   mso-fareast-language:RU;mso-bidi-font-weight:bold'>ID </span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-fareast-language:RU;mso-bidi-font-weight:bold'>Группы<o:p></o:p></span></p>
            </td>
            <td width=110 valign=top style='width:82.35pt;padding:.75pt .75pt .75pt .75pt;
   height:22.4pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-fareast-language:RU;mso-bidi-font-weight:bold'>Нет<o:p></o:p></span></p>
            </td>
        </tr>
        </thead>
    </table>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal;mso-outline-level:3'><b><span style='font-size:13.5pt;
font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:RU'>Возвращает&nbsp;<u>Массив значений</u><o:p></o:p></span></b></p>

    <table class=MsoNormalTable border=1 cellpadding=0 style='mso-cellspacing:1.5pt;
 mso-yfti-tbllook:1184'>
        <thead>
        <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
            <td width=118 style='width:88.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
   text-align:center;line-height:normal'><b><span style='font-size:12.0pt;
   font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-fareast-language:RU'>Имя параметра<o:p></o:p></span></b></p>
            </td>
            <td width=85 style='width:63.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
   text-align:center;line-height:normal'><b><span style='font-size:12.0pt;
   font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-fareast-language:RU'>Тип<o:p></o:p></span></b></p>
            </td>
            <td width=225 style='width:168.6pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
   text-align:center;line-height:normal'><b><span style='font-size:12.0pt;
   font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-fareast-language:RU'>Описание<o:p></o:p></span></b></p>
            </td>
            <td width=185 valign=top style='width:139.05pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
   text-align:center;line-height:normal'><b><span style='font-size:12.0pt;
   font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-fareast-language:RU'>Прим.<o:p></o:p></span></b></p>
            </td>
        </tr>
        </thead>
        <tr style='mso-yfti-irow:1'>
            <td width=118 style='width:88.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>success<o:p></o:p></span></p>
            </td>
            <td width=85 style='width:63.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>string</span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'><o:p></o:p></span></p>
            </td>
            <td width=225 style='width:168.6pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>true – </span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>успешно</span><span lang=EN-US
                                                                 style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>; false -</span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'> ошибка<o:p></o:p></span></p>
            </td>
            <td width=185 valign=top style='width:139.05pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'><o:p>&nbsp;</o:p></span></p>
            </td>
        </tr>
        <tr style='mso-yfti-irow:2'>
            <td width=118 style='width:88.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>error<o:p></o:p></span></p>
            </td>
            <td width=85 style='width:63.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>string<o:p></o:p></span></p>
            </td>
            <td width=225 style='width:168.6pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>Описание ошибки<o:p></o:p></span></p>
            </td>
            <td width=185 valign=top style='width:139.05pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>Если </span><span lang=EN-US
                                                               style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>success</span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'> == </span><span lang=EN-US
                                                              style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>true</span><span
                            lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
  mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'> </span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>параметр не возвращается<o:p></o:p></span></p>
            </td>
        </tr>
        <tr style='mso-yfti-irow:3;mso-yfti-lastrow:yes'>
            <td width=118 style='width:88.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>data<o:p></o:p></span></p>
            </td>
            <td width=85 style='width:63.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>array<o:p></o:p></span></p>
            </td>
            <td width=225 style='width:168.6pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>Массив запрошенных данных<o:p></o:p></span></p>
            </td>
            <td width=185 valign=top style='width:139.05pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>Если </span><span lang=EN-US
                                                               style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>success</span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>==</span><span lang=EN-US
                                                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>false</span><span
                            lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
  mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'> </span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>параметр не возвращается<o:p></o:p></span></p>
            </td>
        </tr>
    </table>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'><o:p>&nbsp;</o:p></span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'>Пример запроса:<o:p></o:p></span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'><br>
</span><a
                href="http://liftgo.pro/api/v1/get-group?access_token=Gk6jUuf7CVKV&amp;group_id=3"><span
                    lang=EN-US style='font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>http</span><span
                    style='font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
mso-fareast-language:RU'>://</span><span lang=EN-US style='font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>liftgo</span><span style='font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>.</span><span lang=EN-US
                                                         style='font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
mso-ansi-language:EN-US;mso-fareast-language:RU'>pro</span><span
                    style='font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
mso-fareast-language:RU'>/</span><span lang=EN-US style='font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>api</span><span style='font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>/</span><span lang=EN-US
                                                         style='font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
mso-ansi-language:EN-US;mso-fareast-language:RU'>v</span><span
                    style='font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
mso-fareast-language:RU'>1/</span><span lang=EN-US style='font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>get</span><span style='font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>-</span><span lang=EN-US
                                                         style='font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
mso-ansi-language:EN-US;mso-fareast-language:RU'>group</span><span
                    style='font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
mso-fareast-language:RU'>?</span><span lang=EN-US style='font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>access</span><span style='font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>_</span><span lang=EN-US
                                                         style='font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
mso-ansi-language:EN-US;mso-fareast-language:RU'>token</span><span
                    style='font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
mso-fareast-language:RU'>=</span><span lang=EN-US style='font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>Gk</span><span style='font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>6</span><span lang=EN-US
                                                         style='font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
mso-ansi-language:EN-US;mso-fareast-language:RU'>jUuf</span><span
                    style='font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
mso-fareast-language:RU'>7</span><span lang=EN-US style='font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>CVKV</span><span style='font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>&amp;</span><span lang=EN-US
                                                             style='font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
mso-ansi-language:EN-US;mso-fareast-language:RU'>group</span><span
                    style='font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
mso-fareast-language:RU'>_</span><span lang=EN-US style='font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>id</span><span style='font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>=3</span></a><span style='font-family:
"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";color:black;
mso-fareast-language:RU'><o:p></o:p></span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-fareast-language:RU'><o:p>&nbsp;</o:p></span></p>

    <p class=MsoNormal><span style='font-size:13.5pt;line-height:107%;font-family:
"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";color:black;
mso-fareast-language:RU'>Ответ</span><span lang=EN-US style='font-size:13.5pt;
line-height:107%;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:EN-US;mso-fareast-language:
RU'>:<o:p></o:p></span></p>

    <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;line-height:107%;
font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-ansi-language:EN-US;mso-fareast-language:RU'>{&quot;success&quot;:&quot;true&quot;,&quot;data&quot;:[{&quot;id&quot;:3,&quot;name&quot;:&quot;Лифт&quot;}]}<o:p></o:p></span>
    </p>

    <span lang=EN-US style='font-size:11.0pt;line-height:107%;font-family:"Calibri",sans-serif;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:"Times New Roman";
mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US;mso-fareast-language:
RU;mso-bidi-language:AR-SA'><br clear=all style='mso-special-character:line-break;
page-break-before:always'>
</span>

    <p class=MsoNormal><span lang=EN-US style='mso-ansi-language:EN-US;mso-fareast-language:
RU'><o:p>&nbsp;</o:p></span></p>

    <h1><a name="_Toc10197927"><span lang=EN-US style='font-size:24.0pt;line-height:
107%;mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;
mso-fareast-language:RU'>get</span></a><span style='mso-bookmark:_Toc10197927'><span
                    style='font-size:24.0pt;line-height:107%;mso-fareast-font-family:"Times New Roman";
mso-fareast-language:RU'>-</span></span><span style='mso-bookmark:_Toc10197927'><span
                    lang=EN-US style='font-size:24.0pt;line-height:107%;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>part</span></span><span
                style='mso-bookmark:_Toc10197927'><span style='font-size:24.0pt;line-height:
107%;mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>-</span></span><span
                style='mso-bookmark:_Toc10197927'><span lang=EN-US style='font-size:24.0pt;
line-height:107%;mso-fareast-font-family:"Times New Roman";mso-ansi-language:
EN-US;mso-fareast-language:RU'>type</span></span><span style='mso-bookmark:
_Toc10197927'></span><span style='font-size:24.0pt;line-height:107%;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'><o:p></o:p></span></h1>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'>Описание:&nbsp;<b>Возвращает список типов запчасти/детали</b><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'>Метод:&nbsp;<b>GET</b><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'>Адрес:</span><span lang=EN-US style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US;
mso-fareast-language:RU'>&nbsp;</span><a
                href="http://liftgo.pro/api/v1/get-part-type"><span lang=EN-US
                                                                    style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>http</span><span
                    style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>://</span><span lang=EN-US
                                                           style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>liftgo</span><span
                    style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>.</span><span lang=EN-US
                                                         style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>pro</span><span
                    style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>/</span><span lang=EN-US
                                                         style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>api</span><span
                    style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>/</span><span lang=EN-US
                                                         style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>v</span><span
                    style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>1/</span><span lang=EN-US
                                                          style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>get</span><span
                    style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>-</span><span lang=EN-US
                                                         style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>part</span><span
                    style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>-</span><span lang=EN-US
                                                         style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>type</span></a><span
                style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-fareast-language:RU'><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal;mso-outline-level:3'><b><span style='font-size:13.5pt;
font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:RU'>Принимает<o:p></o:p></span></b></p>

    <table class=MsoNormalTable border=1 cellpadding=0 width=623 style='width:466.95pt;
 mso-cellspacing:1.5pt;mso-yfti-tbllook:1184'>
        <thead>
        <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
            <td style='padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
   margin-bottom:0cm;margin-left:3.6pt;margin-bottom:.0001pt;text-align:center;
   line-height:normal'><b><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>Имя
   параметра<o:p></o:p></span></b></p>
            </td>
            <td width=80 style='width:59.8pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
   margin-bottom:0cm;margin-left:3.6pt;margin-bottom:.0001pt;text-align:center;
   line-height:normal'><b><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>Тип<o:p></o:p></span></b></p>
            </td>
            <td width=330 style='width:247.35pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
   margin-bottom:0cm;margin-left:3.6pt;margin-bottom:.0001pt;text-align:center;
   line-height:normal'><b><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>Описание<o:p></o:p></span></b></p>
            </td>
            <td width=110 valign=top style='width:82.35pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
   margin-bottom:0cm;margin-left:3.6pt;margin-bottom:.0001pt;text-align:center;
   line-height:normal'><b><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>Обязательное<o:p></o:p></span></b></p>
            </td>
        </tr>
        <tr style='mso-yfti-irow:1;height:20.0pt'>
            <td style='padding:.75pt .75pt .75pt .75pt;height:20.0pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;
   mso-fareast-language:RU;mso-bidi-font-weight:bold'>access_token<o:p></o:p></span></p>
            </td>
            <td width=80 style='width:59.8pt;padding:.75pt .75pt .75pt .75pt;height:
   20.0pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
   margin-bottom:0cm;margin-left:3.6pt;margin-bottom:.0001pt;text-align:center;
   line-height:normal'><span lang=EN-US style='font-size:12.0pt;font-family:
   "Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-ansi-language:EN-US;mso-fareast-language:RU;mso-bidi-font-weight:bold'>string<o:p></o:p></span></p>
            </td>
            <td width=330 style='width:247.35pt;padding:.75pt .75pt .75pt .75pt;
   height:20.0pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-fareast-language:RU;mso-bidi-font-weight:bold'>Токен
   доступа<o:p></o:p></span></p>
            </td>
            <td width=110 valign=top style='width:82.35pt;padding:.75pt .75pt .75pt .75pt;
   height:20.0pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-fareast-language:RU;mso-bidi-font-weight:bold'>Да<o:p></o:p></span></p>
            </td>
        </tr>
        <tr style='mso-yfti-irow:2;mso-yfti-lastrow:yes;height:22.4pt'>
            <td style='padding:.75pt .75pt .75pt .75pt;height:22.4pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;
   mso-fareast-language:RU;mso-bidi-font-weight:bold'>type_id<o:p></o:p></span></p>
            </td>
            <td width=80 style='width:59.8pt;padding:.75pt .75pt .75pt .75pt;height:
   22.4pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
   margin-bottom:0cm;margin-left:3.6pt;margin-bottom:.0001pt;text-align:center;
   line-height:normal'><span lang=EN-US style='font-size:12.0pt;font-family:
   "Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-ansi-language:EN-US;mso-fareast-language:RU;mso-bidi-font-weight:bold'>integer<o:p></o:p></span></p>
            </td>
            <td width=330 style='width:247.35pt;padding:.75pt .75pt .75pt .75pt;
   height:22.4pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;
   mso-fareast-language:RU;mso-bidi-font-weight:bold'>ID </span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-fareast-language:RU;mso-bidi-font-weight:bold'>Группы<o:p></o:p></span></p>
            </td>
            <td width=110 valign=top style='width:82.35pt;padding:.75pt .75pt .75pt .75pt;
   height:22.4pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-fareast-language:RU;mso-bidi-font-weight:bold'>Нет<o:p></o:p></span></p>
            </td>
        </tr>
        </thead>
    </table>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal;mso-outline-level:3'><b><span style='font-size:13.5pt;
font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:RU'>Возвращает&nbsp;<u>Массив значений</u><o:p></o:p></span></b></p>

    <table class=MsoNormalTable border=1 cellpadding=0 style='mso-cellspacing:1.5pt;
 mso-yfti-tbllook:1184'>
        <thead>
        <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
            <td width=118 style='width:88.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
   text-align:center;line-height:normal'><b><span style='font-size:12.0pt;
   font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-fareast-language:RU'>Имя параметра<o:p></o:p></span></b></p>
            </td>
            <td width=85 style='width:63.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
   text-align:center;line-height:normal'><b><span style='font-size:12.0pt;
   font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-fareast-language:RU'>Тип<o:p></o:p></span></b></p>
            </td>
            <td width=225 style='width:168.6pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
   text-align:center;line-height:normal'><b><span style='font-size:12.0pt;
   font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-fareast-language:RU'>Описание<o:p></o:p></span></b></p>
            </td>
            <td width=185 valign=top style='width:139.05pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
   text-align:center;line-height:normal'><b><span style='font-size:12.0pt;
   font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-fareast-language:RU'>Прим.<o:p></o:p></span></b></p>
            </td>
        </tr>
        </thead>
        <tr style='mso-yfti-irow:1'>
            <td width=118 style='width:88.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>success<o:p></o:p></span></p>
            </td>
            <td width=85 style='width:63.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>string</span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'><o:p></o:p></span></p>
            </td>
            <td width=225 style='width:168.6pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>true – </span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>успешно</span><span lang=EN-US
                                                                 style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>; false -</span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'> ошибка<o:p></o:p></span></p>
            </td>
            <td width=185 valign=top style='width:139.05pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'><o:p>&nbsp;</o:p></span></p>
            </td>
        </tr>
        <tr style='mso-yfti-irow:2'>
            <td width=118 style='width:88.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>error<o:p></o:p></span></p>
            </td>
            <td width=85 style='width:63.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>string<o:p></o:p></span></p>
            </td>
            <td width=225 style='width:168.6pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>Описание ошибки<o:p></o:p></span></p>
            </td>
            <td width=185 valign=top style='width:139.05pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>Если </span><span lang=EN-US
                                                               style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>success</span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'> == </span><span lang=EN-US
                                                              style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>true</span><span
                            lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
  mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'> </span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>параметр не возвращается<o:p></o:p></span></p>
            </td>
        </tr>
        <tr style='mso-yfti-irow:3;mso-yfti-lastrow:yes'>
            <td width=118 style='width:88.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>data<o:p></o:p></span></p>
            </td>
            <td width=85 style='width:63.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>array<o:p></o:p></span></p>
            </td>
            <td width=225 style='width:168.6pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>Массив запрошенных данных<o:p></o:p></span></p>
            </td>
            <td width=185 valign=top style='width:139.05pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>Если </span><span lang=EN-US
                                                               style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>success</span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>==</span><span lang=EN-US
                                                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>false</span><span
                            lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
  mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'> </span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>параметр не возвращается<o:p></o:p></span></p>
            </td>
        </tr>
    </table>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'><o:p>&nbsp;</o:p></span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'>Пример запроса:<o:p></o:p></span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'><br>
</span><a
                href="http://liftgo.pro/api/v1/get-part-type?access_token=Gk6jUuf7CVKV&amp;group_id=3"><span
                    lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>http</span><span style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>://</span><span
                    lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>liftgo</span><span style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>.</span><span
                    lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>pro</span><span style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>/</span><span
                    lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>api</span><span style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>/</span><span
                    lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>v</span><span style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>1/</span><span
                    lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>get</span><span style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>-</span><span
                    lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>part</span><span style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>-</span><span
                    lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>type</span><span style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>?</span><span
                    lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>access</span><span style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>_</span><span
                    lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>token</span><span style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>=</span><span
                    lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>Gk</span><span style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>6</span><span
                    lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>jUuf</span><span style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>7</span><span
                    lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>CVKV</span><span style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>&amp;</span><span
                    lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>group</span><span style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>_</span><span
                    lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>id</span><span style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>=3</span></a><span
                style='font-size:10.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-fareast-language:RU'><o:p></o:p></span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'><o:p>&nbsp;</o:p></span></p>

    <p class=MsoNormal><span style='font-size:13.5pt;line-height:107%;font-family:
"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";color:black;
mso-fareast-language:RU'>Ответ</span><span lang=EN-US style='font-size:13.5pt;
line-height:107%;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-ansi-language:EN-US;mso-fareast-language:
RU'>:<o:p></o:p></span></p>

    <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;line-height:107%;
font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-ansi-language:EN-US;mso-fareast-language:RU'>{&quot;success&quot;:&quot;true&quot;,&quot;data&quot;:[{&quot;id&quot;:3,&quot;name&quot;:&quot;Концевые
выключатели&quot;}]}</span><span lang=EN-US style='mso-ansi-language:EN-US;
mso-fareast-language:RU'><o:p></o:p></span></p>

    <span lang=EN-US style='font-size:11.0pt;line-height:107%;font-family:"Calibri",sans-serif;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Calibri;mso-fareast-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:"Times New Roman";
mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US;mso-fareast-language:
RU;mso-bidi-language:AR-SA'><br clear=all style='mso-special-character:line-break;
page-break-before:always'>
</span>

    <p class=MsoNormal><span lang=EN-US style='mso-ansi-language:EN-US;mso-fareast-language:
RU'><o:p>&nbsp;</o:p></span></p>

    <h1><a name="_Toc10197928"><span lang=EN-US style='font-size:24.0pt;line-height:
107%;mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;
mso-fareast-language:RU'>get</span></a><span style='mso-bookmark:_Toc10197928'><span
                    style='font-size:24.0pt;line-height:107%;mso-fareast-font-family:"Times New Roman";
mso-fareast-language:RU'>-</span></span><span style='mso-bookmark:_Toc10197928'><span
                    lang=EN-US style='font-size:24.0pt;line-height:107%;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>mechanism</span></span><span
                style='mso-bookmark:_Toc10197928'><span style='font-size:24.0pt;line-height:
107%;mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>-</span></span><span
                style='mso-bookmark:_Toc10197928'><span lang=EN-US style='font-size:24.0pt;
line-height:107%;mso-fareast-font-family:"Times New Roman";mso-ansi-language:
EN-US;mso-fareast-language:RU'>type</span></span><span style='mso-bookmark:
_Toc10197928'></span><span style='font-size:24.0pt;line-height:107%;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'><o:p></o:p></span></h1>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'>Описание:&nbsp;<b>Возвращает список типов механизма запчасти/детали</b><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'>Метод:&nbsp;<b>GET</b><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'>Адрес:</span><span lang=EN-US style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US;
mso-fareast-language:RU'>&nbsp;</span><a
                href="http://liftgo.pro/api/v1/get-mechanism-type"><span lang=EN-US
                                                                         style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>http</span><span
                    style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>://</span><span lang=EN-US
                                                           style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>liftgo</span><span
                    style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>.</span><span lang=EN-US
                                                         style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>pro</span><span
                    style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>/</span><span lang=EN-US
                                                         style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>api</span><span
                    style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>/</span><span lang=EN-US
                                                         style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>v</span><span
                    style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>1/</span><span lang=EN-US
                                                          style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>get</span><span
                    style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>-</span><span lang=EN-US
                                                         style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>mechanism</span><span
                    style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:RU'>-</span><span lang=EN-US
                                                         style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>type</span></a><span
                style='font-size:13.5pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-fareast-language:RU'><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal;mso-outline-level:3'><b><span style='font-size:13.5pt;
font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:RU'>Принимает<o:p></o:p></span></b></p>

    <table class=MsoNormalTable border=1 cellpadding=0 width=623 style='width:466.95pt;
 mso-cellspacing:1.5pt;mso-yfti-tbllook:1184'>
        <thead>
        <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
            <td style='padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
   margin-bottom:0cm;margin-left:3.6pt;margin-bottom:.0001pt;text-align:center;
   line-height:normal'><b><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>Имя
   параметра<o:p></o:p></span></b></p>
            </td>
            <td width=80 style='width:59.8pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
   margin-bottom:0cm;margin-left:3.6pt;margin-bottom:.0001pt;text-align:center;
   line-height:normal'><b><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>Тип<o:p></o:p></span></b></p>
            </td>
            <td width=330 style='width:247.35pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
   margin-bottom:0cm;margin-left:3.6pt;margin-bottom:.0001pt;text-align:center;
   line-height:normal'><b><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>Описание<o:p></o:p></span></b></p>
            </td>
            <td width=110 valign=top style='width:82.35pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
   margin-bottom:0cm;margin-left:3.6pt;margin-bottom:.0001pt;text-align:center;
   line-height:normal'><b><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>Обязательное<o:p></o:p></span></b></p>
            </td>
        </tr>
        <tr style='mso-yfti-irow:1;height:20.0pt'>
            <td style='padding:.75pt .75pt .75pt .75pt;height:20.0pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;
   mso-fareast-language:RU;mso-bidi-font-weight:bold'>access_token<o:p></o:p></span></p>
            </td>
            <td width=80 style='width:59.8pt;padding:.75pt .75pt .75pt .75pt;height:
   20.0pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
   margin-bottom:0cm;margin-left:3.6pt;margin-bottom:.0001pt;text-align:center;
   line-height:normal'><span lang=EN-US style='font-size:12.0pt;font-family:
   "Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-ansi-language:EN-US;mso-fareast-language:RU;mso-bidi-font-weight:bold'>string<o:p></o:p></span></p>
            </td>
            <td width=330 style='width:247.35pt;padding:.75pt .75pt .75pt .75pt;
   height:20.0pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-fareast-language:RU;mso-bidi-font-weight:bold'>Токен
   доступа<o:p></o:p></span></p>
            </td>
            <td width=110 valign=top style='width:82.35pt;padding:.75pt .75pt .75pt .75pt;
   height:20.0pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-fareast-language:RU;mso-bidi-font-weight:bold'>Да<o:p></o:p></span></p>
            </td>
        </tr>
        <tr style='mso-yfti-irow:2;mso-yfti-lastrow:yes;height:22.4pt'>
            <td style='padding:.75pt .75pt .75pt .75pt;height:22.4pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;
   mso-fareast-language:RU;mso-bidi-font-weight:bold'>type_id<o:p></o:p></span></p>
            </td>
            <td width=80 style='width:59.8pt;padding:.75pt .75pt .75pt .75pt;height:
   22.4pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
   margin-bottom:0cm;margin-left:3.6pt;margin-bottom:.0001pt;text-align:center;
   line-height:normal'><span lang=EN-US style='font-size:12.0pt;font-family:
   "Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-ansi-language:EN-US;mso-fareast-language:RU;mso-bidi-font-weight:bold'>integer<o:p></o:p></span></p>
            </td>
            <td width=330 style='width:247.35pt;padding:.75pt .75pt .75pt .75pt;
   height:22.4pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
   mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;
   mso-fareast-language:RU;mso-bidi-font-weight:bold'>ID </span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-fareast-language:RU;mso-bidi-font-weight:bold'>Группы<o:p></o:p></span></p>
            </td>
            <td width=110 valign=top style='width:82.35pt;padding:.75pt .75pt .75pt .75pt;
   height:22.4pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:
   0cm;margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
   "Times New Roman";mso-fareast-language:RU;mso-bidi-font-weight:bold'>Нет<o:p></o:p></span></p>
            </td>
        </tr>
        </thead>
    </table>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal;mso-outline-level:3'><b><span style='font-size:13.5pt;
font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:RU'>Возвращает&nbsp;<u>Массив значений</u><o:p></o:p></span></b></p>

    <table class=MsoNormalTable border=1 cellpadding=0 style='mso-cellspacing:1.5pt;
 mso-yfti-tbllook:1184'>
        <thead>
        <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
            <td width=118 style='width:88.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
   text-align:center;line-height:normal'><b><span style='font-size:12.0pt;
   font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-fareast-language:RU'>Имя параметра<o:p></o:p></span></b></p>
            </td>
            <td width=85 style='width:63.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
   text-align:center;line-height:normal'><b><span style='font-size:12.0pt;
   font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-fareast-language:RU'>Тип<o:p></o:p></span></b></p>
            </td>
            <td width=225 style='width:168.6pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
   text-align:center;line-height:normal'><b><span style='font-size:12.0pt;
   font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-fareast-language:RU'>Описание<o:p></o:p></span></b></p>
            </td>
            <td width=185 valign=top style='width:139.05pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
   text-align:center;line-height:normal'><b><span style='font-size:12.0pt;
   font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
   mso-fareast-language:RU'>Прим.<o:p></o:p></span></b></p>
            </td>
        </tr>
        </thead>
        <tr style='mso-yfti-irow:1'>
            <td width=118 style='width:88.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>success<o:p></o:p></span></p>
            </td>
            <td width=85 style='width:63.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>string</span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'><o:p></o:p></span></p>
            </td>
            <td width=225 style='width:168.6pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>true – </span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>успешно</span><span lang=EN-US
                                                                 style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>; false -</span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'> ошибка<o:p></o:p></span></p>
            </td>
            <td width=185 valign=top style='width:139.05pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'><o:p>&nbsp;</o:p></span></p>
            </td>
        </tr>
        <tr style='mso-yfti-irow:2'>
            <td width=118 style='width:88.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>error<o:p></o:p></span></p>
            </td>
            <td width=85 style='width:63.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>string<o:p></o:p></span></p>
            </td>
            <td width=225 style='width:168.6pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>Описание ошибки<o:p></o:p></span></p>
            </td>
            <td width=185 valign=top style='width:139.05pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>Если </span><span lang=EN-US
                                                               style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>success</span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'> == </span><span lang=EN-US
                                                              style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>true</span><span
                            lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
  mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'> </span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>параметр не возвращается<o:p></o:p></span></p>
            </td>
        </tr>
        <tr style='mso-yfti-irow:3;mso-yfti-lastrow:yes'>
            <td width=118 style='width:88.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>data<o:p></o:p></span></p>
            </td>
            <td width=85 style='width:63.4pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                    style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>array<o:p></o:p></span></p>
            </td>
            <td width=225 style='width:168.6pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>Массив запрошенных данных<o:p></o:p></span></p>
            </td>
            <td width=185 valign=top style='width:139.05pt;padding:.75pt .75pt .75pt .75pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
  margin-left:3.6pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>Если </span><span lang=EN-US
                                                               style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>success</span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>==</span><span lang=EN-US
                                                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:RU'>false</span><span
                            lang=EN-US style='font-size:12.0pt;font-family:"Times New Roman",serif;
  mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'> </span><span
                            style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
  "Times New Roman";mso-fareast-language:RU'>параметр не возвращается<o:p></o:p></span></p>
            </td>
        </tr>
    </table>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'><o:p>&nbsp;</o:p></span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'>Пример запроса:<o:p></o:p></span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'><br>
</span><a
                href="http://liftgo.pro/api/v1/get-mechanism-type?access_token=Gk6jUuf7CVKV&amp;group_id=3"><span
                    lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>http</span><span style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>://</span><span
                    lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>liftgo</span><span style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>.</span><span
                    lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>pro</span><span style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>/</span><span
                    lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>api</span><span style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>/</span><span
                    lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>v</span><span style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>1/</span><span
                    lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>get</span><span style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>-</span><span
                    lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>mechanism</span><span style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>-</span><span
                    lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>type</span><span style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>?</span><span
                    lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>access</span><span style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>_</span><span
                    lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>token</span><span style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>=</span><span
                    lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>Gk</span><span style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>6</span><span
                    lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>jUuf</span><span style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>7</span><span
                    lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>CVKV</span><span style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>&amp;</span><span
                    lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>group</span><span style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>_</span><span
                    lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
RU'>id</span><span style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:RU'>=3</span></a><span
                style='font-size:10.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";color:black;mso-fareast-language:RU'><o:p></o:p></span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'><o:p>&nbsp;</o:p></span></p>

    <p class=MsoNormal><span style='font-size:13.5pt;line-height:107%;font-family:
"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";color:black;
mso-fareast-language:RU'>Ответ:<o:p></o:p></span></p>

    <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;line-height:107%;
font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-ansi-language:EN-US;mso-fareast-language:RU'>{&quot;success&quot;:&quot;true&quot;,&quot;data&quot;:[{&quot;id&quot;:3,&quot;name&quot;:&quot;</span><span
                style='font-size:10.0pt;line-height:107%;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-fareast-language:
RU'>Тип</span><span style='font-size:10.0pt;line-height:107%;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US;
mso-fareast-language:RU'> </span><span style='font-size:10.0pt;line-height:
107%;font-family:"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";
color:black;mso-fareast-language:RU'>механизма</span><span lang=EN-US
                                                           style='font-size:10.0pt;line-height:107%;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";color:black;mso-ansi-language:EN-US;
mso-fareast-language:RU'> 3&quot;}]}</span><span lang=EN-US style='mso-ansi-language:
EN-US;mso-fareast-language:RU'><o:p></o:p></span></p>

</div>

</body>

</html>
