<?php

use kartik\form\ActiveForm;
use yii\helpers\VarDumper;

/** @var $model \app\modules\api\models\Test */
?>

<?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'method')->dropDownList($model->methods, [
                        'id' => 'ddl-select-method',
                        'prompt' => 'Выберите метод',
                        'disabled' => $model->method,
                    ]) ?>
                    <?= $form->field($model, 'as_array')->checkbox([
                        'disabled' => $model->method,
                    ]) ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'access_token')->hiddenInput()->label(false)?>
                </div>
                <div class="get-nomenclature field" style="display: none;">
                    <div class="col-md-12">
                        <?= $form->field($model, 'component_id')->textInput() ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'pagination_page_size')->textInput() ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'pagination_page_num')->textInput() ?>
                    </div>
                </div>
                <div class="get-brand field" style="display: none;">
                    <div class="col-md-12">
                        <?= $form->field($model, 'brand_id')->textInput() ?>
                    </div>
                </div>
                <div class="get-group field" style="display: none;">
                    <div class="col-md-12">
                        <?= $form->field($model, 'group_id')->textInput() ?>
                    </div>
                </div>
                <div class="get-part-type field" style="display: none;">
                    <div class="col-md-12">
                        <?= $form->field($model, 'type_id')->textInput() ?>
                    </div>
                </div>
                <div class="get-mechanism-type field" style="display: none;">
                    <div class="col-md-12">
                        <?= $form->field($model, 'type_id')->textInput() ?>
                    </div>
                </div>
                <div class="col-md-12">
                    <?= \yii\helpers\Html::submitButton('Отправить запрос', [
                        'id' => 'send-btn',
                        'class' => 'btn btn-success btn-block',
                        'style' => 'display: none;',
                    ]) ?>
                </div>
                <div class="col-md-12">
                    <?= \yii\helpers\Html::a('Новый запрос', ['/api/test'], [
                        'class' => 'btn btn-success btn-block',
                        'style' => $model->method ? '' : 'display: none;',
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="col-md-12">
                <?php if ($model->response && $model->as_array): ?>
                    <?= VarDumper::dump($model->response, 10, true); ?>
                <?php else: ?>
                <?= $model->response;?>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<?php
$script = <<<JS
$(document).ready(function(){
    $(document).on('change', '#ddl-select-method', function() {
        var method = $(this).val();
        var send_btn = $('#send-btn');
        var fields = $('.field');
        
         if (method != '') {
            send_btn.fadeIn(); 
            fields.hide();
            fields.find('input').val('');
            $('.' + method).fadeIn();
        } else {
            fields.hide();
            fields.find('input').val('');
            send_btn.fadeOut();
        }
        
        return true;
    })
})
JS;
$this->registerJs($script);