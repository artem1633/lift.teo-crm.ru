<?php

namespace app\modules\api\controllers;

use app\models\Brand;
use app\models\Functions;
use app\models\Nomenclature;
use app\models\NomenclatureGroup;
use app\models\Settings;
use app\models\TypeOfMechanism;
use app\models\TypeOfParts;
use app\models\Users;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;

/**
 * Default controller for the `api` module
 */
class V1Controller extends Controller
{
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
//                'only' => [
//                    'get-nomenclature',
//
//                ],
                'formats' => [
//                    'application/xml' => Response::FORMAT_XML,
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('/default/index');
    }

    /**
     * Получает детали номенклатуры специалиста
     * Принимает GET параметры:
     * @var string $access_token Токен доступа
     * @var int $component_id ID детали/запчасти
     * @var int $pagination_page_size Кол-во записей на страницу
     * @var int $pagination_page_num Номер страницы (для расчета offset)
     * @return array
     */
    public function actionGetNomenclature()
    {
        $request = Yii::$app->request;

        $token = $request->get('access_token');
        $component_id = $request->get('component_id');
        $pagination_page_size = $request->get('pagination_page_size');
        $pagination_page_num = $request->get('pagination_page_num') ?? 1;

        if (!$token) {
            return ['success' => 'false', 'error' => 'Отсутствует токен доступа'];
        }
        if ($token != Settings::getValueByKey('access_token')) {
            return ['success' => 'false', 'error' => 'Неизвестный токен доступа'];
        }

        $query = Nomenclature::find()->andWhere(['nomenclature_type' => Users::USER_ROLE_TECH_SPECIALIST]);

        if ($component_id) {
            $q1 = clone  $query;
            if (!$q1->andWhere(['id' => $component_id])->exists()) {
                return ['success' => 'false', 'error' => 'Деталь не найдена'];
            } else {
                $query->andWhere(['id' => $component_id]);
            }
        };

        if ($pagination_page_size && !$component_id) {
            $query->limit($pagination_page_size);
        } else {
            $pagination_page_size = null; //Если передан ID детали - обнуляем, чтобы не сработало следующее условие
        }

        if ($pagination_page_num && $pagination_page_size) {
            $offset = ((int)$pagination_page_num - 1) * $pagination_page_size;
            $query->offset($offset);
        }

        $result = $query->all();

//        array_walk($result, function(&$model){
//            $model->brand_id = Functions::getBrand($model->brand_id);
//            $model->group_id = Functions::getGroup($model->group_id);
//            $model->main_photo = Url::to($model->main_photo, true);
//        });

        return ['success' => 'true', 'data' => $result];
    }

    /**
     * Получает брэнд/ы
     * Принимает GET параметры
     * @var string $access_token Токен достпа
     * @var int $brand_id ID брэнда
     * @return array
     */
    public function actionGetBrand()
    {
        $request = Yii::$app->request;

        $token = $request->get('access_token');
        $brand_id = $request->get('brand_id');

        if (!$token) {
            return ['success' => 'false', 'error' => 'Отсутствует токен доступа'];
        }
        if ($token != Settings::getValueByKey('access_token')) {
            return ['success' => 'false', 'error' => 'Неизвестный токен доступа'];
        }

        $query = Brand::find();

        if ($brand_id) {
            $query->andWhere(['id' => $brand_id]);
            $q1 = clone $query;
            if (!$q1->exists()) {
                return ['success' => 'false', 'error' => 'Брэнд не найден'];
            }
        }
        return ['success' => 'true', 'data' => $query->all()];
    }

    /**
     * Получает группу или список групп детали/запчасти
     * Принимает GET параметры
     * @var string $access_token Токен достпа
     * @var int $group_id ID группы
     * @return array
     */
    public function actionGetGroup()
    {
        $request = Yii::$app->request;

        $token = $request->get('access_token');
        $group_id = $request->get('group_id');

        if (!$token) {
            return ['success' => 'false', 'error' => 'Отсутствует токен доступа'];
        }
        if ($token != Settings::getValueByKey('access_token')) {
            return ['success' => 'false', 'error' => 'Неизвестный токен доступа'];
        }

        $query = NomenclatureGroup::find();

        if ($group_id) {
            $query->andWhere(['id' => $group_id]);
            $q1 = clone $query;
            if (!$q1->exists()) {
                return ['success' => 'false', 'error' => 'Группа не найдена'];
            }
        }
        return ['success' => 'true', 'data' => $query->all()];
    }

    /**
     * Получает тип или список типов детали/запчасти
     * Принимает GET параметры
     * @var string $access_token Токен достпа
     * @var int $type_id ID типа
     * @return array
     */
    public function actionGetPartType()
    {
        $request = Yii::$app->request;

        $token = $request->get('access_token');
        $type_id = $request->get('type_id');

        if (!$token) {
            return ['success' => 'false', 'error' => 'Отсутствует токен доступа'];
        }
        if ($token != Settings::getValueByKey('access_token')) {
            return ['success' => 'false', 'error' => 'Неизвестный токен доступа'];
        }

        $query = TypeOfParts::find();

        if ($type_id) {
            $query->andWhere(['id' => $type_id]);
            $q1 = clone $query;
            if (!$q1->exists()) {
                return ['success' => 'false', 'error' => 'Тип детали не найден'];
            }
        }
        return ['success' => 'true', 'data' => $query->all()];
    }

    /**
     * Получает тип или список типов детали/запчасти
     * Принимает GET параметры
     * @var string $access_token Токен достпа
     * @var int $type_id ID типа
     * @return array
     */
    public function actionGetMechanismType()
    {
        $request = Yii::$app->request;

        $token = $request->get('access_token');
        $type_id = $request->get('type_id');

        if (!$token) {
            return ['success' => 'false', 'error' => 'Отсутствует токен доступа'];
        }
        if ($token != Settings::getValueByKey('access_token')) {
            return ['success' => 'false', 'error' => 'Неизвестный токен доступа'];
        }

        $query = TypeOfMechanism::find();

        if ($type_id) {
            $query->andWhere(['id' => $type_id]);
            $q1 = clone $query;
            if (!$q1->exists()) {
                return ['success' => 'false', 'error' => 'Тип детали не найден'];
            }
        }
        return ['success' => 'true', 'data' => $query->all()];
    }
}
