<?php

namespace app\modules\api\controllers;

use app\models\Users;
use app\modules\api\models\Test;
use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

/**
 * Default controller for the `api` module
 */
class TestController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionIndex()
    {
        /** @var Users $identity */
        $identity = Yii::$app->user->identity;
        $request = Yii::$app->request;

        Yii::$app->response->format = Response::FORMAT_RAW;

        if (!$identity || !Users::isAdmin()) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        $model = new Test();

        if ($request->isPost) {
            if ($model->load($request->post())){
                Yii::info($model->attributes, 'test');
                $model->sendRequest();
            }
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }

//    /**
//     * Возвращает необходимые поля для тестирования метода
//     */
//    public function actionGetFields()
//    {
//        $request = Yii::$app->request;
//
//        Yii::$app->response->format = Response::FORMAT_JSON;
//
//        $method = $request->post('method');
//
//        if (!$method) {
//            return [
//                'success' => 0,
//                'error' => 'Неизвестный метод'
//            ];
//        }
//
//        switch ($method) {
//            case 'get-nomenclature':
//                return ['component-id', 'pagination_page_size', 'pagination_page_num'];
//                break;
//            case 'get-brand':
//                break;
//            case 'get-group':
//                break;
//            case 'get-part-type':
//                break;
//            case 'get-mechanism-type':
//                break;
//            default:
//                return [
//                    'success' => 0,
//                    'error' => 'Неизвестный метод'
//                ];
//        }
//    }
}
