<?php

namespace app\modules\drive\controllers;

use app\models\Settings;
use app\modules\drive\models\Auth;
use app\modules\drive\models\Google;
use Google_Service_Drive;
use Google_Service_Drive_DriveFile;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;

/**
 * Default controller for the `drive` module
 */
class GoogleController extends Controller
{
    /**
     * Renders the index view for the module
     * @param null $code
     * @return string
     * @throws \Google_Exception
     */
    public function actionIndex($code = null)
    {
        if ($code) {
            (new Auth)->getTokenWithCode($code);
        }
        return $this->render('index');
    }

    /**
     * @return array|Response
     * @throws \Google_Exception
     */
    public function actionFirstAuthenticate()
    {
        $request = Yii::$app->request;

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $client = (new Auth)->clientInit();

            Yii::info('redirect URI: ' . $client->getRedirectUri(), 'test');

            //Запрашиваем код доступа
            $data = [
                'response_type' => 'code',
                'redirect_uri' => $client->getRedirectUri(),
                'client_id' => $client->getClientId(),
                'scope' => Google_Service_Drive::DRIVE_FILE,
                'access_type' => 'offline',
                'approval_prompt' => 'auto',
            ];

            $get_data = http_build_query($data);

            Yii::info('https://accounts.google.com/o/oauth2/auth?' . $get_data, 'test');

            $url = 'https://accounts.google.com/o/oauth2/auth?' . $get_data;

//            $this->redirect('https://accounts.google.com/o/oauth2/auth?' . $get_data);

            return [
                'size' => 'md',
                'title' => 'Выдача разрещений приложению',
                'content' => $this->renderAjax(Url::to('_oauth_form'), [
                    'url' => $url,
                ]),
            ];
        }

        return $this->redirect('index');
    }

    /**
     * Копирование бэкапов на диск
     * @return bool|string
     * @throws \Google_Exception
     */
    public function actionCopyToDisk_old()
    {
        $path_backups = Url::to('@app/backups');
        $google = new Google();
        $google->client->setDefer(true);
        $driveService = $google->getGoogleDriveService();
        /*Очистка корзины не работает.
        Для возможности очистки корзины в консоли Google в учетных данных нужно добавить область действия "../auth/drive"
        Что влечет за собой проверку сервиса самим Google
        */
//        $driveService->files->emptyTrash();

        //Ищем на диске папку backups
        $backup_folder_id = Settings::getBackupGoogleFolder();
        if (!$backup_folder_id) {
            //Создаем папку
            $fileMetadata = new Google_Service_Drive_DriveFile([
                'name' => 'backup_lift',
                'mimeType' => 'application/vnd.google-apps.folder'
            ]);
            $folder = $driveService->files->create($fileMetadata, [
                'fields' => 'id'
            ]);
            //Пишем ID папки в настройки
            /** @var Settings $settings_model */
            $settings_model = Settings::find()->where(['key' => 'backup_folder_in_google_drive'])->one();
            $settings_model->value = $folder->id;

            if (!$settings_model->save()) {
                Yii::error($settings_model->errors, __METHOD__);
                Yii::$app->session->setFlash('error', 'Ошибка сохранения ID папки Google Drive');
            }
            $backup_folder_id = $folder->id;
        }

        //Получаем файлы бэкапов

        $files = array_diff(scandir($path_backups), array('..', '.'));
        Yii::info($files, 'test');

        $new_file = '';
        foreach ($files as $file) {
            $path_file = $path_backups . '/' . $file;
            //Выбираем самый новый
            if ($new_file) {
                if (filemtime($path_file) > $new_file) {
                    $new_file = $path_file;
                }
            } else {
                $new_file = $path_file;
            }
        }

        $file_name_parts = explode('/', $new_file);
        $file_name = end($file_name_parts);

        Yii::info($file_name_parts, 'test');

        //Закачиваем на GoogleDrive файл бэкапа (тип multipart)
        $fileMetadata = new Google_Service_Drive_DriveFile([
            'name' => $file_name,
            'parents' => [$backup_folder_id]
        ]);
        $content = file_get_contents($new_file);
        $result = $driveService->files->create($fileMetadata, [
            'data' => $content,
            'mimeType' => 'application/x-tar',
            'uploadType' => 'multipart',
            'fields' => 'id'
        ]);

        if (isset($result->id)) {
            //Ели закачка успешна удаляем файл с сервера
//            if (!unlink($new_file)) {
//                Yii::warning('Ошибка удаления скопированного файла бэкапа из папки backups', __METHOD__);
//                return false;
//            }
            Yii::info('Файл загружен. ID файла: ' . $result->id, 'test');
            Yii::info('Файл загружен.', 'test');
        } else {
            Yii::warning('Файл не загружен', 'warning');
        }

        return true;

    }

    /**
     * Копирование бэкапа на гуглодиск использую тип загрузки "resumable"
     * @throws \Google_Exception
     */
    public function actionCopyToDisk()
    {

        $google = new Google();

        //Проверяем наличие папки бэкапов на гугло диске
        $google->checkBackupFolder();

        //Получаем инфу о загружемом файле
        $file = $google->getFileInfo();

        //Получаем ссылку на загрузку файла
        $location = $google->getUploadLink($file);

        //Отправляем файл
        $result = $google->uploadFile($file, $location);

        return $result;
    }
}
