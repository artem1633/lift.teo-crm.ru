<?php

namespace app\modules\drive\models;


use app\models\Settings;
use Google_Service_Drive;
use Google_Service_Drive_DriveFile;
use Yii;
use yii\helpers\Url;

class Google
{

    public $client;

    public $token;

    public $backup_folder_id;


    /**
     * Google constructor.
     * @throws \Google_Exception
     */
    public function __construct()
    {
        $this->client = Auth::clientInit();
    }

    /**
     *
     * @param object $client Google_Client
     * @return Google_Service_Drive
     * @throws \Google_Exception
     */
    public function getGoogleDriveService($client = null)
    {
        if (!$client) {
            $client = Auth::clientInit();
        }

        $driveService = new Google_Service_Drive($client);

        return $driveService;
    }

    /**
     * @return \Google_Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Очистка гугло-корзины
     * @throws \Google_Exception
     */
    public function emptyTrash()
    {
        $driveService = $this->getGoogleDriveService();

        $driveService->files->emptyTrash();
        return true;
    }

    /**
     * @throws \Google_Exception
     */
    public function checkBackupFolder()
    {
        $driveService = $this->getGoogleDriveService();

        //Ищем на Google диске папку бэкапов
        $backup_folder_id = Settings::getBackupGoogleFolder();

        if (!$backup_folder_id) {
            //Создаем папку
            $fileMetadata = new Google_Service_Drive_DriveFile([
                'name' => 'backup_lift',
                'mimeType' => 'application/vnd.google-apps.folder'
            ]);
            $folder = $driveService->files->create($fileMetadata, [
                'fields' => 'id'
            ]);
            //Пишем ID папки в настройки
            /** @var Settings $settings_model */
            $settings_model = Settings::find()->where(['key' => 'backup_folder_in_google_drive'])->one();
            $settings_model->value = $folder->id;

            if (!$settings_model->save()) {
                Yii::error($settings_model->errors, __METHOD__);
                Yii::$app->session->setFlash('error', 'Ошибка сохранения ID папки Google Drive');
            }
            $backup_folder_id = $folder->id;
        }
        $this->backup_folder_id = $backup_folder_id;

        Yii::info('Back up folder ID: ' . $this->backup_folder_id, 'test');
    }

    /**
     * @return \stdClass
     */
    public function getFileInfo()
    {
        $path_backups = Url::to('@app/backups');

        //Получаем самый новый файл бэкапа

        $files = array_diff(scandir($path_backups), array('..', '.', '.gitignore'));
        Yii::info($files, 'test');

        $new_file = '';
        foreach ($files as $file) {
            $path_file = $path_backups . '/' . $file;
            //Выбираем самый новый
            if ($new_file) {
                if (filemtime($path_file) > $new_file) {
                    $new_file = $path_file;
                }
            } else {
                $new_file = $path_file;
            }
        }

        $file_name_parts = explode('/', $new_file);
        $file_name = end($file_name_parts);
        $source = $new_file;

        $file = new \stdClass();
        $file->name = $file_name;
        $file->path = $source;
        $file->type = mime_content_type($file->path);
        $file->size = filesize($file->path);
        $file->parent = [$this->backup_folder_id];

        Yii::info('FileInfo:', 'test');
        Yii::info(json_decode(json_encode($file), true), 'test');

        return $file;

    }

    /**
     * @param \stdClass $file Инфо о файле
     * @return mixed|string
     */
    public function getUploadLink($file)
    {
        $url = 'https://www.googleapis.com/upload/drive/v3/files?uploadType=resumable';

        $this->token = $this->client->getAccessToken()['access_token'];

        $post = [
            'name' => $file->name,
            'parents' => $file->parent
        ];

        $auth_headers = [
            "Authorization: Bearer " . $this->token,
            "Content-Type: application/json; charset=UTF-8",
            "X-Upload-Content-Type: " . $file->type,
            "X-Upload-Content-Length: " . $file->size
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $auth_headers);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);
        $error = curl_error($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        Yii::info('Результат запроса сслки на загрузку файла', 'test');
        Yii::info($info, 'test');
        Yii::info($response, 'test');
        Yii::info($error, 'test');

        $header = explode("\n", $response);
        $location = '';

        foreach ($header as $string) {
            if (strpos($string, 'Location: ') !== false) {
                $location = str_replace('Location: ', '', $string);
                $location = trim($location); //Ссылка на загрузку файла
                break;
            }
        }
        Yii::info('Ссылка для загрузки файла: ' . $location, 'test');

        return $location;
    }

    /**
     * @param \stdClass $file Инфо о файле
     * @param string $location Ссылка для загрузки файла
     * @return bool
     */
    public function uploadFile($file, $location)
    {
        set_time_limit(900);
        $upload_headers = [
            "Authorization: Bearer " . $this->token,
            'Content-Length: ' . $file->size,
            'Content-Type: ' . $file->type,
        ];

        $upload_file = fopen($file->path, "rb");
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $location);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $upload_headers);
        curl_setopt($ch, CURLOPT_HEADER, 1);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PUT, 1);
        curl_setopt($ch, CURLOPT_INFILE, $upload_file);
        curl_setopt($ch, CURLOPT_INFILESIZE, $file->size);

        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 900);

        $response = curl_exec($ch);
        $error = curl_error($ch);
        $info = curl_getinfo($ch);

        curl_close($ch);
        fclose($upload_file);

        Yii::info('Результат загрузки', 'test');
        Yii::info($info, 'test');
        Yii::info($response, 'test');
        if ($error) {
            Yii::error($error, '_error');
        }

        return true;
    }


}