$(document).ready(function () {
    var grid_buttons = $('.kv-panel-before');
    grid_buttons.removeClass('pull-right');
    grid_buttons.css('display', 'flex');
    grid_buttons.css('justify-content', 'start');

    $(document).on('click', '#recalculate-balance-btn', function (e) {
        e.preventDefault();
        var btn = $(this);
        btn.html('<i class="fa fa-spinner fa-pulse fa-fw"></i> Идёт пересчет. Ожидайте');
        btn.attr('disabled', true);
        $.get("/nomenclature/recalculate-fact-balance")
            .done(function (res) {
                if (res.success) {
                    btn.html('<span class="text-success">Пересчет завершен.</span>');
                } else {
                    btn.html('<span class="text-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> ' + res.error + '</span>')
                }
            })
            .fail(function () {
                btn.html('<span class="text-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Ошибка сервера.</span>');
            }).always(function (res) {
                setTimeout(function () {
                    btn.html('<i class="glyphicon glyphicon-repeat"></i> Пересчитать остатки')
                    btn.attr('disabled', false);
                    if (res.success){
                        location.href = "/nomenclature/tech-nomenclature";
                    }
                }, 3000)
        })
    })
});