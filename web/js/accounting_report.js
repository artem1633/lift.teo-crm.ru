$(document).ready(function () {
    var table_block = $('.report-table');
    var progress_block = $('.progress-block');

    function showMessage(type, message) {
        table_block.html('<p class="text-' + type + '"><i class="fa fa-warning"></i> ' + message + '</p>').slideDown(300);
    }

    $(this).on('click', '#accept-report-filter', function () {
        table_block.hide();
        progress_block.slideDown(300);
        var url = $(this).attr('data-url');
        var data = $('#report-filter-form').serializeArray();

        $.post(url, data)
            .done(function (response) {
                if (response.success) {
                    table_block.html(response.data);
                    table_block.show();
                } else {
                    showMessage('danger', response.error);
                }
            })
            .fail(function (response) {
                console.log(response);
                showMessage('danger', response.responseText);
            })
            .always(function () {
                progress_block.slideUp(300);
            })
    });

    $(this).on('click', '#export-report', function () {
        var url = $(this).attr('data-url');
        var data = $('#report-filter-form').serializeArray();
        var export_btn = $(this);
        export_btn.html('<i class="fa fa-spinner fa-spin fa-fw"></i> Формирование файла, ожидайте');

        $.post(url, data)
            .always(function () {
                export_btn.html('<i class="fa fa-sign-out"></i> Экспорт отчета');
            })
    })

    $(this).on('click', '.to-arriving, .to-realization', function (e) {
        e.preventDefault();
        let href = $(this).attr('data-href');
        let detail_id = $(this).attr('detail-id');
        let start = $('#start-date').val();
        let end = $('#end-date').val();

        $.post(href, {id: detail_id, start: start, end: end})
            .done(function (response) {
                if (response.success === 1) {
                    window.open(response.url, '_blank');
                } else {
                    alert(response.error);
                }
            })
            .fail(function (response) {
                alert(response.responseText);
            })
    })
});