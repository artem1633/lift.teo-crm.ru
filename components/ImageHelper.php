<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13.02.2019
 * Time: 9:48
 */

namespace app\components;


use yii\imagine\Image;
use Yii;

class ImageHelper
{
    /**
     * Ужимает картинку и сохраняет поверх оригинала,
     * создает превьюху в папке "thumbs" рядом с основной картинкой
     * @param string $path_image Путь к картинке
     * @return string Путь к превьюхе
     */
    public static function optimize($path_image)
    {
        $target_size = 1024;

        Yii::info($path_image, 'test');

        $file_info = pathinfo($path_image);

        Yii::info($file_info, 'test');

        if (!array_key_exists('extension',$file_info)){
            Yii::error('Отсутствует разширение файла!', 'error');
        }
        if (!array_key_exists('filename',$file_info)){
            Yii::error('Отсутствует имя файла!', 'error');
        }
        $extension = $file_info['extension'];

        if($extension == ""){
            Yii::error('Отсутствует разширение файла!', 'error');
        }

        Image::resize($path_image, $target_size, $target_size)
            ->save(Yii::getAlias('@webroot/' . $path_image), ['quality' => 50]);

        Yii::info('Full path: ' . Yii::getAlias('@webroot/' . $path_image) , 'test');

        //Добавляем к пути перед именем изображения "thumb/" - Папка с превьюшками.

        $route = explode('/', $path_image);
        $name_img = end($route);
        $num = strrpos($path_image, '/');
        $dir_thumbs =  substr($path_image, 0, $num) . '/'. Yii::$app->params['thumbnail_dir'] .'/';

        Yii::info('Thumbs Directory: ' . $dir_thumbs , 'test');
        Yii::info('Name thumb: ' . $name_img , 'test');

        if (!is_dir($dir_thumbs)){
            mkdir($dir_thumbs, 0777, true);
        }
        $path_thumb = $dir_thumbs . $name_img;

        if ($route[count($route) - 2] == 'request') { //Если изображение из папки request - то размеры превьюхи будут другие
            Image::thumbnail($path_image, 200, 200)->save(Yii::getAlias('@webroot/' . $path_thumb), ['quality' => 100]);
        } else {
            Image::thumbnail($path_image, 50, 50)->save(Yii::getAlias('@webroot/' . $path_thumb), ['quality' => 100]);
        }
        return $path_thumb;
    }
}