<?php

use yii\db\Migration;

/**
 * Class m181019_174440_nomenclature
 */
class m181019_122701_nomenclature_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('nomenclature', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Название'),
            'vendor_code' => $this->string(255)->comment('Артикул'),
            'own_vendor_code' => $this->string(255)->comment('Собственный артикул'),
            'brand_id' => $this->integer()->comment('Бренды'),
            'group_id' => $this->integer()->comment('Группы'),
            'weight' => $this->integer()->comment('Вес'),
            'type_of_parts_id' => $this->integer()->comment('Тип запчасти'),
            'type_of_mechanism_id' => $this->integer()->comment('Тип механизма'),
            'description' => $this->text()->comment('Описание'),
            'main_photo' => $this->string(255)->comment('Основное изображение'),
            'additional_photos' => $this->text()->comment('Дополнительные изображения'),
            'created_date' => $this->string(255)->comment('Дата создания и время создания'),
            'updated_date' => $this->string(255)->comment('Дата и время редактирования'),
            'created_by' => $this->integer()->comment('Кто создал'),
        ]);
        //brand handbooks for nomenclature
        $this->createIndex('idx-nomenclature-brand_id', 'nomenclature', 'brand_id', false);
        $this->addForeignKey("fk-nomenclature-brand_id", "nomenclature", "brand_id", "brand", "id");
        //group handbooks for nomenclature
        $this->createIndex('idx-nomenclature-group_id', 'nomenclature', 'group_id', false);
        $this->addForeignKey("fk-nomenclature-group_id", "nomenclature", "group_id", "nomenclature_group", "id");
        //type_of_parts handbooks for nomenclature
        $this->createIndex('idx-nomenclature-type_of_parts_id', 'nomenclature', 'type_of_parts_id', false);
        $this->addForeignKey("fk-nomenclature-type_of_parts_id", "nomenclature", "type_of_parts_id", "type_of_parts", "id");
        //type_of_mechanism handbooks for nomenclature
        $this->createIndex('idx-nomenclature-type_of_mechanism_id', 'nomenclature', 'type_of_mechanism_id', false);
        $this->addForeignKey("fk-nomenclature-type_of_mechanism_id", "nomenclature", "type_of_mechanism_id", "type_of_mechanism", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('nomenclature');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181019_174440_nomenclature cannot be reverted.\n";

        return false;
    }
    */
}
