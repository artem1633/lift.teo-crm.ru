<?php

use yii\db\Migration;

/**
 * Class m181020_192945_type_of_mechanism
 */
class m181019_122602_type_of_mechanism_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('type_of_mechanism', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Наименование'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('type_of_mechanism');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181020_192945_type_of_mechanism cannot be reverted.\n";

        return false;
    }
    */
}
