<?php

use yii\db\Migration;

/**
 * Class m191212_133003_change_fk_from_realization_table
 */
class m191212_133003_change_fk_from_realization_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk-realization-contractor_id', 'realization');
        $this->dropIndex('fk-realization-contractor_id', 'realization');
        $this->dropColumn('realization', 'contractor_id');

        $this->addColumn('realization', 'contractor_id', $this->integer()->comment('Контрагент (юр. лицо)'));

        $this->addForeignKey(
            'fk-realization-contractor_id',
            'realization',
            'contractor_id',
            'juridical_person',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191212_133003_change_fk_from_realization_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191212_133003_change_fk_from_realization_table cannot be reverted.\n";

        return false;
    }
    */
}
