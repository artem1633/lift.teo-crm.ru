<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%reserve}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%request}}`
 * - `{{%logistic_request}}`
 * - `{{%nomenclature}}`
 * - `{{%Users}}`
 */
class m220314_080927_create_reserve_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%reserve}}', [
            'id' => $this->primaryKey(),
            'order_part_id' => $this->integer(),
            'logistic_request_part_id' => $this->integer(),
            'nomenclature_id' => $this->integer(),
            'count' => $this->integer()->comment('Количество'),
            'created_at' => $this->timestamp()->defaultExpression('NOW()'),
            'created_by' => $this->integer(),
        ]);

        // creates index for column `request_id`
        $this->createIndex(
            '{{%idx-reserve-order_part_id}}',
            '{{%reserve}}',
            'order_part_id'
        );

        // add foreign key for table `{{%request}}`
        $this->addForeignKey(
            '{{%fk-reserve-order_part_id}}',
            '{{%reserve}}',
            'order_part_id',
            '{{%order_part}}',
            'id',
            'CASCADE'
        );

        // creates index for column `logistic_request_id`
        $this->createIndex(
            '{{%idx-reserve-logistic_request_part_id}}',
            '{{%reserve}}',
            'logistic_request_part_id'
        );

        // add foreign key for table `{{%logistic_request}}`
        $this->addForeignKey(
            '{{%fk-reserve-logistic_request_part_id}}',
            '{{%reserve}}',
            'logistic_request_part_id',
            '{{%logistic_request_part}}',
            'id',
            'CASCADE'
        );

        // creates index for column `nomenclature_id`
        $this->createIndex(
            '{{%idx-reserve-nomenclature_id}}',
            '{{%reserve}}',
            'nomenclature_id'
        );

        // add foreign key for table `{{%nomenclature}}`
        $this->addForeignKey(
            '{{%fk-reserve-nomenclature_id}}',
            '{{%reserve}}',
            'nomenclature_id',
            '{{%nomenclature}}',
            'id',
            'CASCADE'
        );

        // creates index for column `created_by`
        $this->createIndex(
            '{{%idx-reserve-created_by}}',
            '{{%reserve}}',
            'created_by'
        );

        // add foreign key for table `{{%Users}}`
        $this->addForeignKey(
            '{{%fk-reserve-created_by}}',
            '{{%reserve}}',
            'created_by',
            '{{%users}}',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%request}}`
        $this->dropForeignKey(
            '{{%fk-reserve-order_part_id}}',
            '{{%reserve}}'
        );

        // drops index for column `request_id`
        $this->dropIndex(
            '{{%idx-reserve-order_part_id}}',
            '{{%reserve}}'
        );

        // drops foreign key for table `{{%logistic_request}}`
        $this->dropForeignKey(
            '{{%fk-reserve-logistic_request_part_id}}',
            '{{%reserve}}'
        );

        // drops index for column `logistic_request_id`
        $this->dropIndex(
            '{{%idx-reserve-logistic_request_part_id}}',
            '{{%reserve}}'
        );

        // drops foreign key for table `{{%nomenclature}}`
        $this->dropForeignKey(
            '{{%fk-reserve-nomenclature_id}}',
            '{{%reserve}}'
        );

        // drops index for column `nomenclature_id`
        $this->dropIndex(
            '{{%idx-reserve-nomenclature_id}}',
            '{{%reserve}}'
        );

        // drops foreign key for table `{{%Users}}`
        $this->dropForeignKey(
            '{{%fk-reserve-created_by}}',
            '{{%reserve}}'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            '{{%idx-reserve-created_by}}',
            '{{%reserve}}'
        );

        $this->dropTable('{{%reserve}}');
    }
}
