<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%settings}}`.
 */
class m190413_161847_create_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%settings}}', [
            'id' => $this->primaryKey(),
            'key' => $this->string(),
            'name' => $this->string(),
            'value' => $this->string(),
            'note' => $this->text(),
        ]);

        $this->insert('{{%settings}}', [
           'key' => 'google_refresh_token',
           'name' => 'Refresh токен Google Drive'
        ]);
        $this->insert('{{%settings}}', [
           'key' => 'backup_folder_in_google_drive',
           'name' => 'Папка бэкапов Google Drive (id папки)'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%settings}}');
    }
}
