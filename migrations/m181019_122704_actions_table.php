<?php

use yii\db\Migration;

/**
 * Class m181107_110659_actions_table
 */
class m181019_122704_actions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('actions', [
            'id' => $this->primaryKey(),
            'contractor_id' => $this->integer()->comment('Контрагент'),
            'description' => $this->text()->comment('Описание'),
            'request_id' => $this->integer()->comment('Заявка'),
            'created_date' => $this->string(255)->comment('Дата создания и время создания'),
            'updated_date' => $this->string(255)->comment('Дата и время редактирования'),
            'created_by' => $this->integer()->comment('Кто создал'),
        ]);

        $this->createIndex('idx-actions-contractor_id', 'actions', 'contractor_id', false);
        $this->addForeignKey("fk-actions-contractor_id", "actions", "contractor_id", "contractor", "id");

        $this->createIndex('idx-actions-request_id', 'actions', 'request_id', false);
        $this->addForeignKey("fk-actions-request_id", "actions", "request_id", "request", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('actions');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181107_110659_actions_table cannot be reverted.\n";

        return false;
    }
    */
}
