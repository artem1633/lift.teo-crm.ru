<?php

use yii\db\Migration;

/**
 * Handles adding measure_id to table `{{%nomenclature}}`.
 */
class m190808_092828_add_measure_id_column_to_nomenclature_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%nomenclature}}', 'measure_id', $this->integer()->comment('Единица измерения'));
        $this->addForeignKey(
            'fk-nomenclature-measure_id',
            'nomenclature',
            'measure_id',
            'measure',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%nomenclature}}', 'measure_id');
    }
}
