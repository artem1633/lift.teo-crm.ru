<?php

use yii\db\Migration;

/**
 * Class m191019_194444_order_part_table_expanded
 */
class m181019_194448_order_part_table_expanded_3 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this -> dropColumn('{{order_part}}', 'sell_price');
        $this -> dropColumn('{{order_part}}', 'buy_price');
        $this -> addColumn(
            '{{order_part}}',
            'sell_price',
            $this -> string(255) -> defaultValue(null)
        );
        $this -> addColumn(
            '{{order_part}}',
            'buy_price',
            $this -> string(255) -> defaultValue(null)
        );


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this -> dropColumn('{{order_part}}', 'sell_price');
        $this -> dropColumn('{{order_part}}', 'buy_price');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181019_174440_nomenclature cannot be reverted.\n";

        return false;
    }
    */
}
