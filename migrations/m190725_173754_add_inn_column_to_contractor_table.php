<?php

use yii\db\Migration;

/**
 * Handles adding inn to table `{{%contractor}}`.
 */
class m190725_173754_add_inn_column_to_contractor_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('contractor', 'inn', $this->string()->comment('ИНН'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('contractor', 'inn');
    }
}
