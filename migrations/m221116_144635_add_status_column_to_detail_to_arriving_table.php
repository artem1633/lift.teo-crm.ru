<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%detail_to_arriving}}`.
 */
class m221116_144635_add_status_column_to_detail_to_arriving_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('detail_to_arriving', 'status', $this->integer()->defaultValue(1)->comment('Статус'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('detail_to_arriving', 'status');
    }
}
