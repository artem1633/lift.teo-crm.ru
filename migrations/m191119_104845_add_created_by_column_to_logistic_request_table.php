<?php

use yii\db\Expression;
use yii\db\Migration;

/**
 * Handles adding created_by to table `{{%logistic_request}}`.
 */
class m191119_104845_add_created_by_column_to_logistic_request_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('logistic_request', 'created_by', $this->integer()->comment('Создатель'));

        $this->addForeignKey(
            'fk-logistic_request-created_by',
            'logistic_request',
            'created_by',
            'users',
            'id',
            'SET NULL',
            'CASCADE'
        );

        $this->addColumn('logistic_request', 'created_at',
            $this->timestamp()->defaultValue(new Expression('NOW()')));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-logistic_request-created_by', 'logistic_request');
        $this->dropColumn('logistic_request', 'created_by');
        $this->dropColumn('logistic_request', 'created_at');
    }
}
