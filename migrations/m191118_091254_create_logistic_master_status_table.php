<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%logistic_master_status}}`.
 */
class m191118_091254_create_logistic_master_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%logistic_master_status}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'description' => $this->string()->comment('Описание'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%logistic_master_status}}');
    }
}
