<?php

use yii\db\Migration;

/**
 * Class m191019_194444_order_part_table_expanded
 */
class m181019_194444_order_part_table_expanded extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this -> addColumn(
            '{{order_part}}',
            'detail_id',
            $this -> integer() -> after('comment')
        );
        //suppliers handbooks for request
        $this->createIndex(
            'idx-order_part-detail_id',
            'order_part',
            'detail_id',
            false
        );
        $this->addForeignKey(
            "fk-order_part-detail_id",
            "order_part",
            "detail_id",
            "nomenclature",
            "id"
        );

        $this -> addColumn(
            '{{order_part}}',
            'in_china',
            $this -> boolean() -> after('detail_id')
        );

        $this -> addColumn(
            '{{order_part}}',
            'delivery_time_id',
            $this -> integer() -> after('in_china')
        );
        //delivery_time handbooks for request
        $this->createIndex(
            'idx-order_part-delivery_time_id',
            'order_part',
            'delivery_time_id',
            false
        );
        $this->addForeignKey(
            "fk-order_part-delivery_time_id",
            "order_part",
            "delivery_time_id",
            "delivery_time",
            "id"
        );

        $this -> addColumn(
            '{{order_part}}',
            'suppliers_id',
            $this -> integer() -> after('delivery_time_id')
        );

        //suppliers handbooks for request
        $this->createIndex(
            'idx-order_part-suppliers_id',
            'order_part',
            'suppliers_id',
            false
        );
        $this->addForeignKey(
            "fk-order_part-suppliers_id",
            "order_part",
            "suppliers_id",
            "suppliers",
            "id"
        );

        $this -> addColumn(
            '{{order_part}}',
            'buy_price',
            $this -> integer() -> after('suppliers_id')
        );

        $this -> addColumn(
            '{{order_part}}',
            'sell_price',
            $this -> integer() -> after('buy_price')
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this -> dropColumn('{{order_part}}', 'detail_id');
        $this -> dropColumn('{{order_part}}', 'in_china');
        $this -> dropColumn('{{order_part}}', 'delivery_time_id');
        $this -> dropColumn('{{order_part}}', 'suppliers_id');
        $this -> dropColumn('{{order_part}}', 'buy_price');
        $this -> dropColumn('{{order_part}}', 'sell_price');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181019_174440_nomenclature cannot be reverted.\n";

        return false;
    }
    */
}
