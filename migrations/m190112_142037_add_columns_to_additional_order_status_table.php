<?php

use yii\db\Migration;

/**
 * Class m190112_142037_add_columns_to_additional_order_status_table
 */
class m190112_142037_add_columns_to_additional_order_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('additional_order_status', 'icon', $this->string()->comment('Иконка'));
        $this->addColumn('additional_order_status', 'color', $this->string()->comment('Цвет'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190112_142037_add_columns_to_additional_order_status_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190112_142037_add_columns_to_additional_order_status_table cannot be reverted.\n";

        return false;
    }
    */
}
