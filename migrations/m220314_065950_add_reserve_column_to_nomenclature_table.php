<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%nomenclature}}`.
 */
class m220314_065950_add_reserve_column_to_nomenclature_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%nomenclature}}', 'reserve', $this->integer()->defaultValue(0)->comment('Резерв'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%nomenclature}}', 'reserve');
    }
}
