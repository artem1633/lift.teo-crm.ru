<?php

use yii\db\Migration;

/**
 * Handles adding token_api to table `{{%settings}}`.
 */
class m190531_072331_add_access_token__setting_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'access_token',
            'name' => 'Токен доступа для АПИ',
            'value' => Yii::$app->security->generateRandomString(),
            'note' => 'Используется в запросах к АПИ'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('settings', ['key' => 'access_token']);
    }
}
