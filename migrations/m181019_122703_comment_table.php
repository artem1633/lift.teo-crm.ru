<?php

use yii\db\Migration;

/**
 * Class m181106_142726_comment_table
 */
class m181019_122703_comment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('comment', [
            'id' => $this->primaryKey(),
            'comment' => $this->text()->comment('Комментарии'),
            'manager_id' => $this->integer()->comment('Менеджер'),
            'request_id' => $this->integer()->comment('Заявка'),
            'created_date' => $this->string()->comment('Дата создания и время создания'),
        ]);

        $this->createIndex('idx-comment-manager_id', 'comment', 'manager_id', false);
        $this->addForeignKey("fk-comment-manager_id", "comment", "manager_id", "users", "id");

        $this->createIndex('idx-comment-request_id', 'comment', 'request_id', false);
        $this->addForeignKey("fk-comment-request_id", "comment", "request_id", "request", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181106_142726_comment_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181106_142726_comment_table cannot be reverted.\n";

        return false;
    }
    */
}
