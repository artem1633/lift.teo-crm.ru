<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%logistic_status}}`.
 */
class m191117_075818_create_logistic_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%logistic_status}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Статус'),
            'description' => $this->string()->comment('Описание'),
        ]);

        $this->batchInsert('{{%logistic_status}}',['name'], [
            ['В работе'],
            ['Отгружен поставщиком'],
            ['Склад Маньчжурия'],
            ['Получен'],
            ['Отгружен заказчику'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%logistic_status}}');
    }
}
