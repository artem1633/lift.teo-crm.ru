<?php

use yii\db\Migration;

/**
 * Class m191007_053920_change_total_amount_currency_column_to_detail_to_arriving_table
 */
class m191007_053920_change_total_amount_currency_column_to_detail_to_arriving_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('detail_to_arriving', 'total_amount_currency', $this->double(2)
            ->comment('Общая сумма в валюте'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191007_053920_change_total_amount_currency_column_to_detail_to_arriving_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191007_053920_change_total_amount_currency_column_to_detail_to_arriving_table cannot be reverted.\n";

        return false;
    }
    */
}
