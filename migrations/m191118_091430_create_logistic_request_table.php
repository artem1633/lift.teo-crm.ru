<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%logistic_request}}`.
 */
class m191118_091430_create_logistic_request_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%logistic_request}}', [
            'id' => $this->primaryKey(),
            'payee_id' => $this->integer()->comment('Получатель платежа'),
            'account_number' => $this->string()->comment('Номер счета'),
            'contractor_id' => $this->integer()->comment('Контрагент'),
            'responsible_id' => $this->integer()->comment('Ответственный'),
            'logistic_master_status_id' => $this->integer()->comment('Статус'),
            'logistic_status_id' => $this->integer()->comment('Статус логиста'),
            'invoice_date' => $this->timestamp()->defaultValue(null)->comment('Дата счета'),
            'payed_date' => $this->timestamp()->defaultValue(null)->comment('Дата оплаты')
        ]);

        $this->addForeignKey(
            'fk-logistic_request-payee_id',
            'logistic_request',
            'payee_id',
            'payee',
            'id',
            'SET NULL',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-logistic_request-contractor_id',
            'logistic_request',
            'contractor_id',
            'contractor',
            'id',
            'SET NULL',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-logistic_request-responsible_id',
            'logistic_request',
            'responsible_id',
            'users',
            'id',
            'SET NULL',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-logistic_request-logistic_master_status_id',
            'logistic_request',
            'logistic_master_status_id',
            'logistic_master_status',
            'id',
            'SET NULL',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-logistic_request-logistic_status_id',
            'logistic_request',
            'logistic_status_id',
            'logistic_status',
            'id',
            'SET NULL',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-logistic_request-payee_id','logistic_request');
        $this->dropForeignKey('fk-logistic_request-contractor_id','logistic_request');
        $this->dropForeignKey('fk-logistic_request-responsible_id','logistic_request');
        $this->dropForeignKey('fk-logistic_request-logistic_master_status_id','logistic_request');
        $this->dropForeignKey('fk-logistic_request-logistic_status_id','logistic_request');
        $this->dropTable('{{%logistic_request}}');
    }
}
