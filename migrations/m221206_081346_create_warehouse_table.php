<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%warehouse}}`.
 */
class m221206_081346_create_warehouse_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%warehouse}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'formula' => $this->string()->comment('Формула')
        ]);

        $this->insert('warehouse', [
            'name' => 'Лифтгоу',
        ]);

        $this->insert('warehouse', [
            'name' => 'ПМ Сибирь',
        ]);

        $this->insert('warehouse', [
            'name' => 'В Пути на склад',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%warehouse}}');
    }
}
