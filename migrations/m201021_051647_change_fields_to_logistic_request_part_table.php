<?php

use yii\db\Migration;

/**
 * Class m201021_051647_change_fields_to_logistic_request_part_table
 */
class m201021_051647_change_fields_to_logistic_request_part_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('{{%logistic_request_part}}', ['tn_number' => null, 'box_number' => null]);
        $this->alterColumn('{{%logistic_request_part}}', 'tn_number',
            $this->date()->defaultValue(null)->comment('Дата по договору'));
        $this->renameColumn('{{%logistic_request_part}}', 'tn_number', 'contract_date');

        $this->alterColumn('{{%logistic_request_part}}', 'box_number',
            $this->timestamp()->defaultValue(null)->comment('Дата заказа'));
        $this->renameColumn('{{%logistic_request_part}}', 'box_number', 'order_date');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201021_051647_change_fields_to_logistic_request_part_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201021_051647_change_fields_to_logistic_request_part_table cannot be reverted.\n";

        return false;
    }
    */
}
