<?php

use yii\db\Migration;

/**
 * Handles adding updated to table `order_part`.
 */
class m190212_121641_add_updated_columns_to_order_part_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order_part', 'updated_buy_price', $this
            ->timestamp()
            ->defaultExpression('NOW()')
            ->comment('Дата обновления закупочной цены'));

        $this->addColumn('order_part', 'updated_sell_price', $this
            ->timestamp()
            ->defaultExpression('NOW()')
            ->comment('Дата обновления цены продажи'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190212_121641_add_updated_columns_to_order_part_table cannot be reverted.\n";

        return false;
    }
}
