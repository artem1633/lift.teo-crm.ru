<?php

use yii\db\Migration;

/**
 * Class m181020_192929_type_of_parts
 */
class m181019_122600_type_of_parts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('type_of_parts', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Наименование'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('type_of_parts');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181020_192929_type_of_parts cannot be reverted.\n";

        return false;
    }
    */
}
