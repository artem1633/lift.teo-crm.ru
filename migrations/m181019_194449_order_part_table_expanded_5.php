<?php

use yii\db\Migration;

/**
 * Class m191019_194444_order_part_table_expanded
 */
class m181019_194449_order_part_table_expanded_5 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this -> dropColumn('{{order_part}}', 'quantity');
        $this -> addColumn(
            '{{order_part}}',
            'quantity',
            $this -> string(255) -> defaultValue(null)
        );



    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this -> dropColumn('{{order_part}}', 'quantity');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181019_174440_nomenclature cannot be reverted.\n";

        return false;
    }
    */
}
