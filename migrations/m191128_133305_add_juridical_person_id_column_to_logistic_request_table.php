<?php

use yii\db\Migration;

/**
 * Handles adding juridical_person_id to table `{{%logistic_request}}`.
 */
class m191128_133305_add_juridical_person_id_column_to_logistic_request_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('logistic_request', 'juridical_person_id', $this->integer()->comment('Юр. лицо'));

        $this->addForeignKey(
            'fk-logistic_request-juridical_person_id',
            'logistic_request',
            'juridical_person_id',
            'juridical_person',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-logistic_request-juridical_person_id', 'logistic_request');
        $this->dropColumn('logistic_request', 'juridical_person_id');
    }
}
