<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%detail_to_arriving}}`.
 */
class m220317_120533_add_warehouse_column_to_detail_to_arriving_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%detail_to_arriving}}', 'warehouse', $this->smallInteger()->comment('Склад'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%detail_to_arriving}}', 'warehouse');
    }
}
