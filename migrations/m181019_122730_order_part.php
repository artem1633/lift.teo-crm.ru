<?php

use yii\db\Migration;

/**
 * Class m181019_174440_order_part
 */
class m181019_122730_order_part extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('measure', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->comment('Единица измерения'),
        ]);

        $this->createTable('order_part', [
            'id' => $this->primaryKey(),
            'request_id' => $this->integer()->comment('Заявка'),
            'nomenclature_id' => $this->integer()->comment('Запрос'),
            'vendor_code' => $this->string(255)->comment('Ариткул'),
            'comment' => $this->text()->comment('Комментарий'),
            'quantity' => $this->integer()->comment('Количество'),
            'measure_id' => $this->integer()->comment('Единица измерения'),
            'files_location' => $this->text()->comment('Файлы'),
            'created_date' => $this->string(255)->comment('Дата создания и время создания'),
            'updated_date' => $this->string(255)->comment('Дата и время редактирования'),
            'created_by' => $this->integer()->comment('Кто создал'),
        ]);
        //request handbooks for order_part
        $this->createIndex('idx-order_part-request_id', 'order_part', 'request_id', false);
        $this->addForeignKey("fk-order_part-request_id", "order_part", "request_id", "request", "id");
        //nomenclature handbooks for order_part
        $this->createIndex('idx-order_part-nomenclature_id', 'order_part', 'nomenclature_id', false);
        $this->addForeignKey("fk-order_part-nomenclature_id", "order_part", "nomenclature_id", "nomenclature", "id");
        //measure handbooks for order_part
        $this->createIndex('idx-order_part-measure_id', 'order_part', 'measure_id', false);
        $this->addForeignKey("fk-order_part-measure_id", "order_part", "measure_id", "measure", "id");

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('measure');
        $this->dropTable('order_part');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181019_174440_nomenclature cannot be reverted.\n";

        return false;
    }
    */
}
