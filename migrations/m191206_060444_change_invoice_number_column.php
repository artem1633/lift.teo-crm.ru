<?php

use yii\db\Migration;

/**
 * Class m191206_060444_change_invoice_number_column
 */
class m191206_060444_change_invoice_number_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('logistic_request_part', 'arriving_id', $this->integer()->comment('Поступление (т.н. Инвойс)'));
        $this->addForeignKey(
            'fk-logistic_request_part-arriving_id',
            'logistic_request_part',
            'arriving_id',
            'arriving',
            'id',
            'SET NULL',
            'CASCADE'
        );

        $this->dropColumn('logistic_request_part', 'invoice_number');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191206_060444_change_invoice_number_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191206_060444_change_invoice_number_column cannot be reverted.\n";

        return false;
    }
    */
}
