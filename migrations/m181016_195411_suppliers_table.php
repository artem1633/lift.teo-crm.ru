<?php

use yii\db\Migration;

/**
 * Class m181016_195411_suppliers
 */
class m181016_195411_suppliers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('suppliers', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Имя'),
            'address' => $this->string(255)->comment('Адрес'),
            'contact' => $this->string(255)->comment('Контакт'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('suppliers');
    }

}
