<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%movement_part}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%nomenclature}}`
 * - `{{%arriving}}`
 */
class m220418_055133_create_movement_part_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%movement_part}}', [
            'id' => $this->primaryKey(),
            'nomenclature_id' => $this->integer(),
            'arriving_id' => $this->integer(),
            'warehouse_from' => $this->integer()->comment('Со склада'),
            'warehouse_to' => $this->integer()->comment('На склад'),
            'date' => $this->date()->defaultValue(null),
            'num' => $this->double()->comment('Кол-во'),
            'created_at' => $this->timestamp()->defaultExpression('NOW()'),
            'updated_at' => $this->timestamp()->defaultExpression('NOW() ON UPDATE NOW()'),
        ]);

        // creates index for column `nomenclature_id`
        $this->createIndex(
            '{{%idx-movement_part-nomenclature_id}}',
            '{{%movement_part}}',
            'nomenclature_id'
        );

        // add foreign key for table `{{%nomenclature}}`
        $this->addForeignKey(
            '{{%fk-movement_part-nomenclature_id}}',
            '{{%movement_part}}',
            'nomenclature_id',
            '{{%nomenclature}}',
            'id',
            'CASCADE'
        );

        // creates index for column `arriving_id`
        $this->createIndex(
            '{{%idx-movement_part-arriving_id}}',
            '{{%movement_part}}',
            'arriving_id'
        );

        // add foreign key for table `{{%arriving}}`
        $this->addForeignKey(
            '{{%fk-movement_part-arriving_id}}',
            '{{%movement_part}}',
            'arriving_id',
            '{{%arriving}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%nomenclature}}`
        $this->dropForeignKey(
            '{{%fk-movement_part-nomenclature_id}}',
            '{{%movement_part}}'
        );

        // drops index for column `nomenclature_id`
        $this->dropIndex(
            '{{%idx-movement_part-nomenclature_id}}',
            '{{%movement_part}}'
        );

        // drops foreign key for table `{{%arriving}}`
        $this->dropForeignKey(
            '{{%fk-movement_part-arriving_id}}',
            '{{%movement_part}}'
        );

        // drops index for column `arriving_id`
        $this->dropIndex(
            '{{%idx-movement_part-arriving_id}}',
            '{{%movement_part}}'
        );

        $this->dropTable('{{%movement_part}}');
    }
}
