<?php

use yii\db\Migration;

/**
 * Class m190725_184038_rename_1c_link_fields
 */
class m190725_184038_rename_1c_link_fields extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('contractor', '1c_link');
        $this->addColumn('contractor', 'link_1c', $this->string()->comment('Ссылка в 1С'));
        $this->dropColumn('realization', '1c_link');
        $this->addColumn('realization', 'link_1c', $this->string()->comment('Ссылка в 1С'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190725_184038_rename_1c_link_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190725_184038_rename_1c_link_fields cannot be reverted.\n";

        return false;
    }
    */
}
