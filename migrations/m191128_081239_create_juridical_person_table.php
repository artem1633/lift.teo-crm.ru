<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%juridical_person}}`.
 */
class m191128_081239_create_juridical_person_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%juridical_person}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'full_name' => $this->string()->comment('Полное наименование'),
            'inn' => $this->string()->comment('ИНН'),
            'kpp' => $this->string()->comment('КПП'),
            'bik' => $this->string()->comment('БИК'),
            'pay_off_account' => $this->string()->comment('Расчетный счет'),
            'corr_account' => $this->string()->comment('Кор. счет'),
            'contractor_id' => $this->integer()->comment('Контрагент'),
        ]);

        $this->addForeignKey(
            'fk-juridical_person-contractor_id',
            'juridical_person',
            'contractor_id',
            'contractor',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%juridical_person}}');
    }
}
