<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%realization}}`.
 */
class m190725_165416_create_realization_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%realization}}', [
            'id' => $this->primaryKey(),
            'date' => $this->timestamp()->defaultValue(null),
            'contractor_id' => $this->integer()->comment('Контрагент'),
            'product_id' => $this->integer()->comment('Товар'),
            'sum' => $this->double(2)->comment('Количество'),
            'amount' => $this->double(2)->comment('Сумма (с учетом НДС)'),
            '1c_link' => $this->string()->comment('Ссылка в 1С'),
            'note' => $this->text()->comment('Примечание'),
        ]);

        $this->addForeignKey(
            'fk-realization-contractor_id',
            'realization',
            'contractor_id',
            'contractor',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-realization-product_id',
            'realization',
            'product_id',
            'nomenclature',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%realization}}');
    }
}
