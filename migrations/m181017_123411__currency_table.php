<?php

use yii\db\Migration;

/**
 * Class m181017_123411__currency
 */
class m181017_123411__currency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('_currency', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Наименование'),
            'icon' => $this->string(10),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('_currency');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181017_123411__currency cannot be reverted.\n";

        return false;
    }
    */
}
