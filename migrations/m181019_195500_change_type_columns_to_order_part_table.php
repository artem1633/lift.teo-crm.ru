<?php

use yii\db\Migration;

/**
 * Class m190108_163207_change_type_columns_to_order_part_table
 */
class m181019_195500_change_type_columns_to_order_part_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('order_part', 'quantity', $this->double(3)->comment('Количество'));
        $this->alterColumn('order_part', 'buy_price', $this->double(2)->comment('Цена покупки'));
        $this->alterColumn('order_part', 'sell_price', $this->double(2)->comment('Цена продажи'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190108_163207_change_type_columns_to_order_part_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190108_163207_change_type_columns_to_order_part_table cannot be reverted.\n";

        return false;
    }
    */
}
