<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%payee}}`.
 */
class m191117_090445_create_payee_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%payee}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Наименование'),
            'full_name' => $this->string(255)->comment('Полное наименование'),
            'city_id' => $this->integer()->comment('Город'),
            'address' => $this->string(255)->comment('Адрес'),
            'phone' => $this->string(255)->comment('Телефон'),
            'contact_person' => $this->string(255)->comment('Контактное лицо'),
            'email' => $this->string(255)->comment('Email'),
            'inn' => $this->string()->comment('ИНН'),
            'requisites' => $this->text()->comment('Реквизиты'),
            'comment' => $this->text()->comment('Комментарии'),
            'created_date' => $this->timestamp()->defaultValue(new \yii\db\Expression('NOW()'))->comment('Дата создания и время создания'),
            'updated_date' => $this->timestamp()->defaultValue(null)->comment('Дата и время редактирования'),
        ]);

        $this->addForeignKey('fk-payee-city_id',
            'payee',
            'city_id',
            'cities',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-payee-city_id', '{{%payee}}');
        $this->dropTable('{{%payee}}');
    }
}
