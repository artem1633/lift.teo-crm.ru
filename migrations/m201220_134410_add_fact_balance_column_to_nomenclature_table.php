<?php

use yii\db\Migration;

/**
 * Handles adding fact_balance to table `{{%nomenclature}}`.
 */
class m201220_134410_add_fact_balance_column_to_nomenclature_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%nomenclature}}', 'fact_balance', $this->double()->comment('Фактический остаток.'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%nomenclature}}', 'fact_balance');
    }
}
