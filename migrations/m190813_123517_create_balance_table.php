<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%balance}}`.
 */
class m190813_123517_create_balance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%balance}}', [
            'id' => $this->primaryKey(),
            'detail_id' => $this->integer()->comment('Деталь'),
            'date' => $this->timestamp()
                ->defaultValue(new \yii\db\Expression('NOW()'))
                ->comment('Дата на которую расчитываются остатки'),
            'number' => $this->double(2)->comment('Количество'),
            'total_amount' => $this->double(2)->comment('Общая суммма')
        ]);

        $this->addForeignKey(
            'fk-balance-detail_id',
            '{{%balance}}',
            'detail_id',
            'nomenclature',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%balance}}');
    }
}
