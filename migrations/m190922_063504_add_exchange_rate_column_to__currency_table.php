<?php

use yii\db\Migration;

/**
 * Handles adding exchange_rate to table `{{%_currency}}`.
 */
class m190922_063504_add_exchange_rate_column_to__currency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%_currency}}', 'exchange_rate', $this->double(2)->comment('Курс'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%_currency}}', 'exchange_rate');
    }
}
