<?php

use yii\db\Migration;

/**
 * Class m181214_182706_change_type_weight_column_to_nomenclature_table
 */
class m181214_182706_change_type_weight_column_to_nomenclature_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('nomenclature', 'weight', $this->double(3)->comment('вес'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181214_182706_change_type_weight_column_to_nomenclature_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181214_182706_change_type_weight_column_to_nomenclature_table cannot be reverted.\n";

        return false;
    }
    */
}
