<?php

use yii\db\Migration;

/**
 * Class m190218_081745_add_default_value_in_china_colimn_to_order_part_table
 */
class m190218_081745_add_default_value_in_china_colimn_to_order_part_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('order_part', 'in_china', $this
            ->smallInteger(1)
            ->defaultValue(1)
            ->comment('Есть в китае'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190218_081745_add_default_value_in_china_colimn_to_order_part_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190218_081745_add_default_value_in_china_colimn_to_order_part_table cannot be reverted.\n";

        return false;
    }
    */
}
