<?php

use yii\db\Migration;

/**
 * Class m181016_193829_employee
 */
class m181016_193829_expand_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this -> addColumn('{{users}}', 'access', "enum('Вкл','Выкл') NOT NULL DEFAULT 'Вкл'");
        $this -> addColumn('{{users}}', 'email', $this -> string(255) -> after('access')->comment('Имаил'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{users}}', 'access');
        $this->dropColumn('{{users}}', 'email');

        return false;
    }
}
