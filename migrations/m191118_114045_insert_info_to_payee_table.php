<?php

use yii\db\Migration;

/**
 * Class m191118_114045_insert_info_to_payee_table
 */
class m191118_114045_insert_info_to_payee_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('payee', ['name'], [
            ['Лифтгоу'], ['ПМ Сибирь']
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191118_114045_insert_info_to_payee_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191118_114045_insert_info_to_payee_table cannot be reverted.\n";

        return false;
    }
    */
}
