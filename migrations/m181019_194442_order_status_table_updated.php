<?php

use yii\db\Migration;

/**
 * Class m181019_174440_order_status
 */
class m181019_194442_order_status_table_updated extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this -> dropColumn('{{order_status}}', 'icon');
        $this -> dropColumn('{{order_status}}', 'color');

        $this -> addColumn('{{order_status}}', 'icon', $this -> string(50) -> after('id'));
        $this -> addColumn('{{order_status}}', 'color', $this -> string(50) -> after('icon'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191019_194442_order_status_table_updated cannot be reverted.\n";
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181019_174440_nomenclature cannot be reverted.\n";

        return false;
    }
    */
}
