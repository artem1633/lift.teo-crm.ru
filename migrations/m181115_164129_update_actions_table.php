<?php

use yii\db\Migration;

/**
 * Class m181115_164129_update_actions_table
 */
class m181115_164129_update_actions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this -> addColumn(
            '{{actions}}',
            'manager_id',
            $this -> integer()
        );

        $this->createIndex('idx-actions-manager_id', 'actions', 'manager_id', false);
        $this->addForeignKey("fk-actions-manager_id", "actions", "manager_id", "users", "id");

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this -> dropColumn('{{actions}}', 'manager_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181115_164129_update_actions_table cannot be reverted.\n";

        return false;
    }
    */
}
