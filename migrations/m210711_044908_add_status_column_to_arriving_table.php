<?php

use yii\db\Migration;

/**
 * Handles adding status to table `{{%arriving}}`.
 */
class m210711_044908_add_status_column_to_arriving_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%arriving}}', 'status', $this->smallInteger()->defaultValue(1)
            ->comment('Статус'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%arriving}}', 'status');
    }
}
