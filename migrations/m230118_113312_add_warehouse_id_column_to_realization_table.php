<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%realization}}`.
 */
class m230118_113312_add_warehouse_id_column_to_realization_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('realization', 'warehouse_id', $this->integer()->comment('Склад'));

        $this->createIndex(
            'idx-realization-warehouse_id',
            'realization',
            'warehouse_id'
        );

        $this->addForeignKey(
            'fk-realization-warehouse_id',
            'realization',
            'warehouse_id',
            'warehouse',
            'id',
            'CASCADE'
        );

        \app\models\Realization::updateAll(['warehouse_id' => 2], ['is not', 'id', null]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-realization-warehouse_id',
            'realization'
        );

        $this->dropIndex(
            'idx-realization-warehouse_id',
            'realization'
        );

        $this->dropColumn('realization', 'warehouse_id');
    }
}
