<?php

use yii\db\Migration;

/**
 * Class m191119_102039_add_columns_to_logistic_tables
 */
class m191119_102039_add_columns_to_logistic_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('logistic_status', 'icon', $this->string()->comment('Иконка'));
        $this->addColumn('logistic_status', 'color', $this->string()->comment('Цвет'));
        $this->addColumn('logistic_master_status', 'icon', $this->string()->comment('Иконка'));
        $this->addColumn('logistic_master_status', 'color', $this->string()->comment('Цвет'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191119_102039_add_columns_to_logistic_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191119_102039_add_columns_to_logistic_tables cannot be reverted.\n";

        return false;
    }
    */
}
