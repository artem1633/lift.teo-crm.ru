<?php

use yii\db\Migration;

/**
 * Class m190930_103739_add_columns_to_detail_to_arriving_table
 */
class m190930_103739_add_columns_to_detail_to_arriving_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%detail_to_arriving}}', 'currency_id', $this->integer()->comment('Валюта'));
        $this->addColumn('{{%detail_to_arriving}}', 'exchange_rate', $this->integer()->comment('Курс'));
        $this->addColumn('{{%detail_to_arriving}}', 'total_amount_rub', $this->integer()->comment('Общая сумма в рублях по курсу'));
        $this->addColumn('{{%detail_to_arriving}}', 'total_amount_currency', $this->integer()->comment('Общая сумма в валюте'));

        $this->dropColumn('{{%detail_to_arriving}}', 'total_amount');
        $this->dropColumn('{{%detail_to_arriving}}', 'nds');

        $this->addForeignKey(
            'fk-detail_to_arriving-currency_id',
            'detail_to_arriving',
            'currency_id',
            '_currency',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190930_103739_add_columns_to_detail_to_arriving_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190930_103739_add_columns_to_detail_to_arriving_table cannot be reverted.\n";

        return false;
    }
    */
}
