<?php

use yii\db\Migration;

/**
 * Handles adding number to table `{{%arriving}}`.
 */
class m191204_073156_add_number_column_to_arriving_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('arriving', 'number', $this->string()->comment('Номер'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('arriving', 'number');
    }
}
