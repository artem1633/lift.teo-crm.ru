<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%logistic_request_part}}`.
 */
class m191118_100428_create_logistic_request_part_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%logistic_request_part}}', [
            'id' => $this->primaryKey(),
            'logistic_request_id' => $this->integer()->comment('Счет'),
            'nomenclature_id' => $this->integer()->comment('Деталь'),
            'num' => $this->double()->comment('Количество'),
            'comment' => $this->text()->comment('Комментарий'),
            'request_number' => $this->string()->comment('Номер заявки'),
            'invoice_number' => $this->string()->comment('Инвойс'),
            'supplier_id' => $this->integer()->comment('Поставщик'),
            'logistic_status_id' => $this->integer()->comment('Статус логиста'),
            'tn_number' => $this->string()->comment('Т/Н номер'),
            'box_number' => $this->string()->comment('Номер на коробке'),
            'ship_date' => $this->timestamp()->defaultValue(null)->comment('Дата отгрузки поставщика'),
            'receipt_date_manch' => $this->timestamp()->defaultValue(null)->comment('Дата получения (склад Маньчжурия)'),
            'receipt_date_novosib' => $this->timestamp()->defaultValue(null)->comment('Дата получения (склад Нвосибирск)'),
            'client_ship_date' => $this->timestamp()->defaultValue(null)->comment('Дата отгрузки клиенту'),
            'tn_tk_rus' => $this->string()->comment('Номер ТН ТК в России')
        ]);

        $this->addForeignKey(
            'fk-logistic_request_part-logistic_request_id',
            'logistic_request_part',
            'logistic_request_id',
            'logistic_request',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-logistic_request_part-nomenclature_id',
            'logistic_request_part',
            'nomenclature_id',
            'nomenclature',
            'id',
            'SET NULL',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-logistic_request_part-supplier_id',
            'logistic_request_part',
            'supplier_id',
            'suppliers',
            'id',
            'SET NULL',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-logistic_request_part-logistic_status_id',
            'logistic_request_part',
            'logistic_status_id',
            'logistic_status',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-logistic_request_part-logistic_request_id', 'logistic_request_part');
        $this->dropForeignKey('fk-logistic_request_part-nomenclature_id', 'logistic_request_part');
        $this->dropForeignKey('fk-logistic_request_part-supplier_id', 'logistic_request_part');
        $this->dropForeignKey('fk-logistic_request_part-logistic_status_id', 'logistic_request_part');
        $this->dropTable('{{%logistic_request_part}}');
    }
}
