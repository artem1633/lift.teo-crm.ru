<?php

use yii\db\Migration;

/**
 * Handles adding measure_id to table `{{%logistic_request_part}}`.
 */
class m191128_143217_add_measure_id_column_to_logistic_request_part_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('logistic_request_part', 'measure_id', $this->integer()->comment('Мера'));

        $this->addForeignKey(
            'fk-logistic_request_part-measure_id',
            'logistic_request_part',
            'measure_id',
            'measure',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-logistic_request_part-measure_id', 'logistic_request_part');
        $this->dropColumn('logistic_request_part', 'measure_id');
    }
}
