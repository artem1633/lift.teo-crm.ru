<?php

use yii\db\Migration;

/**
 * Class m181231_080741_change_type_datetime_columns
 */
class m181231_080741_change_type_datetime_columns extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('contractor', 'created_date');
        $this->dropColumn('contractor', 'updated_date');

        $this->addColumn('contractor', 'created_date', $this->timestamp()
            ->defaultExpression('NOW()')
            ->comment('Дата и время создания'));
        $this->addColumn('contractor', 'updated_date', $this->timestamp()
            ->defaultExpression('NOW() ON UPDATE NOW()')
            ->comment('Дата и время редактирования'));

        $this->dropColumn('request', 'created_date');
        $this->dropColumn('request', 'updated_date');

        $this->addColumn('request', 'created_date', $this->timestamp()
            ->defaultExpression('NOW()')
            ->comment('Дата и время создания'));
        $this->addColumn('request', 'updated_date', $this->timestamp()
            ->defaultExpression('NOW() ON UPDATE NOW()')
            ->comment('Дата и время редактирования'));

        $this->dropColumn('actions', 'created_date');
        $this->dropColumn('actions', 'updated_date');

        $this->addColumn('actions', 'created_date', $this->timestamp()
            ->defaultExpression('NOW()')
            ->comment('Дата и время создания'));
        $this->addColumn('actions', 'updated_date', $this->timestamp()
            ->defaultExpression('NOW() ON UPDATE NOW()')
            ->comment('Дата и время редактирования'));

        $this->dropColumn('comment', 'created_date');

        $this->addColumn('comment', 'created_date', $this->timestamp()
            ->defaultExpression('NOW()')
            ->comment('Дата и время создания'));

        $this->dropColumn('nomenclature', 'created_date');
        $this->dropColumn('nomenclature', 'updated_date');

        $this->addColumn('nomenclature', 'created_date', $this->timestamp()
            ->defaultExpression('NOW()')
            ->comment('Дата и время создания'));
        $this->addColumn('nomenclature', 'updated_date', $this->timestamp()
            ->defaultExpression('NOW() ON UPDATE NOW()')
            ->comment('Дата и время редактирования'));

        $this->dropColumn('order_part', 'created_date');
        $this->dropColumn('order_part', 'updated_date');

        $this->addColumn('order_part', 'created_date', $this->timestamp()
            ->defaultExpression('NOW()')
            ->comment('Дата и время создания'));
        $this->addColumn('order_part', 'updated_date', $this->timestamp()
            ->defaultExpression('NOW() ON UPDATE NOW()')
            ->comment('Дата и время редактирования'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
//        echo "m181231_080741_change_type_datetime_columns cannot be reverted.\n";
//
//        return false;
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181231_080741_change_type_datetime_columns cannot be reverted.\n";

        return false;
    }
    */
}
