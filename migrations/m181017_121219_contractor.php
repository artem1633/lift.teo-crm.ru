<?php

use yii\db\Migration;

/**
 * Class m181016_180834_contractor
 */
class m181017_121219_contractor extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('contractor', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Название'),
            'full_name' => $this->string(255)->comment('Полное название'),
            'city_id' => $this->integer()->comment('Город'),
            'address' => $this->string(255)->comment('Адрес'),
            'phone' => $this->string(255)->comment('Телефон'),
            'contact_person' => $this->string(255)->comment('Контактное лицо'),
            'email' => $this->string(255)->comment('Имайл'),
            'requisites' => $this->text()->comment('Реквизиты'),
            'comment' => $this->text()->comment('Комментарии'),
            'manager_id' => $this->integer()->comment('Менеджер'),
            'do_not_touch' => $this->boolean()->comment('Не трогать'),
            'created_date' => $this->string()->comment('Дата создания и время создания'),
            'updated_date' => $this->string()->comment('Дата и время редактирования'),
            'created_by' => $this->integer()->comment('Кто создал'),
        ]);

        //city handbooks for contractor
        $this->createIndex('idx-contractor-city_id', 'contractor', 'city_id', false);
        $this->addForeignKey("fk-contractor-city_id", "contractor", "city_id", "cities", "id");

        //manager handbooks for contractor
        $this->createIndex('idx-contractor-manager_id', 'contractor', 'manager_id', false);
        $this->addForeignKey("fk-contractor-manager_id", "contractor", "manager_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181016_180834_contractor cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181016_180834_contractor cannot be reverted.\n";

        return false;
    }
    */
}
