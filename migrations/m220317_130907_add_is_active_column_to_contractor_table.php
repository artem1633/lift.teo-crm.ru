<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%contractor}}`.
 */
class m220317_130907_add_is_active_column_to_contractor_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%contractor}}', 'is_active',
            $this->smallInteger()->defaultValue(1)->comment('Статус. Активен или нет'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%contractor}}', 'is_active');
    }
}
