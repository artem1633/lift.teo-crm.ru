<?php

use yii\db\Migration;

/**
 * Class m181017_122628_order_status
 */
class m181017_122628_order_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('order_status', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Наименование'),
            'icon' => $this->string(10),
            'color' => $this->string(10),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('order_status');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181017_122628_order_status cannot be reverted.\n";

        return false;
    }
    */
}
