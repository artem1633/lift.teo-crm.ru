<?php

use yii\db\Migration;

/**
 * Class m190923_064631_delete_column_to_realization_table
 */
class m190923_064631_delete_column_to_realization_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk-realization-product_id', '{{%realization}}');
        $this->dropIndex('fk-realization-product_id', '{{%realization}}');

        $this->dropColumn('{{%realization}}', 'product_id');
        $this->dropColumn('{{%realization}}', 'sum');
        $this->dropColumn('{{%realization}}', 'amount');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190923_064631_delete_column_to_realization_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190923_064631_delete_column_to_realization_table cannot be reverted.\n";

        return false;
    }
    */
}
