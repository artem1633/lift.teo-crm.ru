<?php

use yii\db\Migration;

/**
 * Class m190130_144537_change_address_column_type_to_contractor_table
 */
class m190130_144537_change_address_column_type_to_contractor_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('contractor', 'address', $this->text()->comment('Адрес'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190130_144537_change_address_column_type_to_contractor_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190130_144537_change_address_column_type_to_contractor_table cannot be reverted.\n";

        return false;
    }
    */
}
