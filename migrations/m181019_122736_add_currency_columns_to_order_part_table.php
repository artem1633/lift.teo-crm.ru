<?php

use yii\db\Migration;

/**
 * Handles adding currency to table `order_part`.
 */
class m181019_122736_add_currency_columns_to_order_part_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order_part', 'buy_currency', $this->integer()->comment('Валюта покупки'));
        $this->createIndex(
            'idx-order_part-buy_currency',
            'order_part',
            'buy_currency',
            false);
        $this->addForeignKey(
            "fk-order_part-buy_currency",
            "order_part",
            "buy_currency",
            "_currency",
            "id");

        $this->addColumn('order_part', 'sell_currency', $this->integer()->comment('Валюта продажи'));
        $this->createIndex(
            'idx-order_part-sell_currency',
            'order_part',
            'sell_currency',
            false);
        $this->addForeignKey(
            "fk-order_part-sell_currency",
            "order_part",
            "sell_currency",
            "_currency",
            "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
