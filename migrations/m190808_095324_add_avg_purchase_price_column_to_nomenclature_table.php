<?php

use yii\db\Migration;

/**
 * Handles adding avg_purchase_price to table `{{%nomenclature}}`.
 */
class m190808_095324_add_avg_purchase_price_column_to_nomenclature_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%nomenclature}}', 'avg_purchase_price',
            $this->double(2)->comment('Средняя цена закупки'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%nomenclature}}', 'avg_purchase_price');
    }
}
