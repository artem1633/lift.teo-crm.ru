<?php

use yii\db\Migration;

/**
 * Handles adding link_1c to table `{{%juridical_person}}`.
 */
class m191212_075535_add_link_1c_column_to_juridical_person_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('juridical_person', 'link_1c', $this->string()->comment('Ссылка в базе 1С'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('juridical_person', 'link_1c');
    }
}
