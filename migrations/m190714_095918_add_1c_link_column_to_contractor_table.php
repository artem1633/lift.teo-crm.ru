<?php

use yii\db\Migration;

/**
 * Handles adding 1c_link to table `{{%contractor}}`.
 */
class m190714_095918_add_1c_link_column_to_contractor_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%contractor}}', '1c_link', $this->string()->comment('Ссылка на организацию в 1С'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190714_095918_add_1c_link_column_to_contractor_table cannot be reverted.\n";

        return false;
    }
}
