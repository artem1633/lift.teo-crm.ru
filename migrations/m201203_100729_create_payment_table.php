<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%payment}}`.
 */
class m201203_100729_create_payment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%payment}}', [
            'id' => $this->primaryKey(),
            'contractor_id' => $this->integer()->comment('Контрагент'),
            'pay_date' => $this->timestamp()->defaultValue(null)->comment('Дата'),
            'sum' => $this->double()->comment('Сумма'),
            'currency_id' => $this->integer()->comment('Валюта'),
            'exchange_rate' => $this->integer()->comment('Курс к рублю'),
            'created_at' => $this->timestamp()->defaultExpression('NOW()'),
        ]);

        $this->addCommentOnTable('{{%payment}}', 'Оплата');

        $this->addForeignKey(
            'fk-payment-contractor_id',
            '{{%payment}}',
            'contractor_id',
            '{{%contractor}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-payment-currency_id',
            '{{%payment}}',
            'currency_id',
            '{{%_currency}}',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%payment}}');
    }
}
