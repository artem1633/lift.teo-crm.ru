<?php

use yii\db\Migration;

/**
 * Class m210103_072613_change_type_date_column_to_balance_table
 */
class m210103_072613_change_type_date_column_to_balance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
       $this->alterColumn('{{%balance}}', 'date', $this->date()->comment('Дата на которую расчитываются остатки'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210103_072613_change_type_date_column_to_balance_table cannot be reverted.\n";

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210103_072613_change_type_date_column_to_balance_table cannot be reverted.\n";

        return false;
    }
    */
}
