<?php

use yii\db\Migration;

/**
 * Handles adding cost_price to table `{{%nomenclature}}`.
 */
class m190713_080814_add_cost_price_column_to_nomenclature_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%nomenclature}}', 'cost_price', $this->double(2)->comment('Себестоимость'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
