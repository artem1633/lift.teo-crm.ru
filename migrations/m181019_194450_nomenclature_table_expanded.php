<?php

use yii\db\Migration;

/**
 * Class m191019_194450_nomenclature_table_expanded
 */
class m181019_194450_nomenclature_table_expanded extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this -> addColumn(
            '{{nomenclature}}',
            'nomenclature_type',
            $this -> string(255) -> defaultValue(null)
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this -> dropColumn('{{nomenclature}}', 'nomenclature_type');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181019_174440_nomenclature cannot be reverted.\n";

        return false;
    }
    */
}
