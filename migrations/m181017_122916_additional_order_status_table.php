<?php

use yii\db\Migration;

/**
 * Class m181017_122916_additional_order_status
 */
class m181017_122916_additional_order_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('additional_order_status', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Наименование'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('additional_order_status');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181017_122916_additional_order_status cannot be reverted.\n";

        return false;
    }
    */
}
