<?php

use yii\db\Migration;

/**
 * Class m221206_093559_change_total_warehouse_column
 */
class m221206_093559_change_total_warehouse_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('detail_to_arriving', 'warehouse', $this->integer()->comment('Склад'));
        $this->addRelation('detail_to_arriving', 'warehouse');

        $this->addRelation('movement_part', 'warehouse_from');
        $this->addRelation('movement_part', 'warehouse_to');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

    /**
     * Установить связь
     * @param string $table
     * @param string $column
     */
    private function addRelation($table, $column)
    {
        $this->createIndex(
            "idx-{$table}-{$column}",
            "{$table}",
            "{$column}"
        );

        $this->addForeignKey(
            "fk-{$table}-{$column}",
            "{$table}",
            "{$column}",
            "warehouse",
            "id",
            "SET NULL"
        );
    }
}
