<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%detail_to_realization}}`.
 */
class m190923_061439_create_detail_to_realization_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%detail_to_realization}}', [
            'id' => $this->primaryKey(),
            'realization_id' => $this->integer()->comment('Реализация'),
            'detail_id' => $this->integer()->comment('Деталь'),
            'number' => $this->double(2)->comment('Количество'),
            'price' => $this->double(2)->comment('Цена за единицу товара'),
            'total_amount' => $this->double(2)->comment('Общая сумма (включая НДС)')
        ]);

        $this->addForeignKey(
            'fk-detail_to_realization-realization_id',
            'detail_to_realization',
            'realization_id',
            'realization',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-detail_to_realization-detail_id',
            'detail_to_realization',
            'detail_id',
            'nomenclature',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%detail_to_realization}}');
    }
}
