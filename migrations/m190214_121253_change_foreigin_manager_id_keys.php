<?php

use yii\db\Migration;

/**
 * Class m190214_121253_change_foreigin_manager_id_keys
 */
class m190214_121253_change_foreigin_manager_id_keys extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        //Таблица Действий
        $this->dropForeignKey('fk-actions-manager_id', 'actions');
        $this->addForeignKey(
            'fk-actions-manager_id',
            'actions',
            'manager_id',
            'users',
            'id',
            'SET NULL'
        );
        //Таблица Контрагентов
        $this->dropForeignKey('fk-contractor-manager_id', 'contractor');
        $this->addForeignKey(
            'fk-contractor-manager_id',
            'contractor',
            'manager_id',
            'users',
            'id',
            'SET NULL'
        );
         //Таблица Комментов
        $this->dropForeignKey('fk-comment-manager_id', 'comment');
        $this->addForeignKey(
            'fk-comment-manager_id',
            'comment',
            'manager_id',
            'users',
            'id',
            'SET NULL'
        );
        //Таблица Номенклатура
        $this->addForeignKey(
            'fk-nomenclature-created_by',
            'nomenclature',
            'created_by',
            'users',
            'id',
            'SET NULL'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
//        echo "m190214_121253_change_foreigin_manager_id_keys cannot be reverted.\n";

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190214_121253_change_foreigin_manager_id_keys cannot be reverted.\n";

        return false;
    }
    */
}
