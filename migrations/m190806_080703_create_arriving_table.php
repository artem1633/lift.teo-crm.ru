<?php

use yii\db\Expression;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%arriving}}`.
 */
class m190806_080703_create_arriving_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%arriving}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->timestamp()->defaultValue(new Expression('NOW()')),
            'contractor_id' => $this->integer()->comment('Контрагент'),
        ]);

        $this->addForeignKey(
            'fk-arriving-contractor_id',
            'arriving',
            'contractor_id',
            'contractor',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createTable('{{%detail_to_arriving}}', [
            'id' => $this->primaryKey(),
            'arriving_id' => $this->integer()->comment('Поступление'),
            'detail_id' => $this->integer()->comment('Деталь'),
            'number' => $this->double()->comment('Количество'),
            'price' => $this->double()->comment('Цена за штуку'),
            'nds' => $this->double()->comment('НДС %'),
            'total_amount' => $this->double(2)->comment('Общая сумма (включая НДС)')
        ]);

        $this->addForeignKey(
            'fk-detail_to_arriving-arriving_id',
            'detail_to_arriving',
            'arriving_id',
            'arriving',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-detail_to_arriving-detail_id',
            'detail_to_arriving',
            'detail_id',
            'nomenclature',
            'id',
            'SET NULL',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%arriving}}');
    }
}
