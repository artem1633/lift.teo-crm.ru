<?php

use yii\db\Migration;

/**
 * Class m181019_174440_request
 */
class m181019_122702_request_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('request', [
            'id' => $this->primaryKey(),
            'contractor_id' => $this->integer()->comment('Контрагент'),
            'description' => $this->text()->comment('Комментарии'),
            'main_status_id' => $this->integer()->comment('Статус основной'),
            'additional_status_id' => $this->integer()->comment('Статус дополнительный'),
            'additional_request_photos' => $this->text()->comment('Дополнительные изображения'),
            'created_date' => $this->string(255)->comment('Дата создания и время создания'),
            'updated_date' => $this->string(255)->comment('Дата и время редактирования'),
            'created_by' => $this->integer()->comment('Кто создал'),

        ]);
        //contractor handbooks for request
        $this->createIndex('idx-request-contractor_id', 'request', 'contractor_id', false);
        $this->addForeignKey("fk-request-contractor_id", "request", "contractor_id", "contractor", "id");
        //main_status handbooks for request
        $this->createIndex('idx-request-main_status_id', 'request', 'main_status_id', false);
        $this->addForeignKey("fk-request-main_status_id", "request", "main_status_id", "order_status", "id");
        //additional_status handbooks for request
        $this->createIndex('idx-request-additional_status_id', 'request', 'additional_status_id', false);
        $this->addForeignKey("fk-request-additional_status_id", "request", "additional_status_id", "additional_order_status", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('request');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181019_174440_nomenclature cannot be reverted.\n";

        return false;
    }
    */
}
