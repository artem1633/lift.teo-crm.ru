<?php
use yii\helpers\Url;
use app\models\Functions;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'request_id',
        'filter' => Functions::getRequestsList(),
        'value' => function($data){
            return Functions::getRequest($data -> request_id);
        },
        'format' => 'html'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'nomenclature_id',
        'filter' => Functions::getNomenclatureList(),
        'value' => function($data){
            return Functions::getNomenclature($data -> nomenclature_id);
        },
        'format' => 'html'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'detail_id',
        'filter' => Functions::getTechNomenclatureList(),
        'value' => function($data){
            return Functions::getTechNomenclature($data -> detail_id);
        },
        'format' => 'html'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'delivery_time_id',
        'filter' => Functions::getDeliveryTimeList(),
        'value' => function($data){
            return Functions::getDeliveryTime($data -> delivery_time_id);
        },
        'format' => 'html'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'suppliers_id',
        'filter' => Functions::getSuppliersList(),
        'value' => function($data){
            return Functions::getSuppliers($data -> suppliers_id);
        },
        'format' => 'html'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'in_china',
        'filter'=>array("0" => "Нет", "1" => "Да"),
        'value' => function($data){
            return $data -> in_china == 1 ? '<span style="padding-top: 1px; padding-bottom: 2px;" class="btn btn-sm btn-success color_shadow">Да</span>' : '<span style="padding-top: 1px; padding-bottom: 2px;" class="btn btn-sm btn-danger color_shadow">Нет</span>';
        },
        'format' => 'html'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'buy_price',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'sell_price',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'comment',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'quantity',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'measure_id',
        'filter' => Functions::getMeasureList(),
        'value' => function($data){
            return Functions::getMeasure($data -> measure_id);
        },
        'format' => 'html'
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'files_location',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created_date',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'updated_date',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created_by',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Редактировать', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Вы уверены?',
                          'data-confirm-message'=>'Вы уверены что хотите удалить все эти элементы?'],
    ],

];   