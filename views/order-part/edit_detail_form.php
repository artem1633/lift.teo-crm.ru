<?php

use app\models\Users;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Functions;

/* @var $this yii\web\View */
/* @var $model app\models\Request */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="update-detail">

    <div class="row">
        <div class="col-xs-12">
            <?php $form = ActiveForm::begin(); ?>

            <?php
            if ($td == 6) { //Столбец "Деталь" ?>
                <div class="row">
                    <div class="col-xs-10">
                        <div class="detail_id">
                            <?php

                            echo $form->field($model, 'detail')->label(false)->widget(Select2::classname(), [
                                'data' => Functions::getNomenclatureList(Users::getPermission()),
                                'options' => ['placeholder' => 'Номенклатура'],
                                'pluginOptions' => [
                                    'tags' => true,
                                    'allowClear' => true,
                                ],
                            ]); ?>
                        </div>
                    </div>
                    <div class="col-xs-2">
                        <?= Html::a('Добавить деталь', ['nomenclature/create', 'nomenclature_type' => Users::USER_ROLE_MANAGER], [
                            'class' => 'btn btn-default btn-block',
                            'role' => 'modal-remote',
                        ]) ?>
                    </div>

                </div>
            <?php } else if ($td == 2) { //Если поле "Запрос" ?>
                <div class="nomenclature_id">
                    <?php
                    echo $form->field($model, 'nomenclature')->label(false)->widget(Select2::classname(), [
                        'data' => Functions::getNomenclatureList(Users::getPermission()),
                        'options' => ['placeholder' => 'Номенклатура'],
                        'pluginOptions' => [
                            'tags' => true,
                            'allowClear' => true,
                        ],
                    ]); ?>
                </div>
            <?php } else if ($td == 7) { ?>
                <div class="delivery_time_id">
                    <?php

                    echo $form->field($model, 'delivery_time_id')->label(false)->widget(Select2::classname(), [
                        'data' => \app\models\Functions::getDeliveryTimeList(),
                        'options' => ['placeholder' => 'Срок доставки'],
                        'pluginOptions' => [
                            'tags' => true,
                            'allowClear' => true,
                        ],
                    ]); ?>
                </div>
            <?php } else if ($td == 8) { ?>

                <div class="suppliers_id">
                    <?php

                    echo $form->field($model, 'suppliers_id')->label(false)->widget(Select2::classname(), [
                        'data' => \app\models\Functions::getSuppliersList(),
                        'options' => ['placeholder' => 'Поставщик'],
                        'pluginOptions' => [
                            'tags' => true,
                            'allowClear' => true,
                        ],
                    ]); ?>
                </div>
            <?php } else if ($td == 9) { ?>

                <div class="in_china">
                    <?php

                    echo $form->field($model, 'in_china')->label(false)->widget(Select2::classname(), [
                        'data' => array("0" => "Нет", "1" => "Да"),
                        'options' => ['placeholder' => 'Есть в Китае'],
                        'pluginOptions' => [
                            'tags' => true,
                            'allowClear' => true,
                        ],
                    ]); ?>
                </div>
            <?php } ?>

            <?php if (!Yii::$app->request->isAjax) { ?>
                <div class="form-group">
                    <?= Html::submitButton('Редактировать', ['class' => 'btn btn-primary']) ?>
                </div>
            <?php } ?>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>


