<?php

use app\models\Functions;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrderPartSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '';
//$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

if (\app\models\Users::isManager()){
    $create_btn = '';
} else {
    $create_btn = Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
        ['role'=>'modal-remote','title'=> 'Создать новую деталь','class'=>'btn btn-default']);
}

?>
<div class="order-part-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => require(__DIR__.'/_request_columns.php'),
            'toolbar'=> [
                ['content'=>
                    $create_btn .
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Сбросить сетку']).
                    '{toggleData}'
                ],
            ],          
            'striped' => true,
            'condensed' => true,
            'responsive' => true,          
            'panel' => [
                'type' => 'primary', 
                'heading' => '<i class="glyphicon glyphicon-list"></i> Детали',
//                'before'=>'<em>* Resize table columns just like a spreadsheet by dragging the column edges.</em>',
                'after'=>Functions::getBulkButtonWidget().
                        '<div class="clearfix"></div>',
            ]
        ])?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
//    'htmlOptions' => ['class' => 'order_part_modal'],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
