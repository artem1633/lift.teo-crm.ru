<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\OrderPart */

?>
<div class="order-part-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
