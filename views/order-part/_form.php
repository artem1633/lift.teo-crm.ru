<?php

use app\models\Users;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Functions;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\OrderPart */
/* @var $form yii\widgets\ActiveForm */
if ($model->isNewRecord) {
    $action_form = '/order-part/create';
} else {
    $action_form = Url::toRoute(['/order-part/update', 'id' => $model->id]) ;
}
?>
    <div class="order-part-form">
        <?php $form = ActiveForm::begin([
            'action' => $action_form,
        ]); ?>

        <?php
        $check = Functions::checkManager();
        if ($check == "+" || $model->isNewRecord) { ?>
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-10">
                        <?= $form->field($model, 'nomenclature_id')->widget(Select2::classname(), [
                            'name' => 'nomenclature_id',
                            'data' => Functions::getNomenclatureList(Users::USER_ROLE_MANAGER),
                            'options' => ['placeholder' => 'Запрос'],
//                            'search' => true,
                            'pluginOptions' => [
                                'allowClear' => true,
                                'tags' => true,
                            ],
//                            'pluginEvents' => [
//                                    'change' => "function(){
//                                        var elem = $(this);
//                                        var val = elem.val();
//                                        var help_block = elem.siblings('.help-block');
//                                       $.get('/order-part/get-remains', {id:val})
//                                            .done(function(res){
//                                                if (res.success){
//                                                    console.log(res);
//                                                    help_block.html('Фактический остаток: ' + res.data);
//                                                } else {
//                                                    help.block.html(res.error);
//                                                }
//                                            })
//                                            .fail(function(){
//                                                 help.block.html('Ошибка сервера');
//                                            })
//                                    }"
//                            ]
                        ])->label(false);
                        ?>
                    </div>
                    <div class="col-xs-2">
                        <?= Html::a('Добавить деталь', ['/nomenclature/create', 'nomenclature_type' => Users::USER_ROLE_MANAGER, 'from_order_part' => true], [
                            'class' => 'btn btn-default btn-block',
                            'role' => 'modal-remote',
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <?php
                if ($model->isNewRecord) {

                    echo $form->field($model, 'quantity')->textInput(['maxlength' => true, 'placeholder' => "Количество"])->label(false);

                }
                else {

                    if (!empty($model->quantity)) {

                        $only_quantity = explode(', ', $model->quantity);
                        $model->quantity = $only_quantity[0];

                    }

                    echo $form->field($model, 'quantity')->textInput(['maxlength' => true, 'placeholder' => "Количество"])->label(false);
                }
                ?>
            </div>
            <div class="col-xs-6">
                <?php

                echo $form->field($model, 'measure_id')->widget(Select2::classname(), [
                    'name' => 'status',
                    'hideSearch' => true,
                    'data' => Functions::getMeasureList(),
                    'options' => ['placeholder' => 'Мера'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label(false);
                ?>
            </div>
            <?php if (!Users::isManager()) { ?>
                <div class="col-xs-6">
                    <?= $form->field($model, 'delivery_time_id')->label()->widget(Select2::classname(), [
                        'data' => Functions::getDeliveryTimeList(),
                        'options' => ['placeholder' => 'Срок доставки'],
                        'pluginOptions' => [
                            'tags' => true,
                            'allowClear' => true,
                        ],
                    ])->label(false); ?>
                </div>
                <div class="col-xs-6">
                    <?php

                    echo $form->field($model, 'suppliers_id')->label()->widget(Select2::classname(), [
                        'data' => \app\models\Functions::getSuppliersList(),
                        'options' => ['placeholder' => 'Поставщик'],
                        'pluginOptions' => [
                            'tags' => true,
                            'allowClear' => true,
                        ],
                    ])->label(false);
                    ?>
                </div>
                <div class="col-xs-6">
                    <?= $form->field($model, 'buy_price')->textInput(['maxlength' => true, 'placeholder' => "Цена покупки"])->label(false); ?>
                </div>
                <div class="col-xs-6">
                    <?= $form->field($model, 'buy_currency')->widget(Select2::classname(), [
                        'data' => Functions::getCurrency(),
                        'options' => ['placeholder' => 'Валюта покупки'],
                        'pluginOptions' => [
                            'tags' => true,
                            'allowClear' => true,
                        ],
                    ])->label(false);
                    ?>
                </div>
                <div class="col-xs-6">
                    <?= $form->field($model, 'sell_price')->textInput(['maxlength' => true, 'placeholder' => "Цена продажи"])->label(false); ?>
                </div>
                <div class="col-xs-6">
                    <?= $form->field($model, 'sell_currency')->widget(Select2::classname(), [
                        'data' => Functions::getCurrency(),
                        'options' => ['placeholder' => 'Валюта продажи'],
                        'pluginOptions' => [
                            'tags' => true,
                            'allowClear' => true,
                        ],
                    ])->label(false); ?>
                </div>
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-10">
                            <?php
                            echo $form->field($model, 'detail_id')->widget(Select2::classname(), [
                                'name' => 'detail_id',
//                                'hideSearch' => true,
                                'data' => Functions::getNomenclatureList(Users::USER_ROLE_TECH_SPECIALIST),
                                'options' => ['placeholder' => 'Деталь'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'tags' => true,
                                ],
                            ])->label(false);
                            ?>
                        </div>
                        <div class="col-xs-2">
                            <?= Html::a('Добавить деталь', ['nomenclature/create', 'nomenclature_type' => Users::USER_ROLE_TECH_SPECIALIST], [
                                'class' => 'btn btn-default btn-block',
                                'role' => 'modal-remote',
                            ]) ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="col-xs-12">
                <?= $form->field($model, 'comment')->textarea(['rows' => 6, 'placeholder' => 'Комментарий'])->label(false) ?>
            </div>
            <div style="display: none;">
                <?php
                $files_location_hidden = json_decode($model->files_location, true);
                if (!empty($files_location_hidden)) {
                    $files_location_hidden = implode(", ", $files_location_hidden);
                } else {
                    $files_location_hidden = "";
                }
                ?>
                <input id="files_location_hidden" name="files_location_hidden" type="hidden"
                       value="<?= $files_location_hidden ?>">
                <input id="model_id" name="model_id" type="hidden" value="<?= $model->id ?>">
                <input id="updateRecord" name="updateRecord" type="hidden" value="<?= $model->isNewRecord ? 0 : 1 ?>">
            </div>
            <?php
            if ($model->isNewRecord) { ?>
                <div class="col-xs-12">
                    <div class="col-xs-10">
                        <?php
                        if (!Users::isManager()) {
                            echo $form->field($model, 'in_china')->checkbox(['checked' => true]);
                        }
                        ?>
                    </div>
                    <div class="col-xs-2">

                        <?= $form->field($model, 'files[]')->label(false)->widget(FileInput::classname(), [
                            'pluginOptions' => [
                                'showCaption' => false,
                                'showRemove' => false,
                                'showPreview' => false,
                                'showUpload' => false,
                                'browseClass' => 'btn btn-primary btn-block',
                                'browseIcon' => '<i class="fa fa-image"></i> ',
                                'browseLabel' => ''
                            ],
                            'options' => ['multiple' => true]
                        ]); ?>
                    </div>
                </div>
            <?php } else { ?>
                <div class="col-xs-12">
                    <?php if (!empty($files_location_hidden)) {

                        $images = Functions::additionalPhotosHtmlEdit($model->files_location);

                        echo $form->field($model, 'files[]')->widget(FileInput::classname(), [
                            'name' => 'attachment_48[]',
                            'options' => [
                                'multiple' => true
                            ],
                            'pluginOptions' => [
                                'initialPreviewShowDelete' => true,
                                'allowedFileExtensions' => ['jpg', 'jpeg', 'png'],
                                'overwriteInitial' => false,
                                'showPreview' => true,
                                'showCaption' => true,
                                'showRemove' => false,
                                'showUpload' => false,
                                'initialPreview' => $images,
                                'initialPreviewConfig' => [
                                ],
                            ],
                        ])->label(false);
                    } else {

                        echo $form->field($model, 'files[]')->widget(FileInput::classname(), [
                            'name' => 'attachment_48[]',
                            'options' => [
                                'multiple' => true,
                            ],
                            'pluginOptions' => [
                                'allowedFileExtensions' => ['jpg', 'jpeg', 'png'],
                                'showUpload' => false,
                                'browseLabel' => '',
                                'removeLabel' => '',
                                'mainClass' => 'input-group-lg'
                            ]
                        ])->label(false);
                    } ?>
                </div>
            <?php } ?>
        <?php } else { ?>

            <div class="col-xs-12">
                <?php

                echo $form->field($model, 'nomenclature_id')->widget(Select2::classname(), [
                    'name' => 'nomenclature_id',
                    'hideSearch' => true,
                    'data' => Functions::getNomenclatureList(),
                    'options' => ['placeholder' => 'Запрос'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label(false);
                ?>
            </div>

            <div class="col-xs-6">
                <?php
                if ($model->isNewRecord) {

                    $form->field($model, 'quantity')->textInput(['maxlength' => true, 'placeholder' => "Количество"])->label(false);

                } else {

                    if (!empty($model->quantity)) {

                        $only_quantity = explode(', ', $model->quantity);
                        $model->quantity = $only_quantity[0];

                    }

                    echo $form->field($model, 'quantity')->textInput(['maxlength' => true, 'placeholder' => "Количество"])->label(false);
                }
                ?>
            </div>
            <div class="col-xs-6">
                <?php

                echo $form->field($model, 'measure_id')->widget(Select2::classname(), [
                    'name' => 'status',
                    'hideSearch' => true,
                    'data' => Functions::getMeasureList(),
                    'options' => ['placeholder' => 'Мера'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label(false);
                ?>
            </div>

            <div class="col-xs-12">
                <?= $form->field($model, 'delivery_time_id')->label()->widget(Select2::classname(), [
                    'data' => \app\models\Functions::getDeliveryTimeList(),
                    'options' => ['placeholder' => 'Срок доставки'],
                    'pluginOptions' => [
                        'tags' => true,
                        'allowClear' => true,
                    ],
                ])->label(false); ?>
            </div>

            <?php if ($model->isNewRecord) { ?>

                <div class="col-xs-6">
                    <?= $form->field($model, 'buy_price')->textInput(['maxlength' => true, 'placeholder' => "Цена покупки"])->label(false); ?>
                </div>

                <div class="col-xs-6">
                    <?= $form->field($model, 'buy_price_currency')->widget(Select2::classname(), [
                        'data' => \app\models\Functions::getCurrency(),
                        'options' => ['placeholder' => 'Валюта покупки'],
                        'pluginOptions' => [
                            'tags' => true,
                            'allowClear' => true,
                        ],
                    ])->label(false);
                    ?>
                </div>
            <?php } ?>

            <div class="col-xs-6">
                <?php
                if ($model->isNewRecord) {
                    echo $form->field($model, 'sell_price')->textInput(['maxlength' => true, 'placeholder' => "Цена продажи"])->label(false);
                } else {

                    if (!empty($model->sell_price)) {

                        $only_sell_price = explode(', ', $model->sell_price);
                        $model->sell_price = $only_sell_price[0];

                    }

                    echo $form->field($model, 'sell_price')->textInput(['maxlength' => true, 'placeholder' => "Цена продажи"])->label(false);
                }
                ?>
            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'sell_currency')->widget(Select2::classname(), [
                    'data' => \app\models\Functions::getCurrency(),
                    'options' => ['placeholder' => 'Валюта продажи'],
                    'pluginOptions' => [
                        'tags' => true,
                        'allowClear' => true,
                    ],
                ])->label(false);
                ?>
            </div>
            <div class="col-xs-12">
                <?php
                if ($model->isNewRecord) {

                    echo $form->field($model, 'detail_id')->widget(Select2::classname(), [
                        'name' => 'detail_id',
                        'hideSearch' => true,
                        'data' => Functions::getNomenclatureList(),
                        'options' => ['placeholder' => 'Деталь'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(false);

                } else {

                    echo $form->field($model, 'detail_id')->widget(Select2::classname(), [
                        'name' => 'detail_id',
                        'hideSearch' => true,
                        'data' => Functions::getNomenclatureList(),
                        'options' => ['placeholder' => 'Деталь'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(false);
                }
                ?>
            </div>

            <div class="col-xs-12">
                <?= $form->field($model, 'comment')->textarea(['rows' => 6, 'placeholder' => 'Комментарий'])->label(false) ?>
            </div>
            <div style="display: none;">
                <?php
                $files_location_hidden = json_decode($model->files_location, true);
                if (!empty($files_location_hidden)) {
                    $files_location_hidden = implode(", ", $files_location_hidden);
                } else {
                    $files_location_hidden = "";
                }
                ?>
                <input id="files_location_hidden" name="files_location_hidden" type="hidden"
                       value="<?= $files_location_hidden ?>">
                <input id="model_id" name="model_id" type="hidden" value="<?= $model->id ?>">
                <input id="updateRecord" name="updateRecord" type="hidden" value="<?= $model->isNewRecord ? 0 : 1 ?>">
            </div>
            <?php
            if ($model->isNewRecord) { ?>

                <div class="col-xs-12">
                    <div class="col-xs-10">
                        <?= $form->field($model, 'in_china')->checkbox() ?>
                    </div>
                    <div class="col-xs-2">

                        <?= $form->field($model, 'files[]')->label(false)->widget(FileInput::classname(), [
                            'pluginOptions' => [
                                'showCaption' => false,
                                'showRemove' => false,
                                'showPreview' => false,
                                'showUpload' => false,
                                'browseClass' => 'btn btn-primary btn-block',
                                'browseIcon' => '<i class="fa fa-image"></i> ',
                                'browseLabel' => ''
                            ],
                            'options' => ['multiple' => true]
                        ]); ?>
                    </div>
                </div>

            <?php } else { ?>

                <div class="col-xs-12">
                    <?php if (!empty($files_location_hidden)) {

                        $images = \app\models\Functions::additionalPhotosHtmlEdit($model->files_location);

                        echo $form->field($model, 'files[]')->widget(FileInput::classname(), [
                            'name' => 'attachment_48[]',
                            'options' => [
                                'multiple' => true
                            ],
                            'pluginOptions' => [
                                'initialPreviewShowDelete' => true,
                                'allowedFileExtensions' => ['jpg', 'jpeg', 'png'],
                                'overwriteInitial' => false,
                                'showPreview' => true,
                                'showCaption' => true,
                                'showRemove' => false,
                                'showUpload' => false,
                                'initialPreview' => $images,
                                'initialPreviewConfig' => [
                                ],
                            ],
                        ])->label(false);
                    } else {

                        echo $form->field($model, 'files[]')->widget(FileInput::classname(), [
                            'name' => 'attachment_48[]',
                            'options' => [
                                'multiple' => true,
                            ],
                            'pluginOptions' => [
                                'allowedFileExtensions' => ['jpg', 'jpeg', 'png'],
                                'showUpload' => false,
                                'browseLabel' => '',
                                'removeLabel' => '',
                                'mainClass' => 'input-group-lg'
                            ]
                        ])->label(false);
                    } ?>
                </div>

            <?php } ?>
        <?php } ?>

        <?php if (!Yii::$app->request->isAjax) { ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>

    </div>

<?php
$script = <<< JS
$( document ).ready(function() {
    
    $("a.gallery").fancybox();
    
    var update_record = $('#updateRecord').val();
         console.log('update_record');
         console.log(update_record);
    if (update_record == 1) {
        
         var deleted_item = '';
         
         
         var files_location = $('#files_location_hidden').val();
         console.log('files_location');
         console.log(files_location);
         var model_id = $('#model_id').val();
        
         $(".kv-file-remove.btn.btn-sm.btn-kv.btn-default.btn-outline-secondary").on('click', function () {
            console.log('v-file-remove clicked');
            var parent = $(this).closest('.file-preview-frame.krajee-default.file-preview-initial');
      
            deleted_item = parent.find('.file-preview-image').attr('src');
            console.log('deleted_item');
            console.log(deleted_item);
            //
            parent.addClass('hidden');
            
             $.post(
                '/order-part/additional-delete',
                {
                    // deleted_item_main: deleted_item_main,
                    deleted_item: deleted_item,
                    files_location: files_location, 
                    model_id: model_id
                },
                function(data) {
                    console.log('data');
                    console.log(data);
                }
            ).fail(function(data) {
                    console.log('error data');
                    console.log(data);
            });
    });   
    }
});

JS;
$this->registerJs($script);
?>