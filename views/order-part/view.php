<?php

use yii\widgets\DetailView;
use app\models\Functions;

/* @var $this yii\web\View */
/* @var $model app\models\OrderPart */
?>
<div class="order-part-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
               'attribute' => 'request_id',
               'format' => 'html',
               'value' => function($data) {
                   return Functions::getRequest($data -> request_id);
               }
            ],
            [
                'attribute' => 'nomenclature_id',
                'format' => 'html',
                'value' => function($data) {
                    return Functions::getNomenclatureWithImageNoEdit($data -> nomenclature_id);
                }
            ],
            'comment:ntext',
            [
                'attribute' => 'detail_id',
                'format' => 'html',
                'value' => function($data) {
                    return Functions::getNomenclatureWithImageNoEdit($data -> detail_id);
                }
            ],
            [
                'attribute' => 'in_china',
                'format' => 'html',
                'value' => function($data) {
                    $check = Functions::checkManager();
                    if ($check == '-') {
                        return '-';
                    } else

//                    return Functions::getInChina($data -> in_china);
                    return $data -> in_china == 1 ?
                        '<span style="padding-top: 1px; padding-bottom: 2px;" class="btn btn-sm btn-success color_shadow">Да</span>'
                        : '<span style="padding-top: 1px; padding-bottom: 2px;" class="btn btn-sm btn-danger color_shadow">Нет</span>';
                }
            ],
            'delivery_time_id:datetime',
            [
                'attribute' => 'suppliers_id',
                'format' => 'html',
                'value' => function($data) {
                    return Functions::getSuppliersNoEdit($data -> suppliers_id);
                }
            ],
            'buy_price',
            'sell_price',
            [
                'attribute' => 'sell_price',
                'format' => 'html',
                'value' => function($data) {
                    $check = Functions::checkManager();
                    if ($check == '-') {
                        return '-';
                    } else
                    return $data -> sell_price;
                }
            ],
            'quantity',
//            [
//                'attribute' => 'measure_id',
//                'format' => 'html',
//                'value' => function($data) {
//                    return Functions::getMeasure($data -> measure_id);
//                }
//            ],
            [
                'attribute' => 'files_location',
                'format' => 'html',
                'value' => function($data) {

                    return Functions::photosHtml($data -> files_location);
                }
            ],

            'created_date',
            'updated_date',
            [
                'attribute' => 'created_by',
                'format' => 'html',
                'value' => function($data) {
                    return Functions::getCreatedBy($data -> created_by);
                }
            ],
        ],
    ]) ?>

</div>

<?php
$script = <<< JS
    $(document).ready(function() {
        
        var fancy_box = $('.fancy_box');
        for (var i = 0; i < fancy_box.length; i++) {
            $(fancy_box[i]).attr('data-fancybox', 'gallery');
        }
        $("a.gallery").fancybox();
        
    });
JS;
$this->registerJs($script);
?>