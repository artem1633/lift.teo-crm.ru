<?php

use app\models\Currency;
use app\models\Nomenclature;
use app\models\Users;
use yii\helpers\Url;
use app\models\Functions;

if (Users::isManager()) {
    $template = '{view}';
} else {
    $template = '{view} {update} {delete}';
}
return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
//     [
//     'class'=>'\kartik\grid\DataColumn',
//     'attribute'=>'id',
//     ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
////        'attribute' => 'detail_id',
//        'attribute' => 'detailName',
//        'label' => 'Деталь 2',
//        'value' => function($data){
//            return Functions::getTechNomenclature($data -> detail_id);
//        },
//    ],
    [
        'class' => '\kartik\grid\DataColumn',
//        'attribute'=>'nomenclature_id',
        'attribute' => 'nomenclatureName',
        'label' => 'Запрос',
//        'filter' => Functions::getNomenclatureList(Users::USER_ROLE_MANAGER),
        'value' => function ($data) {
            return Functions::getNomenclature($data->nomenclature_id);
        },
        'vAlign' => 'middle',
        'format' => 'html'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'detailName',
        'label' => 'Деталь',
//        'filter' => Functions::getNomenclatureList(Users::USER_ROLE_TECH_SPECIALIST),
        'value' => function ($data) {
            return Functions::getTechNomenclature($data->detail_id);
        },
        'vAlign' => 'middle',
        'format' => 'html'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'ownVendorCode',
        'label' => 'Собст. арт.',
        'value' => function ($data) {
            return Nomenclature::getOwnerVendorCodeByDetailId($data->detail_id);
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'created_date',
        'vAlign' => 'middle',
        'format' => 'datetime'
    ],
    [
        'class' => 'kartik\grid\EditableColumn',
        'attribute' => 'buy_price',
        'value' => function ($data) {
            if ($data->buy_price) {
                $icon = Currency::findOne($data->buy_currency)->icon;
                return $data->buy_price . ' <i class = "fa ' . $icon . '"></i>';
            }
        },
        'editableOptions' => [
            'formOptions' => ['action' => ['/request/editable']],
            'afterInput' => function ($form, $widget) {
                echo $form->field($widget->model, 'buy_currency')->dropDownList(Functions::getCurrency())->label(false);
            },
            'editableValueOptions' => [
                'disabled' => Users::isManager(),
            ],
        ],
        'format' => 'raw',
        'vAlign' => 'middle',
    ],
    [
        'class' => 'kartik\grid\EditableColumn',
        'attribute' => 'sell_price',
        'value' => function ($data) {
            if ($data->sell_price) {
                return $data->sell_price . ' <i class="fa ' . Currency::findOne($data->sell_currency)->icon . '"></i>';
            }
        },
        'editableOptions' => [
            'formOptions' => ['action' => ['/request/editable']],
            'afterInput' => function ($form, $widget) {
                echo $form->field($widget->model, 'sell_currency')->dropDownList(Functions::getCurrency())->label(false);
            },
            'editableValueOptions' => [
                'disabled' => Users::isManager(),
            ],
        ],
        'vAlign' => 'middle',
        'format' => 'raw'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'request_id',
//        'filter' => Functions::getRequestsList(),
        'value' => function ($data) {
            return Functions::getRequest($data->request_id);
        },
        'vAlign' => 'middle',
        'format' => 'html'
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'template' => $template,
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'Просмотр', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Редактировать', 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Удалить',
            'data-confirm' => false, 'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Вы уверены?',
            'data-confirm-message' => 'Вы уверены что хотите удалить все эти элементы?'],
    ],

];   