<?php

use app\models\Users;
use johnitvn\ajaxcrud\CrudAsset;

CrudAsset::register($this);

?>
<aside class="main-sidebar">

    <section class="sidebar">

        <?php

        if (Users::isManager()) {
            $items = [
                ['label' => 'Menu', 'options' => ['class' => 'header']],
                ['label' => 'Контрагенты', 'icon' => 'cubes', 'url' => ['/contractor']],
                ['label' => 'Заявки', 'icon' => 'files-o', 'url' => ['/request']],
                ['label' => 'События', 'icon' => 'history', 'url' => ['/actions']],
                [
                    'label' => 'Номенклатура',
                    'icon' => 'book',
                    'url' => '#',
                    'items' => [
                        ['label' => 'Для менеджера', 'icon' => 'user', 'url' => ['/nomenclature/manager-nomenclature']],
                        ['label' => 'Для специалиста', 'icon' => 'user', 'url' => ['/nomenclature/tech-nomenclature']],
                    ],
                ],
                ['label' => 'Детали по заявкам', 'icon' => 'cogs', 'url' => ['/order-part/request-details']],
                [
                    'label' => 'Складской учет',
                    'icon' => 'server',
                    'url' => ['#'],
                    'items' => [
                        [
                            'label' => 'Импорт реализации',
                            'url' => ['/realization/import'],
                            'template' => '<a href="{url}" role="modal-remote"><i class="fa fa-sign-in"></i> {label}</a>'
                        ],
                        [
                            'label' => 'Экспорт заявок',
                            'url' => ['/accounting/export'],
                            'template' => '<a href="{url}" role="modal-remote"><i class="fa fa-sign-out"></i> {label}</a>'
                        ],
                        ['label' => 'Реализации', 'icon' => 'cube', 'url' => ['/realization']],
                        ['label' => 'Поступления', 'icon' => 'level-down', 'url' => ['/arriving']],
                        ['label' => 'Оплата', 'icon' => 'level-up', 'url' => ['/payment']],
                        ['label' => 'Отчет', 'icon' => 'pie-chart', 'url' => ['/accounting/report-new']],
                    ],
                ],
                ['label' => 'Логистика', 'icon' => 'map-signs', 'url' => ['/logistic-request']],
            ];
        } elseif (Users::isTech()) {
            $items = [
                ['label' => 'Menu', 'options' => ['class' => 'header']],
                ['label' => 'Заявки', 'icon' => 'files-o', 'url' => ['/request']],
                ['label' => 'События', 'icon' => 'history', 'url' => ['/actions']],
                [
                    'label' => 'Номенклатура',
                    'icon' => 'book',
                    'url' => '#',
                    'items' => [
                        ['label' => 'Для специалиста', 'icon' => 'user', 'url' => ['/nomenclature/tech-nomenclature']],
                    ],
                ],
                ['label' => 'Детали по заявкам', 'icon' => 'cogs', 'url' => ['/order-part/request-details']],
                [
                    'label' => 'Складской учет',
                    'icon' => 'server',
                    'url' => ['#'],
                    'items' => [
                        [
                            'label' => 'Импорт реализации',
                            'url' => ['/accounting/import'],
                            'template' => '<a href="{url}" role="modal-remote"><i class="fa fa-sign-in"></i> {label}</a>'
                        ],
                        [
                            'label' => 'Экспорт заявок',
                            'url' => ['/accounting/export'],
                            'template' => '<a href="{url}" role="modal-remote"><i class="fa fa-sign-out"></i> {label}</a>'
                        ],
                        ['label' => 'Реализации', 'icon' => 'cube', 'url' => ['/realization']],
                        ['label' => 'Поступления', 'icon' => 'level-down', 'url' => ['/arriving']],
                        ['label' => 'Отчет', 'icon' => 'pie-chart', 'url' => ['/accounting/report-new']],
                    ],
                ],
                ['label' => 'Логистика', 'icon' => 'map-signs', 'url' => ['/logistic-request']],
            ];
        } elseif (Users::isAdmin() || Users::isUser() || Users::isLogist()) {
            $items = [
                ['label' => 'Menu', 'options' => ['class' => 'header']],
                ['label' => 'Контрагенты', 'icon' => 'cubes', 'url' => ['/contractor']],
                ['label' => 'Заявки', 'icon' => 'files-o', 'url' => ['/request']],
                ['label' => 'События', 'icon' => 'history', 'url' => ['/actions']],
                [
                    'label' => 'Номенклатура',
                    'icon' => 'book',
                    'url' => '#',
                    'items' => [
                        ['label' => 'Для менеджера', 'icon' => 'user', 'url' => ['/nomenclature/manager-nomenclature']],
                        ['label' => 'Для специалиста', 'icon' => 'user', 'url' => ['/nomenclature/tech-nomenclature']],
                    ],
                ],
                [
                    'label' => 'Справочники',
                    'icon' => 'list-alt',
                    'url' => '#',
                    'items' => [
                        ['label' => 'Сотрудники', 'icon' => 'user', 'url' => ['/users']],
                        ['label' => 'Поставщики', 'icon' => 'bus', 'url' => ['/suppliers']],
                        ['label' => 'Города', 'icon' => 'building', 'url' => ['/cities']],
                        ['label' => 'Группы номенклатур', 'icon' => 'id-card', 'url' => ['/nomenclature-group']],
                        ['label' => 'Бренды', 'icon' => 'navicon', 'url' => ['/brand']],
                        ['label' => 'Статусы заказов', 'icon' => 'spinner', 'url' => ['/order-status']],
                        [
                            'label' => 'Статус Доп Заказы',
                            'icon' => 'circle-o-notch',
                            'url' => ['/additional-order-status']
                        ],
                        ['label' => 'Срок поставки', 'icon' => 'calendar-o', 'url' => ['/delivery-time']],
                        ['label' => 'Валюта', 'icon' => 'dollar', 'url' => ['/currency']],
                        ['label' => 'Тип механизма', 'icon' => 'gear', 'url' => ['/type-of-mechanism']],
                        ['label' => 'Тип запчасти', 'icon' => 'gear', 'url' => ['/type-of-parts']],
                        ['label' => 'Единица Измерения', 'icon' => 'gear', 'url' => ['/measure']],
                        ['label' => 'Склады', 'icon' => 'inbox', 'url' => ['/warehouse']],
                        ['label' => 'Логистика', 'icon' => 'map-signs', 'url' => '#', 'items' => [
                            ['label' => 'Получ. платежей', 'icon' => 'user', 'url' => ['/payee']],
                            ['label' => 'Осн. статусы', 'icon' => 'map-signs', 'url' => ['/logistic-master-status']],
                            ['label' => 'Статусы логиста', 'icon' => 'map-signs', 'url' => ['/logistic-status']],
                        ]],
                        [
                            'label' => 'Логи',
                            'icon' => 'gear',
                            'url' => '#',
                            'items' => [
                                [
                                    'label' => 'Приложение',
                                    'icon' => 'file',
                                    'url' => ['/site/get-log', 'name' => 'app']
                                ],
                                ['label' => 'Ошибки', 'icon' => 'file', 'url' => ['/site/get-log', 'name' => '_error']],
                                ['label' => 'Тест', 'icon' => 'file', 'url' => ['/site/get-log', 'name' => 'test']],
                            ]
                        ],
                    ],
                ],
                ['label' => 'Детали по заявкам', 'icon' => 'cogs', 'url' => ['/order-part/request-details']],
                ['label' => 'АПИ', 'icon' => 'sign-in', 'url' => ['/api']],
                [
                    'label' => 'Складской учет',
                    'icon' => 'server',
                    'url' => ['#'],
                    'items' => [
                        [
                            'label' => 'Импорт реализации',
                            'url' => ['/realization/import'],
                            'template' => '<a href="{url}" role="modal-remote"><i class="fa fa-sign-in"></i> {label}</a>'
                        ],
                        [
                            'label' => 'Экспорт заявок',
                            'url' => ['/accounting/export'],
                            'template' => '<a href="{url}" role="modal-remote"><i class="fa fa-sign-out"></i> {label}</a>'
                        ],
                        ['label' => 'Реализации', 'icon' => 'cube', 'url' => ['/realization']],
                        ['label' => 'Поступления', 'icon' => 'level-down', 'url' => ['/arriving']],
                        ['label' => 'Оплата', 'icon' => 'level-up', 'url' => ['/payment']],
                        ['label' => 'Отчет', 'icon' => 'pie-chart', 'url' => ['/accounting/report-new']],
                    ],
                ],
                ['label' => 'Логистика', 'icon' => 'map-signs', 'url' => ['/logistic-request']],
                ['label' => 'Юр. лица', 'icon' => 'street-view', 'url' => ['/juridical-person']],

            ];
        }

        if (isset(Yii::$app->user->id)) {
            try {
                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                        'items' => $items,
                    ]
                );
            } catch (Exception $e) {
                Yii::error($e->getMessage(), '_error');
            }
        }
        ?>

    </section>

</aside>
