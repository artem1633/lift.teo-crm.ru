<?php

use app\models\Users;
use app\modules\drive\models\Auth;
use yii\helpers\Html;
use yii\bootstrap\Modal;

?>

    <header class="main-header">

        <?= Html::a('<span class="logo-mini">LIFT</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

        <nav class="navbar navbar-static-top" role="navigation">

            <a href="#" onclick="$.post('/site/menu-position');" class="sidebar-toggle" data-toggle="push-menu"
               role="button"><span class=""></span> </a>
            <div class="navbar-custom-menu">
                <?php if (!(new Auth)->checkAccessToken() && Users::isAdmin()): ?>
                <div class="google-btn"><?= Html::a('Необходима выдача прав для Google Drive', ['/drive'], ['class' => 'btn btn-warning']) ?></div>
                <?php endif; ?>
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="hidden-xs">
                            <?php
                            if (!empty(Yii::$app->user->identity->fio)) {
                                echo Yii::$app->user->identity->fio;
                            }
                            ?>
                        </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-header">
                                <p> <?php
                                    if (!empty(Yii::$app->user->identity->fio)) {
                                        echo Yii::$app->user->identity->fio;
                                    }
                                    ?> </p>
                            </li>
                            <?php
                            if (!empty(Yii::$app->user->identity->id)) { ?>
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <?= Html::a('Изменить пароль', ['users/change', 'id' => Yii::$app->user->identity->id],
                                            ['role' => 'modal-remote', 'title' => 'Изменить пароль', 'class' => 'btn btn-default btn-flat']); ?>
                                    </div>
                                    <div class="pull-right">
                                        <?= Html::a(
                                            'Выход',
                                            ['/site/logout'],
                                            ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                        ) ?>
                                    </div>
                                </li>
                            <?php }
                            ?>
                        </ul>
                    </li>

                </ul>
            </div>
        </nav>
    </header>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "size" => "modal-lg",
    "options" => [
        "tabindex" => false,
    ],
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>