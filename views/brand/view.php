<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Brand */
?>
<div class="brand-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
