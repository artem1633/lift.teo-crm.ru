<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Payee */
?>
<div class="payee-view">

    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'name',
                'full_name',
                [
                    'attribute' => 'city_id',
                    'value' => $model->city->name ?? null
                ],
                'address',
                'phone',
                'contact_person',
                'email:email',
                'inn',
                'requisites:ntext',
                'comment:ntext',
                'created_date:datetime',
                'updated_date:datetime',
            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), '_error');
        echo $e->getMessage();
    } ?>

</div>
