<?php

/* @var $this yii\web\View */
/* @var $model app\models\Payee */

?>
<div class="payee-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
