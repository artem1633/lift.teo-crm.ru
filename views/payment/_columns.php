<?php

use app\models\Payment;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'contractor_name',
        'label' => 'Контрагент',
        'content' => function (Payment $model) {
            $full_name = $model->contractor->full_name ?? null;
            if ($full_name) {
                return $full_name;
            }
            return $model->contractor->name;
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'pay_date',
        'filter' => Html::input('date', 'PaymentSearch[pay_date]', '', [
            'class' => 'form-control'
        ]),
        'format' => 'date',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'sum',
        'content' => function (Payment $model) {
            $cur = null;
            if ($model->currency) {
                $cur = '&nbsp;<span class="fa ' . $model->currency->icon . '"></span>';
            }
            return $model->sum . $cur;
        }
    ],
//    [
//        'class' => '\kartik\grid\DataColumn',
//        'attribute' => 'currency_id',
//        'content' => function (Payment $model) {
//            if ($model->currency) {
//                return $model->currency->name;
//            }
//            return null;
//        }
//    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'created_at',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'View', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Update', 'data-toggle' => 'tooltip'],
        'deleteOptions' => [
            'role' => 'modal-remote',
            'title' => 'Delete',
            'data-confirm' => false,
            'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Are you sure?',
            'data-confirm-message' => 'Are you sure want to delete this item'
        ],
    ],

];   