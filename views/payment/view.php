<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Payment */
?>
<div class="payment-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            [
                'attribute' => 'contractor_id',
                'value' => $model->contractor->name,
            ],
            'pay_date:date',
            'sum',
            [
                'attribute' => 'currency_id',
                'value' => $model->currency->name,
            ],
        ],
    ]) ?>

</div>
