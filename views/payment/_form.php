<?php

use app\models\Currency;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use app\models\Contractor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Payment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'contractor_id')->widget(Select2::class, [
        'data' => (new Contractor)->getListWithInn(),
        'options' => [
            'placeholder' => 'Выберите контрагента'
        ]
    ]) ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'pay_date')->widget(DatePicker::class, [
                'pluginOptions' => [
                    'autoclose' => true,
                ],
                'options' => [
                    'closeOnSelect' => true,
                ],
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'sum')->input('number') ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'currency_id')->widget(Select2::class, [
                'data' => Currency::getList(),
                'options' => [
                    'placeholder' => 'Выберите валюту',
                    'allowClose' => true,
                ],
                'pluginEvents' => [
                    'change' => 'function() { 
                            console.log($(this).val()); 
                            $.get("/payment/get-last-exchange-rate", {currency_id: $(this).val()}, function(res){
                                $("#exchange-rate").val(res.data);
                            })
                        }'
                ]
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'exchange_rate')->input('number', [
                'id' => 'exchange-rate'
            ]) ?>
        </div>
    </div>

    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>

