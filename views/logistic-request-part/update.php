<?php

/* @var $this yii\web\View */
/* @var $model app\models\LogisticRequestPart */
?>
<div class="logistic-request-part-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
