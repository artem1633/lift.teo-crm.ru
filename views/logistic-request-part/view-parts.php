<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LogisticRequestPartSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $request_model \app\models\LogisticRequest */
/* @var string $additional_info Доп информация по счету */

$this->title = '';
$this->params['breadcrumbs'][] = 'Счет №' . $request_model->account_number;

CrudAsset::register($this);

?>
    <div class="row">
        <div class="request_change">
            <div class="col-xs-12">
                <b>Счет №</b>
                <?= Html::input('text', 'logistic_request_number', $request_model->account_number, [
                    'class' => 'text-center',
                    'placeholder' => 'Заявка',
                    'id' => 'target-request-number',
                ]); ?>
                <?= Html::a('<span class="btn btn-info fa fa-arrow-circle-right"></span>',
                    ['/logistic-request-part/view-parts', 'logistic_request_id' => $request_model->id], [
                        'id' => 'request_change_btn',
                    ]) ?>
            </div>
            <br><br>
        </div>
    </div>
    <div class="logistic-request-part-index">
        <div id="ajaxCrudDatatable">
            <?php
            try {
                echo GridView::widget([
                    'id' => 'crud-datatable-parts',
                    'dataProvider' => $dataProvider,
//                    'filterModel' => $searchModel,
                    'pjax' => true,
                    'columns' => require(__DIR__ . '/_columns_view_parts.php'),
                    'toolbar' => [
                        [
                            'content' =>
                                $additional_info .
                                $request_model->getHeaderTableButtons(),
//                            Html::a('<i class="glyphicon glyphicon-plus"></i>',
//                                ['create', 'logistic_request_id' => $request_model->id],
//                                [
//                                    'role' => 'modal-remote',
//                                    'title' => 'Добавить позицию',
//                                    'class' => 'btn btn-default'
//                                ]) .
//                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
//                                ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Сбросить сетку таблицы']) .
//                            '{toggleData}' .
//                            '{export}'
                        ],
                    ],
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'panel' => [
                        'type' => 'primary',
                        'heading' => '<i class="glyphicon glyphicon-list"></i> Список позиций',
//                    'before' => '<em>* Resize table columns just like a spreadsheet by dragging the column edges.</em>',
                        'after' => BulkButtonWidget::widget([
                                'buttons' => Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить',
                                    ["bulkdelete"],
                                    [
                                        "class" => "btn btn-danger btn-xs",
                                        'role' => 'modal-remote-bulk',
                                        'data-confirm' => false,
                                        'data-method' => false,// for overide yii data api
                                        'data-request-method' => 'post',
                                        'data-confirm-title' => 'Вы уверены?',
                                        'data-confirm-message' => 'Подтвердите удаление выбранных элементов'
                                    ]),
                            ]) .
                            '<div class="clearfix"></div>',
                    ]
                ]);
            } catch (Exception $e) {
                Yii::error($e->getMessage(), '_error');
                echo $e->getMessage();
            } ?>
        </div>
    </div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>
<?php
$script = <<<JS

$(document).ready(function () {
        //Клик по кнопке перехода к указанной заявке
    $(document).on('click', '#request_change_btn', function(e) {
        e.preventDefault();
        var request_number = $('#target-request-number').val();
         $.get(
            '/logistic-request-part/view-parts-redirect',
            {number: request_number}
        )
    });
    
     changeColor();
    
    //Раскрашиваем Select2
    function changeColor(){ 
        var url = "/logistic-request/get-statuses-color";
        var id =  $request_model->id;
        console.log(id);
        $.get(
            url, 
            {
                id: id
            },
            )
            .done(function( colors ) {
                console.log( "Master Status: " + colors[0] );
                console.log( "Status: " + colors[1] );
                setColor("#select2-w0-container", colors[0]);
                setColor("#select2-w1-container", colors[1]);
            });
    }
    
    function setColor(id, color) {
                var status = $(id);
                var parent_status = $(id).parent(".select2-selection");
                parent_status.css("background-color", color);
                console.log('Цвет' + color);
                if (color === ""){
                     status.css("color", "black");
                } else {
                     status.css("color", "white");
                }
    }
    
    $(document).on('click', '#copy-name-btn', function() {
        var popover_id = $(this).closest('.kv-editable-popover').attr('id');
        var text_id = popover_id.replace('-popover', '-targ');
        // console.log(text_id);
        var sel = $('#' + text_id);
        sel.selectText();
        document.execCommand('copy');
    });
});
jQuery.fn.selectText = function(){
    var doc = document;
    var element = this[0];
    var range;
    // console.log(this, element);
    if (doc.body.createTextRange) {
        range = document.body.createTextRange();
        range.moveToElementText(element);
        range.select();
    } else if (window.getSelection) {
        var selection = window.getSelection();        
        range = document.createRange();
        range.selectNodeContents(element);
        selection.removeAllRanges();
        selection.addRange(range);
    }
};
JS;

$this->registerJs($script);