<?php

/* @var $this yii\web\View */
/* @var $model app\models\LogisticRequestPart */

?>
<div class="logistic-request-part-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
