<?php

use app\models\Arriving;
use app\models\Functions;
use app\models\LogisticRequestPart;
use app\models\LogisticStatus;
use app\models\Measure;
use app\models\Nomenclature;
use app\models\Suppliers;
use kartik\date\DatePicker;
use kartik\editable\Editable;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    //nomenclature_id
    [
        'class' => '\kartik\grid\EditableColumn',
        'attribute' => 'nomenclature_id',
        'value' => function (LogisticRequestPart $model) {
            return $model->nomenclature->name ?? null;
        },
        'editableOptions' => [
            'inputType' => Editable::INPUT_SELECT2,
            'options' => [
                'data' => Nomenclature::getList(),
            ],
            'formOptions' => [
                'action' => ['/logistic-request-part/editable'],
            ],
            'asPopover' => true,
            'format' => Editable::FORMAT_LINK,
            'afterInput' => function ($form, $widget) {
                echo Html::button('Скопировать наименование', [
                    'class' => 'btn btn-block',
                    'id' => 'copy-name-btn'
                ]);
            },
            'resetButton' => [
                'data-dismiss' => "popover-x"
            ],
            'pluginEvents' => [
                "editableSuccess" => "function(event, val, form, data) {
//                        var id_logistic_request_part = $(this).closest('tr').attr('data-key');
//                        var new_nomenclature_id = val
//                        var href_img = $('#photo-' + id_logistic_request_part).find('a'); //Ссылка картинки
//                        var src_img = $('#photo-' + id_logistic_request_part).find('img'); //Сама картинка
//
//                        $.ajax({
//                            url: '/nomenclature/get-nomenclature-info',
//                            data: {id: new_nomenclature_id},
//                            success: function(response){
//                                console.log(response['thumbs']);
//                                //Меняем картинку
//                                if (response['photo']){
//                                   href_img.attr('href', response['photo']);
//                               } else {
//                                    href_img.attr('href', '#');
//                               }
//                               src_img.attr('src', response['thumbs']);
//                               
//                               //Меняем артикул
//                               $('#vendor-code-' + id_logistic_request_part).html(response['own_vendor_code'])
//                            }
//                        })
                    location.reload()
                }",
            ],
        ],
        'contentOptions' => function ($model, $key, $index, $column) {
            return [
                'id' => $key,
            ];
        },
        'format' => 'raw',
        'vAlign' => 'middle',
    ],
    //num
    [
        'class' => '\kartik\grid\EditableColumn',
        'attribute' => 'num',
        'content' => function (LogisticRequestPart $model) {
            $measure = $model->measure->name ?? '';
            $num = $model->num ?? '';
            if ($measure) {
                return $num . ' (' . $measure . ')';
            }
            return $num;
        },
        'editableOptions' => [
            'inputType' => Editable::INPUT_TEXT,
            'formOptions' => [
                'action' => ['/logistic-request-part/editable'],
            ],
            'afterInput' => function ($form, $widget) {
                echo $form->field($widget->model, 'measure_id')->dropDownList(Measure::getList())->label(false);
            },
            'asPopover' => true,
            'format' => Editable::FORMAT_LINK,
            'resetButton' => [
                'data-dismiss' => "popover-x"
            ],
        ],
        'vAlign' => 'middle',
    ],
    //comment
    [
        'class' => '\kartik\grid\EditableColumn',
        'attribute' => 'comment',
        'editableOptions' => [
            'inputType' => Editable::INPUT_TEXT,
            'formOptions' => [
                'action' => ['/logistic-request-part/editable'],
            ],
            'asPopover' => true,
            'format' => Editable::FORMAT_LINK,
            'resetButton' => [
                'data-dismiss' => "popover-x"
            ],
        ],
        'vAlign' => 'middle',
    ],
    //vendor_code
    [
        'class' => '\kartik\grid\EditableColumn',
        'attribute' => 'vendor_code',
        'editableOptions' => [
            'inputType' => Editable::INPUT_SELECT2,
            'options' => [
                'data' => Nomenclature::getOwnVendorList(),
            ],
            'formOptions' => [
                'action' => ['/logistic-request-part/editable'],
            ],
            'asPopover' => true,
            'format' => Editable::FORMAT_LINK,
            'resetButton' => [
                'data-dismiss' => "popover-x"
            ],
            'pluginEvents' => [
                "editableSuccess" => "function(event, val, form, data) {
                        //перегружаем страницу
                               location.reload()
                }",
            ],
        ],
        'value' => function (LogisticRequestPart $model) {
            return $model->nomenclature->own_vendor_code ?? null;
        },
        'vAlign' => 'middle',
        'contentOptions' => function ($model, $key, $index, $column) {
            return [
                'id' => 'vendor-code-' . $key,
            ];
        }
    ],
    //img_src
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'img_src',
        'label' => 'Фото',
        'content' => function (LogisticRequestPart $model) {
//            return Functions::getMainPhoto($model->nomenclature ?? null);
            return Html::a(Functions::getMainPhoto($model->nomenclature ?? null),
                [$model->nomenclature->main_photo ?? '#'], [
                    'data-fancybox' => 'gallery-' . $model->nomenclature_id ?? null,
                ]);
        },
        'vAlign' => 'middle',
        'contentOptions' => function ($model, $key, $index, $column) {
            return [
                'id' => 'photo-' . $key,
            ];
        },
    ],
    //request_number
    [
        'class' => '\kartik\grid\EditableColumn',
        'attribute' => 'request_number',
        'editableOptions' => [
            'inputType' => Editable::INPUT_TEXT,
            'formOptions' => [
                'action' => ['/logistic-request-part/editable'],
            ],
            'asPopover' => true,
            'format' => Editable::FORMAT_LINK,
            'resetButton' => [
                'data-dismiss' => "popover-x"
            ],
        ],
        'vAlign' => 'middle',
    ],
    //arriving_id
    [
        'class' => '\kartik\grid\EditableColumn',
        'attribute' => 'arriving_info',
        'value' => function(LogisticRequestPart $model){
            return $model->arriving->number ?? null;
        },
        'editableOptions' => [
            'inputType' => Editable::INPUT_SELECT2,
            'options' => [
                'data' => (new Arriving())->getListInfo(),
            ],
            'formOptions' => [
                'action' => ['/logistic-request-part/editable'],
            ],
            'asPopover' => true,
            'format' => Editable::FORMAT_LINK,
            'resetButton' => [
                'data-dismiss' => "popover-x"
            ],
        ],
        'vAlign' => 'middle',
    ],
    //supplier_id
    [
        'class' => '\kartik\grid\EditableColumn',
         'attribute'=>'supplier_id',
//        'attribute' => 'supplier_name',
        'editableOptions' => [
            'inputType' => Editable::INPUT_SELECT2,
            'options' => [
                'data' => (new Suppliers)->getList(),
            ],
            'formOptions' => [
                'action' => ['/logistic-request-part/editable'],
            ],
            'asPopover' => true,
            'format' => Editable::FORMAT_LINK,
            'afterInput' => function ($form, $widget) {
                echo Html::button('Скопировать наименование', [
                    'class' => 'btn btn-block',
                    'id' => 'copy-name-btn'
                ]);
            },
            'resetButton' => [
                'data-dismiss' => "popover-x"
            ],
        ],

        'content' => function (LogisticRequestPart $model) {
            return $model->supplier->name ?? null;
        },
        'vAlign' => 'middle',
    ],
    //logistic_status_id
    [
        'class' => '\kartik\grid\EditableColumn',
        'attribute' => 'logistic_status_id',
        'filter' => (new LogisticStatus())->getList(),
        'editableOptions' => [
            'inputType' => Editable::INPUT_SELECT2,
            'options' => [
                'data' => (new LogisticStatus())->getList(),
            ],
            'formOptions' => [
                'action' => ['/logistic-request-part/editable'],
            ],
            'asPopover' => true,
            'format' => Editable::FORMAT_LINK,
            'resetButton' => [
                'data-dismiss' => "popover-x"
            ],
        ],
        'content' => function (LogisticRequestPart $model) {
            $status = $model->logisticStatus ?? null;
            if ($status){
                Yii::info($status->attributes, 'test');
                $icon = $status->icon ? 'fa ' . $status->icon : null;
                return Html::a('<span class="' . $icon . '"> ' . $status->name . '</span>', '#', [
                    'class' => 'btn btn-block btn-sm color_shadow',
                    'style' => 'background-color: ' . $status->color,
                ]);
            }
            return null;
        },
        'vAlign' => 'middle',
    ],
    //contract_date
    [
        'class' => '\kartik\grid\EditableColumn',
        'attribute' => 'contract_date',
        'editableOptions' => [
            'inputType' => Editable::INPUT_DATE,
            'options' => [
                'type' => DatePicker::TYPE_INPUT,
            ],
            'formOptions' => [
                'action' => ['/logistic-request-part/editable'],
            ],
            'asPopover' => false,
            'format' => Editable::FORMAT_LINK,
            'resetButton' => [
                'data-dismiss' => "popover-x"
            ],
        ],
        'format' => 'date',
        'vAlign' => 'middle',
    ],
    //order_date
    [
        'class' => '\kartik\grid\EditableColumn',
        'attribute' => 'order_date',
        'editableOptions' => [
            'inputType' => Editable::INPUT_DATE,
            'options' => [
                'type' => DatePicker::TYPE_INPUT,
            ],
            'formOptions' => [
                'action' => ['/logistic-request-part/editable'],
            ],
            'asPopover' => false,
            'format' => Editable::FORMAT_LINK,
            'resetButton' => [
                'data-dismiss' => "popover-x"
            ],
        ],
        'format' => 'date',
        'vAlign' => 'middle',
    ],
    //ship_date
    [
        'class' => '\kartik\grid\EditableColumn',
        'attribute' => 'ship_date',
        'editableOptions' => [
            'inputType' => Editable::INPUT_DATE,
            'options' => [
                    'type' => DatePicker::TYPE_INPUT,
            ],
            'formOptions' => [
                'action' => ['/logistic-request-part/editable'],
            ],
            'asPopover' => false,
            'format' => Editable::FORMAT_LINK,
            'resetButton' => [
                'data-dismiss' => "popover-x"
            ],
        ],
        'format' => 'date',
        'vAlign' => 'middle',
    ],
    //receipt_date_manch
    [
        'class' => '\kartik\grid\EditableColumn',
        'attribute' => 'receipt_date_manch',
        'editableOptions' => [
            'inputType' => Editable::INPUT_DATE,
            'options' => [
                'type' => DatePicker::TYPE_INPUT,
            ],
            'formOptions' => [
                'action' => ['/logistic-request-part/editable'],
            ],
            'asPopover' => false,
            'format' => Editable::FORMAT_LINK,
            'resetButton' => [
                'data-dismiss' => "popover-x"
            ],
        ],
        'format' => 'date',
        'vAlign' => 'middle',
    ],
    //receipt_date_novosib
    [
        'class' => '\kartik\grid\EditableColumn',
        'attribute' => 'receipt_date_novosib',
        'editableOptions' => [
            'inputType' => Editable::INPUT_DATE,
            'options' => [
                'type' => DatePicker::TYPE_INPUT,
            ],
            'formOptions' => [
                'action' => ['/logistic-request-part/editable'],
            ],
            'asPopover' => false,
            'format' => Editable::FORMAT_LINK,
            'resetButton' => [
                'data-dismiss' => "popover-x"
            ],
        ],
        'format' => 'date',
        'vAlign' => 'middle',
    ],
    //client_ship_date
    [
        'class' => '\kartik\grid\EditableColumn',
        'attribute' => 'client_ship_date',
        'editableOptions' => [
            'inputType' => Editable::INPUT_DATE,
            'options' => [
                'type' => DatePicker::TYPE_INPUT,
            ],
            'formOptions' => [
                'action' => ['/logistic-request-part/editable'],
            ],
            'asPopover' => false,
            'format' => Editable::FORMAT_LINK,
            'resetButton' => [
                'data-dismiss' => "popover-x"
            ],
        ],
        'format' => 'date',
        'vAlign' => 'middle',
    ],
    //tn_tk_rus
    [
        'class' => '\kartik\grid\EditableColumn',
        'attribute' => 'tn_tk_rus',
        'editableOptions' => [
            'inputType' => Editable::INPUT_TEXT,
            'formOptions' => [
                'action' => ['/logistic-request-part/editable'],
            ],
            'asPopover' => true,
            'format' => Editable::FORMAT_LINK,
            'resetButton' => [
                'data-dismiss' => "popover-x"
            ],
        ],
        'vAlign' => 'middle',
    ],
    //ActionColumn
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'View', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Update', 'data-toggle' => 'tooltip'],
        'deleteOptions' => [
            'role' => 'modal-remote',
            'title' => 'Delete',
            'data-confirm' => false,
            'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Are you sure?',
            'data-confirm-message' => 'Are you sure want to delete this item'
        ],
    ],

];   