<?php

use app\models\Arriving;
use app\models\LogisticStatus;
use app\models\Measure;
use app\models\Nomenclature;
use app\models\Suppliers;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LogisticRequestPart */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="logistic-request-part-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'logistic_request_id')->hiddenInput()->label(false) ?>

    <div class="row">
        <div class="col-md-7">
            <?= $form->field($model, 'nomenclature_id')->widget(Select2::className(), [
                'data' => Nomenclature::getListWithOwnVendorCode(),
                'options' => [
                    'placeholder' => 'Выберите деталь'
                ]
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'num')->textInput() ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'measure_id')->dropDownList(Measure::getList(), [
                'prompt' => 'Выберите...',
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'comment')->textarea(['rows' => 3]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'request_number')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'arriving_id')->widget(Select2::className(), [
                'data' => (new Arriving())->getListInfo(),
                'options' => [
                    'placeholder' => 'Выберите инвойс'
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ]

            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'supplier_id')->widget(Select2::className(), [
                'data' => (new Suppliers)->getList(),
                'options' => [
                    'placeholder' => 'Выберите поставщика'
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ]
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'logistic_status_id')->widget(Select2::className(), [
                'data' => (new LogisticStatus())->getList(),
                'hideSearch' => true,
                'options' => [
                    'placeholder' => 'Выберите статус'
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ]
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'contract_date')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'order_date')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'ship_date')->widget(DatePicker::className(), [
                'pluginOptions' => [
                    'autoclose' => true,
//                    'format' => 'dd.m.yyyy'
                ]
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'receipt_date_manch')->widget(DatePicker::className(), [
                'pluginOptions' => [
                    'autoclose' => true,
//                    'format' => 'dd.m.yyyy'
                ]
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'receipt_date_novosib')->widget(DatePicker::className(), [
                'pluginOptions' => [
                    'autoclose' => true,
//                    'format' => 'dd.m.yyyy'
                ]
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'client_ship_date')->widget(DatePicker::className(), [
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'dd.m.yyyy'
                ]
            ]) ?>
        </div>
    </div>

    <?= $form->field($model, 'tn_tk_rus')->textInput(['maxlength' => true]) ?>

    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
