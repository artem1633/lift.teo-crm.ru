<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LogisticRequestPart */
?>
<div class="logistic-request-part-view">

    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                [
                    'attribute' => 'logistic_request_id',
                    'value' => $model->logisticRequest->account_number ?? null,
                ],
                [
                    'attribute' => 'nomenclature_id',
                    'value' => $model->nomenclature->name ?? null,
                ],
                'num',
                'comment:ntext',
                'request_number',
                [
                    'attribute' => 'arriving_id',
                    'value' => $model->arriving->number ?? null,
                ],
                [
                    'attribute' => 'supplier_id',
                    'value' => $model->supplier->name ?? null,
                ],
                [
                    'attribute' => 'logistic_status_id',
                    'value' => $model->logisticStatus->name ?? null,
                ],
                'contract_date',
                'order_date',
                'ship_date:date',
                'receipt_date_manch:date',
                'receipt_date_novosib:date',
                'client_ship_date:date',
                'tn_tk_rus',
            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), '_error');
        echo $e->getMessage();
    } ?>

</div>
