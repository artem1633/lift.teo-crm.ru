<?php

/** @var $acc_model \app\models\Accounting */

/** @var $contractors \app\models\Contractor */

use app\models\Realization;

?>

<div class="container">
    <?php
    if (!$acc_model->report_start_date ?? null): ?>
        <h2>Для построения отчета необходимо выбрать период</h2>
    <?php elseif (!$contractors):?>
        <h2>Ничего не найдено</h2>
    <?php else: ?>
        <div class="row">
            <h3>
                Расход за период с <?php
                try {
                    echo Yii::$app->formatter->asDate($acc_model->report_start_date);
                } catch (\yii\base\InvalidConfigException $e) {
                    Yii::error($e->getMessage(), '_error');
                } ?>
                по <?php
                try {
                    echo Yii::$app->formatter->asDate($acc_model->report_end_date);
                } catch (\yii\base\InvalidConfigException $e) {
                    Yii::error($e->getMessage(), '_error');
                } ?>
            </h3>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <table class="profit-table table table-bordered">
                    <tbody>
                    <?php /** @var \app\models\Arriving $arr */
                    foreach ($contractors as $contractor): ?>
                        <?php
                        Yii::info('Контрагент: ' . $contractor->full_name ?? $contractor->name, 'test');
                        ?>
                        <tr>
                            <td colspan="5"
                                style="font-weight: bold;"><?= $contractor->full_name ?? $contractor->name ?>
                            </td>
                        </tr>
                        <tr>
                            <td>№ п/п</td>
                            <td>Наименование</td>
                            <td>Средняя цена</td>
                            <td>Кол-во</td>
                            <td>Итого</td>
                        </tr>

                        <?php
                        $counter = 0;
                        $realizations = Realization::getFromContractor($contractor->id, $acc_model->report_start_date,
                            $acc_model->report_end_date);
                        /** @var Realization $rel */
                        foreach ($realizations as $rel): ?>
                            <?php Yii::info($rel->toArray(), 'test'); ?>
                            <?php
//                            \yii\helpers\VarDumper::dump($rel, 10, true);
                            /** @var double $average_price Средняя цена продажи */
                            $average_price = Realization::getAveragePrice($rel->product_id,
                                $acc_model->report_start_date, $acc_model->report_end_date);
                            /** @var int $detail_sum Количество деталей */
                            $detail_sum = Realization::getSum($rel->product_id, $acc_model->report_start_date,
                                $acc_model->report_end_date);
                            $total = Realization::getTotal($rel->product_id, $acc_model->report_start_date,
                                $acc_model->report_end_date);;
                            ?>
                            <?php //\yii\helpers\VarDumper::dump($average_price, 10, true); ?>

                            <?php $counter += 1 ?>
                            <tr>
                                <td><?= $counter; ?></td>
                                <td><?= $rel->product->name ?? ''; ?></td>
                                <td><?= $average_price; ?></td>
                                <td><?= $detail_sum; ?></td>
                                <td><?= $total; ?></td>
                            </tr>
                        <?php endforeach; ?>

                        <?php
                        $counter = 0;
                    endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php endif; ?>
</div>


