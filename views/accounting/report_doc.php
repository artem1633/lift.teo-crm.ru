<?php

use app\models\Nomenclature;
use johnitvn\ajaxcrud\CrudAsset;
use yii\bootstrap\Modal;

try {
    $this->registerJsFile('/js/accounting_report.js', [
        'depends' => [
            'yii\web\YiiAsset',
            'yii\bootstrap\BootstrapAsset',
        ]
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

CrudAsset::register($this);

/** @var $acc_model \app\models\Accounting */
/** @var Nomenclature[] $details Позиции номенклатуры */

?>

<div class="container">
    <h1>Отчеты</h1>

    <div class="row">
        <div class="col-xs-12">
            <?php require(__DIR__ . '/_report_filter.php'); ?>
        </div>
    </div>
    <div class="row progress-block"  style="display:none;">
        <div class="col-xs-12">
            <div class="progress">
                <div class="progress-bar progress-bar-striped active" role="progressbar"
                     aria-valuenow="100"
                     aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                </div>
            </div>
        </div>
        <div class="col-xs-12 text-center">
            <div class="progress-block-description">
                <h4>Построение отчета... Не перезагружайте страницу</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 report-table">
            <?php if ($details): ?>
                <?php require(__DIR__ . '/_report_table.php'); ?>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "options" => [
        "tabindex" => -1,
    ],
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>

