<?php

use app\models\Accounting;
use app\models\DetailToArriving;
use app\models\Measure;
use app\models\Nomenclature;
use app\models\Realization;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

CrudAsset::register($this);

/** @var $acc_model Accounting */
/** @var Nomenclature[] $details Проданные или/и купленные детали */

//if (strpos($acc_model->report_start_date, ':')) {
//    $acc_model->report_start_date = explode(' ', $acc_model->report_start_date)[0];
//}
//if (strpos($acc_model->report_end_date, ':')) {
//    $acc_model->report_end_date = explode(' ', $acc_model->report_end_date)[0];
//}
//\yii\helpers\VarDumper::dump($acc_model, 10,  true);
?>

    <div class="container">
        <h1>Отчет</h1>
        <?php $form = ActiveForm::begin(['id' => 'report-filter-form']); ?>
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($acc_model, 'report_start_date')->widget(DatePicker::class, [
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'options' => [
                        'id' => 'start-date',
                        'autocomplete' => 'off',
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ])->label('Начало периода') ?>
            </div>
            <div class="col-xs-6">
                <?= $form->field($acc_model, 'report_end_date')->widget(DatePicker::class, [
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'options' => [
                        'id' => 'end-date',
                        'autocomplete' => 'off',
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ])->label('Конец периода') ?>
            </div>

        </div>
        <div class="row filter-2">
            <div class="col-xs-6">
                <?= $form->field($acc_model, 'detail_id')->widget(Select2::className(), [
                    'data' => Nomenclature::getList(),
                    'options' => [
                        'placeholder' => 'Выберите деталь',
//                        'multiple' => true
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ])->label('Наименование детали') ?>
            </div>
            <div class="col-xs-4">
                <?= $form->field($acc_model, 'own_vendor_code')->widget(Select2::className(), [
                    'data' => Nomenclature::getOwnVendorList(),
                    'options' => [
                        'placeholder' => 'Выберите артикул',
//                        'multiple' => true
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ])->label('Артикул') ?>
            </div>
            <div class="col-xs-2">
                <?= Html::submitButton('Сформировать', ['class' => 'btn btn-info btn-block']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
        <?php

        if (!$acc_model->report_start_date ?? null): ?>
            <h2>Для построения отчета необходимо выбрать период</h2>
        <?php elseif (!$details): ?>
            <h2>Ничего не найдено</h2>
        <?php else: ?>
            <div class="row">
                <div class="col-md-12">
                    <?= Html::a('<i class="fa fa-sign-out"></i> Экспорт отчета', ['report-export'], [
                        'id' => 'export-report',
                        'class' => 'btn btn-success pull-right',
                        'style' => 'margin-bottom: 1rem;'
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <table class="report-table table table-bordered">
                        <tbody>
                        <tr>
                            <th>№ п/п</th>
                            <th>Артикул</th>
                            <th>Наименование детали</th>
                            <th>Начальный остаток</th>
                            <th>Приход</th>
                            <th>Расход</th>
                            <th>Конечный остаток</th>
                        </tr>
                        <?php /** @var Nomenclature $detail */
                        $counter = 1;
                        foreach ($details as $detail): ?>

                            <?php
                            $start_balance = [];
                            $detail_measure = '&nbsp;' . Measure::getMeasure($detail->measure_id);
                            ?>
                            <tr>
                                <td><?= $counter; ?></td>
                                <td><?= $detail->own_vendor_code; ?></td>
                                <td><?= $detail->name; ?></td>
                                <td>
                                    <?php
                                    $start_balance = Accounting::getBalanceForDate($detail->id,
                                        date('Y-m-d 23:59:59',
                                            strtotime($acc_model->report_start_date) - (24 * 60 * 60)));
                                    echo $start_balance['num'] . $detail_measure;
                                    echo '<br>';
                                    echo $start_balance['amount'] . '&nbsp;руб.';

                                    ?>
                                </td>
                                <?php
                                $dta = DetailToArriving::getSum($detail->id, $acc_model->report_start_date,
                                    $acc_model->report_end_date);
                                ?>
                                <td>
                                    <div style="position:relative;">
                                        <?= $dta['sum'] . $detail_measure ?>
                                        <br>
                                        <?php
                                        if (isset($dta['by_currency'])) {
                                            /** @var DetailToArriving $currency */
                                            foreach ($dta['by_currency'] as $cur_key => $amount) {
                                                echo $amount . '&nbsp;' .
                                                    '<span class="fa ' . $cur_key . '"></span><br>';
                                            }
                                        }
                                        ?>
                                        <?= $dta['total_amount']
                                        . '&nbsp;руб.' ?>
                                        <?= Html::a('', ['/accounting/arriving-by-detail'], [
                                            'class' => 'outer-link glyphicon glyphicon-log-out',
                                            'style' => 'position: absolute; cursor: pointer; right: 0; top: 0;',
                                            'detail-id' => $detail->id,
                                        ]) ?>
                                    </div>
                                </td>
                                <td>
                                    <div style="position:relative;">
                                        <?php
                                        $realization = Realization::getRealization($detail->id,
                                            $acc_model->report_start_date,
                                            $acc_model->report_end_date);
                                        echo $realization['num'] . $detail_measure;
                                        echo '<br>';
                                        echo $realization['amount'] . '&nbsp;руб.'

                                        ?>
                                        <?= Html::a('', ['/accounting/realization-by-detail'], [
                                            'class' => 'outer-link glyphicon glyphicon-log-out',
                                            'style' => 'position: absolute; cursor: pointer; right: 0; top: 0;',
                                            'detail-id' => $detail->id,
                                        ]) ?>
                                    </div>
                                </td>
                                <td>
                                    <?php
                                    Yii::info($start_balance, 'test');
                                    Yii::info($dta, 'test');
                                    Yii::info($realization, 'test');
                                    echo ($start_balance['num'] + $dta['sum'] - $realization['num']) . $detail_measure;
                                    echo '<br>';
                                    echo ($start_balance['amount'] + $dta['total_amount'] - $realization['amount']) . '&nbsp;руб.';
                                    ?>
                                </td>
                            </tr>
                            <?php $counter += 1; ?>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        <?php endif; ?>
    </div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "options" => [
        "tabindex" => -1,
    ],
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>

<?php
$script = <<<JS
$(document).ready(function() {
  $(document).on('click', '.outer-link', function(e){
      e.preventDefault();
      var href = $(this).attr('href');
      var id = $(this).attr('detail-id');
      var start = $('#start-date').val();
      var end = $('#end-date').val();
      $.post(
          href,
          {
              id: id,
              start: start,
              end: end
          },
          function(response) {
              if (response['success'] === 0){
                  alert(response['data']);
              }
            
          }
      )
  });
  
  $(document).on('click', '#export-report', function(event){
        event.preventDefault();
        var data = $('#report-filter-form').serializeArray();
       
      $.post(
          '/accounting/report-export',
          {
              Accounting: data
          },
          function(response) {
              console.log(response);
          }
          
      )
  })
})
JS;
$this->registerJs($script);
