<?php

/**
 * @var array $test_data Содержание XML файла
 * @var string $path_file Путь к файлу экспорта
 */

//use kartik\grid\GridView;
use yii\bootstrap\Modal;

?>

<div class="container">
    <?php if ($path_file): ?>
        <div class="row">
            <div class="col-xs-12">
                <?= \yii\helpers\Html::a('<span class="glyphicon glyphicon-download-alt"></span> Скачать сформированный файл.', ['download-file', 'path' => $path_file]) ?>
            </div>
        </div>
    <?php endif; ?>
    <?php
    if ($test_data) {
        \yii\helpers\VarDumper::dump($test_data, 10, true);
    }
    ?>
</div>

<div class="arriving-index">
    <div id="ajaxCrudDatatable">
        <?php
//        try {
//            echo GridView::widget([
//                'id' => 'crud-datatable',
//                'dataProvider' => $dataProvider,
////                'filterModel' => $searchModel,
//                'pjax' => true,
//                'columns' => require(__DIR__ . '/_columns.php'),
//                'toolbar' => [
//                    [
//                        'content' =>
//                            Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
//                                [
//                                    'role' => 'modal-remote',
//                                    'title' => 'Create new Realizations',
//                                    'class' => 'btn btn-default'
//                                ]) .
//                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
//                                ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Reset Grid']) .
//                            '{toggleData}' .
//                            '{export}'
//                    ],
//                ],
//                'striped' => true,
//                'condensed' => true,
//                'responsive' => true,
//                'panel' => [
//                    'type' => 'primary',
//                    'heading' => '<i class="glyphicon glyphicon-list"></i> Список реализаций',
////                    'before' => '<em>* Resize table columns just like a spreadsheet by dragging the column edges.</em>',
//                    'after' => Functions::getBulkButtonWidget() .
//                        '<div class="clearfix"></div>',
//                ]
//            ]);
//        } catch (Exception $e) {
//            Yii::error($e->getMessage(), '_error');
//        } ?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>