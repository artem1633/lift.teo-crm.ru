<?php

use johnitvn\ajaxcrud\CrudAsset;
use kartik\date\DatePicker;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

CrudAsset::register($this);

/** @var $acc_model \app\models\Accounting */
/** @var $profit_contractors \app\models\Contractor[] */
/** @var $expense_contractors \app\models\Contractor[] */

if (strpos($acc_model->report_start_date, ':')){
    $acc_model->report_start_date = explode(' ',$acc_model->report_start_date)[0];
}
if (strpos($acc_model->report_end_date, ':')){
    $acc_model->report_end_date = explode(' ', $acc_model->report_end_date)[0];
}
Yii::info($acc_model->toArray(), 'test');
?>

<div class="container">
    <h1>Отчеты</h1>

    <div class="row">
        <?php $form = ActiveForm::begin(); ?>

        <div class="report-filter">

            <div class="col-xs-5">
                <?= $form->field($acc_model, 'report_start_date')->widget(DatePicker::class, [
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'options' => [
                        'autocomplete' => 'off',
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ])->label('Начало периода') ?>
            </div>
            <div class="col-xs-5">
                <?= $form->field($acc_model, 'report_end_date')->widget(DatePicker::class, [
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'options' => [
                        'autocomplete' => 'off',
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ])->label('Конец периода') ?>
            </div>
            <div class="col-xs-2">
                <?= Html::submitButton('Сформировать', ['class' => 'btn btn-info btn-block']) ?>
            </div>

        </div>
        <?php ActiveForm::end(); ?>

    </div>
    <div class="row">
        <div class="report-index">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#default-tab-1" data-toggle="tab" aria-expanded="true">
                        <span class="visible-xs">Приход</span>
                        <span class="hidden-xs">Приход</span>
                    </a>
                </li>
                <li class="">
                    <a href="#default-tab-2" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs">Расход</span>
                        <span class="hidden-xs">Расход</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade active in" id="default-tab-1">
                    <?= $this->render('_view-profit', [
                        'acc_model' => $acc_model,
                        'contractors' => $profit_contractors,
                    ]) ?>
                </div>
                <div class="tab-pane fade" id="default-tab-2">
                    <?= $this->render('_view-expense', [
                        'acc_model' => $acc_model,
                        'contractors' => $expense_contractors,
                    ]) ?>
                </div>
            </div>
        </div>

    </div>
</div>

<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "options" => [
        "tabindex" => -1,
    ],
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>

