<?php


use app\models\DetailToArriving;

/** @var $acc_model \app\models\Accounting */
/** @var $contractors \app\models\Contractor[] */
$counter = 0;
?>

<div class="container">
    <?php
    if (!$acc_model->report_start_date ?? null): ?>
        <h2>Для построения отчета необходимо выбрать период</h2>
    <?php elseif (!$contractors):?>
        <h2>Ничего не найдено</h2>
    <?php else: ?>
        <div class="row">
            <h3>
                Приход за период с <?php
                try {
                    echo Yii::$app->formatter->asDate($acc_model->report_start_date);
                } catch (\yii\base\InvalidConfigException $e) {
                    Yii::error($e->getMessage(), '_error');
                } ?>
                по <?php
                try {
                    echo Yii::$app->formatter->asDate($acc_model->report_end_date);
                } catch (\yii\base\InvalidConfigException $e) {
                    Yii::error($e->getMessage(), '_error');
                } ?>
            </h3>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <table class="profit-table table table-bordered">
                    <tbody>

                    <?php foreach ($contractors as $contractor): ?>
                        <?php Yii::info('Контрагент: ' . $contractor->full_name ?? $contractor->name, 'test');?>
                        <tr>
                            <td colspan="5"
                                style="font-weight: bold;"><?= $contractor->full_name ?? $contractor->name ?>
                            </td>
                        </tr>
                        <tr>
                            <td>№ п/п</td>
                            <td>Наименование</td>
                            <td>Средняя цена</td>
                            <td>Кол-во</td>
                            <td>Итого</td>
                        </tr>

                        <?php
                        /** @var DetailToArriving $dtas */
                        $dtas = DetailToArriving::getFromContractor($contractor->id, $acc_model->report_start_date,
                            $acc_model->report_end_date);
//                        \yii\helpers\VarDumper::dump($dtas, 10, true);
//                        Yii::info($dtas, 'test');
                        /** @var DetailToArriving $dta */
                        foreach ($dtas as $dta): ?>
                            <?php Yii::info($dta->toArray(), 'test'); ?>
                            <?php
//                            \yii\helpers\VarDumper::dump($dta, 10, true);
                            /** @var double $average_price Средняя цена закупки */
                            $average_price = DetailToArriving::getAveragePrice($dta->detail_id,
                                $acc_model->report_start_date, $acc_model->report_end_date);
                            /** @var int $detail_sum Количество деталей */
                            $detail_sum = DetailToArriving::getSum($dta->detail_id, $acc_model->report_start_date,
                                $acc_model->report_end_date);
                            $total = DetailToArriving::getTotal($dta->detail_id, $acc_model->report_start_date,
                                $acc_model->report_end_date);;
                            ?>
                            <?php //\yii\helpers\VarDumper::dump($average_price, 10, true); ?>

                            <?php $counter += 1 ?>
                            <tr>
                                <td><?= $counter; ?></td>
                                <td><?= $dta->detail->name ?? ''; ?></td>
                                <td><?= $average_price; ?></td>
                                <td><?= $detail_sum; ?></td>
                                <td><?= $total; ?></td>
                            </tr>
                        <?php endforeach; ?>

                        <?php
                        $counter = 0;
                    endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>

    <?php endif; ?>
</div>


