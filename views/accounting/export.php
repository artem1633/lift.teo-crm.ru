<?php

use app\models\OrderStatus;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/** @var $model \app\models\Accounting */
/** @var $model \app\models\Accounting */

if (!$model->export_request_status) {
    $model->export_request_status = 13; //Готова к счету
}

?>

<div class="row">
    <div class="import-form">

        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

        <div class="col-md-6">
            <?= $form->field($model, 'export_start_date')->widget(DatePicker::class,
                [
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'options' => [
                        'autocomplete' => 'off',
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]); ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'export_end_date')->widget(DatePicker::class,
                [
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'options' => [
                        'autocomplete' => 'off',
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]); ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'export_request_status')->widget(Select2::class, [
                'data' => OrderStatus::getList(),
                'size' => Select2::MEDIUM,
                'options' => [
                    'placeholder' => 'Выберите статус',
                    'multiple' => true
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>