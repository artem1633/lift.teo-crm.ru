<?php

use app\models\Accounting;
use app\models\DetailToArriving;
use app\models\Measure;
use app\models\MovementPart;
use app\models\Nomenclature;
use app\models\Realization;
use yii\helpers\Html;

/** @var Nomenclature[] $details Позиции номенклатуры */
/** @var Accounting $acc_model */
/** @var int $remains_type Тип отчета. 1 - вся номенклатура, 2 - без нулвых остатков */

?>
<div class="row">
    <div class="col-md-12">
        <?= Html::button('<i class="fa fa-sign-out"></i> Экспорт отчета', [
            'id' => 'export-report',
            'class' => 'btn btn-success pull-right',
            'style' => 'margin: 1rem;',
            'data-url' => 'report-export',
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <table class="report-table table table-bordered table-hover">
            <tbody>
            <tr>
                <th>№ п/п</th>
                <th>Артикул</th>
                <th>Наименование детали</th>
                <th>Начальный остаток</th>
                <th>Приход</th>
                <th>Расход</th>
                <th>Конечный остаток</th>
            </tr>
            <?php /** @var Nomenclature $detail */
            $counter = 1;
            foreach ($details as $warehouse => $items): ?>
                <tr class="bg-gray-active"><td colspan="7"><strong> <?= DetailToArriving::getWarehouses()[$warehouse] ?? '' ?></strong></td></tr>
                <?php foreach ($items as $detail): ?>
                    <?php
                    $start_balance = [];
                    $dta = [];
                    $realization = [];
                    if ($warehouse == DetailToArriving::WAREHOUSE_BASIS){
                        //Для основного склада
                        $start_balance = Accounting::getBalanceForDate($detail->id,
                            date('Y-m-d 23:59:59',
                                strtotime($acc_model->report_start_date) - (24 * 60 * 60)));
                        $dta = DetailToArriving::getSum($detail->id, $acc_model->report_start_date,
                            $acc_model->report_end_date);
                        $realization = Realization::getRealization($detail->id,
                            $acc_model->report_start_date,
                            $acc_model->report_end_date);
                        $end_balance = $start_balance['num'] + $dta['sum'] - $realization['num'];
                    } else {
                        $start_balance['num'] = MovementPart::getBalanceByWarehouseOnDate($detail->id, $warehouse, $acc_model->report_start_date);
                        Yii::debug($start_balance, 'start_balance report_table');
                        /** @var float $dta Приход */
                        $dta['sum'] = MovementPart::getArrivingForPeriod($detail->id, $warehouse, $acc_model->report_start_date, $acc_model->report_end_date);
                        $realization['num'] = MovementPart::getWriteOffForPeriod($detail->id, $warehouse, $acc_model->report_start_date, $acc_model->report_end_date);
                        $end_balance = $start_balance['num'] + $dta['sum'] - $realization['num'];
                    }
                    if ($end_balance == 0 && $remains_type == $acc_model::REMAINS_TYPE_NOT_ZERO) {
                        //Если конечный остаток равен нулю и в настройке отчета выставлено
                        // не показывать нулевые остатки - пропускаем
                        continue;
                    }
                    ?>
                    <?php
                    $detail_measure = '&nbsp;' . Measure::getMeasure($detail->measure_id);
                    ?>
                    <tr>
                        <td><?= $counter; ?></td>
                        <td><?= $detail->own_vendor_code; ?></td>
                        <td><?= $detail->name; ?></td>
                        <td>
                            <?php
                            echo $start_balance['num'] . $detail_measure;
                            //                        echo '<br>';
                            //                        echo $start_balance['amount'] . '&nbsp;руб.';
                            ?>
                        </td>
                        <?php

                        ?>
                        <td>
                            <div style="position:relative;">
                                <?= $dta['sum'] . $detail_measure ?>
                                <br>
                                <?php
                                //                            if (isset($dta['by_currency'])) {
                                //                                /** @var DetailToArriving $currency */
                                //                                foreach ($dta['by_currency'] as $cur_key => $amount) {
                                //                                    echo $amount . '&nbsp;' .
                                //                                        '<span class="fa ' . $cur_key . '"></span><br>';
                                //                                }
                                //                            }
                                ?>
                                <?php // echo $dta['total_amount']. '&nbsp;руб.' ?>
                                <?= Html::a('', '#', [
                                    'class' => 'outer-link glyphicon glyphicon-log-out to-arriving',
                                    'style' => 'position: absolute; cursor: pointer; right: 0; top: 0;',
                                    'data-href' => '/accounting/arriving-by-detail?warehouse=' . $warehouse,
                                    'detail-id' => $detail->id,
                                    'hidden' => !$dta['sum'],
                                ]) ?>
                            </div>
                        </td>
                        <td>
                            <div style="position:relative;">
                                <?php
                                echo $realization['num'] . $detail_measure;
                                //                            echo '<br>';
                                //                            echo $realization['amount'] . '&nbsp;руб.'
                                ?>
                                <?= Html::a('', '#', [
                                    'class' => 'outer-link glyphicon glyphicon-log-out to-realization',
                                    'style' => 'position: absolute; cursor: pointer; right: 0; top: 0;',
                                    'data-href' => '/accounting/realization-by-detail?warehouse=' . $warehouse,
                                    'detail-id' => $detail->id,
                                    'hidden' => !$realization['num'],
                                ]) ?>
                            </div>
                        </td>
                        <td>
                            <?php
                            echo round($end_balance, 2) . $detail_measure;
                            //                        echo '<br>';
                            //                        echo ($start_balance['amount'] + $dta['total_amount'] - $realization['amount']) . '&nbsp;руб.';
                            ?>
                        </td>
                    </tr>
                    <?php $counter += 1; ?>
                <?php endforeach; ?>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

