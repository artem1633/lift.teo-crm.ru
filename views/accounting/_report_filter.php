<?php

use app\models\Accounting;
use app\models\Brand;
use app\models\DetailToArriving;
use app\models\Nomenclature;
use app\models\NomenclatureGroup;
use app\models\TypeOfParts;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var Accounting $acc_model */

?>
<?php

$form = ActiveForm::begin(['id' => 'report-filter-form']); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($acc_model, 'report_start_date')->widget(DatePicker::class, [
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'options' => [
                    'id' => 'start-date',
                    'autocomplete' => 'off',
                ],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ])->label('Начало периода') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($acc_model, 'report_end_date')->widget(DatePicker::class, [
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'options' => [
                    'id' => 'end-date',
                    'autocomplete' => 'off',
                ],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ])->label('Конец периода') ?>
        </div>

    </div>
    <div class="row filter-2">
        <div class="col-md-6">
            <?= $form->field($acc_model, 'detail_id')->widget(Select2::class, [
                'data' => Nomenclature::getList(),
                'options' => [
                    'placeholder' => 'Выберите деталь',
//                        'multiple' => true
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ]
            ])->label('Наименование детали') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($acc_model, 'own_vendor_code')->widget(Select2::class, [
                'data' => Nomenclature::getOwnVendorList(),
                'options' => [
                    'placeholder' => 'Выберите артикул',
//                        'multiple' => true
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ]
            ])->label('Артикул') ?>
        </div>

    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($acc_model, 'brand_id')->widget(Select2::class, [
                'data' => Brand::getList(),
                'options' => [
                    'placeholder' => 'Выберите бренд',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ]
            ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($acc_model, 'type_of_parts_id')->widget(Select2::class, [
                'data' => TypeOfParts::getList(),
                'options' => [
                    'placeholder' => 'Выберите тип',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ]
            ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($acc_model, 'warehouse')->widget(Select2::class, [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Warehouse::find()->all(), 'id', 'name'),
                'options' => [
                    'placeholder' => 'Выберите склад',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ]
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($acc_model, 'nomenclature_group_id')->widget(Select2::class, [
                'data' => NomenclatureGroup::getList(),
                'options' => [
                    'placeholder' => 'Выберите группу',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ]
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($acc_model, 'remains_type')->widget(Select2::class, [
                'data' => [
                    $acc_model::REMAINS_TYPE_NOT_ZERO => 'Номенклатруа без нулевых остатков',
                    $acc_model::REMAINS_TYPE_ALL => 'Вся номенклатура',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ]
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= Html::button('Сформировать', [
                'class' => 'btn btn-info btn-block',
                'data-url' => '/accounting/get-report',
                'id' => 'accept-report-filter'
            ]) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>