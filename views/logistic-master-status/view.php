<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LogisticMasterStatus */
?>
<div class="logistic-master-status-view">
 
    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'name',
                'description',
            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), '_error');
        echo $e->getMessage();
    } ?>

</div>
