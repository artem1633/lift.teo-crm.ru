<?php

/* @var $this yii\web\View */
/* @var $model app\models\LogisticMasterStatus */
?>
<div class="logistic-master-status-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
