<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OrderStatus */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-status-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Название'])->label(false) ?>

    <?= $form->field($model, 'icon')->widget('\insolita\iconpicker\Iconpicker',
        [
            'iconset'=>'fontawesome',
            'clientOptions'=>['rows'=>8,'cols'=>10,'placement'=>'right'],
        ])->label('Выбрать иконку');
    ?>

    <?= $form->field($model, 'color')->textInput(['type' => 'color']) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
