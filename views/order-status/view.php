<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\OrderStatus */
?>
<div class="order-status-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            'name',
            [
                'attribute' => 'icon',
                'value' => function($data){
                    return '<span class="fa ' .  $data -> icon . '"></span>';
                },
                'format' => 'html',
            ],
//            'color',
            [
                'attribute' => 'color',
                'value' => function($data){
                    return '<span style="padding:5px; border-radius:2px; background-color: ' .  $data -> color . '">'. $data -> color .'</span>';
                },
                'format' => 'html',
            ],
        ],
    ]) ?>

</div>
