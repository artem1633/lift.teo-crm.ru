<?php

use app\models\Contractor;
use app\models\JuridicalPerson;
use app\models\LogisticMasterStatus;
use app\models\LogisticStatus;
use app\models\Payee;
use app\models\Users;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LogisticRequest */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="logistic-request-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-8">
            <?= $form->field($model, 'payee_id')->widget(Select2::className(), [
                'data' => (new Payee())->getList(),
                'hideSearch' => true,
                'options' => [
                    'placeholder' => 'Выберите получателя платежа'
                ]
            ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'account_number')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'contractor_id')->widget(Select2::className(), array(
                'data' => (new Contractor())->getListWithInn(),
                'value' => $model->contractor_id ?? null,
                'options' => array(
                    'placeholder' => 'Выберите контрагента'
                )
            )) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'juridical_person_id')->widget(Select2::className(), array(
                'data' => (new JuridicalPerson())->getListWithInn(),
                'value' => $model->juridical_person_id ?? null,
                'options' => array(
                    'placeholder' => 'Выберите юр. лицо'
                )
            )) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'responsible_id')->widget(Select2::className(), [
                'data' => (new Users())->getActiveManagers(),
                'options' => [
                    'placeholder' => 'Выберите ответственного'
                ]
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'logistic_master_status_id')->widget(Select2::className(), [
                'data' => (new LogisticMasterStatus())->getList(),
                'hideSearch' => true,
                'options' => [
                    'placeholder' => 'Выберите статус'
                ]
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'logistic_status_id')->widget(Select2::className(), [
                'data' => (new LogisticStatus())->getList(),
                'hideSearch' => true,
                'options' => [
                    'placeholder' => 'Выберите статус'
                ]
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'invoice_date')->widget(DatePicker::className(), [
                'pluginOptions' => [
                    'autoclose' => true,
//                    'format' => 'dd.m.yyyy'
                ]
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'payed_date')->widget(DatePicker::className(), [
                'pluginOptions' => [
                    'autoclose' => true,
//                    'format' => 'dd.m.yyyy'
                ]
            ]) ?>
        </div>
    </div>

    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
