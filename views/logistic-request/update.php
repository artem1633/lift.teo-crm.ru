<?php

/* @var $this yii\web\View */
/* @var $model app\models\LogisticRequest */
?>
<div class="logistic-request-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
