<?php

use app\models\LogisticMasterStatus;
use app\models\LogisticRequest;
use app\models\LogisticRequestPart;
use app\models\LogisticStatus;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
//        'attribute' => 'payee_id',
        'attribute' => 'payee_name',
        'content' => function (LogisticRequest $model) {
            if ($model->payee->full_name ?? null) {
                $payee = $model->payee->full_name ?? null;
            } else {
                $payee = $model->payee->name ?? null;
            }
            return Html::a($payee, ['/payee/view', 'id' => $model->payee_id], ['role' => 'modal-remote']);
        },
        'vAlign' => 'middle',

    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'account_number',
        'content' => function (LogisticRequest $model) {
            if ($model->account_number ?? null) {
                return Html::a($model->account_number,
                    ['/logistic-request-part/view-parts', 'logistic_request_id' => $model->id],
                    ['data-pjax' => 0]);
            }
            return null;
        },
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'requests_numbers',
        'content' => function (LogisticRequest $model) {
            $numbers = LogisticRequestPart::find()
                ->andWhere(['logistic_request_id' => $model->id])
                ->select('DISTINCT(request_number)')
                ->column();

            return implode(', ', $numbers);
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'invoices',
        'label' => 'Инвойсы',
        'content' => function (LogisticRequest $model) {
            return $model->getInvoices();
        },
    ],
    [
        'class' => '\kartik\grid\DataColumn',
//        'attribute' => 'contractor_id',
        'attribute' => 'contractor_name',
        'content' => function (LogisticRequest $model) {
            if ($model->contractor->full_name ?? null) {
                $contractor_name = $model->contractor->full_name ?? null;
            } else {
                $contractor_name = $model->contractor->name ?? null;
            }

            return Html::a($contractor_name, ['/contractor/view', 'id' => $model->contractor_id],
                ['role' => 'modal-remote']);
        },
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'juridical_person_name',
        'value' => function (LogisticRequest $model) {
            return $model->juridicalPerson->name ?? $model->juridicalPerson->full_name ?? null;
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
//        'attribute' => 'responsible_id',
        'attribute' => 'responsible_name',
        'content' => function (LogisticRequest $model) {
            if (!$model->responsible_id ?? null) {
                return null;
            }

            $responsible = $model->responsible->fio ?? null;
            if (!$responsible) {
                return null;
            }
            return Html::a($responsible, ['/users/view', 'id' => $model->responsible_id], ['role' => 'modal-remote']);
        },
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'logistic_master_status_id',
        'filter' => (new LogisticMasterStatus())->getList(),
        'content' => function (LogisticRequest $model) {
            $status = $model->logisticMasterStatus ?? null;
            if ($status) {
                Yii::info($status->attributes, 'test');
                $icon = $status->icon ? 'fa ' . $status->icon : null;
                return Html::button('<span class="' . $icon . '"> ' . $status->name . '</span>', [
                    'class' => 'btn btn-block btn-sm color_shadow',
                    'style' => 'background-color: ' . $status->color,
                ]);
            }
            return null;
        },
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'logistic_status_id',
        'filter' => (new LogisticStatus())->getList(),
        'content' => function (LogisticRequest $model) {
            $status = $model->logisticStatus ?? null;
            if ($status) {
                Yii::info($status->attributes, 'test');
                $icon = $status->icon ? 'fa ' . $status->icon : null;
                return Html::button('<span class="' . $icon . '"> ' . $status->name . '</span>', [
                    'class' => 'btn btn-block btn-sm color_shadow',
                    'style' => 'background-color: ' . $status->color,
                ]);
            }
            return null;
        },
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'invoice_date',
        'format' => 'date',
        'vAlign' => 'middle',
        'filter' => '<input class="form-control" type="date" name="LogisticRequestSearch[invoice_date]" value="'.\yii\helpers\ArrayHelper::getValue($_GET, 'LogisticRequestSearch.invoice_date').'">',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'payed_date',
        'format' => 'date',
        'vAlign' => 'middle',
        'filter' => '<input class="form-control" type="date" name="LogisticRequestSearch[payed_date]" value="'.\yii\helpers\ArrayHelper::getValue($_GET, 'LogisticRequestSearch.payed_date').'">',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'client_ship_date',
        'vAlign' => 'middle',
        'content' => function(LogisticRequest $model){
            return $model->logisticRequestParts ? Yii::$app->formatter->asDate($model->logisticRequestParts[0]->client_ship_date) : '';
        },
        'filter' => '<input class="form-control" type="date" name="LogisticRequestSearch[client_ship_date]" value="'.\yii\helpers\ArrayHelper::getValue($_GET, 'LogisticRequestSearch.client_ship_date').'">',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'template' => '{add-position} {view-parts} {update} {delete}',
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'buttons' => [
            'add-position' => function ($url, LogisticRequest $model) {
                Yii::info($url, 'test');
                return \yii\helpers\Html::a('<span class="glyphicon glyphicon-plus"></span>',
                    ['/logistic-request-part/create', 'logistic_request_id' => $model->id], [
                        'role' => 'modal-remote',
                        'title' => 'Добавить позицию к счету'
                    ]);
            },
            'view-parts' => function ($url, LogisticRequest $model) {
                Yii::info($url, 'test');
                return \yii\helpers\Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                    ['/logistic-request-part/view-parts', 'logistic_request_id' => $model->id], [
                        'data-pjax' => 0,
                        'title' => 'Перейти к позициям'
                    ]);
            },
        ],
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'View', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Update', 'data-toggle' => 'tooltip'],
        'deleteOptions' => [
            'role' => 'modal-remote',
            'title' => 'Delete',
            'data-confirm' => false,
            'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Вы увререны?',
            'data-confirm-message' => 'Подтвердите удаление элемента'
        ],
    ],

];   