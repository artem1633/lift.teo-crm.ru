<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LogisticRequest */
?>
<div class="logistic-request-view">

    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                [
                    'attribute' => 'payee_id',
                    'value' => $model->payee->full_name ? $model->payee->full_name ?? null : $model->payee->name ?? null,
                ],
                'account_number',
                [
                    'attribute' => 'contractor_id',
                    'value' => $model->contractor->full_name ? $model->contractor->full_name ?? null : $model->contractor->name ?? null,
                ],
                [
                    'attribute' => 'juridical_person_id',
                    'value' => $model->juridicalPerson->name ?? $model->juridicalPerson->full_name ?? null,
                ],
                [
                    'attribute' => 'responsible_id',
                    'value' => $model->responsible->fio ?? null,
                ],
                [
                    'attribute' => 'logistic_master_status_id',
                    'value' => $model->logisticMasterStatus->name ?? null,
                ],
                [
                    'attribute' => 'logistic_status_id',
                    'value' => $model->logisticStatus->name ?? null,
                ],
                'invoice_date:date',
                'payed_date:date',
            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), '_error');
        echo $e->getMessage();
    } ?>

</div>
