<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TypeOfParts */
?>
<div class="type-of-parts-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
