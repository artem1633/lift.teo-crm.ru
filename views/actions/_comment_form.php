<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Actions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="actions-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'contractor_id')->label(false)->widget(\kartik\select2\Select2::classname(), [
        'data' => \app\models\Functions::getContractorsList(),
        'options' => ['placeholder' => 'Контрагент'],
        'pluginOptions' => [
            'tags' => true,
            'allowClear' => true,
        ],
    ]); ?>



    <?= $form->field($model, 'description')->textarea(['rows' => 6])->label(false) ?>

    <?= $form->field($model, 'request_id')->label(false)->widget(\kartik\select2\Select2::classname(), [
        'data' => \app\models\Functions::getRequestIdsList(),
        'options' => ['placeholder' => 'Введите номер Заявки'],
        'pluginOptions' => [
            'tags' => true,
            'allowClear' => true,
        ],
    ]); ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
