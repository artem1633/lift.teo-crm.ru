<?php

use app\models\Functions;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Actions */
/* @var $form yii\widgets\ActiveForm */

$contractor_id = Yii::$app->request->get('contractor_id');

Yii::info('ID контрагента: ' . $contractor_id)
?>

<div class="actions-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'contractor_id')->hiddenInput(['value' => $contractor_id])->label(false) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6])->label(false) ?>

    <?= $form->field($model, 'request_id')->label(false)->widget(Select2::classname(), [
        'data' => Functions::getRequestIdsList(),
        'options' => ['placeholder' => 'Номер Заявки'],
        'pluginOptions' => [
            'tags' => true,
            'allowClear' => true,
        ],
    ]); ?>

    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
