<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Actions */
?>
<div class="actions-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
