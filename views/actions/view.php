<?php

use yii\widgets\DetailView;
use app\models\Functions;

/* @var $this yii\web\View */
/* @var $model app\models\Actions */
?>
<div class="actions-view">
    <div class="col-xs-12">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
//                'id',
                [
                    'attribute' => 'contractor_id',
                    'value' => function($data){
                        return Functions::getContractor($data -> contractor_id);
                    },
                    'format' => 'html'

                ],
                'description:ntext',
                'request_id',
                'created_date:datetime',
                'updated_date:datetime',
                [
                   'attribute' => 'created_by',
                    'value' => function($data){
                        return Functions::getCreatedBy($data -> created_by);
                    },
                    'format' => 'html'
                ],
            ],
        ]) ?>
    </div>

</div>
