<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Actions */

?>
<div class="actions-create">
    <?= $this->render('_comment_form', [
        'model' => $model,
    ]) ?>
</div>
