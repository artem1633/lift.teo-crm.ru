<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Functions;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_date',
        'value' => function($actions){
            return Html::a(date('d.m.Y H:i', strtotime($actions -> created_date)), ['actions-view', 'id' => $actions->id]);
        },
        'format' => 'html',
        'filter' => '<input class="form-control" type="date" name="ActionsSearch[created_date]" value="'.\yii\helpers\ArrayHelper::getValue($_GET, 'ActionsSearch.created_date').'">',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'updated_date',
        'value' => function($actions){
            return Html::a(date('d.m.Y H:i', strtotime($actions -> updated_date)), ['actions-view', 'id' => $actions->id]);
        },
        'format' => 'html',
        'filter' => '<input class="form-control" type="date" name="ActionsSearch[updated_date]" value="'.\yii\helpers\ArrayHelper::getValue($_GET, 'ActionsSearch.updated_date').'">',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'contractor_id',
//        'filter' => Functions::getContractorsList(),
//        'value' => function($data){
//            return Functions::getContractor($data -> contractor_id);
//        },
//        'format' => 'html'
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'contractorName',
        'value' => function($data){
            return Functions::getContractor($data -> contractor_id);
        },
        'format' => 'html'
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'manager_id',
//        'filter' => Functions::getManagersList(),
//        'value' => function($data){
//            return Functions::getManager($data -> manager_id);
//        },
//        'format' => 'html'
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'managerName',
        'value' => function($data){
            return Functions::getManager($data -> manager_id);
        },
        'format' => 'html'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'description',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'request_id',
        'value' => function($actions){
            return Html::a('Заявка № ' . $actions -> request_id, ['/request/view', 'id' => $actions -> request_id]);
        },
        'format' => 'html'
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created_by',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'], 
    ],

];   