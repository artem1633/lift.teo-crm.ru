<?php

use yii\widgets\DetailView;
use app\models\Functions;

/* @var $this yii\web\View */
/* @var $model app\models\Actions */
?>
<div class="actions-view">
    <div class="col-xs-3">
        <a href="<?=Yii::$app->request->referrer?>"><span class="fa fa-chevron-circle-left"></span>&nbsp;Назад</a>
        <a class="pull-right" href="/actions/update?id=<?=$model -> id?>"><span class="fa fa-pen-square"></span>&nbsp;Редактировать</a>
    </div>
    <div class="col-xs-6">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
//                'id',
                [
                    'attribute' => 'contractor_id',
                    'value' => function($data){
                        return Functions::getContractor($data -> contractor_id);
                    },
                    'format' => 'html'

                ],
                'description:ntext',
                'request_id',
                'created_date',
                'updated_date',
                [
                   'attribute' => 'created_by',
                    'value' => function($data){
                        return Functions::getCreatedBy($data -> created_by);
                    },
                    'format' => 'html'
                ],
            ],
        ]) ?>
    </div>
    <div class="col-xs-3"></div>

</div>
