<?php

/* @var $this yii\web\View */
/* @var $model app\models\DetailToRealization */
/* @var $realization_model app\models\Realization */

?>
<div class="detail-to-realization-create">
    <?= $this->render('_form', [
        'model' => $model,
        'realization_model' => $realization_model,
    ]) ?>
</div>
