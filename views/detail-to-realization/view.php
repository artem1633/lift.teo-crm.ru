<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DetailToRealization */
?>
<div class="detail-to-realization-view">

    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'realization_id',
                'detail_id',
                'number',
                'price',
                'total_amount',
            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), 'error');
    } ?>

</div>
