<?php

use app\models\Functions;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 

/* @var $this yii\web\View */
/* @var $realization_model app\models\Realization */
/* @var $searchModel app\models\DetailToRealizationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Реализация №' . $realization_model->id;
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="detail-to-realization-index">
    <div id="ajaxCrudDatatable">
        <?php
        try {
            echo GridView::widget([
                'id' => 'crud-datatable',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax' => true,
                'columns' => require(__DIR__ . '/_columns.php'),
                'toolbar' => [
                    [
                        'content' =>
                            Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create', 'realization_id' => $realization_model->id],
                                [
                                    'role' => 'modal-remote',
                                    'title' => 'Create new Detail To Realizations',
                                    'class' => 'btn btn-default'
                                ]) .
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                                ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Сброс таблицы']) .
                            '{toggleData}' .
                            Html::a('Назад к реализациям', ['/realization/index'], [
                                'class' => 'btn btn-default',
                                'data-pjax' => 0,
                            ])
//                            '{export}'
                    ],
                ],
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'type' => 'primary',
                    'heading' => '<i class="glyphicon glyphicon-list"></i> Список деталей',
//                    'before' => '<em>* Resize table columns just like a spreadsheet by dragging the column edges.</em>',
                    'after' => Functions::getBulkButtonWidget() .
                        '<div class="clearfix"></div>',
                ]
            ]);
        } catch (Exception $e) {
            Yii::error($e->getMessage(), '_error');
        } ?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
