<?php

use app\models\Nomenclature;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DetailToRealization */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="detail-to-realization-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'realization_id')->hiddenInput()->label(false) ?>
        <div class="row">
            <div class="col-xs-4">
                <?= Html::activeLabel($model, 'vendor_code') ?>
                <div class="input-group">
                    <?= Html::activeTextInput($model, 'vendor_code', [
                        'id' => 'vendor-code',
                        'class' => 'form-control'
                    ]) ?>
                    <span class="input-group-btn">
                    <?= Html::button('<i class="glyphicon glyphicon-search"></i>', [
                        'id' => 'check-btn',
                        'class' => 'btn btn-default'
                    ]); ?>
                </span>
                </div>
                <?= Html::error($model, 'vendor_code', [
                    'class' => 'help-block',
                    'id' => 'help-vendor-code',
                ]) ?>
            </div>
            <div class="col-xs-8">
                <?= Html::activeLabel($model, 'detail_id'); ?>
                <div class="input-group detail-group">
                    <?= Html::a('<i class="glyphicon glyphicon-eye-open"></i>',
                        ['/nomenclature/view', 'id' => $model->detail_id], [
                            'id' => 'view-btn',
                            'class' => 'btn btn-default',
                            'data-pjax' => 1,
                            'role' => 'modal-remote',
                        ]); ?>
                    <?= $form->field($model, 'detail_id')->widget(Select2::class, [
                        'data' => Nomenclature::getList(),
                        'options' => ['placeholder' => 'Выберите деталь'],
                        'pluginOptions' => [
                            'tags' => true,
                            'allowClear' => true,
                        ],
                    ])->label(false) ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'number')->textInput() ?>
            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'total_amount')->textInput() ?>
            </div>
        </div>

        <?php if (!Yii::$app->request->isAjax) { ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                    ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>

    </div>
<?php
$script = <<<JS
$(document).ready(function() {
    $(document).on('change', '#detailtorealization-detail_id', function() {
        var link = $('#view-btn');
        var href = link.attr('href');

        var value = '';
        if (href.includes('id=')){
            value = href.replace(/(id=)[^&]+/ig, '$1' + $(this).val());
        } else {
            //Добавляем параметр
            value = href + '?id=' + $(this).val();
        }
        link.attr('href', value);
    });
    
    $(document).on('click', '#check-btn', function(){
        var input_vendor = $('#vendor-code');
        var vendor_code = input_vendor.val();
        var help_block = $('#help-vendor-code');
        $.get(
            'get-id-by-vendor-code',
            {
                code: vendor_code
            },
            function(response) {
                console.log(response);
                if (response['error'] === 1){
                    help_block.html('<p class="text-danger">' + response['data'] + '</p>')
                } else {
                    help_block.html('');
                    $('#detailtorealization-detail_id').val(response['data']['id']);
                    $('#select2-detailtorealization-detail_id-container').html(response['data']['name']);
                }
            }
        )
    });
});
JS;

$this->registerJs($script);