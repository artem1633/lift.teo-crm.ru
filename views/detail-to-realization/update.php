<?php

/* @var $this yii\web\View */
/* @var $model app\models\DetailToRealization */
?>
<div class="detail-to-realization-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
