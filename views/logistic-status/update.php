<?php

/* @var $this yii\web\View */
/* @var $model app\models\LogisticStatus */
?>
<div class="logistic-status-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
