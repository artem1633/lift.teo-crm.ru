<?php

use app\models\Functions;
use app\models\Users;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ContractorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '';
//$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

$export = Users::isAdmin() ? '{export}': '';

?>
    <div class="contractor-index">
        <div id="ajaxCrudDatatable">
            <?= GridView::widget([
                'id' => 'crud-datatable',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax' => true,
                'responsiveWrap' => false,
                'options' => ['style' => 'font-size:12px;'],
                'columns' => require(__DIR__ . '/_columns.php'),
                'toolbar' => [
//                    ['content' =>
//                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
//                            ['role' => 'modal-remote', 'title' => 'Создать нового Контрагента', 'class' => 'btn btn-default']) .
//                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
//                            ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Сбросить сетку']) .
//                        '{toggleData}'
//                    ],
                ],
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'hover' => true,
                'panel' => [
                    'type' => 'primary',
                    'heading' => '<i class="glyphicon glyphicon-list"></i> Список Контрагентов',
                    'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
                            ['role' => 'modal-remote', 'title' => 'Создать нового Контрагента', 'class' => 'btn btn-default']) .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                            ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Сбросить сетку']) .
                        '{toggleData}' . $export,
                    'after' => Functions::getBulkButtonWidget() .

                        '<div class="clearfix"></div>',
                ]
            ]) ?>
        </div>
    </div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>

    <!--contractor-index-->
    <!--btn-toolbar kv-grid-toolbar toolbar-container-->
<?php
//$script = <<< JS
//    $(document).ready(function() {
//
//      var grid_buttons = $('.contractor-index .btn-toolbar.kv-grid-toolbar.toolbar-container');
//      console.log('grid_buttons');
//      console.log(grid_buttons);
//
//      grid_buttons.removeClass('pull-right').addClass('pull-left');
//
//    });
//JS;
//$this->registerJs($script);
?>