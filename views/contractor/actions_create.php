<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Actions */

?>
<div class="actions-create">
    <?= $this->render('actions_form', [
        'model' => $model,
    ]) ?>
</div>
