<?php

use app\models\Functions;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Contractor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contractor-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-md-6">
        <?= $form->field($model, 'name')->textInput([
            'maxlength' => true,
            'placeholder' => 'Наименование'
        ])->label(false) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'full_name')->textInput([
            'maxlength' => true,
            'placeholder' => 'Полное нименование'
        ])->label(false) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'city_id')->label(false)->widget(Select2::class, [
            'data' => Functions::getCitiesList(),
            'options' => ['placeholder' => 'Город'],
            'pluginOptions' => [
                'tags' => true,
                'allowClear' => true,
            ],
        ]); ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'address')->textInput(['maxlength' => true, 'placeholder' => 'Адрес'])->label(false) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'phone')->textInput(['maxlength' => true, 'placeholder' => 'Телефон'])->label(false) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'contact_person')->textInput([
            'maxlength' => true,
            'placeholder' => 'Контактное лицо'
        ])->label(false) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'manager_id')->label(false)->widget(Select2::class, [
            'data' => Functions::getManagersList(),
            'options' => ['placeholder' => 'Менеджер '],
            'pluginOptions' => [
                'tags' => true,
                'allowClear' => true,
            ],
        ]); ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => 'Email'])->label(false) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'inn')->textInput(['maxlength' => true, 'placeholder' => 'ИНН'])->label(false) ?>
    </div>
    <div class="col-md-3">
        <?=
        $form->field($model, 'do_not_touch')
            ->checkbox([
                'label' => '&nbsp;Не Трогать',
                'labelOptions' => [
                    'style' => 'padding-left:20px;'
                ],
                'disabled' => false
            ]);
        ?>
    </div>
    <div class="col-md-12">
        <?= $form->field($model, 'is_active')->dropDownList($model::getStatusList()) ?>
    </div>
    <div class="col-md-12">
        <?= $form->field($model, 'requisites')->textarea(['rows' => 6, 'placeholder' => 'Реквизиты'])->label(false) ?>
    </div>

    <div class="col-md-12">
        <?= $form->field($model, 'comment')->textarea(['rows' => 6, 'placeholder' => 'Комментарии'])->label(false) ?>
    </div>


    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать',
                    ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
