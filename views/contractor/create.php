<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Contractor */

?>
<div class="contractor-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
