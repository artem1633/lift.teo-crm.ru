<?php
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'contractor_id',
//        'filter' => \app\models\Functions::getContractorsList(),
//        'value' => function($data){
//            return \app\models\Functions::getContractor($data -> contractor_id);
//        },
//        'format' => 'html'
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'description',

    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'request_id',
        'filter' => \app\models\Functions::getRequestsList(),
        'value' => function($data){
            return \app\models\Functions::getRequest($data -> request_id);
        },
        'format' => 'html'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'format' => 'datetime',
        'attribute'=>'created_date',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'format' => 'datetime',
        'attribute'=>'updated_date',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            if ($action === 'delete') {
                $url = Url::to(['/actions/delete', 'id' => $key]);
                return $url;
            }
            if ($action === 'update') {
                $url = Url::to(['/actions/update', 'id' => $key]);
                return $url;
            }
            if ($action === 'view') {
                $url = Url::to(['/actions/view', 'id' => $key]);
                return $url;
            }
            return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Редактирование', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удаление',
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Вы уверены?',
                          'data-confirm-message'=>'Вы уверены что хотите удалить все эти элементы?'],
    ],

];