<?php

use app\models\Functions;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use yii\helpers\Html;
use johnitvn\ajaxcrud\BulkButtonWidget;
use johnitvn\ajaxcrud\CrudAsset;

/* @var $this yii\web\View */
/* @var $model app\models\Contractor */
/* @var $actionsDataProvider \yii\data\ActiveDataProvider */
?>

<?php CrudAsset::register($this); ?>


<div class="contractor-view">
    <div class="contractor_view_table">
        <div class="col-xs-3">
            <a href="<?= Yii::$app->request->referrer ?>"><span class="fa fa-chevron-circle-left"></span>&nbsp;Назад</a>
            <a class="pull-right" href="/contractor/update?id=<?= $model->id ?>"><span class="fa fa-pen-square"></span>&nbsp;Редактировать</a>
        </div>
        <div class="col-xs-6">
            <?php
            try {
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'name',
                        'full_name',
                        'inn',
                        [
                            'attribute' => 'city_id',
                            'value' => function ($data) {
                                if (isset($data->city_id)) {
                                    return Functions::getCity($data->city_id);
                                }
                                return null;
                            },
                        ],
                        'address',
                        'phone',
                        'contact_person',
                        'email:email',
                        'requisites:ntext',
                        'comment:ntext',
                        [
                            'attribute' => 'manager_id',
                            'value' => function ($data) {
                                return Functions::getManager($data->manager_id);
                            },
                            'format' => 'html'
                        ],
                        [
                            'attribute' => 'do_not_touch',
                            'value' => function ($data) {
                                return !$data->do_not_touch ? 'Выкл' : 'Вкл';
                            },
                        ],
                        'created_date:datetime',
                        'updated_date:datetime',
                        [
                            'attribute' => 'created_by',
                            'value' => function ($data) {
                                return Functions::getCreatedBy($data->created_by);
                            },
                            'format' => 'html'
                        ],
                    ],
                ]);
            } catch (Exception $e) {
                Yii::error($e->getMessage(), '_error');
            } ?>
        </div>
        <div class="col-xs-3"></div>
    </div>

</div>
<div class="contractor_actions">
    <div class="row">
        <div class="col-xs-12">
            <div class="request_comment">

                <?php
                try {
                    echo GridView::widget([
                        'id' => 'crud-datatable',
                        'dataProvider' => $actionsDataProvider,
                        //                'filterModel' => $searchModel,
                        'pjax' => true,
                        //                    'responsiveWrap' => false,
                        'options' => ['style' => 'font-size:12px;'],
                        'columns' => require(__DIR__ . '/_action_columns.php'),
                        'toolbar' => [
                            [
                                'content' =>
                                    Html::a('<i class="fa fa-plus">&nbsp;Новое Событие</i>',
                                        ['/actions/create?contractor_id=' . $model->id],
                                        [
                                            'role' => 'modal-remote',
                                            'title' => 'Добавить Событие',
                                            'class' => 'btn btn-primary',
                                            'style' => 'background-color:#00a65a;color:white'
                                        ]) .
                                    '{toggleData}'
                            ],
                        ],
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'panel' => [
                            'type' => 'primary',
                            'heading' => "<h4>События {$model->name}</h4>",
                            'after' => BulkButtonWidget::widget([
                                    'buttons' => Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить Все',
                                        ["/action/bulkdelete"],
                                        [
                                            "class" => "btn btn-danger btn-xs",
                                            'role' => 'modal-remote-bulk',
                                            'data-confirm' => false,
                                            'data-method' => false,// for overide yii data api
                                            'data-request-method' => 'post',
                                            'data-confirm-title' => 'Вы уверены?',
                                            'data-confirm-message' => 'Вы уверены что хотите удалить все эти элементы?'
                                        ]),
                                ]) .
                                '<div class="clearfix"></div>',
                        ]
                    ]);
                } catch (Exception $e) {
                    Yii::error($e->getMessage(), '_error');
                } ?>
            </div>
        </div>
    </div>
</div>

