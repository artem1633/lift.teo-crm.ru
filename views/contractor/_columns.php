<?php

use app\models\Contractor;
use app\models\Functions;
use app\models\Users;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
if (Users::isAdmin()){
    $template = '{view} {update} {delete}';
} else {
    $template = '{view} {update}';
}

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'name',
        'contentOptions' => ['class' => 'kv-align-middle'],
        'value' => function (Contractor $data) {
            if ($data->is_active == $data::STATUS_DEACTIVATE){
                $badge = '<div class="text-right" style="margin-bottom: 5px;"><span class="badge badge-pill badge-default">Не активен</span></div>';
            } else {
                $badge = '';
            }
            return $badge . Html::a($data->name, ['view', 'id' => $data->id], ['data-pjax' => 0]) ;
        },
        'format' => 'raw',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'full_name',
//        'contentOptions' => function ($model, $key, $index, $column) {
//            return ['class' => 'kv-align-middle'];
//        },
        'contentOptions' => ['class' => 'kv-align-middle'],
        'value' => function (Contractor $data) {
            return Html::a($data->full_name, ['view', 'id' => $data->id], ['data-pjax' => 0]);
//            return '<a href="/contractor/view?id=' . $data->id . '" class="">' . $data->full_name . '</a>';
        },
        'format' => 'raw',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'inn',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'do_not_touch',
        'contentOptions' => ['class' => 'kv-align-middle'],
        'filter' => array("1" => "Не трогать"),
        'value' => function ($data) {
            return !$data->do_not_touch ? '' : '<span style="padding-top: 1px; padding-bottom: 2px;" class="btn btn-sm btn-danger color_shadow">Не трогать</span>';
        },
        'format' => 'html',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'city_id',
        'filter' => Functions::getCitiesList(),
        'value' => function ($data) {
            if (isset($data->city_id)){
                return Functions::getCity($data->city_id);
            }
            return null;
        },
        'contentOptions' => ['class' => 'kv-align-middle'],
//        'filter'=>\yii\helpers\ArrayHelper::map(\app\models\Cities::find()->asArray()->all(), 'id', 'name'),
//        'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'city_id', \yii\helpers\ArrayHelper::map(\app\models\Cities::find()->asArray()->all(), 'id', 'name'),['class'=>'form-control', /*'prompt' => 'Select Category'*/]),
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'address',
//    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'phone',
        'contentOptions' => ['class' => 'kv-align-middle'],
//        'contentOptions'=>['style'=>'width: 20px;'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'contact_person',
        'contentOptions' => ['class' => 'kv-align-middle'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'email',
        'contentOptions'=>['class' => 'kv-align-middle'],
    ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'requisites',
//     ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'comment',
//     ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'manager_id',
        'contentOptions'=>['class' => 'kv-align-middle'],
        'filter' => Functions::getManagersList(),
//         'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'city_id', \yii\helpers\ArrayHelper::map(\app\models\Users::find()->where(['permission' => 'manager'])->asArray()->all(), 'id', 'fio'),['class'=>'form-control', /*'prompt' => 'Select Category'*/]),
        'value' => function ($data) {
            return Functions::getManager($data->manager_id);
        },
        'format' => 'html'
    ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'created_date',
//     ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'updated_date',
        'format' => 'datetime',
        'contentOptions'=>['class' => 'kv-align-middle'],
        'filter' => '<input class="form-control" type="date" name="ContractorSearch[updated_date]" value="'.\yii\helpers\ArrayHelper::getValue($_GET, 'ContractorSearch.updated_date').'">',
    ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'created_by',
//     ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'template' => $template,
        'urlCreator' => function ($action, $model, $key) {
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['title' => 'Просмотр', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Редактирование', 'data-toggle' => 'tooltip'],
        'deleteOptions' => [
            'role' => 'modal-remote', 'title' => 'Удаление',
            'data-confirm' => false, 'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Вы уверены?',
            'data-confirm-message' => 'Вы уверены что хотите удалить все эти элементы?'
        ],
//        'deleteOptions' => function($model, $key, $index) {
//                return [
//                    'role' => 'modal-remote', 'title' => 'Удаление',
//                    'data-confirm' => false, 'data-method' => false,// for overide yii data api
//                    'data-request-method' => 'post',
//                    'data-toggle' => 'tooltip',
//                    'data-confirm-title' => 'Вы уверены?',
//                    'data-confirm-message' => 'Вы уверены что хотите удалить все эти элементы?'
//                ];
//        }

    ],

];   