<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\NomenclatureGroup */
?>
<div class="nomenclature-group-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
