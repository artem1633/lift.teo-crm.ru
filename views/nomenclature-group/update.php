<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NomenclatureGroup */
?>
<div class="nomenclature-group-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
