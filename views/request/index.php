<?php

use app\models\Functions;
use app\models\Users;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 

/* @var $this yii\web\View */
/* @var $searchModel app\models\RequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '';
//$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

$btn_all_detail = '';
if (Users::isManager()){
    $nomenclature_url = '/nomenclature/manager-nomenclature';
} elseif (Users::isAdmin()){
    $nomenclature_url = '/nomenclature';
} else {
    $nomenclature_url = '/nomenclature/tech-nomenclature';
}

?>
<div class="request-index">
    <div id="ajaxCrudDatatable">
        <?php
        try {
            echo GridView::widget([
                'id' => 'crud-datatable',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax' => true,
                'responsiveWrap' => false,
                'options' => ['style' => 'font-size:12px;'],
                'columns' => require(__DIR__ . '/_columns.php'),
                'toolbar' => [
                    [
                        'content' =>
                            Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
                                [
                                    'role' => 'modal-remote',
                                    'title' => 'Создать новую заявку',
                                    'class' => 'btn btn-default'
                                ]) .
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                                ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Сбросить сетку']) .
                            '{toggleData}'
                    ],
                ],
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'type' => 'primary',
                    'heading' => '<span class="">Заявки </span>' .
                        //                    Html::a('<i class="fa fa-list">&nbsp;Все Детали</i>', [$nomenclature_url],
                        //                        ['title'=> 'Все Детали','class'=>'btn btn-info nomenclature_view', 'style'=>'color:white']),
                        Html::a('<i class="fa fa-list">&nbsp;Все Детали</i>', ['/order-part/request-details'],
                            [
                                'title' => 'Все Детали',
                                'class' => 'btn btn-info nomenclature_view',
                                'style' => 'color:white'
                            ]),
                    //                'before'=>'<em>* Resize table columns just like a spreadsheet by dragging the column edges.</em>',
                    'after' => Functions::getBulkButtonWidget() .
                        '<div class="clearfix"></div>',
                ]
            ]);
        } catch (Exception $e) {
            Yii::error($e->getMessage(), '_error');
            echo $e->getMessage();
        } ?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

<?php
$script = <<< JS
$( document ).ready(function() {
    $(document).on('click','#add_part', function() {
        $.ajax({
                url: '/request/create',
                data: {
                    add_part: true
                },
                success: function (html) {
                    console.log('html');
                    console.log(html);
                }, 
                fail: function(data) {
                   console.log('error data');
                    console.log(data);
                }
        });
    });
});
JS;
//$this->registerJs($script);
?>