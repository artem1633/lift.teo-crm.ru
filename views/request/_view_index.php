<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '';
//$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="request-index">
    <div id="ajaxCrudDatatable">
        <?php
        try {
            echo GridView::widget([
                'id' => 'crud-datatable',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax' => true,
                'responsiveWrap' => false,
                'options' => ['style' => 'font-size:12px;'],
                'columns' => require(__DIR__ . '/_columns.php'),
                'toolbar' => [
                    [
                        'content' =>
                            Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
                                [
                                    'role' => 'modal-remote',
                                    'title' => 'Create new Requests',
                                    'class' => 'btn btn-default'
                                ]) .
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                                ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Сбросить сетку']) .
                            '{toggleData}'
                    ],
                ],
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'type' => 'primary',
                    'heading' => '<i class="glyphicon glyphicon-list"></i> Requests listing',
                    'before' => '<em>* Resize table columns just like a spreadsheet by dragging the column edges.</em>',
                    'after' => BulkButtonWidget::widget([
                            'buttons' => Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить Все',
                                ["bulkdelete"],
                                [
                                    "class" => "btn btn-danger btn-xs",
                                    'role' => 'modal-remote-bulk',
                                    'data-confirm' => false,
                                    'data-method' => false,// for overide yii data api
                                    'data-request-method' => 'post',
                                    'data-confirm-title' => 'Вы уверены?',
                                    'data-confirm-message' => 'Вы уверены что хотите удалить все эти элементы?'
                                ]),
                        ]) .
                        '<div class="clearfix"></div>',
                ]
            ]);
        } catch (Exception $e) {
            Yii::error($e->getMessage(), '_error');
            echo $e->getMessage();
        } ?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

<?php
$script = <<< JS
// $(':file').filestyle({
//  
// });
$( document ).ready(function() {
    $(document).on('click','.part_remove', function(event) {
        $(event.target).closest('.request-multi-form').remove();
    });
    $(document).on('click','.part_add', function(event) {
        var index = $('.multi_form .request-multi-form').length - 1;
        console.log('index');
        console.log(index);
        
        $.ajax({
                url: '/request/create-part',
                data: {
                    index: index,
                },
                async: false,
                // type: 'json',
                type: 'get',
                success: function (html) {
                    console.log('html');
                    console.log(html);
                    $('.multi_form').append(html);
                }, 
                fail: function(data) {
                   console.log('error data');
                    console.log(data);
                }
        });
          
    });
    $(document).on('change','.nomenclature_id select', function(event) {
        // $('.nomenclature_id').find(":selected").val();
        var nomenclature_parent = $(this).parent();
        var request_multi_form = $(nomenclature_parent).closest('.request-multi-form');
        console.log('parent');
        console.log($(nomenclature_parent));
        var nomenclature = $(nomenclature_parent).find(":selected");
        console.log('nomenclature');
        console.log(nomenclature);
        var nomenclature_id = $(nomenclature).val();
        console.log('nomenclature_id');
        console.log(nomenclature_id);
        var vendor_code = $(request_multi_form).find('.vendor_code');
        var vendor_code_input = $(vendor_code).find('input[type="text"]');
        console.log('vendor_code_input 1');
        console.log(vendor_code_input.length);
        if (vendor_code_input.length == 0) {
            vendor_code_input = vendor_code;
            console.log('vendor_code_input 2');
            console.log(vendor_code_input.length);
        }
        //
        if (nomenclature_id.length > 0) {
            //ajax
            $.ajax({
                url: '/request/get-nomenclature',
                data: {
                    id: nomenclature_id,
                },
                async: false,
                // type: 'json',
                type: 'get',
                success: function (vendor_code) {
                    console.log('vendor_code');
                    console.log(vendor_code);
                    vendor_code_input.val(vendor_code);
                }, 
                fail: function(data) {
                   console.log('error data');
                    console.log(data);
                }
        });
        }
    });
    
});

JS;
$this->registerJs($script);
?>