<?php
use yii\helpers\Url;
use app\models\Functions;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'comment',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'manager_id',
//        'filter' => function($data){
//            return Functions::getManager($data -> manager_id);
//        },
        'value' => function($data){
            return Functions::getManager($data -> manager_id);
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_date',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            if ($action === 'delete') {
                $url = Url::to(['/comment/delete', 'id' => $key]);
                return $url;
            }
            if ($action === 'update') {
                $url = Url::to(['/comment/update', 'id' => $key]);
                return $url;
            }
            if ($action === 'view') {
                $url = Url::to(['/comment/view', 'id' => $key]);
                return $url;
            }
            return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Редактирование', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удаление',
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Вы уверены?',
                          'data-confirm-message'=>'Вы уверены что хотите удалить все эти элементы?'],
    ],

];