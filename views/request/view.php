<?php

use app\models\Functions;
use app\models\Users;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset;
use yii\helpers\Html;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\models\OrderStatus;
use kartik\file\FileInput;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Request */
/* @var $requestImages array */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $commentDataProvider \yii\data\ActiveDataProvider */

$order_parts = Functions::getOrderParts($model->id);
$order_status = OrderStatus::findOne($model->id);

Yii::$app->session->set('request_id', $model->id); //Сохраняем заявку, чтобы при создании детали соорентироваться куда сохранить вновь созданную деталь

$additional_info = '<p style="font-size: 1rem;">Контрагент: '
    . Functions::getContractor($model->contractor_id) . ' | '
    . Functions::getUserRole($model->created_by) . ': '
    . Functions::getCreatedBy($model->created_by) . ' | '
    . 'Дата создания: ' . Yii::$app->formatter->asDatetime($model->created_date) . ' | '
    . 'Дата изменения: ' . Yii::$app->formatter->asDatetime($model->updated_date)// date('d.m.Y H:i', strtotime($model->updated_date))
    . '</p>';

if (Users::isManager() || Users::isTech()) {
    $delete_order_btn = '';
    $bulkButton_widget = '';
} else {
    $delete_order_btn = Html::a('<i class="fa fa-remove">&nbsp;Удалить Заявку</i>', ['/request/delete?id=' . $model->id],
        [
            'data-pjax' => 0,
            'title' => 'Удалить Заявку',
            'class' => 'btn btn-default',
            'style' => 'background-color: #dd4b39; color: white;',
        ]);
    $bulkButton_widget = BulkButtonWidget::widget([
        'buttons' => Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить Все',
            ["/order-part/bulkdelete"],
            [
                "class" => "btn btn-danger btn-xs",
                'role' => 'modal-remote-bulk',
                'data-confirm' => false, 'data-method' => false,// for overide yii data api
                'data-request-method' => 'post',
                'data-confirm-title' => 'Вы уверены?',
                'data-confirm-message' => 'Вы уверены что хотите удалить все эти элементы?'
            ]),
    ]);
}

if (Users::isTech()) {
    $add_detail_btn = '';
    $change_request_btn = '';
} else {
    $add_detail_btn = Html::a('<i class="fa fa-plus">&nbsp;Добавить Деталь</i>', ['/order-part/create', 'request_id' => $model->id],
        ['role' => 'modal-remote', 'title' => 'Добавить Деталь', 'class' => 'btn btn-default', 'style' => 'background-color:#00a65a;color:white']);
    $change_request_btn = Html::a('<i class="fa fa-edit">&nbsp;Редактировать Заявку</i>', ['/request/update', 'id' => $model->id],
        ['role' => 'modal-remote', 'title' => 'Редактировать Заявку', 'class' => 'btn btn-default', 'style' => 'background-color:#337ab7;color:white']);
}
?>
<?php CrudAsset::register($this); ?>

<?php
$order_statuses = OrderStatus::find()->all();
$current_order_status = OrderStatus::find()->where(['id' => $model->main_status_id])->one();


foreach ($order_statuses as $order_status) {
    $order_status->name = '<span style="padding-top: 3px; padding-bottom: 2px; background-color:' . $order_status->color . '" class="btn btn-sm color_shadow fa ' . $order_status->icon . '">&nbsp; ' . $order_status->name . '</span>';
}

$order_status_list = ArrayHelper::map($order_statuses, 'id', 'name');


?>
<div class="request-view-index">
    <div class="row">
        <div class="request_change">
            <div class="col-xs-12">
                Заявка №
                <?= Html::input('text', 'request_change', $model->id, [
                    'class' => 'text-center',
                    'placeholder' => 'Заявка',
                ]); ?>

                <span class="btn btn-info fa fa-arrow-circle-right request_change_btn"></span>
                <input id="model_id" name="model_id" type="hidden" value="<?= $model->id ?>">
                <?php
                if (!empty($model->main_status_id)) { ?>
                    <input id="main_status_id" name="model_id" type="hidden" value="<?= $model->main_status_id ?>">
                <?php } ?>
            </div>
            <br><br>
        </div>
    </div>
    <!-- Panels -->
    <div class="row">
        <div class="col-xs-10">
            <div class="row">
                <input class="check_manager" type="hidden" value="<?= Functions::checkManager() ?>">
                <div class="col-xs-12">
                    <div id="ajaxCrudDatatable">
                        <?php
                        try {
                            echo GridView::widget([
                                'id' => 'crud-datatable',
                                'dataProvider' => $dataProvider,
                                //                'filterModel' => $searchModel,
                                'pjax' => true,
                                'responsiveWrap' => false,
                                'options' => ['style' => 'font-size:12px;'],
                                'columns' => require(__DIR__ . '/_view_columns.php'),
                                'hover' => true,
                                'toolbar' => [
                                    [
                                        'content' =>
                                            $additional_info .
                                            '<div class="btn-group" style="display: flex;">' .
                                            Html::a('<i class="fa fa-chevron-circle-left">&nbsp;Назад</i>',
                                                ['/request'],
                                                [
                                                    'title' => 'Назад',
                                                    'class' => 'btn btn-default',
                                                    'data-pjax' => 0,
                                                    'style' => 'background-color: #3c8dbc; color: white;'
                                                ]) .
                                            $add_detail_btn .
                                            $change_request_btn .
                                            $delete_order_btn .
                                            Select2::widget([
                                                'name' => 'main_status_dropdown',
                                                'data' => Functions::getMainStatusesList(),
                                                'value' => $model->main_status_id,
                                                'theme' => Select2::THEME_BOOTSTRAP,
                                                'hideSearch' => true,
                                                'size' => 'md',
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                                'options' => [
                                                    'placeholder' => 'Выберите статус',
                                                    'multiple' => false,
                                                    'onchange' => '$.post(
                                                         "' . Url::toRoute('/request/change-main-order-status') . '", 
                                                            {
                                                                status_id: $(this).val(),
                                                                model_id: ' . $model->id . ',
                                                            },
                                                            )
                                                            .done(function( color ) {
                                                                //console.log( "Status Data Loaded: " + color );
                                                                 var selector = $("#select2-w0-container");
                                                                 var parent_selector = $("#select2-w0-container").parent(".select2-selection");
                                                                 parent_selector.css("background-color", color);
                                                                 selector.css("color", "white");
                                                              });'
                                                ],
                                            ]) .
                                            Select2::widget([
                                                'name' => 'additional_status_dropdown',
                                                'data' => Functions::getAdditionalStatusesList(),
                                                'value' => $model->additional_status_id,
                                                'theme' => Select2::THEME_BOOTSTRAP,
                                                'hideSearch' => true,
                                                'size' => 'md',
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                                'options' => [
                                                    'placeholder' => 'Выберите статус',
                                                    'multiple' => false,
                                                    'onchange' => '$.post(
                                                         "' . Url::toRoute('/request/change-additional-order-status') . '", 
                                                            {
                                                                status_id: $(this).val(),
                                                                model_id: ' . $model->id . '
    
                                                            },
                                                            ).done(function( color ) {
                                                                //console.log( "Additional Status Data Loaded: " + color );
                                                                 var selector = $("#select2-w1-container");
                                                                 var parent_selector = $("#select2-w1-container").parent(".select2-selection");
                                                                 parent_selector.css("background-color", color);
                                                                 selector.css("color", "white");
                                                              });'
                                                ],
                                            ]) .
                                            //                                    Html::dropDownList('status_dropdown', $model->main_status_id, Functions::getMainStatusesList(),
                                            //                                        [
                                            //                                            'title' => 'Статус', 'class' => 'btn btn-info'
                                            //                                        ]) .
                                            //                                    Html::dropDownList('additional_status_dropdown', $model->additional_status_id, Functions::getAdditionalStatusesList(),
                                            //                                        ['title' => 'Статус менеджера', 'class' => 'btn btn-primary']) .
                                            //                                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                                            //                                            ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Сбросить сетку']).
                                            '</div>' .
                                            '{toggleData}' //.
                                        //                                    '{export}'

                                    ],

                                ],
                                'striped' => true,
                                'condensed' => true,
                                'responsive' => true,
                                'panel' => [
                                    'type' => 'primary',
                                    //                            'heading' => $additional_info,
                                    //                    'heading' => '<i class="glyphicon glyphicon-list"></i> Requests listing',
                                    'after' => $bulkButton_widget .
                                        '<div class="clearfix"></div>',
                                ]
                            ]);
                        } catch (Exception $e) {
                            Yii::error($e->getMessage(), '_error');
                            echo $e->getMessage();
                        } ?>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="request_comment">
                        <h4>Коментарии</h4>
                        <?php
                        try {
                            echo GridView::widget([
                                'id' => 'crud-datatable',
                                'dataProvider' => $commentDataProvider,
                                //                'filterModel' => $searchModel,
                                'pjax' => true,
                                'responsiveWrap' => false,
                                'options' => ['style' => 'font-size:12px;'],
                                'columns' => require(__DIR__ . '/_action_columns.php'),
                                'toolbar' => [
                                    [
                                        'content' =>
                                            Html::a('<i class="fa fa-plus">&nbsp;Новый Коментарий</i>',
                                                ['/actions/comment-create', 'request_id' => $model->id],
                                                [
                                                    'role' => 'modal-remote',
                                                    'title' => 'Добавить Деталь',
                                                    'class' => 'btn btn-default',
                                                    'style' => 'background-color:#00a65a;color:white'
                                                ]) .
                                            '{toggleData}' //.
                                        //                                    '{export}'
                                    ],
                                ],
                                'striped' => true,
                                'condensed' => true,
                                'responsive' => true,
                                'panel' => [
                                    'type' => 'primary',
                                    //                    'heading' => '<i class="glyphicon glyphicon-list"></i> Requests listing',
                                    //                    'before'=>'<em>* Resize table columns just like a spreadsheet by dragging the column edges.</em>',
                                    'after' => $bulkButton_widget .
                                        '<div class="clearfix"></div>',
                                ]
                            ]);
                        } catch (Exception $e) {
                            Yii::error($e->getMessage(), '_error');
                            echo $e->getMessage();
                        } ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- Images -->
        <div class="col-xs-2 text-center">
            <div class="add_request_image text-center">
                <!--                Modal-->

                <?php
                Modal::begin([
                    'toggleButton' => [
                        'label' => '<i class="fa fa-plus text-success"><span class="add_first_image"> Добавить изображения.</span></i>', 'class' => 'btn btn-default btn-block'
                    ],
                ]);
                $form1 = ActiveForm::begin([
                    'options' => [
                        'enctype' => 'multipart/form-data'
                    ]
                ]);
                try {
                    echo FileInput::widget([
                        'name' => 'file',
                        'pluginOptions' => [
                            'browseClass' => 'btn btn-success',
                            'uploadClass' => 'btn btn-info',
                            'removeIcon' => '<i class="glyphicon glyphicon-trash"></i> ',
                            'uploadUrl' => Url::to([
                                '/request/file-upload',
                                'model_name' => 'request',
                                'id' => $model->id
                            ]),
                        ],
                        'options' => [
                            'multiple' => true,
                        ]
                    ]);
                } catch (Exception $e) {
                    Yii::error($e->getMessage(), '_error');
                    echo $e->getMessage();
                }
                ActiveForm::end();
                Modal::end();
                ?>
                <!--                Modal end-->
            </div>

            <?php $cnt = 0;
            foreach ($requestImages as $requestImage) { ?>
                <div class="file_location text-center" style="display: flex; flex-direction: column;">
                    <input class="model_id" type="hidden" value="<?= $requestImage['id'] ?>">
                    <input class="model_name" type="hidden" value="<?= $requestImage['model_name'] ?>">
                    <?php if (!empty($requestImage['files_location'])) {
                        foreach ($requestImage['files_location'] as $file) { ?>
                            <div class="col-xs-3 filter-item"
                                 style="width: 100%; height: 100%;">
                                <a data-fancybox="gallery-side"
                                   href="<?= $file //Удалить '/web'. т.к. на серваке не бычит         ?>">
                                    <img class="request_image" style="width: 100%; padding: 10px 0;"
                                         src="<?= Functions::getThumbnailPath($file) //Удалить '/web'. т.к. на серваке не бычит         ?>"
                                         alt="image">
                                </a>
                                <?php if (!Users::isTech()) { ?>
                                    <span class="btn btn fa fa-remove text-danger remove_request_image"
                                          style="position: absolute; right: 10px; top: 15px;"></span>
                                <?php } ?>
                                <!--                                        <h5>-->
                                <?php // echo $requestImage['description'] ?><!--</h5>-->
                                <!--                                    <div class="col-xs-2">-->
                                <!--                                        <div class="add_request_image">-->
                                <!--                                            --><?php
                                //                                            Modal::begin([
                                //                                                'toggleButton' => [
                                //                                                    'label' => '<i class="fa fa-plus text-success"></i>', 'class' => 'btn btn-default'
                                //                                                ],
                                //                                            ]);
                                //                                            $form1 = ActiveForm::begin([
                                //                                                'options' => [
                                //                                                    'enctype' => 'multipart/form-data'
                                //                                                ]
                                //                                            ]);
                                //                                            echo FileInput::widget([
                                //                                                'name' => 'file',
                                //                                                'pluginOptions' => [
                                //                                                    'browseClass' => 'btn btn-success',
                                //                                                    'uploadClass' => 'btn btn-info',
                                //                                                    'removeIcon' => '<i class="glyphicon glyphicon-trash"></i> ',
                                //                                                    'uploadUrl' => Url::to(['/request/file-upload?model_name=' . $requestImage['model_name'] . '&id=' . $requestImage['id']]),
                                //                                                ],
                                //                                                'options' => [
                                //                                                    'multiple' => true,
                                //                                                ]
                                //                                            ]);
                                //                                            ActiveForm::end();
                                //                                            Modal::end();
                                //                                            ?>
                                <!--                                        </div>-->
                                <!--                                    </div>-->
                            </div>
                        <?php } ?>
                    <?php } else {
                        $cnt++;
                    } ?>

                </div>
            <?php }
            //            if ($cnt == count($requestImages)) { ?>

            <!--            --><?php //} ?>
        </div>
        <div id="h-images" class="hidden"></div>
        <!-- /Images -->
    </div>
    <!-- /Panels -->
</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>

<?php
$script = <<< JS


$(document).ready(function() {
    
    changeColor();
    preloadPhoto();
    
    //Раскрашиваем Select2
    function changeColor(){ 
        var url = "/request/get-statuses-color";
        $.post(
            url, 
            {
                model_id: $model->id
            },
            )
            .done(function( colors ) {
                // console.log( "Status Data Loaded: " + colors[0] );
                // console.log( "Adv Status Data Loaded: " + colors[1] );
                setColor("#select2-w0-container", colors[0]);
                setColor("#select2-w1-container", colors[1]);
            });
    }
    
    function setColor(id, color) {
                var status = $(id);
                var parent_status = $(id).parent(".select2-selection");
                parent_status.css("background-color", color);
                status.css("color", "white");
    }

    function preloadPhoto() {
      //Подгружаем доп фотки для позиций заявки
      var url = "/request/get-additional-photo";
      var fancys = $("a[data-fancybox*='gallery-']");
      
      // console.log(fancys);
      
      var out = [];
      var i = 0;
      fancys.each(function(index, element){
          // console.log(index + ': ' + $(element).attr("data-fancybox"));
          out[i] = $(element).attr("data-fancybox");
          i++;
      });
    
      $.post(
            url, 
            {
               "data[]": out
            },
            )
            .done(function( result ) {
                // console.log( "Data Loaded: " + result );
                $("#h-images").html(result);
            });
      }
      //
      $(document).on ('click', '.kv-editable-reset', function(){
          // alert('Clear!!!!');
      });
    
    $(document).on('click', '.reserve-btn', function(){
        let btn = $(this);
        let url = btn.attr('data-url')
        let color_class = '';
        let btn_text = btn.text();
        
        let num = btn.parents('td').prev().find('button').html();
        let total_reserve_a = btn.parents('tr').find('.total-reserve').find('a');
        let total_reserve = 0;
        if (total_reserve_a.text() !== '' && total_reserve_a.text() !== 'undefined'){
            total_reserve = total_reserve_a.text();
        }
        
        $.get(url)
        .done(function (response){
            if (response.success === true){
                if (response.data == 1){
                    color_class = 'btn-success';
                    btn_text = 'Да';
                    //Добавляем Кол-во в Резерв
                    total_reserve_a.text(Number(total_reserve) + Number(num));
                } else {
                    color_class = 'btn-default';
                    btn_text = 'Нет'
                    //Вычитаем из общего резерва
                    total_reserve_a.text(Number(total_reserve) - Number(num));
                }
            } else {
                alert(response.error);
            }
        }).fail(function(response){
            alert(response.responseText())
        }).always(function (){
            btn.removeClass('btn-success');
            btn.removeClass('btn-default');
            btn.addClass(color_class).text(btn_text);
        });
    });
        
      $(document).on('click','.in-china-btn', function() {
        // var model_id = $(this).attr('id');
        var model_id = $(this).closest('tr').attr('data-key');
        var btn = $('#in-china-' + model_id);
       $.ajax({
             url: '/request/change-china-status',
             data: {
                 id: model_id
             },
             success: function(data) {
                 // console.log('Ответ: ' + data);
               if (data == 1){
                  btn.removeClass('btn-danger');
                  btn.addClass('btn-success');
                  btn.html('Да');
               } else {
                  btn.removeClass('btn-success');
                  btn.addClass('btn-danger');
                  btn.html('Нет');
               }
             }
       });
    });
    
    //Клик по кнопке перехода к указанной заявке
    $(document).on('click','.request_change_btn', function() {
        var request_change_input = $('input[name="request_change"]').val();
        var url = '/request/view?id=' + request_change_input;
        window.location = url;
    });
    //При закрытии формы добавления картинки
    $(document).on('click','.add_request_image .close', function() {
        console.log('add_request_image modal closed');
        location.reload(true);
    });
    $(document).on('click','.remove_request_image', function(event) {
        // var remove_btn =  event.target;
        // debugger;
        var current_file_location = $(event.target).closest('.file_location');
        var current_file_col = $(event.target).closest('.col-xs-3');
        var current_model_id = $(current_file_location).find('.model_id').val();
        var current_model_name = $(current_file_location).find('.model_name').val();
        console.log('current_model_id');
        console.log(current_model_id);
        // var current_image = $(current_file_location).find('.request_image').attr('src');
        var current_image = $(current_file_location).find('a').attr('href');
        console.log('current_image');
        console.log(current_image);
            $.ajax({
                    url: '/request/file-remove',
                    data: {
                        model_id: current_model_id,
                        model_name: current_model_name,
                        removed_image: current_image
                    },
                    async: false,
                    // type: 'json',
                    type: 'get',
                    success: function (data) {
                        console.log('data');
                        console.log(data);
                        $(current_file_col).remove();

                    }
            });
    });
    $(document).on('click', '#copy-detail-name-btn', function() {
        var popover_id = $(this).closest('.kv-editable-popover').attr('id');
        var text_id = popover_id.replace('-popover', '-targ');
        // console.log(text_id);
        var sel = $('#' + text_id);
        sel.selectText();
        document.execCommand('copy');
    });
    $(document).on('click', '#detail-name-manager', function() {
        event.preventDefault();
        debugger;
        $(this).selectText();
        document.execCommand('copy');
    })
});
jQuery.fn.selectText = function(){
    var doc = document;
    var element = this[0];
    var range;
    // console.log(this, element);
    if (doc.body.createTextRange) {
        range = document.body.createTextRange();
        range.moveToElementText(element);
        range.select();
    } else if (window.getSelection) {
        var selection = window.getSelection();        
        range = document.createRange();
        range.selectNodeContents(element);
        selection.removeAllRanges();
        selection.addRange(range);
    }
};
JS;
$this->registerJs($script);
?>

