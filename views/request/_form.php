<?php

use app\models\Contractor;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Request */
/* @var $form yii\widgets\ActiveForm */
$order_part = new \app\models\OrderPart();
?>

    <div class="request-form">

        <?php $form = ActiveForm::begin([
            'options' => [
                'enctype' => 'multipart/form-data'
            ]
        ]); ?>

        <div class="row">
            <div class="col-md-8">
                <?= $form->field($model, 'contractor_id')->label(false)->widget(Select2::classname(), [
                    'data' => \app\models\Functions::getContractorsList(),
                    'options' => [
                            'id' => 'contractor',
                            'placeholder' => 'Контрагент'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'closeOnSelect' => true,
                    ],
                ]); ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'email')->label(false)->widget(Select2::classname(), [
                    'data' => Contractor::getEmailList(),
                    'options' => [
                        'id' => 'contractor-email',
                        'placeholder' => 'Email',
                    ],
                    'pluginOptions' => [
//                        'allowClear' => true,
                        'closeOnSelect' => true,
                    ],
                ]); ?>
            </div>
        </div>


        <?= $form->field($model, 'description')->textarea([
            'rows' => 6,
            'placeholder' => "Комментарий"
        ])->label(false) ?>

        <?php if (!Yii::$app->request->isAjax) { ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать',
                    ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>

    </div>

<?php
$script = <<<JS
    $(document).ready(function() {
       var input = $('#contractor-email');
      
       input.change(function() {
           $("#contractor option[value='" + input.val() + "']").attr("selected", "true");
           $('#select2-contractor-container').text($('#contractor option:selected').text());
       })
    })
JS;
$this->registerJs($script);

