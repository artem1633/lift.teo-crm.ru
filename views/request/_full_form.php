<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use app\models\Functions;
/* @var $this yii\web\View */
/* @var $model app\models\Request */
/* @var $form yii\widgets\ActiveForm */
$order_part = new \app\models\OrderPart();
?>

<div class="request-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data'
        ]
    ]); ?>

    <?= $form->field($model, 'contractor_id')->label(false)->widget(\kartik\select2\Select2::classname(), [
        'data' => \app\models\Functions::getContractorsList(),
        'options' => ['placeholder' => 'Контрагент'],
        'pluginOptions' => [
            'tags' => true,
            'allowClear' => true,
        ],
    ]); ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6,'placeholder' => "Комментарий"])->label(false) ?>

    <?= $form->field($model, 'main_status_id')->label(false)->widget(\kartik\select2\Select2::classname(), [
        'data' => \app\models\Functions::getMainStatusesList(),
        'options' => ['placeholder' => 'Статус'],
        'pluginOptions' => [
            'tags' => true,
            'allowClear' => true,
        ],
    ]); ?>

    <?= $form->field($model, 'additional_status_id')->label(false)->widget(\kartik\select2\Select2::classname(), [
        'data' => \app\models\Functions::getAdditionalStatusesList(),
        'options' => ['placeholder' => 'Статус Менеджера'],
        'pluginOptions' => [
            'tags' => true,
            'allowClear' => true,
        ],
    ]); ?>

    <?= $form->field($model, 'in_china')->checkbox()->label(false) ?>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>

</div>


