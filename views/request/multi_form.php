<?php

use kartik\file\FileInput;
use kartik\select2\Select2;
use app\models\Functions;
$order_part = new \app\models\OrderPart();
?>

<?php
if (empty($partIndex)) {
    $partIndex = 0;
}
?>

<div class="request-multi-form" data-index="<?= $partIndex?>">
    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-1">
                <span class="btn part_remove fa fa-remove text-danger"></span>
            </div>
            <div class="col-xs-2">
                <div class="nomenclature_id">
                    <?php
                    // With a model and without ActiveForm
                    echo Select2::widget([
                        'model' => $order_part,
                        'attribute' => '[' . $partIndex . ']nomenclature_id',
//                    'attribute' => 'state_2',
                        'options' => ['placeholder' => 'Номенклатура', 'multiple' => false],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'tags' => true,
                            'minimumInputLength' => 1,
                            'language' => [
                                'errorLoading' => new \yii\web\JsExpression("function () { return 'ошибка...'; }"),
                            ],
                            'ajax' => [
                                'url' => ['/request/nomenclature-list'],
                                'dataType' => 'json',
                                'data' => new \yii\web\JsExpression('function(params) { return {q:params.term}; }')
                            ],
                            'escapeMarkup' => new \yii\web\JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new \yii\web\JsExpression('function(data) { return data.text; }'),
                            'templateSelection' => new \yii\web\JsExpression('function (data) { return data.text; }'),
                        ],
                    ]);
                    ?>
                </div>

            </div>
            <div class="col-xs-2">
                <div class="form-group field-orderpart-<?= $partIndex?>-quantity">
                    <input type="text" id="orderpart-<?= $partIndex?>-quantity" class="form-control quantity" name="OrderPart[<?= $partIndex?>][quantity]" placeholder="Количество">
                    <div class="help-block"></div>
                </div>
            </div>
            <div class="col-xs-1">
                <?php
                    // Render a simple select by hiding the search control.
                    echo Select2::widget([
                        'name' => 'status',
                        'model' => $order_part,
                        'attribute' => '[' . $partIndex . ']measure_id',
                        'hideSearch' => true,
                        'data' => Functions::getMeasureList(),
                        'options' => ['placeholder' => 'Мера'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                ?>
            </div>
            <div class="col-xs-2">
                <div class="form-group field-orderpart-<?= $partIndex?>-vendor_code">
                    <input type="text" id="orderpart-<?= $partIndex?>-vendor_code" class="form-control vendor_code" name=OrderPart[<?= $partIndex?>][vendor_code]" maxlength="255" placeholder="Ариткул">
                    <div class="help-block"></div>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="form-group field-orderpart-<?= $partIndex?>-comment">
                    <input type="text" id="orderpart-<?= $partIndex?>-comment" class="form-control comment" name="OrderPart[<?= $partIndex?>][comment]" placeholder="Коментарий">
                    <div class="help-block"></div>
                </div>
            </div>
            <div class="col-xs-1">
                <?php
                echo FileInput::widget([
                    'model' => $order_part,
                    'attribute' => '[' . $partIndex . ']'.'files[]',
                    'pluginOptions' => [
                        'showCaption' => false,
                        'showRemove' => false,
                        'showPreview' => false,
                        'showUpload' => false,
                        'browseClass' => 'btn btn-primary btn-block',
                        'browseIcon' => '<i class="fa fa-files-o"></i> ',
                        'browseLabel' =>  ''
                    ],
                    'options' => ['multiple' => true]
                ]);
                ?>
            </div>
        </div>
    </div>
</div>
