<?php

use kartik\grid\GridView;
use yii\helpers\Url;
use app\models\Functions;

if (\app\models\Users::isTech()){
    $template = '{view} {update}';
} else {
    $template = '{view} {update} {delete}';
}

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'id',
        'filter' => function () {
            return Functions::getRequestsList();
        },
        'value' => function ($data) {
            return Functions::getRequest($data->id);
        },
        'format' => 'html',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'contractorName',
        'value' => function ($data) {
            return Functions::getContractor($data->contractor_id);
        },
        'format' => 'raw',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'created_by',
        'filter' => Functions::getManagersList(),
        'value' => function ($data) {
            Yii::info('$data->created_by = ' . $data->created_by, 'test');
            return Functions::getCreatedBy($data->created_by);
        },
        'format' => 'html',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'main_status_id',
        'filter' => Functions::getMainStatusesList(),
        'value' => function ($data) {
            return Functions::getMainStatus($data->main_status_id);
        },
        'format' => 'html',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'in_china',
//        'filter'=>array(0 => "Нет", 1 => "Да"),
//        'value' => function($data){
////            return Functions::getInChina($data -> id);
//            return Functions::getInChina($data -> id) == 1 ? '<span style="padding-top: 1px; padding-bottom: 2px;" class="btn btn-sm btn-success color_shadow">Да</span>' : '<span style="padding-top: 1px; padding-bottom: 2px;" class="btn btn-sm btn-danger color_shadow">Нет</span>';
//        },
//        'format' => 'html'
//    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'additional_status_id',
        'filter' => Functions::getAdditionalStatusesList(),
        'value' => function ($data) {
            return Functions::getAdditionalStatus($data->additional_status_id);
        },
        'format' => 'html',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'additional_request_photos',
//    ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'created_date',
//         'format' => 'datetime',
//     ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_date',
        'format' => 'datetime',
        'filter' => '<input class="form-control" type="date" name="RequestSearch[created_date]" value="'.\yii\helpers\ArrayHelper::getValue($_GET, 'RequestSearch.created_date').'">',
    ],
//    [
//        'class' => '\kartik\grid\DataColumn',
//        'attribute' => 'created_date',
//        'label' => 'Создано',
//        'format' => 'datetime',
//        'filter' => DatePicker::widget([
//            'name' => 'created_date',
//            'layout' => '{input}{picker}',
//            'pluginOptions' => [
//                'autoclose' => true,
//                'format' => 'yyyy-mm-dd'
//            ]
//        ]),
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'updated_date',
        'format' => 'datetime',
        'filter' => '<input class="form-control" type="date" name="RequestSearch[datetime]" value="'.\yii\helpers\ArrayHelper::getValue($_GET, 'RequestSearch.datetime').'">',
    ],
//    [
//        'class' => '\kartik\grid\DataColumn',
//        'attribute' => 'updated_date',
//        'label' => 'Обновлено',
//        'format' => 'datetime',
//        'filter' => DatePicker::widget([
//            'name' => 'updated_date',
//            'layout' => '{input}{picker}',
//            'pluginOptions' => [
//                'autoclose' => true,
//                'format' => 'yyyy-mm-dd'
//            ],
//        ]),
//    ],
    [
        'class' => 'kartik\grid\ActionColumn',
//        'dropdown' => false,
        'vAlign' => 'middle',
        'template' => $template,
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['title' => 'Просмотр', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Редактировать', 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Удалить',
            'data-confirm' => false, 'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
//            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Вы уверены?',
            'data-confirm-message' => 'Вы уверены что хотите удалить выбранные элементы?'],
    ],

];   