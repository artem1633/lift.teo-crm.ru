<?php

use app\models\Currency;
use app\models\DeliveryTime;
use app\models\Measure;
use app\models\Nomenclature;
use app\models\OrderPart;
use app\models\Reserve;
use app\models\Suppliers;
use app\models\User;
use app\models\Users;
use kartik\editable\Editable;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Functions;

if (Users::isTech()) {
    $hidden_delete_btn = 'hidden';
} else {
    $hidden_delete_btn = '';
}

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    //nomenclature_id
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'nomenclature_id',
        'value' => function ($data) {
            if ($data->nomenclature_id) {
                return Functions::getNomenclatureWithImage($data->nomenclature_id, $data->request_id);
            } else {
                return Html::a('Подобрать деталь', ['/order-part/update', 'id' => $data->id], ['role' => 'modal-remote', 'title' => 'Редактирование', 'data-toggle' => 'tooltip']);
            }
        },
        'format' => 'raw',
//        'hAlign' => 'center',
        'vAlign' => 'middle',
    ],
    //quantity Количество
    [
        'class' => 'kartik\grid\EditableColumn',
        'attribute' => 'quantity',
        'value' => function ($data) {
            if ($data->quantity) {
                $measure_model = Measure::findOne($data->measure_id);
                if ($measure_model) {
                    return $data->quantity . ' ' . $measure_model->name;
                }
            }
            return '';
        },
        'editableOptions' => [
            'formOptions' => ['action' => ['/request/editable']], // point to the new action
            'afterInput' => function ($form, $widget) {
                echo $form->field($widget->model, 'measure')->dropDownList(Functions::getMeasureList())->label(false);
            },
            'editableValueOptions' => [
                'disabled' => Users::isTech(),
                'editableValueOptions' => [
                    'style' => 'text-align: left;'
                ],
            ],
        ],
        'width' => '100px',
        'pageSummary' => true,
        'label' => 'Кол-во',
//        'hAlign' => 'center',
        'vAlign' => 'middle',
    ],
    //Резерв
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'is_reserve',
        'value' => function (OrderPart $data) {
            $reserve_count = $data->reserve->count ?? 0;
            return Html::button($reserve_count ? 'Да' : 'Нет', [
                'class' => $reserve_count ? 'btn btn-success btn-xs reserve-btn' : 'btn btn-default btn-xs reserve-btn',
                'data-url' => '/order-part/switch-reserve-for-order-part?id=' . $data->id,
            ]);
        },
        'label' => 'Резерв',
        'vAlign' => 'middle',
        'format' => 'raw',
    ],
    //comment Комментарий
    [
        'class' => 'kartik\grid\EditableColumn',
        'attribute' => 'comment',
        'editableOptions' => [
            'formOptions' => ['action' => ['/request/editable']], // point to the new action
            'editableValueOptions' => [
                'style' => 'text-align: left;'
            ],
        ],
        'hAlign' => 'left',
        'vAlign' => 'middle',
    ],
    //vendor_code Артикул
    [
        'class' => 'kartik\grid\EditableColumn',
        'attribute' => 'vendor_code',
        'editableOptions' => [
            'formOptions' => ['action' => ['/request/editable']], // point to the new action
        ],
        'contentOptions' => [
            'style' => 'text-align: center;'
        ],
        'format' => 'html',
        'vAlign' => 'middle',
//        'hAlign' => 'center'
    ],
    //detail_id фото
    [
        'class' => '\kartik\grid\DataColumn',
        'contentOptions' => function ($model, $key) {
            return [
                'id' => 'photo-' . $key,
            ];
        },

        'value' => function ($data) {
            if ($data->detail_id) {
                /** @var Nomenclature $nom_model */
                $nom_model = Nomenclature::find()->where(['id' => $data->detail_id])->one();
                if ($nom_model) {
                    $main_photo = $nom_model->main_photo;
                    if (is_file(Yii::getAlias('@webroot/' . $main_photo))) {
                        $photo = Html::img(Functions::getThumbnailPath($main_photo), [
                            'style' => 'max-width: 50px; margin: 0 0.5rem;',
                        ]);
                        return Html::a($photo, [$main_photo], [
                            'data-fancybox' => 'gallery-' . $data->detail_id
                        ]);
                    }
                }

            }
            return Html::img('/images/site/no-image.png', [
                'style' => 'max-width: 50px; margin: 0 0.5rem;',
            ]);
        },
        'format' => 'raw',
        'vAlign' => 'middle',
        'hAlign' => 'center',
    ],
    //резерв
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'reserve',
        'label' => 'Резерв',
        'visible' => !User::isManager(),
        'value' => function (OrderPart $data) {
            $total = Reserve::getTotalReservesCountByDetail($data->detail_id);
            return Html::a($total, ['/nomenclature/get-reserve-requests', 'id' => $data->nomenclature_id], [
                'role' => 'modal-remote',
                'title' => 'Просмотреть список счетов и заявок'
            ]);
        },
        'contentOptions' => [
            'class' => 'total-reserve',
        ],
        'format' => 'raw',
        'vAlign' => 'middle',
        'hAlign' => 'center',
    ],
    //detail_id Наименование
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'detail_id',
        'visible' => User::isManager(),
        'value' => function ($data) {
            return Html::tag('p', Nomenclature::findOne($data->detail_id)->name ?? null, [
                'id' => 'detail-name-manager',
                'style' => 'cursor: pointer;',
            ]);
        },
        'format' => 'raw',
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\EditableColumn',
        'attribute' => 'detail_id',
        'visible' => !User::isManager(),
        'value' => function ($data) {
            if ($data->detail_id) {
                $nom_model = Nomenclature::findOne($data->detail_id);
                if ($nom_model) {
                    return $nom_model->name;
                }
            }
            return '';
        },
        'editableOptions' => [
            'inputType' => Editable::INPUT_SELECT2,
//            'data' => Functions::getTechNomenclatureList(),
            'options' => [
                'data' => Functions::getTechNomenclatureList(),
            ],
            'formOptions' => [
                'action' => ['/request/editable'],
            ],
            'asPopover' => true,
            'format' => Editable::FORMAT_LINK,
            'editableValueOptions' => [
//                'style' => 'display: flex; align-items: center; border: none; text-align: left;',
                'disabled' => Users::isManager(),
            ],
            'afterInput' => function ($form, $widget) {

                $order_part_id = OrderPart::getOrderPartId($widget->displayValue);

                if (!$order_part_id) {
                    $order_part_id = $widget->model->id;
                }

                echo Html::a('Добавить новую деталь', ['/nomenclature/create', 'nomenclature_type' => Users::getPermission(), 'order_part_id' => $order_part_id], [
                    'class' => 'btn btn-info btn-block',
                    'role' => 'modal-remote',
                    'data-dismiss' => 'popover-x',
                ]);
                echo Html::button('Скопировать наименование', [
                    'class' => 'btn btn-block',
                    'id' => 'copy-detail-name-btn'
                ]);
            },
            'resetButton' => [
                'data-dismiss' => "popover-x"
            ],
            'pluginEvents' => [
                "editableReset" => "function(event, val, form, data) {
                     var text_div = $(this);
                     console.log($(this).closest('td').attr('data-col-seq'));
                     console.log($(this).attr('id'));
                     var id_order_part = $(this).closest('tr').attr('data-key');
                     var td = $('#photo-' + id_order_part);
                     var eye = $('#eye-' + id_order_part).find('a') //Глаз редактирования детали
                     var href_img = $('#photo-' + id_order_part).find('a'); //Ссылка картинки
                     var src_img = $('#photo-' + id_order_part).find('img'); //Сама картинка
                     
                     //console.log('id_order_part: ' + id_order_part)
                     $.ajax({
                        url: '/order-part/clear-detail',
                        data: {id: id_order_part},
                        success: function(response){
                            if (response){
                                href_img.remove();
                                td.html(src_img.attr('src', '/images/site/no-image.png'));
                                src_img.attr('src', '/images/site/no-image.png');
                                text_div.find('.kv-editable-link').html('<em>(не задано)</em>');
                                eye.attr('href', '#');
                                eye.removeClass('glyphicon-eye-open');
                                eye.addClass('glyphicon-eye-close');
//                                  location.reload();
                            }
                        }
                     })
                 }",
                "editableSuccess" => "function(event, val, form, data) {

                        var id_order_part = $(this).closest('tr').attr('data-key');
                        var new_nomenclature_id = val
                        var href_img = $('#photo-' + id_order_part).find('a'); //Ссылка картинки
                        var src_img = $('#photo-' + id_order_part).find('img'); //Сама картинка
                        var eye = $('#eye-' + id_order_part).find('a') //Глаз редактирования детали
                        
                        $.ajax({
                            url: '/nomenclature/get-image-url',
                            data: {id: new_nomenclature_id},
                            success: function(response){
                                console.log(response['thumbs']);
                                //Меняем картинку
                                if (response){
                                   href_img.attr('href', response['photo']);
                                   src_img.attr('src', response['thumbs']);
                               } else {
                                    src_img.attr('src','/images/site/no-image.png')
                               }
                               //Меняем ссылку глаза
                               eye.attr('href', '/nomenclature/view?id=' + new_nomenclature_id);
                               //Меняем картинку глаза
                                eye.removeClass('glyphicon-eye-close');
                                eye.addClass('glyphicon-eye-open');
                               
                            }
                        })
                }",
            ],
        ],
        'contentOptions' => function ($model, $key) {
            return [
                'id' => $key,
            ];
        },
        'format' => 'raw',
        'vAlign' => 'middle',
//        'hAlign' => 'left',
    ],
    //detail_id Глаз
    [
        'class' => 'kartik\grid\DataColumn',
        'attribute' => 'detail_id',
        'value' => function ($data) {
            if ($data->detail_id && !Users::isManager()) {
                return Html::a('', ['/nomenclature/view', 'id' => $data->detail_id, 'request_id' => $data->request_id], [
                    'class' => 'glyphicon glyphicon-eye-open',
                    'role' => 'modal-remote',
                ]);
            } else {
                return Html::a('', ['/request/view', 'id' => $data->request_id], [
                    'class' => 'glyphicon glyphicon-eye-close',
                    ''
                ]);
            }

        },
        'contentOptions' => function ($model, $key) {
            return [
                'id' => 'eye-' . $key,
            ];
        },
        'label' => '',
        'format' => 'raw',
        'vAlign' => 'middle',
//        'hAlign' => 'center',
    ],
    //delivery_time_id Срок доставки
    [
        'class' => '\kartik\grid\EditableColumn',
        'attribute' => 'delivery_time_id',
        'value' => function ($data) {
            if ($data->delivery_time_id) {
                $dt_model = DeliveryTime::findOne($data->delivery_time_id);
                if ($dt_model) {
                    return $dt_model->name;
                }
            }
            return '';
        },
        'editableOptions' => [
            'formOptions' => ['action' => ['/request/editable']],
            'inputType' => Editable::INPUT_DROPDOWN_LIST,
            'data' => Functions::getDeliveryTimeList(),
            'editableValueOptions' => [
                'style' => 'text-align: left;',
                'disabled' => Users::isManager(),
            ],
        ],
        'format' => 'html',
        'vAlign' => 'middle',
//        'hAlign' => 'center',
    ],
    //suppliers_id Поставщик
    [
        'class' => '\kartik\grid\EditableColumn',
        'attribute' => 'suppliers_id',
        'visible' => !Users::isManager(),
        'value' => function ($data) {
            if ($data->suppliers_id) {
                $sup_model = Suppliers::findOne($data->suppliers_id);
                if ($sup_model) {
                    return $sup_model->name;
                }
            }
            return '';
        },
        'editableOptions' => [
            'formOptions' => ['action' => ['/request/editable']],
            'inputType' => Editable::INPUT_DROPDOWN_LIST,
            'data' => Functions::getSuppliersList(),
        ],
        'format' => 'raw',
        'vAlign' => 'middle',
        'hAlign' => 'center',
    ],
    //in_china
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'in_china',
        'visible' => !Users::isManager(),
        'value' => function ($data) {
            if ($data->id) {
                return Functions::getInChina3($data->id);
            }
            return '';
        },
        'format' => 'raw',
        'vAlign' => 'middle',
        'hAlign' => 'center',
    ],
    //buy_price
    [
        'class' => 'kartik\grid\EditableColumn',
        'attribute' => 'buy_price',
        'value' => function ($data) {
            $cur_model = Currency::findOne($data->buy_currency);
            if ($cur_model) {
                $icon = $cur_model->icon;
                if ($data->buy_price && $data->updated_buy_price) {
                    return $data->buy_price . ' <i class = "fa ' . $icon . '"></i><br>(' . Yii::$app->formatter->asDate($data->updated_buy_price) . ')';
                }
            }
            return '';
        },
        'editableOptions' => [
            'formOptions' => ['action' => ['/request/editable']],
            'afterInput' => function ($form, $widget) {
                //Валюта покупки по умолчанию - доллар
                $widget->model->buy_currency = Currency::find()->where(['name' => 'доллар'])->one()->id;
                echo $form->field($widget->model, 'buy_currency')->dropDownList(Functions::getCurrency())->label(false);
            },
            'editableValueOptions' => [
                'disabled' => Users::isManager(),
            ],
        ],
        'format' => 'html',
        'vAlign' => 'middle',
        'hAlign' => 'center',
    ],
    //sell_price
    [
        'class' => 'kartik\grid\EditableColumn',
        'attribute' => 'sell_price',
        'value' => function ($data) {
            $cur_model = Currency::findOne($data->sell_currency);
            if ($cur_model) {
                $icon = $cur_model->icon;
                if ($data->sell_price && $data->updated_sell_price) {
                    return $data->sell_price . ' <i class="fa ' . $icon . '"></i><br>(' . Yii::$app->formatter->asDate($data->updated_sell_price) . ')';
                }
            }
            return '';
        },
        'editableOptions' => function () {
            return [
                'formOptions' => ['action' => ['/request/editable']],
                'editableValueOptions' => [
                    'disabled' => Users::isManager(),
                ],
                'afterInput' => function ($form, $widget) {
                    //Валюта продажи по умолчанию - рубль
                    $widget->model->sell_currency = Currency::find()->where(['name' => 'рубль'])->one()->id;
                    echo $form->field($widget->model, 'sell_currency')->dropDownList(Functions::getCurrency())->label(false);
                },
            ];
        },
        'vAlign' => 'middle',
        'hAlign' => 'center',
        'format' => 'html'
    ],
    //ActionColumns
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key) {
            if ($action === 'delete') {
                $url = Url::to(['/order-part/delete', 'id' => $key]);
                return $url;
            }
            if ($action === 'update') {
                $url = Url::to(['/order-part/update', 'id' => $key]);
                return $url;
            }
            if ($action === 'view') {
                $url = Url::to(['/order-part/view', 'id' => $key]);
                return $url;
            }

            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'Просмотр', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Редактирование', 'data-toggle' => 'tooltip', 'class' => 'hidden'],
        'deleteOptions' => [
            'role' => 'modal-remote',
            'title' => 'Удаление',
            'data-confirm' => false,
            'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Вы уверены?',
            'data-confirm-message' => 'Вы уверены что хотите удалить выбранные элементы?',
            'class' => $hidden_delete_btn
        ],
    ],

];
