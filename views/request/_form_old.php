<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use app\models\Functions;
/* @var $this yii\web\View */
/* @var $model app\models\Request */
/* @var $form yii\widgets\ActiveForm */
$order_part = new \app\models\OrderPart();
?>

<div class="request-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data'
        ]
    ]); ?>

    <?= $form->field($model, 'contractor_id')->label(false)->widget(\kartik\select2\Select2::classname(), [
        'data' => \app\models\Functions::getContractorsList(),
        'options' => ['placeholder' => 'Контрагент'],
        'pluginOptions' => [
            'tags' => true,
            'allowClear' => true,
        ],
    ]); ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6, 'placeholder' => "Комментарий"])->label(false) ?>

    <div class="multi_form">

        <div class="request-multi-form" data-index="<?= 0 ?>">
           <div class="row">
               <div class="col-xs-12">
                   <?php $form = ActiveForm::begin(); ?>
                   <div class="col-xs-1">
                       <span class="text-center btn part_add fa fa-plus text-success"></span>
                   </div>
                   <div class="col-xs-2">
                       <div class="nomenclature_id">
                           <?php
                           // With a model and without ActiveForm
                           echo \kartik\select2\Select2::widget([
                               'model' => $order_part,
                               'attribute' => '[0]nomenclature_id',
                               'options' => ['placeholder' => 'Номенклатура', 'multiple' => false],
                               'pluginOptions' => [
                                   'allowClear' => true,
                                   'tags' => true,
                                   'minimumInputLength' => 1,
                                   'language' => [
                                       'errorLoading' => new \yii\web\JsExpression("function () { return 'ошибка...'; }"),
                                   ],
                                   'ajax' => [
                                       'url' => ['/request/nomenclature-list'],
                                       'dataType' => 'json',
                                       'data' => new \yii\web\JsExpression('function(params) { return {q:params.term}; }')
                                   ],
                                   'escapeMarkup' => new \yii\web\JsExpression('function (markup) { return markup; }'),
                                   'templateResult' => new \yii\web\JsExpression('function(data) { return data.text; }'),
                                   'templateSelection' => new \yii\web\JsExpression('function (data) { return data.text; }'),
                               ],
                           ]);
                           ?>
                       </div>
                   </div>
                   <div class="col-xs-2">
                       <div class="quantity">
                           <?= $form->field($order_part, '[0]'.'quantity')->textInput(['maxlength' => true, 'placeholder' => "Количество"])->label(false); ?>
                       </div>
                   </div>
                   <div class="col-xs-1">
                       <?php
                       // Render a simple select by hiding the search control.
                       echo \kartik\select2\Select2::widget([
                           'name' => 'status',
                           'model' => $order_part,
                           'attribute' => '[0]'.'measure_id',
                           'hideSearch' => true,
                           'data' => Functions::getMeasureList(),
                           'options' => ['placeholder' => 'Мера'],
                           'pluginOptions' => [
                               'allowClear' => true
                           ],
                       ]);
                       ?>
                   </div>
                   <div class="col-xs-2">
                       <div class="vendor_code">
                           <?= $form->field($order_part, '[0]'.'vendor_code')->textInput(['maxlength' => true, 'placeholder' => "Ариткул"])->label(false); ?>
                       </div>
                   </div>
                   <div class="col-xs-3">
                       <?= $form->field($order_part, '[0]'.'comment')->textInput(['placeholder' => "Коментарий"])->label(false); ?>
                   </div>
                   <div class="col-xs-1">
                       <?php
                       echo FileInput::widget([
                           'model' => $order_part,
                           'attribute' => '[0]'.'files[]',
                           'pluginOptions' => [
                               'showCaption' => false,
                               'showRemove' => false,
                               'showPreview' => false,
                               'showUpload' => false,
                               'browseClass' => 'btn btn-primary btn-block',
                               'browseIcon' => '<i class="fa fa-files-o"></i> ',
                               'browseLabel' =>  ''
                           ],
                           'options' => ['multiple' => true]
                       ]);
                       ?>
                   </div>
                   <?php ActiveForm::end(); ?>
               </div>
           </div>
        </div>

    </div>

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>

</div>


