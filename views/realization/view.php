<?php

use app\models\Realization;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Realization */
?>
<div class="realization-view">
 
    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'date:datetime',
                [
                    'attribute'=>'contractor_id',
                    'value' => function (Realization $model){
                        return $model->contractor->full_name ?? null;
                    }
                ],
//                'note:ntext',
            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), '_error');
    } ?>

</div>
