<?php

use app\models\DetailToRealization;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;

/** @var  int $realization ID документа реализации */

$dataProvider = new ActiveDataProvider([
    'query' => DetailToRealization::find()->andWhere(['realization_id' => $realization]),
]);

?>

<?php
try {
    echo GridView::widget([
        'id' => 'crud-datatable',
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => 'kartik\grid\SerialColumn',
                'width' => '30px',
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'detail_id',
                'value' => function (DetailToRealization $model) {
                    return $model->detail->name ?? null;
                }
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'number',
                'value' => function (DetailToRealization $model) {
                    $detail = $model->detail ?? null;

                    if ($detail) {
                        $measure = $model->detail->getMeasure()->one()->name ?? null;
                        if ($measure) {
                            $measure = '(' . $measure . ')';
                        }
                        return $model->number . ' ' . $measure;
                    }
                    return $model->number;
                }
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'price',
                'value' => function (DetailToRealization $model) {
                    return $model->price . ' руб.';
                }
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'total_amount',
                'value' => function (DetailToRealization $model) {
                    return $model->total_amount . ' руб.';
                }
            ],

        ],
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
    ]);
} catch (Exception $e) {
    Yii::error($e->getMessage(), '_error');
} ?>
