<?php

use app\models\Realization;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => 'kartik\grid\ExpandRowColumn',
        'width' => '50px',
        'value' => function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detail' => function (Realization $model, $key, $index, $column) {
            return \Yii::$app->controller->renderPartial('_expand-goods', ['realization' => $model->id]);
        },
        'headerOptions' => ['class' => 'kartik-sheet-style'],
        'expandOneOnly' => true
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'date',
        'format' => 'datetime',
    ],
    [
        'attribute' => 'id',
        'label' => '№'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'contractor_id',
        'value' => function (Realization $model) {
            return $model->contractor->full_name ?? null;
        }
    ],
    [
        'attribute' => 'warehouse_id',
        'label' => 'Организация',
        'value' => 'warehouse.name',
    ],
    [
        'attribute' => 'total_amount',
        'label' => 'Сумма',
        'value' => function (Realization $model){
            return $model->getTotalAmount() . ' руб.';
        }

    ],

//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'note',
//    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'template' => '{real_edit} {edit} {delete}',
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'buttons' => [
            'real_edit' => function($url, Realization $model){
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                    ['update', 'id' => $model->id], [
                        'title' => 'Редактировать реализацию',
                        'role'=>'modal-remote',
                    ]);
            },
            'edit' => function ($url, Realization $model){
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                    ['/detail-to-realization/index', 'realization_id' => $model->id], [
                        'title' => 'Просмотр реализации',
                        'data-pjax' => 0,
                    ]);
            }
        ],
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'View', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Update', 'data-toggle' => 'tooltip'],
        'deleteOptions' => [
            'role' => 'modal-remote',
            'title' => 'Удалить',
            'data-confirm' => false,
            'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Вы уверены?',
            'data-confirm-message' => 'Подтвердите удаление данного элемента'
        ],
    ],

];   