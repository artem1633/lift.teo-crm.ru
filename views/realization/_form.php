<?php

use app\models\JuridicalPerson;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Realization */
/* @var $form yii\widgets\ActiveForm */

CrudAsset::register($this);

?>
<div class="row">
<div class="realization-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12">
        <?= $form->field($model, 'warehouse_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Warehouse::find()->all(), 'id', 'name')) ?>
    </div>

    <div class="col-xs-4">
        <?= $form->field($model, 'date')->widget(DatePicker::class, [
            'pluginOptions' => [
                'todayHighlight' => true,
                'autoclose' => true,
                'format' => 'yyyy-mm-dd'
            ]
        ]) ?>
    </div>
    <div class="col-xs-8">
        <?= $form->field($model, 'contractor_id')->widget(Select2::class, [
            'data' => (new JuridicalPerson)->getListWithInn(),
            'options' => ['placeholder' => 'Выберите контрагента (юр. лицо)'],
            'pluginOptions' => [
                'tags' => true,
                'allowClear' => true,
            ],
        ]) ?>
    </div>

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>


