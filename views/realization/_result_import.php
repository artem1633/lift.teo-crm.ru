<?php

use yii\helpers\VarDumper;

/**
 * @var array $data Информация об импорте
 */


?>

<div class="container">

    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title">Импорт завершен</h3>
        </div>
        <div class="panel-body">
            <p>Заказов: <?= $data['order_count'] ?></p>
            <p>Импортировано деталей: <?= $data['product_count'] ?></p>
        </div>
    </div>
    <?php if ($data['errors']): ?>
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">Ошибки импорта</h3>
            </div>
            <div class="panel-body">
                <?= $data['errors']; ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if ($data['warnings']): ?>
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h3 class="panel-title">Предупреждения импорта</h3>
            </div>
            <div class="panel-body">
                <?= $data['warnings']; ?>
            </div>
        </div>
    <?php endif; ?>

    <?php
    if (isset($test_data) && $test_data) {
        VarDumper::dump($test_data, 10, true);
    }
    ?>
</div>
