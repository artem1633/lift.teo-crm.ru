<?php

/* @var $this yii\web\View */
/* @var $model app\models\Realization */
?>
<div class="realization-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
