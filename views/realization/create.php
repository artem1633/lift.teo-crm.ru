<?php

/* @var $this yii\web\View */
/* @var $model app\models\Realization */

?>
<div class="realization-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
