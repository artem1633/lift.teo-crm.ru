<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AdditionalOrderStatus */
?>
<div class="additional-order-status-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
