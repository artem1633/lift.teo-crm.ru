<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AdditionalOrderStatus */
?>
<div class="additional-order-status-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
