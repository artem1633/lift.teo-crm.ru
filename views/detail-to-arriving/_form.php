<?php

use app\models\Currency;
use app\models\DetailToArriving;
use app\models\Nomenclature;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DetailToArriving */
/* @var $form yii\widgets\ActiveForm */
/* @var int $arriving_id ID поступления */

if ($model->isNewRecord) {
    $model->arriving_id = $arriving_id;
}
?>

    <div class="detail-to-arriving-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'arriving_id')->hiddenInput(['value' => $model->arriving_id])->label(false) ?>

        <div class="row">
            <div class="col-md-3">
                <?= Html::activeLabel($model, 'vendor_code') ?>
                <div class="input-group">
                    <?= Html::activeTextInput($model, 'vendor_code', [
                        'id' => 'vendor-code',
                        'class' => 'form-control'
                    ]) ?>
                    <span class="input-group-btn">
                    <?= Html::button('<i class="glyphicon glyphicon-search"></i>', [
                        'id' => 'check-btn',
                        'class' => 'btn btn-default'
                    ]); ?>
                </span>
                </div>
                <?= Html::error($model, 'vendor_code', [
                    'class' => 'help-block',
                    'id' => 'help-vendor-code',
                ]) ?>
            </div>

            <div class="col-md-9">
                <?= Html::activeLabel($model, 'detail_id'); ?>
                <div class="input-group detail-group">
                    <?= Html::a('<i class="glyphicon glyphicon-eye-open"></i>',
                        ['/nomenclature/view', 'id' => $model->detail_id], [
                            'id' => 'view-btn',
                            'class' => 'btn btn-default',
                            'data-pjax' => 1,
                            'role' => 'modal-remote',
                        ]); ?>
                    <?= $form->field($model, 'detail_id')->widget(Select2::class, [
                        'data' => Nomenclature::getList(),
                        'options' => [
                            'placeholder' => 'Выберите деталь'
                        ],
                        'pluginOptions' => [
                            'tags' => true,
                            'allowClear' => true,
                        ],
                    ])->label(false) ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'number')->textInput([
                    'id' => 'detail-number',
                    'class' => 'form-control'
                ]) ?>
            </div>
            <div class="col-md-9">
                <?= $form->field($model, 'warehouse')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Warehouse::find()->all(), 'id', 'name')) ?>
            </div>
        </div>

        <div class="row">

            <div class="col-md-3">
                <?= $form->field($model, 'total_amount_currency')->textInput([
                    'id' => 'detail-total-amount-currency',
                ]) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'currency_id')->dropDownList(Currency::getList(), [
                    'value' => $model->currency_id ? $model->currency_id : 1,
                    'id' => 'detail-currency',
                ]) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'exchange_rate')->textInput([
                    'id' => 'detail-exchange',
                    'value' => $model->exchange_rate ? $model->exchange_rate : 1,
                ]) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'total_amount_rub')->textInput([
                    'id' => 'detail-total-amount-rub',
                    'disabled' => true,
                ]) ?>
            </div>
        </div>


        <?php if (!Yii::$app->request->isAjax) { ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                    ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>

    </div>

<?php
$script = <<<JS
$(document).ready(function() {
    // var price = 0;

    $(document).on('click', '#check-btn', function(){
        var input_vendor = $('#vendor-code');
        var vendor_code = input_vendor.val();
        var help_block = $('#help-vendor-code');
        $.get(
            'get-id-by-vendor-code',
            {
                code: vendor_code
            },
            function(response) {
                console.log(response);
                if (response['error'] === 1){
                    help_block.html('<p class="text-danger">' + response['data'] + '</p>')
                } else {
                    help_block.html('');
                    $('#detailtoarriving-detail_id').val(response['data']['id']);
                    $('#select2-detailtoarriving-detail_id-container').html(response['data']['name']);
                }
            }
        )
    });
    $(document).on('change', '#detailtoarriving-detail_id', function() {
        var link = $('#view-btn');
        var href = link.attr('href');
        var value = '';
        if (href.includes('id=')){
            value = href.replace(/(id=)[^&]+/ig, '$1' + $(this).val());
        } else {
            //Добавляем параметр
            value = href + '?id=' + $(this).val();
        }
        link.attr('href', value);
    });
    $(document).on('change', '#detail-currency', function(){
        var ex_input = $('#detail-exchange');
        var currency_id = $(this).val();
        $.get(
            '/detail-to-arriving/get-exchange-value',
            {
                id: currency_id
            },
            function(response) {
                ex_input.val(response);
                setTotalAmountRub();
            }
        )
    });
    $(document).on('input', '#detail-exchange', function(){
        setTotalAmountRub();
    });
    $(document).on('input', '#total_amount_currency', function(){
        setTotalAmountRub();
    });
    
    function setTotalAmountRub(){
        var amount_currency = $('#detail-total-amount-currency').val();
        var exchange_rate = $('#detail-exchange').val();
        if (amount_currency && exchange_rate){
            $('#detail-total-amount-rub').val((amount_currency * exchange_rate).toFixed(2))
        }
    }
});
JS;

$this->registerJs($script);
