<?php

use app\models\DetailToArriving;
use kartik\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'detail_name',
        'label' => 'Деталь',
        'value' => function (DetailToArriving $model) {
            return $model->detail->name ?? null;
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'number',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'warehouse',
        'filter' => \yii\helpers\ArrayHelper::map(\app\models\Warehouse::find()->all(), 'id','name'),
        'value' => function (DetailToArriving $model) {
            $warehouse = \app\models\Warehouse::findOne($model->warehouse);
            if($warehouse){
                return $warehouse->name;
            }
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'price',
        'value' => function (DetailToArriving $model) {
            return $model->price . '&nbsp;<span class="fa ' . $model->currency->icon . '"></span>';
        },
        'format' => 'raw'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'total_amount_currency',
        'value' => function (DetailToArriving $model) {
            return $model->total_amount_currency . '&nbsp;<span class="fa ' . $model->currency->icon . '"></span>';
        },
        'format' => 'raw'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'Статус',
        'label' => 'Статус',
        'content' => function($model){
            if($model->status == DetailToArriving::STATUS_SAVED){
                return Html::a('Сохранено', ['change-status', 'id' => $model->id], ['class' => 'btn btn-block btn-danger', 'role' => 'modal-remote']);
            } else {
                return Html::a('Проведено', ['change-status', 'id' => $model->id], ['class' => 'btn btn-block btn-success', 'role' => 'modal-remote']);
            }
        },
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'template' => '{update} {delete}',
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'View', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Update', 'data-toggle' => 'tooltip'],
        'deleteOptions' => [
            'role' => 'modal-remote',
            'title' => 'Delete',
            'data-confirm' => false,
            'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Are you sure?',
            'data-confirm-message' => 'Are you sure want to delete this item'
        ],
    ],

];   