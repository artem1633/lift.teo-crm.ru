<?php

use app\models\Functions;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;

/* @var $this yii\web\View */
/* @var $arriving_model \app\models\Arriving */
/* @var $searchModel app\models\DetailToArrivingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Поступление №' . $arriving_model->number;
//$this->params['breadcrumbs'][] =  $this->title;

CrudAsset::register($this);

?>
<div class="detail-to-arriving-index">
    <div id="ajaxCrudDatatable">
        <?php
        try {
            echo GridView::widget([
                'id' => 'crud-datatable',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax' => true,
                'columns' => require(__DIR__ . '/_columns.php'),
                'toolbar' => [
                    [
                        'content' =>
                            Html::a('<i class="glyphicon glyphicon-plus"></i>',
                                ['create', 'arriving_id' => $arriving_model->id],
                                [
                                    'role' => 'modal-remote',
                                    'title' => 'Добавить деталь',
                                    'class' => 'btn btn-default'
                                ]) .
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                                ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Reset Grid']) .
                            '{toggleData}' .
                            Html::a('Назад к поступлениям', ['/arriving/index'], [
                                'class' => 'btn btn-default',
                                'data-pjax' => 0,
                            ])
//                            . '{export}'
                    ],
                ],
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'type' => 'primary',
                    'heading' => '<i class="glyphicon glyphicon-list"></i> Список деталей поступления №' . $arriving_model->id,
//                    'before' => '<em>* Resize table columns just like a spreadsheet by dragging the column edges.</em>',
                    'after' => Functions::getBulkButtonWidget() .
                        '<div class="clearfix"></div>',
                ]
            ]);
        } catch (Exception $e) {
            Yii::error($e->getMessage(), '_error');
            echo $e->getMessage();
            echo $e->getTraceAsString();
        } ?>
    </div>
</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>
