<?php

/* @var $this yii\web\View */
/* @var $model app\models\DetailToArriving */
/* @var int $arriving_id ID поступления*/

?>
<div class="detail-to-arriving-create">
    <?= $this->render('_form', [
        'model' => $model,
        'arriving_id' => $arriving_id,
    ]) ?>
</div>
