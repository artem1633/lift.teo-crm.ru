<?php

/* @var $this yii\web\View */
/* @var $model app\models\DetailToArriving */
?>
<div class="detail-to-arriving-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
