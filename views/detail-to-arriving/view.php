<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DetailToArriving */
?>
<div class="detail-to-arriving-view">

    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                [
                    'attribute' => 'detail_id',
                    'value' => $model->detail->name ?? null,
                ],
                'number',
                'price',
                'nds_amount',
                'total_amount_rub',
                [
                    'attribute' => 'warehouse',
                    'value' => $model->warehouseName ?? null,
                ],
            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), '_error');
    } ?>

</div>
