<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TypeOfMechanism */
?>
<div class="type-of-mechanism-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
