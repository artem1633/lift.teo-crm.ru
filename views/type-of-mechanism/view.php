<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TypeOfMechanism */
?>
<div class="type-of-mechanism-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
