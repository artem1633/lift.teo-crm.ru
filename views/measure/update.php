<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Measure */
?>
<div class="measure-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
