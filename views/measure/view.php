<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Measure */
?>
<div class="measure-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
