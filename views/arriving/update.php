<?php

/* @var $this yii\web\View */
/* @var $model app\models\Arriving */
/* @var $dta_model app\models\DetailToArriving */
?>
<div class="arriving-update">

    <?= $this->render('_form', [
        'model' => $model,
//        'dta_model' => $dta_model,
    ]) ?>

</div>
