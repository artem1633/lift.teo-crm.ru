<?php

use app\models\Arriving;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'number',
//        'width' => '100px',
//        'label' => '#',
        'hAlign' => 'center',
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'created_at',
        'format' => 'datetime'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
//        'attribute'=>'contractor_id',
        'attribute' => 'contractor_name',
        'label' => 'Контрагент',
        'value' => function (Arriving $model) {
            $full_name = $model->contractor->full_name ?? null;
            if ($full_name){
                return $full_name;
            }
            return $model->contractor->name;
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'total_amount',
        'value' => function (Arriving $model) {
            $total = '';
            //Показываем суммы с группировкой по валютам
            /** @var \app\models\DetailToArriving $amount_info */
            foreach ($model->totalAmountCurrency() as $amount_info) {
                $icon = \app\models\Currency::findOne($amount_info->currency_id)->icon ?? '';
                $total .= (double)$amount_info->total_cur . '&nbsp;<span class="fa ' .$icon . '"></span><br>';
            }
            return $total;
        },
        'format' => 'raw',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'status',
        'filter' => Arriving::getStatusList(),
        'value' => function (Arriving $model) {
            return $model->statusLabel;
        },
        'format' => 'raw',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'template' => '{arr_edit} {change-status} {edit} {delete}',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'buttons' => [
            'arr_edit' => function ($url, Arriving $model) {
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                    ['update', 'id' => $model->id], [
                        'title' => 'Редактировать поступление',
                        'role' => 'modal-remote',
                    ]);
            },
            'edit' => function ($url, Arriving $model) {
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                    ['/detail-to-arriving/index', 'arriving_id' => $model->id], [
                        'title' => 'Просмотр поступления',
                        'data-pjax' => 0,
                    ]);
            },
            'change-status' => function($url, Arriving $model){
                if ($model->status == $model::STATUS_SAVED){
                    $str = 'glyphicon glyphicon-ok-circle';
                    $title = 'Провести';
                } else {
                    $str = 'glyphicon glyphicon-remove-circle';
                    $title = 'Отменить проведение';
                }
                return Html::a("<i class='{$str}' title='{$title}'></i>", ['change-status', 'id' => $model->id], [
                    'role' => 'modal-remote'
                ]);
            }
        ],

        'viewOptions' => ['role' => 'modal-remote', 'title' => 'Просмотр', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Редактировать', 'data-toggle' => 'tooltip'],
        'deleteOptions' => [
            'role' => 'modal-remote',
            'title' => 'Delete',
            'data-confirm' => false,
            'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Вы уверены?',
            'data-confirm-message' => 'Подтвердите удаление элемента'
        ],
    ],

];   