<?php

use app\models\Contractor;
use app\models\JuridicalPerson;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var array $data Инфо для акта */
/* @var $model \app\models\ReconciliationReport */
/* @var double $start_balance Начальное сальдо */
/* @var string $currency_icon Иконка валюты */

$this->title = 'Акт сверки';
$this->params['breadcrumbs'][] = 'Складской учет';
$this->params['breadcrumbs'][] = $this->title;

$total_pay = 0;
$total_sell = 0;

CrudAsset::register($this);

?>
<div class="container reconciliation-report-index">
    <div class="row filter">
        <?php $form = ActiveForm::begin([
            'id' => 'reconciliation-report-form',
            'method' => 'get',
        ]); ?>
        <div class="col-xs-12">
            <?= $form->field($model, 'contractor_id')->widget(Select2::class, [
                'data' => (new Contractor)->getListWithInn(),
                'options' => [
                    'placeholder' => 'Выберите контрагента',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
                'pluginEvents' => [
                    "change" => "function() { 
                        $('#submit-act-filter').attr('disabled', false);
                     }",
                ],
            ])->label(false) ?>
        </div>
        <div class="col-xs-12">
<?//= $form->field($model, 'juridical_person_id')->widget(Select2::class, [
//                'data' => (new JuridicalPerson())->getList(),
//                'options' => [
//                    'placeholder' => 'Выберите юр. лицо',
//                ],
//                'pluginOptions' => [
//                    'allowClear' => true,
//                ],
//                'pluginEvents' => [
//                    "change" => "function() {
//                        $('#submit-act-filter').attr('disabled', false);
//                     }",
//                ],
//            ])->label(false) ?>
        </div>
        <div class="col-xs-4">
            <?= $form->field($model, 'report_start_date')->widget(DatePicker::class, [
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'options' => [
                    'id' => 'start-date',
                    'autocomplete' => 'off',
                    'placeholder' => 'Начало периода'
                ],
                'pluginOptions' => [
                    'autoclose' => true,
//                    'format' => 'yyyy-mm-dd',
                    'format' => 'dd.mm.yyyy'
                ]
            ])->label(false) ?>
        </div>
        <div class="col-xs-4">
            <?= $form->field($model, 'report_end_date')->widget(DatePicker::class, [
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'options' => [
                    'id' => 'end-date',
                    'autocomplete' => 'off',
                    'placeholder' => 'Конец периода'
                ],
                'pluginOptions' => [
                    'autoclose' => true,
//                    'format' => 'yyyy-mm-dd'
                    'format' => 'dd.mm.yyyy'
                ]
            ])->label(false) ?>
        </div>
        <div class="col-xs-4">
            <?= Html::submitButton('Сформировать',
                [
                    'class' => 'btn btn-info btn-block',
                    'id' => 'submit-act-filter',
                    'disabled' => ($model->contractor_id || $model->juridical_person_id) ? false : true
                ]) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <table class="report-table table table-bordered">
                <thead>
                <tr>
                    <th>Дата</th>
                    <th>Документ</th>
                    <th>Дебет</th>
                    <th>Кредит</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td colspan="2"><b>Сальдо начальное</b></td>
                    <td><b><?= $start_balance . $currency_icon;; ?></b></td>
                    <td></td>
                </tr>
                <?php if ($data): ?>
                    <?php foreach ($data as $item): ?>
                        <?php //\Yii::info($item, 'test'); ?>
                        <?php (isset($item['type']) && $item['type'] == 'arriving') ? $type = 1 : $type = 2; ?>
                        <?php
                        if ($type === 1) {
                            $total_sell += $item['sum'];
                        } else {
                            $total_pay += $item['sum'];
                        }
                        ?>
                        <tr>
                            <td><?= $item['date']; ?></td>
                            <td><?php if ($type == 1) {
                                    echo Html::a('Продажа (№' . $item['number'] . ')',
                                        ['/detail-to-arriving', 'arriving_id' => $item['id']]);
                                } else {
                                   echo Html::a('Оплата', ['/payment/view', 'id' => $item['id']], [
                                           'role' => 'modal-remote',
                                   ]);
                                } ?></td>
                            <td><?= $type == 1 ? $item['sum'] . '&nbsp;<span class="fa ' . $item['icon'] . '"></span>' : '' ?></td>
                            <td><?= $type == 2 ? $item['sum'] . '&nbsp;<span class="fa ' . $item['icon'] . '"></span>' : '' ?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                <tr>
                    <td colspan="2"><b>Оборты за период</b></td>
                    <td><b><?= $total_sell . $currency_icon; ?></b></td>
                    <td><b><?= $total_pay . $currency_icon;?></b></td>
                </tr>
                <tr>
                    <td colspan="2"><b>Сальдо конечное</b></td>
                    <td><b><?= round($total_sell - $total_pay + $start_balance) . $currency_icon; ?></b></td>
                    <td></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>
