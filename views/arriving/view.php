<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Arriving */
?>
<div class="arriving-view">

    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'number',
                'created_at:datetime',
                [
                    'attribute' => 'contractor_id',
                    'value' => $model->contractor->full_name ?? $model->contractor->name,
                ],
            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), '_error');
    } ?>

</div>
