<?php

/**
 * @var array $data Информация об импорте
 */


?>

<div class="container">

    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title">Импорт завершен</h3>
        </div>
        <div class="panel-body">
            <p>Всего юр. лиц: <?= $data['count'] ?></p>
            <p>Импортировано юр. лиц: <?= $data['person_count'] ?></p>
            <p>Пропущено юр. лиц: <?= $data['person_missing'] ?></p>
        </div>
    </div>
    <?php if ($data['warnings']): ?>
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h3 class="panel-title">Предупреждения импорта</h3>
            </div>
            <div class="panel-body">
                <?= $data['warnings']; ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if ($data['errors']): ?>
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">Ошибки импорта</h3>
            </div>
            <div class="panel-body">
                <?= $data['errors']; ?>
            </div>
        </div>
    <?php endif; ?>
</div>
