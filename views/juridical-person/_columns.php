<?php

use app\models\JuridicalPerson;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'name',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'full_name',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'inn',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'kpp',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'bik',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'pay_off_account',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'corr_account',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'contractor_id',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'contractor_name',
        'content' => function (JuridicalPerson $model) {
            $contractor = $model->contractor ?? null;
            if ($contractor){
                return \yii\helpers\Html::a($model->contractor->name ?? $model->contractor->full_name,
                    ['/contractor/view', 'id' => $model->contractor->id], ['role' => 'modal-remote']);
            }
            return null;
        }
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'View', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Update', 'data-toggle' => 'tooltip'],
        'deleteOptions' => [
            'role' => 'modal-remote',
            'title' => 'Delete',
            'data-confirm' => false,
            'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Are you sure?',
            'data-confirm-message' => 'Are you sure want to delete this item'
        ],
    ],

];   