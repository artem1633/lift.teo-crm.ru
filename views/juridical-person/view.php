<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\JuridicalPerson */
?>
<div class="juridical-person-view">

    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'name',
                'full_name',
                'inn',
                'kpp',
                'bik',
                'pay_off_account',
                'corr_account',
                [
                    'attribute' => 'contractor_id',
                    'value' => $model->contractor->name ?? $model->contractor->full_name,
                ],
            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), '_error');
        echo $e->getMessage();
    } ?>

</div>
