<?php

use kartik\file\FileInput;
use yii\widgets\ActiveForm;

?>

    <div class="row">
        <div class="import-form">

            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

            <div class="col-md-12">
                <?= $form->field($model, 'file')->widget(FileInput::class, [
                    'options' => [
                        'multiple' => false,
                    ],
                    'pluginOptions' => [
                        'showPreview' => false,
                    ]
                ])->label('Выберите XML файл для импорта и нажмите "Загрузить"'); ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
