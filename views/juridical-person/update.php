<?php

/* @var $this yii\web\View */
/* @var $model app\models\JuridicalPerson */
?>
<div class="juridical-person-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
