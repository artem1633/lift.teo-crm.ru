<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-8">
            <?= $form->field($model, 'fio')->textInput(['maxlength' => true, 'placeholder' => 'ФИО'])->label(false) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'telephone')->textInput(['maxlength' => true, 'placeholder' => 'Телефон'])->label(false) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'login')->textInput(['maxlength' => true, 'placeholder' => 'Логин'])->label(false) ?>
        </div>
        <div class="col-md-6">
            <?= $model->isNewRecord ? $form->field($model, 'password')->textInput(['maxlength' => true, 'placeholder' => 'Пароль'])->label(false) : $form->field($model, 'new_password')->textInput(['maxlength' => true, 'placeholder' => 'Новый пароль'])->label(false) ?>
        </div>
        <div class="col-md-6">
            <?php
            if (Yii::$app -> user -> identity -> permission == "administrator") {
                echo $form->field($model, 'permission')->dropDownList($model->getRoleList(), ['placeholder' => 'Выберите должность']);
                } else {
                echo '<p style="padding-top: 20px"><b>Должность</b></p>';
                echo '<p style="padding-top: 10px">' . $model -> permission . '</p>';
            }
            ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'access')->dropDownList([ 'Вкл' => 'Вкл', 'Выкл' => 'Выкл'], ['placeholder' => 'Доступ'])->label() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => 'Имайл'])->label(false) ?>
        </div>
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
