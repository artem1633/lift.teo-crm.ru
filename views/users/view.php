<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
?>
<div class="users-view">

    <div class="col-xs-12">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
//                'id',
                'fio',
                'login',
                'email',
                [
                    'attribute' => 'permission',
                    'value' => $model->getRoleDescription(),
                ],
                'access',
                'telephone',
            ],
        ]) ?>
    </div>

</div>
