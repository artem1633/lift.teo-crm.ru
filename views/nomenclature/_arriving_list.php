<?php

/* @var $this yii\web\View */
/* @var $detail app\models\Nomenclature */

/* @var $arrivings \app\models\Arriving[] Поступления */
/* @var $logistic_requests \app\models\LogisticRequest[] Поступления */

use yii\helpers\Html;

?>

<div class="request-list">
    <table class="table table-hover">
        <tbody>
        <?php foreach ($arrivings as $item): ?>
            <tr>
                <td>Поступление №<?= Html::a($item->number, [
                        '/detail-to-arriving',
                        'arriving_id' => $item->id,
                        'DetailToArrivingSearch[detail_name]' => $detail->name
                    ], [
                        'title' => 'Перейти к поступлению',
                        'target' => '_blank'
                    ]); ?></td>
                <td><?= $item->count ?> шт.</td>
            </tr>
        <?php endforeach; ?>
        <?php foreach ($logistic_requests as $item): ?>
            <tr>
                <td>Счет №<?= Html::a($item->number, [
                        '/logistic-request-part/view-parts',
                        'logistic_request_id' => $item->id,
                    ], [
                        'title' => 'Перейти к счету',
                        'target' => '_blank'
                    ]); ?></td>
                <td><?= $item->count ?> шт.</td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>