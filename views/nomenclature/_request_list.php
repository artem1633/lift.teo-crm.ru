<?php

/* @var $this yii\web\View */
/* @var $model app\models\Nomenclature */
/* @var $requests \app\models\Request[] Заявки */
/* @var $invoices array Счета (из логистики)*/

use yii\helpers\Html;

?>

    <div class="request-list">
        <table class="table table-hover">
            <tbody>
            <?php foreach ($invoices as $item): ?>
            <tr>
                <td>Счет №<?= Html::a($item->account_number, [
                        '/logistic-request-part/view-parts',
                        'logistic_request_id' => $item->id
                    ], [
                            'title' => 'Перейти к счету'
                    ]); ?></td>
                <td><?= $item->count ?> шт.</td>
            </tr>
            <?php endforeach; ?>
            <?php foreach ($requests as $item): ?>
                <tr>
                    <td>Заявка №<?= Html::a($item->account_number, ['/request/view', 'id' => $item->id], [
                            'title' => 'Перейти к заявке'
                        ]) ?></td>
                    <td><?= $item->count; ?></td>
                </tr>
            <?php endforeach; ?>

            </tbody>
        </table>
    </div>