<?php

use app\models\Functions;
use app\models\Measure;
use app\models\Users;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Nomenclature */
/* @var $form yii\widgets\ActiveForm */
/* @var int $request_id ID запроса */

?>

    <div class="nomenclature-form">

        <?php $form = ActiveForm::begin([
            'options' => [
                'method' => 'post',
                'enctype' => 'multipart/form-data'
            ]
        ]); ?>

        <?= $form->field($model, 'request_id')->hiddenInput()->label(false)?>

        <div style="display: none;">
            <?php
            $additional_photos_hidden = json_decode($model->additional_photos, true);
            if (!empty($additional_photos_hidden)) {
                $additional_photos_hidden = implode(", ", $additional_photos_hidden);
            } else {
                $additional_photos_hidden = "";
            }
            Yii::info('Доп. фотки: ' . $model->additional_photos, __METHOD__);
            ?>
            <input id="additional_photos_hidden" name="additional_photos_hidden" type="hidden"
                   value="<?= $additional_photos_hidden ?>">
            <input id="main_photo_hidden" name="main_photo_hidden" type="hidden" value="<?= $model->main_photo ?>">
            <input id="model_id" name="model_id" type="hidden" value="<?= $model->id ?>">
            <input id="updateRecord" name="updateRecord" type="hidden" value="<?= $model->isNewRecord ? 0 : 1 ?>">
        </div>
        <?php if (Users::isAdmin()) { ?>
            <div class="col-xs-12">
                <?php
                $nom_type = Yii::$app->request->get('nomenclature_type');

                if ($nom_type) {
                    if ($nom_type == Users::USER_ROLE_TECH_SPECIALIST || $nom_type == Users::USER_ROLE_ADMIN) {
                        $model->nomenclature_type = Users::USER_ROLE_TECH_SPECIALIST;
                    } else {
                        $model->nomenclature_type = Users::USER_ROLE_MANAGER;
                    }
                }


                ?>
                <?= $form->field($model, 'nomenclature_type')->dropDownList([
                    Users::USER_ROLE_MANAGER => 'База менеджера',
                    Users::USER_ROLE_TECH_SPECIALIST => 'База тех.специалиста',
                ], ['prompt' => 'Выберите базу, в которую будет добавлена деталь'])->label(false) ?>
            </div>
        <?php } ?>
        <div class="col-xs-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Название'])->label(false) ?>
        </div>
        <div class="col-xs-6">
            <?= $form->field($model, 'vendor_code')->textInput(['maxlength' => true, 'placeholder' => 'Артикул'])->label(false) ?>
        </div>
        <div class="col-xs-6">
            <?php echo $form->field($model, 'own_vendor_code')->textInput([
                    'maxlength' => true,
                'placeholder' => 'Собственный артикул',
                'disabled' => Users::isTech(),
            ])->label(false) ?>
        </div>
        <div class="col-xs-6">
            <?= $form->field($model, 'brand_id')->label(false)->widget(Select2::classname(), [
                'data' => Functions::getBrandsList(),
                'options' => ['placeholder' => 'Бренд'],
                'pluginOptions' => [
                    'tags' => true,
                    'allowClear' => true,
                ],
            ]); ?>
        </div>
        <div class="col-xs-6">
            <?= $form->field($model, 'group_id')->label(false)->widget(Select2::classname(), [
                'data' => Functions::getGroupList(),
                'options' => ['placeholder' => 'Группа'],
                'pluginOptions' => [
                    'tags' => true,
                    'allowClear' => true,
                ],
            ]); ?>
        </div>
        <div class="col-xs-3">
            <?= $form->field($model, 'weight')->textInput(['placeholder' => 'Вес'])->label(false) ?>
        </div>
        <div class="col-xs-3">
            <?= $form->field($model, 'measure_id')->dropDownList(Measure::getList())->label(false) ?>
        </div>
        <div class="col-xs-6">
            <?= $form->field($model, 'type_of_parts_id')->label()->widget(Select2::classname(), [
                'data' => Functions::getTypeOfPartsList(),
                'options' => ['placeholder' => 'Тип запчасти'],
                'pluginOptions' => [
                    'tags' => true,
                    'allowClear' => true,
                ],
            ])->label(false); ?>
        </div>
        <div class="col-xs-6">
            <?= $form->field($model, 'type_of_mechanism_id')->label(false)->widget(Select2::classname(), [
                'data' => Functions::getTypeOfMechanismList(),
                'options' => ['placeholder' => 'Тип механизма'],
                'pluginOptions' => [
                    'tags' => true,
                    'allowClear' => true,
                ],
            ])->label(false); ?>
        </div>
        <div class="col-xs-12">
            <?= $form->field($model, 'description')->textarea(['rows' => 6, 'placeholder' => 'Описание'])->label(false) ?>
        </div>
        <div class="col-xs-12 file-preview ">
            <div class="row ">
                <div class="col-xs-12">
                    <?= $form->field($model, 'file')->fileInput(['multiple' => false, 'accept' => 'image/*']) ?>
                </div>
                <div class="col-xs-12 text-center">
                    <?php
                    if (!$model->isNewRecord) {
                        //Показываем основное изображение
//                        VarDumper::dump($model, 10, true);
                        echo Html::img($model->main_photo, ['style' => 'max-height: 200px']);
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <?php

            if ($model->isNewRecord) {
                echo $form->field($model, 'files[]')->widget(FileInput::classname(), [
                    'name' => 'attachment_48[]',
                    'options' => [
                        'multiple' => true,
                        'accept' => 'image/*'
                    ],
                    'pluginOptions' => [
                        'showUpload' => false,
                        'allowedFileExtensions' => ['jpg', 'jpeg', 'png'],
                        'browseLabel' => '',
                        'removeLabel' => '',
                        'mainClass' => 'input-group-lg'
                    ]
                ])->label(false);
            } else { //Если редактирование
                if (!empty($additional_photos_hidden)) {
                    $images = Functions::additionalPhotosHtmlEdit($model->additional_photos);
                    echo $form->field($model, 'files[]')->widget(FileInput::classname(), [
                        'name' => 'attachment_48[]',
                        'options' => [
                            'multiple' => true
                        ],
                        'pluginOptions' => [
                            'initialPreviewShowDelete' => true,
                            'allowedFileExtensions' => ['jpg', 'jpeg', 'png'],
                            'overwriteInitial' => false,
                            'showPreview' => true,
                            'showCaption' => true,
                            'showRemove' => true,
                            'showUpload' => true,
                            'initialPreview' => $images,
                            'initialPreviewConfig' => [
                            ],
                        ],
                    ])->label(false);
                } else { //Если нет доп фоток
                    echo $form->field($model, 'files[]')->widget(FileInput::classname(), [
                        'name' => 'attachment_48[]',
                        'options' => [
                            'multiple' => true,
                            'accept' => 'image/*'
                        ],
                        'pluginOptions' => [
                            'showUpload' => false,
                            'browseLabel' => '',
                            'removeLabel' => '',
                            'mainClass' => 'input-group-lg'
                        ]
                    ])->label(false);
                }
            }
            ?>
        </div>


        <?php if (!Yii::$app->request->isAjax) { ?>
            <div class="col-xs-12">
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить изменения', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary nomenclature_update']) ?>
                </div>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>

    </div>

<?php
$script = <<< JS
$( document ).ready(function() {
    var update_record = $('#updateRecord').val();
         console.log('update_record');
         console.log(update_record);
    if (update_record === 1) {
         // var deleted_item_main = [];
         var deleted_items = [];
         
         // var main_photo = $('#main_photo_hidden').val();
         // console.log('main_photo');
         // console.log(main_photo);
         
         var additional_photos = $('#additional_photos_hidden').val();
         console.log('additional_photos');
         console.log(additional_photos);
         var model_id = $('#model_id').val();
         // btn btn-default btn-secondary fileinput-remove fileinput-remove-button
         // $(".btn.btn-default.btn-secondary.fileinput-remove.fileinput-remove-button").on('click', function () {
         //    
         // });
         $(".kv-file-remove.btn.btn-sm.btn-kv.btn-default.btn-outline-secondary").on('click', function () {
            console.log('v-file-remove clicked');
            var parent = $(this).closest('.file-preview-frame.krajee-default.file-preview-initial');
            //
            // if (parent.hasClass('main_photo')) {
            //     deleted_item_main.push(parent.find('.file-preview-image').attr('src'));
            // }else 
            //     if (parent.hasClass('additional_photos')) {
            //    
            // }
            deleted_items.push(parent.find('.file-preview-image').attr('src'));
            console.log('deleted_items');
            console.log(deleted_items);
            //
            parent.addClass('hidden');
            $('.nomenclature_update').on('click', function() {
            $.post(
                '/nomenclature/additional-delete',
                {
                    // deleted_item_main: deleted_item_main,
                    deleted_items: deleted_items,
                    additional_photos: additional_photos, 
                    model_id: model_id
                },
                function(data) {
                    console.log('data');
                    console.log(data);
                }
            ).fail(function(data) {
                    console.log('error data');
                    console.log(data);
            });
        });
    });   
    }
});

JS;
$this->registerJs($script);
?>