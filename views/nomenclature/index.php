<?php

use app\models\Functions;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NomenclatureSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $nomenclature_type string */

$this->title = '';
//$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<style>
    table tr th {
        color: #3c8dbc !important;
    }
</style>
    <div class="nomenclature-index">
        <div id="ajaxCrudDatatable">
            <?php
            $current_nomenclature_type = '';
            if (!empty($nomenclature_type)) {
                $current_nomenclature_type = '?nomenclature_type=' . $nomenclature_type;
            }
            ?>
            <?php
            try {
                echo GridView::widget([
                    'id' => 'crud-datatable',
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'pjax' => true,
                    'responsiveWrap' => false,
                    'options' => ['style' => 'font-size:12px;'],
                    'columns' => require(__DIR__ . '/_columns.php'),
                    'toolbar' => [
                        [
                            'content' =>
                                Html::a('<i class="glyphicon glyphicon-plus check_repeat"></i>',
                                    ['create' . $current_nomenclature_type],
                                    [
                                        'role' => 'modal-remote',
                                        'title' => 'Создать новую номенклатуру',
                                        'class' => 'btn btn-default'
                                    ]) .
//                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
//                                ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Сбросить сетку']) .
                                '{toggleData}' .
                                Html::a('<i class="glyphicon glyphicon-repeat"></i> Пересчитать остатки',
                                    ['recalculate-fact-balance'],
                                    [
                                        'id' => 'recalculate-balance-btn',
                                        'data-pjax' => 1,
                                        'class' => 'btn btn-default',
                                        'title' => 'Пересчитать остатки',
                                        'style' => $nomenclature_type === \app\models\Users::USER_ROLE_TECH_SPECIALIST ? 'display:block' : 'display:none',
                                    ])
                        ],
                    ],
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'panel' => [
                        'type' => 'primary',
                        'heading' =>
                            Html::button('<i class="fa fa-chevron-circle-left" style="font-size: 1.7rem; padding: 0;">&nbsp;Назад</i>',
                                [
                                    'title' => 'Все Детали',
                                    'class' => 'btn',
                                    'style' => 'color:white;background:transparent; border:transparent',
                                    'onclick' => 'window.history.back();'
                                ]) .
                            '<i class="glyphicon glyphicon-list"></i> Номенклатура',
                        //                'before'=>'<em>* Resize table columns just like a spreadsheet by dragging the column edges.</em>',
                        'after' => Functions::getBulkButtonWidget() .
                            '<div class="clearfix"></div>',
                    ]
                ]);
            } catch (Exception $e) {
                Yii::error($e->getTraceAsString(), __METHOD__);
                Yii::$app->session->addFlash('error', $e->getMessage());
            } ?>
        </div>
    </div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>

<?php Modal::end(); ?>
