<?php

use app\models\DetailToArriving;
use app\models\Nomenclature;
use app\models\Reserve;
use app\models\Users;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Functions;

if (Users::isTech() || Users::isManager()) {
    $template = '{view}';
} else {
    $template = '{view} {update} {delete}';
}
return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'name',
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'vendor_code',
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'own_vendor_code',
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'brand_id',
        'filter' => Functions::getBrandsList(),
        'value' => function ($data) {
            return Functions::getBrand($data->brand_id);
        },
        'format' => 'html',
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'group_id',
        'filter' => Functions::getGroupList(),
        'value' => function ($data) {
            return Functions::getGroup($data->group_id);
        },
        'format' => 'html',
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'weight',
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'type_of_parts_id',
        'filter' => Functions::getTypeOfPartsList(),
        'value' => function ($data) {
            return Functions::getTypeOfPart($data->type_of_parts_id);
        },
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'reserve',
        'filter' => [1 => 'Только резерв'],
        'value' => function (Nomenclature $data) {
            $total_reserve = Reserve::getTotalReservesCountByDetail($data->id);
            if ($total_reserve > 0){
                return Html::a($total_reserve, ['get-reserve-requests', 'id' => $data->id], [
                    'role' => 'modal-remote',
                    'data-pjax' => 1,
                    'title' => 'Просмотреть список счетов и заявок'
                ]);
            } else {
                return '';
            }

        },
        'format' => 'raw',
        'vAlign' => 'middle',
        'hAlign' => 'center',
        'visible' => Users::isAdmin() || Users::isTech(),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'on_the_way',
        'filter' => [1 => 'Только в пути'],
        'value' => function (Nomenclature $data) {

            $count = DetailToArriving::getOnTheWayCount($data->id);
            return Html::a($count, ['get-way-arriving', 'id' => $data->id], [
                'role' => 'modal-remote'
            ]);


        },
        'format' => 'raw',
        'vAlign' => 'middle',
        'hAlign' => 'center',
        'visible' => Users::isAdmin() || Users::isTech(),
    ],
    [
        'attribute' => 'sibir',
        'filter' => [1 => 'Все детали', 2 => 'В наличии'],
        'label' => 'Склад «ПМ Сибирь»',
        'value' => function (Nomenclature $data) {
            return DetailToArriving::getBasisCount($data->id);
        },
        'hAlign' => 'center',
        'vAlign' => 'middle'
    ],
    [
//        'attribute' => 'fact_balance',
        'attribute' => "liftgou",
        'filter' => [1 => 'Все детали', 2 => 'В наличии'],
        'value' => function (Nomenclature $data) {


            $count = DetailToArriving::getOnWayCountLiftgou($data->id);
            return Html::a($count, ['get-way-arriving', 'id' => $data->id], [
                'role' => 'modal-remote'
            ]);
        },
        'format' => 'raw',
        'label' => 'Склад «Лифтгоу»',
        'hAlign' => 'center',
        'vAlign' => 'middle'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'description',
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'main_photo',
        'value' => function (Nomenclature $data) {
            return (!$data->main_photo) ? '' : '<div>' . '<img class="main_photo" src="' . $data->main_photo . '">' . '</div>';
        },
        'format' => 'html',
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'avg_purchase_price',
        'label' => 'Себестоимость',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'template' => $template,
        'urlCreator' => function ($action, $model, $key) {
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['title' => 'Просмотр', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Редактирование', 'data-toggle' => 'tooltip'],
        'deleteOptions' => [
            'role' => 'modal-remote',
            'title' => 'Удаление',
            'data-confirm' => false,
            'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Вы уверены?',
            'data-confirm-message' => 'Вы уверены что хотите удалить все эти элементы?'
        ],
    ],

];