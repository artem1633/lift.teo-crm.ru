<?php

use app\models\Users;
use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Functions;

/* @var $this yii\web\View */
/* @var $model app\models\Nomenclature */

?>
<div class="nomenclature-view">
    <div class="col-xs-3">
        <?php
        if ($model->request_id) {
            echo Html::a('<span class="fa fa-chevron-circle-left"></span>&nbsp;Назад к заявке<br>',
                ['/request/view', 'id' => $model->request_id]);
        } else {
            echo Html::a('<span class="fa fa-chevron-circle-left"></span>&nbsp;Назад', Yii::$app->request->referrer);
        }
        ?>
        <?php
        if (!Users::isManager() || $model->nomenclature_type == 'manager') {
            echo Html::a('<span class="fa fa-pen-square"></span>&nbsp;Редактировать',
                ['/nomenclature/update', 'id' => $model->id, 'request_id' => $model->request_id]);
        }
        ?>
    </div>
    <div class="col-xs-6">
        <?php if (!empty($model)) { ?>
            <?php
            try {
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'name',
                        'vendor_code',
                        'own_vendor_code',
                        [
                            'attribute' => 'brand_id',
                            'value' => function ($data) {
                                return Functions::getBrand($data->brand_id);
                            },
                        ],
                        [
                            'attribute' => 'group_id',
                            'value' => function ($data) {
                                return Functions::getGroup($data->group_id);
                            },
                        ],
                        'weight',
                        [
                            'attribute' => 'measure_id',
                            'value' => $model->measure->name ?? null,
                        ],
                        [
                            'attribute' => 'type_of_parts_id',
                            'value' => function ($data) {
                                return Functions::getTypeOfPart($data->type_of_parts_id);
                            },
                        ],
                        [
                            'attribute' => 'type_of_mechanism_id',
                            'value' => function ($data) {
                                return Functions::getTypeOfMechanism($data->type_of_mechanism_id);
                            },
                        ],
                        'description',
                        [
                            'attribute' => 'main_photo',
                            'value' => function ($data) {
                                if (!$data->main_photo) {
                                    return '';
                                } else {
                                    return '<a class="fancy_box" href="' . $data->main_photo . '">' . "<img src='" . $data->main_photo . "' style='max-height: 100px'>" . '</a>';

                                }
                            },
                            'format' => 'html',
                        ],
                        'updated_date',
                        [
                            'attribute' => 'additional_photos',
                            'value' => function ($data) {
                                return !json_decode($data->additional_photos,
                                    true) ? '' : '<div style="display: flex; flex-wrap: wrap;">' . Functions::photosHtml($data->additional_photos) . '<div>';
                            },
                            'format' => 'html',
                        ]
                    ],
                ]);
            } catch (Exception $e) {
                Yii::error($e->getMessage(), '_error');
            } ?>
        <?php } ?>
    </div>
</div>
<div class="clearfix"></div>

<?php
$script = <<< JS
    $(document).ready(function() {
        
        var fancy_box = $('.fancy_box');
        for (var i = 0; i < fancy_box.length; i++) {
            $(fancy_box[i]).attr('data-fancybox', 'gallery');
        }
        $("a.gallery").fancybox();
        
    });
JS;
$this->registerJs($script);
?>
