<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DeliveryTime */
?>
<div class="delivery-time-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
