<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DeliveryTime */
?>
<div class="delivery-time-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
