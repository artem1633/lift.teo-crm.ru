<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Warehouse $model */

?>
<div class="warehouse-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
