<?php

namespace app\controllers;

use app\models\DetailToArriving;
use app\models\ReconciliationReport;
use Yii;
use app\models\Arriving;
use app\models\ArrivingSearch;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * ArrivingController implements the CRUD actions for Arriving model.
 */
class ArrivingController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['administrator', 'manager', 'tech_specialist', 'logist']
                    ],
                    [
                        'actions' => ['update', 'delete', 'create'],
                        'allow' => false,
                        'roles' => ['user'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['user'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Arriving models.
     * @param int $id Идентификатор детали
     * @param string $start Начало периода
     * @param string $end КОнец периода
     * @return mixed
     */
    public function actionIndex($id = 0, $start = '', $end = '')
    {
        $searchModel = new ArrivingSearch();
        if ($id && $start && $end) {
            $dataProvider = new ActiveDataProvider([
                'query' => Arriving::find()
                    ->joinWith(['details dta'])
                    ->andWhere(['dta.detail_id' => $id])
                    ->andWhere(['BETWEEN', 'arriving.created_at', $start, $end])
            ]);
        } else {
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        }
        $dataProvider->sort->defaultOrder = ['created_at' => SORT_DESC];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Arriving model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Arriving #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Закрыть',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Редактировать', ['update', 'id' => $id],
                        ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Arriving model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Arriving();
        $dta_model = new DetailToArriving();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Создание поступления",
                    'size' => 'large',
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                        'dta_model' => $dta_model,
                    ]),
                    'footer' => Html::button('Закрыть',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Далее', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => "Создание поступления",
                        'content' => '<span class="text-success">Поуступление успешно создано</span>',
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
//                            Html::a('Создать еще',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
                            Html::a('Перейти к добавлению деталей',
                                ['/detail-to-arriving/index', 'arriving_id' => $model->id],
                                ['class' => 'btn btn-primary'])

                    ];
                } else {
                    return [
                        'title' => "Создание поступления",
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing Arriving model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Редактирование поступления #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'forceClose' => true,
                    ];
                } else {
                    return [
                        'title' => "Редактирование поступления #" . $id,
                        'content' => $this->renderAjax('update', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Arriving model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Arriving model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBulkdelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Arriving model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Arriving the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Arriving::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionReconciliationReport()
    {
        $request = Yii::$app->request;

        $rec_report = new ReconciliationReport();
        $data = null;
        $cur_icon = '';

        $rec_report->load($request->get());

        if ($rec_report->contractor_id || $rec_report->report_start_date || $rec_report->report_end_date) {
            if (!$rec_report->report_start_date) {
                $rec_report->report_start_date = '2000-01-01 00:00:00';
            }

            if (!$rec_report->report_end_date) {
                $rec_report->report_end_date = date('Y-m-d H:i:s', time());
            }

            $data = $rec_report->getDataReport();
            //Преобразуем даты для вывода в фильтрах
            $rec_report->report_start_date = date('d.m.Y', strtotime($rec_report->report_start_date));
            $rec_report->report_end_date = date('d.m.Y', strtotime($rec_report->report_end_date));
            $cur_icon = '&nbsp;<span class="fa ' . $data['data'][0]['icon'] . '"></span>';
        }

        return $this->render('reconciliation_report', [
            'model' => $rec_report,
            'data' => $data['data'],
            'start_balance' => $data['start_balance'] ?? 0,
            'currency_icon' => $cur_icon,
        ]);

    }

    /**
     * Меняет статус поступления на противоположный
     * @param $id
     * @return array
     */
    public function actionChangeStatus($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = Arriving::findOne($id);

        if ($model->status == $model::STATUS_SAVED) {
            $model->status = $model::STATUS_ACCEPTED;
        } else {
            $model->status = $model::STATUS_SAVED;

        }

        if (!$model->save()){
            Yii::error($model->errors, '_error');
        }
        return [
            'forceReload' => '#crud-datatable-pjax',
            'forceClose' => true,
        ];
    }
}
