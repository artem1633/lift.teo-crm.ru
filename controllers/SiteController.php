<?php

namespace app\controllers;

use app\components\AccessController;
use app\models\Functions;
use app\models\Reserve;
use app\models\Settings;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends AccessController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
//                'only' => ['logout'],
                'rules' => [
//                    [
//                        'actions' => ['index','logout'],
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
                    [
                        'actions' => ['login', 'error', 'backup', 'check-reserve'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['administrator', 'manager', 'user', 'tech_specialist', 'logist'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
//            return $this->redirect(['/index']);
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionTest()
    {
        echo md5('admin');
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect('login');
//        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionAuthorization()
    {
        if (isset(Yii::$app->user->identity->id)) {
            return $this->render('error');
        } else {
            Yii::$app->user->logout();
            return $this->redirect(['login']);
        }
    }

    /**
     * Создание бэкапа
     * @throws \yii\base\Exception
     */
    public function actionBackup()
    {
        /** @var \demi\backup\Component $backup */
        $backup = Yii::$app->backup;

        $file = $backup->create();

        if ($file) {
            //Подчищаем бэкапы
            Functions::cleanBackups();
        }


        return 'Backup file created: ' . $file . PHP_EOL;


    }

    /**
     * Просмотр файлов в папке бэкапов
     */
    public function actionViewBackups()
    {
        $path_backup_dir = Url::to('@app/backups');
        Yii::info($path_backup_dir, 'test');
        $files = array_diff(scandir($path_backup_dir), ['..', '.', '.gitignore']);
        $prepared_files = [];
        foreach ($files as $file) {
            $path_file = $path_backup_dir . '/' . $file;
            if (is_file($path_file)) {
                array_push($prepared_files, $file . ' ' . (round(filesize($path_file) / 1024 / 1024, 2)) . ' Mb');
            }
        }
        VarDumper::dump($prepared_files, 10, true);
        die();
    }

    /**
     * Скачивание файлов логов в зависимости от типа лога
     * @param string $name Тип логов
     * Типы логов:
     * test, api, app
     * @return \yii\console\Response|NotFoundHttpException|Response
     * @throws NotFoundHttpException
     */
    public function actionGetLog($name)
    {
        $file_path = '';

        switch ($name) {
            case 'test':
                $file_path = Url::to('@app/runtime/logs/test.log');
                break;
            case 'api':
                $file_path = Url::to('@app/runtime/logs/api_error.log');
                break;
            case 'app':
                $file_path = Url::to('@app/runtime/logs/app.log');
                break;
            case '_error':
                $file_path = Url::to('@app/runtime/logs/_error.log');
                break;

        }

        if (is_file($file_path)) {
            return Yii::$app->response->sendFile($file_path);
        }

        throw new NotFoundHttpException('Файл не найден');
    }

    /**
     * Удаление старых бэкапов
     */
    public function actionCleanBackups()
    {
        Functions::cleanBackups();
    }

    /**
     * Проверка резервов, установленных менеджерами. Снятие отметок, если прошло больше трёх дней
     * @return array
     */
    public function actionCheckReserve(): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::warning('check reserve started', 'check reserve test');
        //Проверка времени в настройках
        $key = 'reserve_checked_time';
        $last_time = Settings::getValueByKey($key);

        if ($last_time === null) {
            //Добавляем настройку
            $last_time = time();
            $item = new Settings([
                'key' => 'reserve_checked_time',
                'name' => 'Время последней проверки резервов',
                'value' => date('Y-m-d H:i:s', $last_time),
            ]);
            if (!$item->save()) {
                Yii::error($item->errors, '_error');
                return [
                    'success' => false,
                    'error' => $item->errors,
                ];
            }
        } else {
            //Дата и время в настройках имеется, сверяем с текущим
            $max_period = 60 * 60; //Ограничение частоты проверки
            $cur_time = time();
            $last_time = strtotime($last_time);

            if (($cur_time - $last_time) <= $max_period) {
                //Макс. период еще не прошел и пользователь не админ
                return [
                    'success' => false,
                    'error' => 'Превышение частоты проверки. Попробуйте позже'
                ];
            }
        }

        $result = Reserve::check();

        Settings::setValueByKey($key, date('Y-m-d H:i:s', time()));
        Yii::warning('check reserve ended', 'check reserve test');

        return [
            'success' => true,
            'result' => $result,
        ];
    }
}
