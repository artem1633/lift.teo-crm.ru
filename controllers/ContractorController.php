<?php

namespace app\controllers;

use app\components\AccessController;
use app\models\Actions;
use app\models\ActionsSearch;
use Yii;
use app\models\Contractor;
use app\models\ContractorSearch;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * ContractorController implements the CRUD actions for Contractor model.
 */
class ContractorController extends AccessController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Contractor models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new ContractorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 40;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Contractor model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Контрагент #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Редактировать',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            $actionsSearchModel = new ActionsSearch();
            $actionsDataProvider = $actionsSearchModel->searchByContractor($id);

            return $this->render('view', [
                'model' => $this->findModel($id),
                'actionsDataProvider' => $actionsDataProvider,
            ]);
        }
    }

    /**
     * Creates a new Contractor model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Contractor();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Создать нового Контрагента",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary pull-right','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post())){
//                $model -> created_date = Functions::rusDate(time(), '%DAYWEEK%, j %MONTH% Y, G:i');
//                $model -> updated_date = Functions::rusDate(time(), '%DAYWEEK%, j %MONTH% Y, G:i');
                //Кто создал? пользователь который залогинился.
                $model -> created_by = Yii::$app -> user -> identity -> id;
                if ($model->save()) {
                    return [
                        'forceReload'=>'#crud-datatable-pjax',
                        'title'=> "Создать новый Контрагент",
                        'content'=>'<span class="text-success">Создание Контрагента прошло успешно.</span>',
                        'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать еще',['create'],['class'=>'btn btn-primary pull-right','role'=>'modal-remote'])

                    ];
                }else{
                    return [
                        'title'=> "Создать нового Контрагента",
                        'content'=>$this->renderAjax('create', [
                            'model' => $model,
                        ]),
                        'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn-primary pull-right','type'=>"submit"])

                    ];
                }
            }else{           
                return [
                    'title'=> "Создать новый Контрагент",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary pull-right','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Contractor model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Редактировать Контрагента #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary pull-right','type'=>"submit"])
                ];
            }else if($model->load($request->post())){
                if (!$model->created_by) { //Если пользователь, создавыший контрагента был удален назначаем создателем текущего пользователя
                   $model->created_by = Yii::$app->user->id;
                }
                if ($model->save()) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
//                    return [
//                        'forceReload'=>'#crud-datatable-pjax',
//                        'title'=> "Контрагент #".$id,
//                        'content'=>$this->renderAjax('view', [
//                            'model' => $model,
//                        ]),
//                        'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
//                            Html::a('Редактировать',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
//                    ];
                }else{
                    return [
                        'title'=> "Редактировать Контрагента #".$id,
                        'content'=>$this->renderAjax('update', [
                            'model' => $model,
                        ]),
                        'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn-primary pull-right','type'=>"submit"])
                    ];
                }
            }else{
                 return [
                    'title'=> "Редактировать Контрагента #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary pull-right','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Contractor model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $pks = Actions::find() -> where(['contractor_id' => $id]) -> all();
        if (count($pks) > 0) {
            foreach ( $pks as $pk ) {
                $pk->delete();
            }
        }
        $this->findModel($id)->delete();
//        if (Yii::$app -> user -> identity -> permission == "administrator") {
//            $this->findModel($id)->delete();
//            Yii::$app -> session -> setFlash("success", "Контагентов удален.");
//        } else {
//            Yii::$app -> session -> setFlash("error", "Вы не можете удалять Контагентов.");
////            $this -> refresh();
////            return $this->redirect(['index']);
//        }
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Contractor model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBulkdelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }
//        if (Yii::$app -> user -> identity -> permission == "administrator") {
//            $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
//            foreach ( $pks as $pk ) {
//                $model = $this->findModel($pk);
//                $model->delete();
//            }
//            Yii::$app -> session -> setFlash("success", "Контагенты удалены.");
//        } else {
//            Yii::$app -> session -> setFlash("error", "Вы не можете удалять Контагентов.");
//        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Contractor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Contractor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Contractor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }

}
