<?php

namespace app\controllers;

use app\models\LogisticRequest;
use Yii;
use app\models\LogisticRequestPart;
use app\models\LogisticRequestPartSearch;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * LogisticRequestPartController implements the CRUD actions for LogisticRequestPart model.
 */
class LogisticRequestPartController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['administrator', 'logist']
                    ],
                    [
                        'actions' => ['editable'],
                        'allow' => true,
                        'roles' => ['tech_specialist']
                    ],
                    [
                        'actions' => ['update', 'delete', 'create', 'editable', 'fix-log-req-part'],
                        'allow' => false,
                        'roles' => ['user', 'manager', 'tech_specialist'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['user', 'manager', 'tech_specialist'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Lists all LogisticRequestPart models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LogisticRequestPartSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LogisticRequestPart model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Заявка #" . $model->request_number,
                'content' => $this->renderAjax('view', [
                    'model' => $model,
                ]),
                'footer' => Html::button('Закрыть',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Редактировать', ['update', 'id' => $id],
                        ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new LogisticRequestPart model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @param int $logistic_request_id Идентификатор счета
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionCreate($logistic_request_id)
    {
        $request = Yii::$app->request;

        $logistic_request = LogisticRequest::find()->andWhere(['id' => $logistic_request_id])->one() ?? null;

        if (!$logistic_request) {
            throw new NotFoundHttpException('Счет не найден!');
        }

        $model = new LogisticRequestPart();
        $model->logistic_request_id = $logistic_request_id;

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Добавление позиции к счету " . $logistic_request->account_number,
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                        'logistic_request' => $logistic_request
                    ]),
                    'footer' => Html::button('Закрыть',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post())) {
                    $model->logistic_request_id = $logistic_request_id;
                    $model->contract_date = $logistic_request->payed_date;
                    if (!$model->save()) {
                        return [
                            'title' => "Добавление позиции к счету " . $logistic_request->account_number,
                            'content' => $this->renderAjax('create', [
                                'model' => $model,
                                'logistic_request' => $logistic_request
                            ]),
                            'footer' => Html::button('Закрыть',
                                    ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                        ];
                    }
                    return [
                        'forceReload' => '#crud-datatable-parts-pjax',
                        'title' => "Добавление позиции к счету " . $logistic_request->account_number,
                        'content' => '<span class="text-success">Позиция успешно добавлена</span>',
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Добавить еще', ['create', 'logistic_request_id' => $logistic_request->id],
                                ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                    ];
                } else {
                    return [
                        'title' => "Добавление позиции к счету " . $logistic_request->account_number,
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                            'logistic_request' => $logistic_request
                        ]),
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'logistic_request' => $logistic_request
                ]);
            }
        }

    }

    /**
     * Updates an existing LogisticRequestPart model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $request_number = $model->logisticRequest->account_number ?? null;

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Редактирование позиции счтета №" . $request_number,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return [
                        'forceReload' => '#crud-datatable-parts-pjax',
                        'forceClose' => true,
//                        'title' => "LogisticRequestPart #" . $id,
//                        'content' => $this->renderAjax('view', [
//                            'model' => $model,
//                        ]),
//                        'footer' => Html::button('Закрыть',
//                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
//                            Html::a('Редактировать', ['update', 'id' => $id],
//                                ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                    ];
                } else {
                    return [
                        'title' => "Редактирование позиции счтета №" . $request_number,
                        'content' => $this->renderAjax('update', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing LogisticRequestPart model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-parts-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing LogisticRequestPart model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBulkdelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-parts-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the LogisticRequestPart model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LogisticRequestPart the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LogisticRequestPart::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Выводит все позиции для счета
     * @param int $logistic_request_id Идентификатор счета
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionViewParts($logistic_request_id)
    {
        /** @var LogisticRequest $request_model */
        $request_model = LogisticRequest::find()->andWhere(['id' => $logistic_request_id])->one() ?? null;

        if (!$request_model) {
            throw new NotFoundHttpException('Счет не найден');
        }
        $params = Yii::$app->request->queryParams;
        $params['LogisticRequestPartSearch']['logistic_request_id'] = $logistic_request_id;

        $searchModel = new LogisticRequestPartSearch();
        $dataProvider = $searchModel->search($params);

        Yii::info(Yii::$app->request->queryParams, 'test');

        $additional_info = $request_model->getAdditionalInfo();

        return $this->render('view-parts', [
            'request_model' => $request_model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'additional_info' => $additional_info,
        ]);
    }

    /**
     * Редирект на указанную заявку
     * @param $number
     */
    public function actionViewPartsRedirect($number)
    {
        $id = LogisticRequest::find()->andWhere(['account_number' => $number])->one()->id ?? null;

        $this->redirect(['view-parts', 'logistic_request_id' => $id]);
    }

    /**
     * Редактирование таблицы позицый счета
     * @return array|bool
     * @throws \yii\base\InvalidConfigException
     */
    public function actionEditable()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $request = Yii::$app->request;
        $has_editable = $request->post('hasEditable');

        Yii::info('actionEditable', 'request');

        if (isset($has_editable)) {
            Yii::info('is Editable', 'request');

            $editable_attribute = $request->post('editableAttribute');

            Yii::info('$editable_attribute = ' . $editable_attribute, 'request');

            $editable_key = $request->post('editableKey');
            Yii::info('$editable_key: ' . $editable_key, 'request');

            $logistic_request_part_model = LogisticRequestPart::findOne($editable_key);

            $editable_index = $request->post('editableIndex');
            Yii::info('$editable_index: ' . $editable_index, 'request');

            Yii::info($request->post(), 'request');

            $editable_model = $request->post('LogisticRequestPart');
            $new_value = $editable_model[$editable_index][$editable_attribute];

            $measure_id = $editable_model['measure_id'] ?? null;

            if ($measure_id) {
                $logistic_request_part_model->measure_id = $measure_id;
            }

            $nomenclature_id = $editable_model[$editable_index]['vendor_code'] ?? null;

            if ($nomenclature_id) {
                $logistic_request_part_model->nomenclature_id = $nomenclature_id;
            }

            if ($editable_attribute == 'arriving_info') {
                $logistic_request_part_model->arriving_id = $new_value;
            }

            if ($editable_attribute == 'contract_date' || $editable_attribute == 'order_date') {
                $new_value = date('Y-m-d 00:00:00', strtotime($new_value));
            }

            Yii::info('$new_value: ' . $new_value, 'request');

            $logistic_request_part_model->$editable_attribute = $new_value;

            Yii::info($logistic_request_part_model->getAttributes(), 'request');

            if (!$logistic_request_part_model->save()) {
                Yii::error($logistic_request_part_model->errors);
            }

            switch ($editable_attribute) {
                case (in_array($editable_attribute,
                    [
                        'ship_date',
                        'receipt_date_manch',
                        'receipt_date_novosib',
                        'client_ship_date',
                        'order_date',
                        'contract_date'
                    ])):
                    $date = $logistic_request_part_model->$editable_attribute ?? date('Y-m-d');
                    $output = Yii::$app->formatter->asDate($date);
                    break;
                case 'nomenclature_id':
                    $output = $logistic_request_part_model->nomenclature->name ?? null;
                    break;
                case 'vendor_code':
                    $output = $logistic_request_part_model->nomenclature->own_vendor_code ?? null;
                    break;
                case 'supplier_id':
                    $output = $logistic_request_part_model->supplier->name ?? null;
                    break;
                case 'logistic_status_id':
                    $status = $logistic_request_part_model->logisticStatus;

                    Yii::debug($status->getAttributes(), 'request');

                    $output = Html::a('<span class="fa ' . $status->icon . '"> ' . $status->name . '</span>', '#', [
                        'class' => 'btn btn-block btn-sm color_shadow',
                        'style' => 'background-color: ' . $status->color ?? null,
                    ]);

                    break;
                case 'num':
                    $logistic_request_part_model->measure_id = $editable_model['measure_id'] ?? null;

                    if ($logistic_request_part_model->measure_id) {
                        $output = $logistic_request_part_model->num . ' (' . $logistic_request_part_model->measure->name . ')';
                    } else {
                        $output = $logistic_request_part_model->num;
                    }
                    break;
                case 'arriving_info':
                    $output = $logistic_request_part_model->arriving->number ?? null;
                    break;
                default:
                    $output = $logistic_request_part_model->$editable_attribute ?? null;
            }

            Yii::debug('Output: ' . $output, 'request');

            return [
                'output' => $output,
            ];

//            if ($editable_attribute == 'comment') {
//
//                $editable_model = $request->post('LogisticRequestPart');
//                Yii::info('$editable_model', 'request');
//                Yii::info($editable_model, 'request');
//
//                $new_value = $editable_model[$editable_index]['comment'];
//
//                Yii::info('new_value', 'request');
//                Yii::info($new_value, 'request');
//
//                $logistic_request_part_model->comment = $new_value;
//
//            }
//            elseif ($editable_attribute == 'supplier_id') {
//                $editable_model = $request->post('LogisticRequestPart');
//                $new_value = $editable_model[$editable_index]['supplier_id'];
//                $logistic_request_part_model->supplier_id = $new_value;
//                $logistic_request_part_model->save();
//                return [
//                    'output' => $logistic_request_part_model->supplier->name ?? null,
//                ];
//            }
//            elseif ($editable_attribute == 'nomenclature_id') {
//                $editable_model = $request->post('LogisticRequestPart');
//                $new_value = $editable_model[$editable_index]['nomenclature_id'];
//                $logistic_request_part_model->nomenclature_id = $new_value;
//                $logistic_request_part_model->save();
//                return [
//                    'output' => $logistic_request_part_model->nomenclature->name ?? null,
//                ];
//            }
//            elseif ($editable_attribute == 'logistic_status_id') {
//                $editable_model = $request->post('LogisticRequestPart');
//                $new_value = $editable_model[$editable_index]['logistic_status_id'];
//                $logistic_request_part_model->logistic_status_id = $new_value;
//                $logistic_request_part_model->save();
//                return [
//                    'output' => $logistic_request_part_model->logisticStatus->name ?? null,
//                ];
//            }
//            elseif ($editable_attribute == 'ship_date') {
//                $editable_model = $request->post('LogisticRequestPart');
//                $new_value = $editable_model[$editable_index]['ship_date'];
//                $logistic_request_part_model->ship_date = $new_value;
//                $logistic_request_part_model->save();
//                return [
//                    'output' => Yii::$app->formatter->asDate($logistic_request_part_model->ship_date ?? date()),
//                ];
//            }
//            elseif ($editable_attribute == 'receipt_date_manch') {
//                $editable_model = $request->post('LogisticRequestPart');
//                $new_value = $editable_model[$editable_index]['receipt_date_manch'];
//                $logistic_request_part_model->receipt_date_manch = $new_value;
//                $logistic_request_part_model->save();
//                return [
//                    'output' => Yii::$app->formatter->asDate($logistic_request_part_model->receipt_date_manch ?? date()),
//
//                ];
//            }
//            elseif ($editable_attribute == 'receipt_date_novosib') {
//                $editable_model = $request->post('LogisticRequestPart');
//                $new_value = $editable_model[$editable_index]['receipt_date_novosib'];
//                $logistic_request_part_model->receipt_date_novosib = $new_value;
//                $logistic_request_part_model->save();
//                return [
//                    'output' => Yii::$app->formatter->asDate($logistic_request_part_model->receipt_date_novosib ?? date()),
//
//                ];
//            }
//            elseif ($editable_attribute == 'client_ship_date') {
//                $editable_model = $request->post('LogisticRequestPart');
//                $new_value = $editable_model[$editable_index]['client_ship_date'];
//                $logistic_request_part_model->client_ship_date = $new_value;
//                $logistic_request_part_model->save();
//                return [
//                    'output' => Yii::$app->formatter->asDate($logistic_request_part_model->client_ship_date ?? date()),
//
//                ];
//            }
//
//            if (!$logistic_request_part_model->save()) {
//                Yii::error($logistic_request_part_model->errors);
//            }
//            return true;
        }
        return false;
    }

    /**
     * Согласно задаче от 06.11.2020 дата оплаты счета логистики является датой по договору позиций этого счета
     * Данный метод проставляет в пустые поля дату логистического счета
     */
    public function actionFixLogReqPart()
    {
        $table_name = LogisticRequestPart::tableName();
        $sql = <<<SQL
UPDATE  $table_name AS lrp 
SET lrp.contract_date = IF(lrp.contract_date IS NULL, (SELECT lr.payed_date FROM logistic_request AS lr WHERE lr.id = lrp.logistic_request_id), lrp.contract_date)
SQL;
        try {
            $result =  Yii::$app->db->createCommand($sql)->execute();
            return 'Success. Rows changed: ' . $result;
        } catch (Exception $e) {
            Yii::error($e->getMessage(), '_error');
            echo $e->getMessage();
        }
    }
}
