<?php

namespace app\controllers;

use app\components\AccessController;
use app\components\ImageHelper;
use app\models\Accounting;
use app\models\Arriving;
use app\models\DetailToArriving;
use app\models\Functions;
use app\models\LogisticRequest;
use app\models\LogisticRequestPart;
use app\models\OrderPart;
use app\models\Request;
use app\models\Users;
use Yii;
use app\models\Nomenclature;
use app\models\NomenclatureSearch;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * NomenclatureController implements the CRUD actions for Nomenclature model.
 */
class NomenclatureController extends AccessController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Nomenclature models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NomenclatureSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 40;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionManagerNomenclature()
    {

        $searchModel = new NomenclatureSearch();
        $dataProvider = $searchModel->managerSearch(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 40;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'nomenclature_type' => Users::USER_ROLE_MANAGER,
        ]);
    }

    public function actionTechNomenclature()
    {
        $searchModel = new NomenclatureSearch();
        $dataProvider = $searchModel->techSearch(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 40;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'nomenclature_type' => Users::USER_ROLE_TECH_SPECIALIST,
        ]);
    }

    /**
     * Displays a single Nomenclature model.
     * @param integer $id
     * @param null $no_ajax
     * @param int $request_id ID заявки
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id, $no_ajax = null, $request_id = null)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->request_id = $request_id;

        Yii::info($model->toArray(), 'test');

        if ($no_ajax) {
            return $this->render('view', [
                'model' => $model,
            ]);
        }
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Номенклатура #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $model,
                ]),
                'footer' => Html::button('Закрыть',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Редактировать', ['update', 'id' => $id],
                        ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new Номенклатура model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Nomenclature();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::info('is Ajax', 'test');

            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                Yii::info('is Get', 'test');

//                if ($request->get('request_id')){
//                    Yii::$app->session->set('request_id', $request->get('request_id'));
//                }

                return [
                    'title' => "Создать новую Номенклатуру",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post())) {
                    Yii::info('is Post', 'test');

                    Yii::info('Деталь: ' . $model->name, 'test');

                    //Кто создал? пользователь который залогинился.
                    $model->created_by = Yii::$app->user->identity->id;


                    if (!$model->nomenclature_type) { //Если деталь добавляет не админ
                        if (isset($_GET['nomenclature_type'])) {
                            $model->nomenclature_type = $_GET['nomenclature_type'];
                        }
                    }

                    Yii::info('Тип номенклатуры: ' . $model->nomenclature_type, 'test');
                    Yii::info($model->toArray(), 'test');

                    if ($model->save()) {
                        //загрузка основной картинки.
                        $model->file = UploadedFile::getInstance($model, 'file');
                        $main_path = "images/nomenclature/main_photo/";
                        if (!empty($model->file)) {
                            if (!file_exists(($main_path))) {
                                mkdir($main_path, 0777, true);
                            }
                            $main_photo = $main_path . time() . Functions::generateRandomString(5) . '.' . $model->file->extension;
                            $model->file->saveAs($main_photo);
                            ImageHelper::optimize($main_photo);
                            Yii::$app->db->createCommand()->update('nomenclature',
                                ['main_photo' => "/" . $main_photo],
                                ['id' => $model->id]
                            )->execute();
                            $model->file = [];
                        }
                        //
                        //загрузка дополнительных картинок.
                        $model->files = UploadedFile::getInstances($model, 'files');
                        $path = "images/nomenclature/";
                        if (!empty($model->files)) {
                            Yii::info('model -> files', 'test');
                            Yii::info($model->files, 'test');
                            $name = [];
                            if (!file_exists(($path))) {
                                mkdir($path, 0777, true);
                            }
                            foreach ($model->files as $uploadedFile) {
                                $current_name = $path . time() . Functions::generateRandomString(5) . '.' . $uploadedFile->extension;
                                $uploadedFile->saveAs($current_name);
                                ImageHelper::optimize($current_name);
                                $name[] = "/" . $current_name;
//                    }
                            }

                            $model->files = [];
//                        \Yii::info('photos_name', 'upload');
//                        \Yii::info($name, 'upload');
                            if (!empty($name)) {
                                Yii::$app->db->createCommand()->update('nomenclature',
                                    ['additional_photos' => json_encode($name, true)],
                                    ['id' => $model->id]
                                )->execute();
//                            $model -> additional_photos = json_encode($name);
                            }
                        }
                        $order_part_id = Yii::$app->request->get('order_part_id');

                        if ($order_part_id) {
                            //Деталь создается из заявки, сохраняем вновь созданную деталь в полученную позицию заявки
                            $order_part_model = OrderPart::findOne($order_part_id);
                            $order_part_model->detail_id = $model->id;

                            if (!$order_part_model->save()) {
                                Yii::error($order_part_model->errors, __METHOD__);
                            }
                        }

                        $from_order_part = Yii::$app->request->get('from_order_part');

                        if ($from_order_part) {
                            $request_id = Yii::$app->session->get('request_id');

                            //Форма добавления детали в номенклатуру была открыта из формы добавления позиции к заявке
                            $op_model = new OrderPart();
                            $op_model->request_id = $request_id;
                            $op_model->nomenclature_id = $model->id;
                            return [
                                'title' => "Новая позиция к заявке №" . $request_id,
                                'content' => $this->renderAjax('/order-part/create', [
                                    'model' => $op_model,
                                ]),
                                'footer' => Html::button('Закрыть',
                                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                    Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                            ];
                        }

                        //
                        return [
                            'forceReload' => '#crud-datatable-pjax',
                            'title' => "Создать новую Номенклатуру",
                            'content' => '<span class="text-success">Номенклатура успешно создана</span>',
                            'footer' => Html::button('Закрыть',
                                    ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::a('Создать ещё', ['create', 'nomenclature_type' => Users::getPermission()],
                                    ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                        ];
                    } else {
                        Yii::error($model->errors, 'error');
                        return [
                            'forceReload' => '#crud-datatable-pjax',
                            'title' => "Создать новую Номенклатуру",
                            'content' => '<span class="text-success">Номенклатура успешно создана</span>',
                            'footer' => Html::button('Закрыть',
                                    ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::a('Создать ещё', ['create', 'nomenclature_type' => Users::getPermission()],
                                    ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                        ];
                    }

                } else {
                    Yii::error($model->errors, __METHOD__);
                    return [
                        'title' => "Создать новую Номенклатуру",
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            Yii::info('non Ajax', 'test');

            if ($model->load($request->post())) {
//                $model -> additional_photos = $_SESSION['additional_photos'];
                if ($model->save()) {
//                    $_SESSION['folder_name'] = null;
//                    $_SESSION['additional_photos'] = null;
                    return $this->redirect(['view', 'id' => $model->id]);
                }

            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

        return $this->redirect(['view', 'id' => $model->id]);
    }

    /**
     * Updates an existing Nomenclature model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param int $request_id ID заявки
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionUpdate($id, $request_id = null)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->request_id = $request_id;

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Редактировать Номенклатуру #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить',
                            ['class' => 'btn btn-primary nomenclature_update', 'type' => "submit"])
                ];
            } else {
                if ($model->load($request->post())) {
//                $model->updated_date = Functions::rusDate(time(), '%DAYWEEK%, j %MONTH% Y, G:i');
//                $model -> updated_date = date('l jS \of F Y h:i:s A');
                    if ($model->save()) {
                        //Загрузка основной картинки
                        $model->file = UploadedFile::getInstance($model, 'file');
                        $main_path = "images/nomenclature/main_photo/";
                        if (!empty($model->file)) {
                            if (!file_exists(($main_path))) {
                                mkdir($main_path, 0777, true);
                            }

                            $main_photo = $main_path . time() . Functions::generateRandomString(5) . '.' . $model->file->extension;
                            $model->file->saveAs($main_photo);
                            ImageHelper::optimize($main_photo);
                            $deleted_main_element = substr($model->main_photo, 1, strlen($model->main_photo));

                            if ($deleted_main_element != null && file_exists($deleted_main_element)) {

                                Yii::warning('Удаляемый файл: ' . $deleted_main_element, __METHOD__);
                                Functions::deleteFile($deleted_main_element);
                            }
                            Yii::$app->db->createCommand()->update('nomenclature',
                                ['main_photo' => "/" . $main_photo],
                                ['id' => $model->id]
                            )->execute();
                            $model->file = [];
                        }
                        //загрузка дополнительных картинок.
                        $model->files = UploadedFile::getInstances($model, 'files');
                        $path = "images/nomenclature/";
                        if (!empty($model->files)) {
                            $name = [];
                            if (!file_exists(($path))) {
                                mkdir($path, 0777, true);
                            }
                            foreach ($model->files as $uploadedFile) {
                                $current_name = $path . time() . Functions::generateRandomString(5) . '.' . $uploadedFile->extension;
                                $uploadedFile->saveAs($current_name);
                                ImageHelper::optimize($current_name);
                                $name[] = "/" . $current_name;
                            }

                            $model->files = [];

                            if (!empty($name)) {
                                $current_model_from_db = Nomenclature::find()->where(['id' => $model->id])->one();
                                if (empty($current_model_from_db)) {
                                } else {
                                    $additional_photos_from_db = json_decode($current_model_from_db->additional_photos,
                                        true);
                                    if (!empty($additional_photos_from_db)) {
                                        $update_additional_photos = array_merge($name, $additional_photos_from_db);
                                    } else {
                                        $update_additional_photos = $name;
                                    }

                                    Yii::$app->db->createCommand()->update('nomenclature',
                                        ['additional_photos' => json_encode($update_additional_photos, true)],
                                        ['id' => $model->id]
                                    )->execute();
                                }
                            }
                        }

                        return [
                            'forceReload' => '#crud-datatable-pjax',
                            'title' => "Номенклатура #" . $id,
                            'forceClose' => true,
//                        'content' => $this->renderAjax('view', [
//                        ]),
                            'footer' => Html::button('Закрыть',
                                    ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::a('Редактировать', ['update', 'id' => $id],
                                    ['class' => 'btn btn-primary nomenclature_update', 'role' => 'modal-remote'])
                        ];
                    } else {
                        Yii::error($model->errors);
                        return [
                            'title' => "Редактировать Номенклатуру #" . $id,
                            'content' => $this->renderAjax('update', [
                                'model' => $model,
                            ]),
                            'footer' => Html::button('Закрыть',
                                    ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Сохранить', [
                                    'class' => 'btn btn-primary nomenclature_update',
                                    'style' => 'margin-top:20px',
                                    'type' => "submit"
                                ])
                        ];
                    }
                } else {
                    return [
                        'title' => "Редактировать Номенклатуру #" . $id,
                        'content' => $this->renderAjax('update', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', [
                                'class' => 'btn btn-primary nomenclature_update',
                                'style' => 'margin-top:20px',
                                'type' => "submit"
                            ])
                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
//                VarDumper::dump('STOP'); die();
//                $model->updated_date = Functions::rusDate(time(), '%DAYWEEK%, j %MONTH% Y, G:i');
//                $model -> updated_date = date('l jS \of F Y h:i:s A');
                if ($model->save()) {
                    //Загрузка основной картинки
                    $model->file = UploadedFile::getInstance($model, 'file');
                    $main_path = "images/nomenclature/main_photo/";
                    if (!empty($model->file)) {
                        if (!file_exists(($main_path))) {
                            mkdir($main_path, 0777, true);
                        }

                        $main_photo = $main_path . time() . Functions::generateRandomString(5) . '.' . $model->file->extension;
                        $model->file->saveAs($main_photo);
                        ImageHelper::optimize($main_photo);
                        $deleted_main_element = substr($model->main_photo, 1, strlen($model->main_photo));

                        if ($deleted_main_element != null && file_exists($deleted_main_element)) {

                            Yii::warning('Удаляемый файл: ' . $deleted_main_element, __METHOD__);
                            Functions::deleteFile($deleted_main_element);
                        }
                        Yii::$app->db->createCommand()->update('nomenclature',
                            ['main_photo' => "/" . $main_photo],
                            ['id' => $model->id]
                        )->execute();
                        $model->file = [];
                    }
                    //загрузка дополнительных картинок.
                    $model->files = UploadedFile::getInstances($model, 'files');
                    $path = "images/nomenclature/";
                    if (!empty($model->files)) {
                        $name = [];
                        if (!file_exists(($path))) {
                            mkdir($path, 0777, true);
                        }
                        foreach ($model->files as $uploadedFile) {
                            $current_name = $path . time() . Functions::generateRandomString(5) . '.' . $uploadedFile->extension;
                            $uploadedFile->saveAs($current_name);
                            ImageHelper::optimize($current_name);

                            $name[] = "/" . $current_name;
                        }

                        $model->files = [];

                        if (!empty($name)) {
                            $current_model_from_db = Nomenclature::find()->where(['id' => $model->id])->one();
                            if (empty($current_model_from_db)) {
                            } else {
                                $additional_photos_from_db = json_decode($current_model_from_db->additional_photos,
                                    true);
                                if (!empty($additional_photos_from_db)) {
                                    $update_additional_photos = array_merge($name, $additional_photos_from_db);
                                } else {
                                    $update_additional_photos = $name;
                                }

                                Yii::$app->db->createCommand()->update('nomenclature',
                                    ['additional_photos' => json_encode($update_additional_photos, true)],
                                    ['id' => $model->id]
                                )->execute();
                            }
                        }
                    }

                    return $this->redirect(['view', 'id' => $model->id, 'request_id' => $model->request_id]);
                } else {
                    Yii::error($model->errors);
                    return $this->redirect(['view', 'id' => $model->id, 'request_id' => $model->request_id]);
                }

            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Nomenclature model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->fullDelete($id);
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Nomenclature model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBulkdelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $this->fullDelete($pk);
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceЗакрыть' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Nomenclature model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Nomenclature the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Nomenclature::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }

    /**
     * Удаление доп фотографий
     *
     * @throws \yii\db\Exception
     */
    public static function actionAdditionalDelete()
    {
        $deleted_items = $_POST['deleted_items'];

        Yii::info('$deleted_items', 'test');
        Yii::info($deleted_items, 'test');

        //optional
//        $sub_deleted_items = [];
//        if (!empty($deleted_items)) {
//            foreach ($deleted_items as $deleted_item) {
//                $sub_deleted_items[] = substr($deleted_item, 4, strlen($deleted_item));
//            }
//        }
        //
        Yii::info('$deleted_items', 'test');
        Yii::info($deleted_items, 'test');

        $ok = false;
        $additional_photos = $_POST['additional_photos'];
        if (!empty($additional_photos)) {
            $ok = true;
        }
        $additional_photos = explode(", ", $additional_photos);

        Yii::info('$additional_photos', 'test');
        Yii::info($additional_photos, 'test');

        $model_id = $_POST['model_id'];
        $new_additional_photos = [];
        if ($ok) {
            foreach ($additional_photos as $additional_photo) {
                if (!in_array($additional_photo, $deleted_items)) {
                    $new_additional_photos[] = $additional_photo;
                }
            }
        }

        Yii::info('$new_additional_photos', 'test');
        Yii::info($new_additional_photos, 'test');

        if (!empty($deleted_items)) {
            //optional
//            foreach ($sub_deleted_items as $deleted_item) {
            foreach ($deleted_items as $deleted_item) {
                $deleted_element = substr($deleted_item, 1, strlen($deleted_item));
                if ($deleted_element != null && file_exists($deleted_element)) {
                    Functions::deleteFile($deleted_element);
                }
            }
        }
        if (count($new_additional_photos) > 0) {
            Yii::$app->db->createCommand()->update(
                'nomenclature',
                ['additional_photos' => json_encode($new_additional_photos, true)],
                ['id' => $model_id]
            )->execute();
        } else {
            Yii::$app->db->createCommand()->update(
                'nomenclature',
                ['additional_photos' => null],
                ['id' => $model_id]
            )->execute();
        }

    }

    /**
     * Удаление всех доп фотографий позиции номенклатуры
     * @param $id
     */
    public static function fullDelete($id)
    {
        $model = Nomenclature::findOne($id);

        if ($model !== null) {
            $additional_photos = json_decode($model->additional_photos, true);
            $main_photo = $model->main_photo;

            $main_photo = substr($main_photo, 1, strlen($main_photo));
            if ($main_photo != null && file_exists($main_photo)) {
                Functions::deleteFile($main_photo);
            }
            if (!empty($additional_photos)) {
                foreach ($additional_photos as $additional_photo) {
                    $deleted_element = substr($additional_photo, 1, strlen($additional_photo));
                    if ($deleted_element != null && file_exists($deleted_element)) {
                        Functions::deleteFile($deleted_element);
                    }
                }
            }
        }
    }

    /**
     * @param int $id Код детали в номенклатуре
     * @return array
     */
    public function actionGetImageUrl($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $photo = Nomenclature::findOne($id)->main_photo;
        $thumbs = Functions::getThumbnailPath(Nomenclature::findOne($id)->main_photo);
        return [
            'photo' => $photo,
            'thumbs' => $thumbs
        ];
    }

    /**
     * Возвращает все поля детали
     * @param $id
     * @return array
     */
    public function actionGetNomenclatureInfo($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return Nomenclature::getInfo($id);
    }

    /**
     * Пересчитывает остатки для номенклатуры специалиста
     */
    public function actionRecalculateFactBalance()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        set_time_limit(60 * 10);

        $details = Nomenclature::find()
            ->andWhere(['nomenclature_type' => Users::USER_ROLE_TECH_SPECIALIST,]);

        $date = date('Y-m-d H:i:s', time());

//        $counter = 0;

        /** @var Nomenclature $detail */
        foreach ($details->each() as $detail) {
            Yii::info($detail->name, 'test');
            $result = Accounting::getBalanceForDate($detail->id, $date);
            $detail->fact_balance = $result['num'];
            if (!$detail->save()) {
                Yii::error('Detail ID: ' . $detail->id, '_error');
                Yii::error($detail->additional_photos, '_error');
                Yii::error($detail->errors, '_error');
            }

            Yii::info($result, 'test');
        }

        return [
            'success' => true,
        ];
    }

    /**
     * @param int $id Идентификатор детали
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionGetReserveRequests(int $id): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findModel($id);

        //Получаем все счета и заявки в которых выставлен резерв на деталь
        $invoices = LogisticRequest::getReserveForNomenclature($model->id);
        $requests = Request::getReserveForNomenclature($model->id);

        return [
            'title' => "Резерв на '{$model->name}'",
            'size' => 'middle',
            'content' => $this->renderAjax('_request_list', [
                'model' => $model,
                'requests' => $requests,
                'invoices' => $invoices,
            ]),
            'footer' => Html::button('Закрыть',
                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])

        ];


    }

    /**
     * страница со списком поступлений для детали
     * @param int $id Идентификатор детали
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionGetWayArriving(int $id): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;

        $detail = $this->findModel($id);
        $arrivings = Arriving::find()
            ->joinWith(['details as d'])
            ->select(['d.number AS count', 'arriving.number', 'arriving.id'])
            ->andWhere(['d.detail_id' => $detail->id, 'd.warehouse' => DetailToArriving::WAREHOUSE_ON_THE_WAY])
            ->all();

        if (!$arrivings) $arrivings = [];

//        $logistic_requests = LogisticRequest::find()
//            ->joinWith(['logisticRequestParts as lrp'])
//            ->select([
//                LogisticRequest::tableName().'.id',
//               'lrp.num AS count',
//                LogisticRequest::tableName() .'.account_number AS number'])
//            ->andWhere(['lrp.nomenclature_id' => $detail->id])
//            ->all();
        $logistic_requests = null;
        if (!$logistic_requests) $logistic_requests = [];


        if ($request->isGet) {
            return [
                'title' => 'Список поступлений',
                'content' => $this->renderAjax('_arriving_list', [
                    'detail' => $detail,
                    'arrivings' => $arrivings,
                    'logistic_requests' => $logistic_requests,
                ]),
                'footer' => Html::button('Закрыть',
                    ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
            ];
        }

        return [];
    }

}
