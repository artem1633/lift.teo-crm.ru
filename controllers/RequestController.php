<?php

namespace app\controllers;

use app\components\AccessController;
use app\components\ImageHelper;
use app\models\Actions;
use app\models\ActionsSearch;
use app\models\AdditionalOrderStatus;
use app\models\Comment;
use app\models\Currency;
use app\models\DeliveryTime;
use app\models\Functions;
use app\models\Measure;
use app\models\Nomenclature;
use app\models\OrderPart;
use app\models\OrderStatus;
use app\models\RequestOrderPartSearch;
use app\models\Suppliers;
use Yii;
use app\models\Request;
use app\models\RequestSearch;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * RequestController implements the CRUD actions for Request model.
 */
//class RequestController extends Controller
class RequestController extends AccessController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post', 'get'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Request models.
     * @return mixed
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;

        $searchModel = new RequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 40;

        if ($request->get('RequestSearch')) {
            if ($request->get('created_date')) {
                $searchModel->created_date = $request->get('created_date');
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            }
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Request model.
     * @return mixed
     * @throws Exception
     */
    public function actionEditable()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $request = Yii::$app->request;
        $has_editable = $request->post('hasEditable');

        Yii::info('actionEditable', 'request');

        if (isset($has_editable)) {
            Yii::info('is Editable', 'request');

            $editable_attribute = $request->post('editableAttribute');

            Yii::info('$editable_attribute', 'request');
            Yii::info($editable_attribute, 'request');

            $editable_key = $request->post('editableKey');
            Yii::info('$editable_key', 'request');
            Yii::info($editable_key, 'request');

            $order_part_model = OrderPart::findOne($editable_key);

            $editable_index = $request->post('editableIndex');
            Yii::info('$editable_index', 'request');
            Yii::info($editable_index, 'request');

            Yii::info($request->post(), 'request');

            if ($editable_attribute == 'sell_price') {

                $editable_model = $request->post('OrderPart');
                Yii::info('$editable_model', 'request');
                Yii::info($editable_model, 'request');

                $new_sell_price = $editable_model[$editable_index]['sell_price'];
                $new_sell_currency = $editable_model['sell_currency'];

                Yii::info('$new_sell_price: ' . $new_sell_price, 'request');
                Yii::info('$new_sell_currency: ' . $new_sell_currency, 'request');

                $order_part_model->sell_currency = $new_sell_currency;
                $order_part_model->updated_sell_price = date('Y-m-d H:i:s', time());

                if ($new_sell_price) {
                    $order_part_model->sell_price = $new_sell_price;
                } else {
                    $order_part_model->sell_price = 0;
                }
                $order_part_model->save();
                $updated_date = date('d.m.Y', strtotime($order_part_model->updated_sell_price));

                return [
                    'output' => $order_part_model->sell_price . ' <i class="fa ' . Currency::findOne($order_part_model->sell_currency)->icon . '"></i><br>(' . $updated_date . ')',
                ];
            }
            else if ($editable_attribute == 'buy_price') {

                $editable_model = $request->post('OrderPart');
                $new_buy_price = $editable_model[$editable_index]['buy_price'];
                $new_buy_currency = $editable_model['buy_currency'];;

                Yii::info('$new_buy_price' . $new_buy_price, 'request');
                Yii::info('$new_buy_currency' . $new_buy_currency, 'request');

                $order_part_model->buy_currency = $new_buy_currency;
                $order_part_model->updated_buy_price = date('Y-m-d H:i:s', time());

                if ($new_buy_price) {
                    $order_part_model->buy_price = $new_buy_price;
                } else {
                    $order_part_model->buy_price = 0;
                }
                if (!$order_part_model->save()){
                    Yii::error($order_part_model->errors, __METHOD__);
                }
                $updated_date = date('d.m.Y', strtotime($order_part_model->updated_buy_price));
                return [
                    'output' => $order_part_model->buy_price . ' <i class="fa ' . Currency::findOne($order_part_model->buy_currency)->icon . '"></i><br>(' . $updated_date . ')',
                ];
            }
            else if ($editable_attribute == 'quantity') {

                $editable_model = $request->post('OrderPart');
                $measure_id = $editable_model['measure'];

                Yii::info('$editable_model', 'request');
                Yii::info($editable_model, 'request');
                Yii::info('$measure_id: ' . $measure_id, 'test');

                $new_value = $editable_model[$editable_index]['quantity'];

                Yii::info('$new_value: ' . $new_value, 'request');

                $order_part_model->measure_id = $measure_id;

                Yii::info('$order_part_model->measure_id = ' . $order_part_model->measure_id, 'test');

                if ($new_value) {
                    Yii::info('$new_value: ' . $new_value, 'request');
                    $order_part_model->quantity = $new_value;
                } else {
                    $order_part_model->quantity = 0;
                }

                $order_part_model->save();
                return [
                    'output' => $order_part_model->quantity . ' ' . Measure::findOne($order_part_model->measure_id)->name
                ];

            }
            else if ($editable_attribute == 'comment') {

                $editable_model = $request->post('OrderPart');
                Yii::info('$editable_model', 'request');
                Yii::info($editable_model, 'request');

                $new_value = $editable_model[$editable_index]['comment'];

                Yii::info('new_value', 'request');
                Yii::info($new_value, 'request');

                $order_part_model->comment = $new_value;

            }
            else if ($editable_attribute == 'vendor_code') {
//                $update_model = OrderPart::find()->where(['id' => $editable_key])->one();
                $editable_model = $request->post('OrderPart');
                Yii::info('$editable_model', 'request');
                Yii::info($editable_model, 'request');

                $new_value = $editable_model[$editable_index]['vendor_code'];

                Yii::info('new_value', 'request');
                Yii::info($new_value, 'request');


                if (!empty($new_value)) {
//                    if($update_model->validate(array('sell_price')) ){
                    // valid
                    $vendor_code = $new_value;

                    Yii::info('vendor_code', 'request');
                    Yii::info($vendor_code, 'request');

//                        if ($update_model -> save()) {
//                            Yii::info('editable model save', 'request');
//                        }
                    try {
//                            $update_model -> save();
                        $update = Yii::$app->db->createCommand()->update('order_part',
                            ['vendor_code' => $vendor_code],
                            ['id' => $editable_key]
                        )->execute();
                        if ($update) {
                            Yii::info('model updated!', 'request');
                            Yii::$app->response->format = Response::FORMAT_JSON;
                            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
//                                $this -> refresh();
                        }
                    } catch (\Exception $e) {
                        Yii::info('Exception', 'request');
                        Yii::info($e->getMessage() . PHP_EOL . $e->getTraceAsString(), 'request');
                    } catch (\Error $e) {
                        Yii::info('Exception', 'request');
                        Yii::info($e->getMessage() . PHP_EOL . $e->getTraceAsString(), 'request');
                    }

                } else {
//                        return json_encode('sell_price required');
//                        return json_encode('nothing');
                    Yii::$app->db->createCommand()->update('order_part',
                        ['vendor_code' => null],
                        ['id' => $editable_key]
                    )->execute();

                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
//                        return Yii::$app->response->format;
//                        $this -> refresh();
                }

            }
            else if ($editable_attribute == 'delivery_time_id') {
                $editable_model = $request->post('OrderPart');
                $new_value = $editable_model[$editable_index]['delivery_time_id'];
                $order_part_model->delivery_time_id = $new_value;
                $order_part_model->save();
                return [
                    'output' => DeliveryTime::findOne($order_part_model->delivery_time_id)->name
                ];
            }
            else if ($editable_attribute == 'suppliers_id') {
                $editable_model = $request->post('OrderPart');
                $new_value = $editable_model[$editable_index]['suppliers_id'];
                $order_part_model->suppliers_id = $new_value;
                $order_part_model->save();
                return [
                    'output' => Suppliers::findOne($order_part_model->suppliers_id)->name,
                ];
            }
            else if ($editable_attribute == 'detail_id') {
                $editable_model = $request->post('OrderPart');
                $new_value = $editable_model[$editable_index]['detail_id'];
                $order_part_model->detail_id = $new_value;
                $order_part_model->save();
                return [
                    'output' => Nomenclature::findOne($order_part_model->detail_id)->name,
                ];
            }

            if (!$order_part_model->save()) {
                Yii::error($order_part_model->errors);
            }
            return true;
        }
        return false;
    }

    /**
     * @param $id
     * @return array|string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $has_editable = $request->post('hasEditable');

        if (!empty($has_editable)) {
//            Yii::info('hasEditable?', 'request');

            $editable_attribute = $request->post('editableAttribute');

            if ($editable_attribute == 'sell_price') {
                return json_encode('this action handling in another action');
            } else if ($editable_attribute == 'buy_price') {
                return json_encode('this action handling in another action');
            } else if ($editable_attribute == 'quantity') {
                return json_encode('this action handling in another action');
            } else if ($editable_attribute == 'comment') {
                return json_encode('this action handling in another action');
            } else if ($editable_attribute == 'vendor_code') {
                return json_encode('this action handling in another action');
            }
        }

        if ($request->isAjax) {
            Yii::info('is Ajax', 'test');
            Yii::$app->response->format = Response::FORMAT_JSON;

            return [
                'title' => "Заявка #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Редактировать', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            Yii::info('no Ajax', 'test');
            $searchModel = new RequestOrderPartSearch();
            $commentSearchModel = new ActionsSearch();
            $dataProvider = $searchModel->search($id);
            $commentDataProvider = $commentSearchModel->searchByRequest($id);
            $request_order_parts_images = [];
            $request_images = [];
            $request_order_parts = OrderPart::find()->where(['request_id' => $id])->all();
            foreach ($request_order_parts as $request_order_part) {
//                $files_location = substr($request_order_part -> files_location, 1, strlen($request_order_part -> files_location));
                $request_order_parts_images[] = [
                    'id' => $request_order_part->id,
                    'model_name' => "order_part",
                    'description' => Functions::getNomenclature($request_order_part->nomenclature_id),
                    'files_location' => json_decode($request_order_part->files_location, true)
                ];
            }
            //select request own images
            $current_request = Request::find()->where(['id' => $id])->one();
//            Yii::info('current_request', 'test');
//            Yii::info($current_request, 'test');
            $request_images[] = [
                'id' => $current_request->id,
                'model_name' => "request",
                'description' => 'фото заявки',
                'files_location' => json_decode($current_request->additional_request_photos, true)
            ];
            $all_images = array_merge($request_order_parts_images, $request_images);
            $dataProvider->pagination->pageSize = 40;

            return $this->render('view', [
                'model' => $this->findModel($id),
//                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'commentDataProvider' => $commentDataProvider,
                'requestImages' => $all_images,
            ]);
        }
    }

    /**
     * Creates a new Request model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Request();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;

            Yii::info($request->get('add_part'), 'test');

            if ($model->load($request->post())) {
//                $model -> created_date = Functions::rusDate(time(), '%DAYWEEK%, j %MONTH% Y, G:i');
//                $model -> updated_date = Functions::rusDate(time(), '%DAYWEEK%, j %MONTH% Y, G:i');
                $model->created_by = Yii::$app->user->identity->id;

                if ($model->save()) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => "Создать новую Заявку",
                        'content' => 'Заявка создана успешно.',
                        'footer' => Html::a('Перейти к добавлению позиций', ['/request/view', 'id' => $model->id], ['class' => 'btn btn-primary'])
                    ];
//                    return $this->redirect(['view', 'id' => $model->id]);
                } else { //Если ошибка сохранения
                    return [
                        'title' => "Создаие Заявки",
                        'content' => $model->errors,
                        'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
//                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                    ];
                }

            } else {
                return [
                    'title' => "Создать новую Заявку",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing Request model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Редактировать Заявку #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post())) {

//                $model -> updated_date = Functions::rusDate(time(), '%DAYWEEK%, j %MONTH% Y, G:i');
                if ($model->save()) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
//                    return [
//                        'forceReload'=>'#crud-datatable-pjax',
//                        'title'=> "Request #".$id,
//                        'content'=>$this->renderAjax('view', [
//                            'model' => $model,
//                        ]),
//                        'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
//                            Html::a('Редактировать',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
//                    ];
                } else {
                    return [
                        'title' => "Редактировать Заявку #" . $id,
                        'content' => $this->renderAjax('update', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                    ];
                }
            } else {
                return [
                    'title' => "Редактировать Заявку #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionUpdateCol()
    {
        $request = Yii::$app->request;
//        $model = $this->findModel($id);

        Yii::info('actionUpdateCol', 'request');
//        Yii::info($param, 'request');
        Yii::info($request, 'request');

//        if($model->load($request->post())){
//
//            $model -> updated_date = Functions::rusDate(time(), '%DAYWEEK%, j %MONTH% Y, G:i');
//            if ($model->save()) {
////                Yii::$app->response->format = Response::FORMAT_JSON;
////                return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
////                    return [
////                        'forceReload'=>'#crud-datatable-pjax',
////                        'title'=> "Request #".$id,
////                        'content'=>$this->renderAjax('view', [
////                            'model' => $model,
////                        ]),
////                        'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
////                            Html::a('Редактировать',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
////                    ];
//            } else {
//
//            }
//        }else{
//            return 'Something went wrong...';
//        }
        return 1;
    }

    /**
     * @param $id
     * @return array|Response
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $request_model = $this->findModel($id);
        $additional_request_photos = $request_model->additional_request_photos;

        Yii::info('_delete request', 'test');
        Yii::info('additional_request_photos', 'test');
        Yii::info($additional_request_photos, 'test');

        $additional_request_photos = json_decode($additional_request_photos, true);
        Yii::info($additional_request_photos, 'test');
        if ($additional_request_photos) {
            if (count($additional_request_photos) > 0) {
                foreach ($additional_request_photos as $additional_request_photo) {
                    $additional_request_photo = substr($additional_request_photo, 1, strlen($additional_request_photo));
                    if ($additional_request_photo != null && file_exists($additional_request_photo)) {
                        Yii::info('removed:', 'test');
                        Yii::info($additional_request_photo, 'test');
                        Functions::deleteFile($additional_request_photo);
                    }
                }
            }
        }

        $related_order_parts = OrderPart::find()->where(['request_id' => $id])->all();
        foreach ($related_order_parts as $related_order_part) {

            $files_locations = $related_order_part->files_location;
            $files_locations = json_decode($files_locations, true);

            if ($files_locations && count($files_locations) > 0) {
                foreach ($files_locations as $files_location) {
                    $files_location = substr($files_location, 1, strlen($files_location));
                    if ($files_location != null && file_exists($files_location)) {
                        Functions::deleteFile($files_location);
                    }
                }
            }

            $related_order_part->delete();
        }
        $related_comments = Comment::find()->where(['request_id' => $id])->all();
        if ($related_comments && count($related_comments) > 0) {
            foreach ($related_comments as $related_comment) {
                Yii::info('removed comment = ' . $related_comment->id, 'test');
                $related_comment->delete();
            }
        }

        $related_actions = Actions::find()->where(['request_id' => $id])->all();
        if ($related_actions && count($related_actions) > 0) {
            foreach ($related_actions as $related_action) {
                Yii::info('removed action = ' . $related_action->id, 'test');
                $related_action->delete();
            }
        }

        $request_model->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * @return array|Response
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBulkdelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * @param $id
     * @return RequestController|Request|null
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Request::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }

    public function actionCreatePart($index)
    {

//        \Yii::info('model_1', 'test');
//        \Yii::info($index, 'test');

//        renderPartial
        return $this->renderAjax('multi_form', [
//        return $this -> renderPartial('multi_form',[
            'partIndex' => ++$index
        ]);
    }

    /**
     * @param null $q
     * @param null $id
     * @return array
     * @throws Exception
     */
    public function actionNomenclatureList($q = null, $id = null)
    {

        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = Nomenclature::find();
            $query->select('id, name AS text')
                ->from('nomenclature')
                ->where(['like', 'name', $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Nomenclature::find()->where(['id' => $id])->one()->name];
        }
        return $out;
    }

    public function actionGetNomenclature($id)
    {
        $nomenclature = Nomenclature::findOne($id);
        $vendor_code = $nomenclature->vendor_code;
        return $vendor_code;
    }

    /**
     * @param $model_id
     * @param $new_status_id
     * @return int|string
     */
    public function actionChangeOrderStatus($model_id, $new_status_id)
    {

        try {
            $update = Yii::$app->db->createCommand()->update('request',
                ['main_status_id' => $new_status_id],
                ['id' => $model_id]
            )->execute();
            return $update;
//            if ($update) {
//                return "updated successfully.";
//            } else {
//                return "something went wrong! " . "model_id = " . $model_id . " new_status_id=" . $new_status_id;
//            }
        } catch (Exception $e) {
            Yii::info('Exception when updating: ' . $e->getMessage(), 'test');
            return null;
        }
    }

    public function actionGetOrderStatuses()
    {

        $order_statuses_objects = OrderStatus::find()->all();
        $order_statuses = [];
        foreach ($order_statuses_objects as $order_statuses_object) {
            $order_statuses[] = [
                'id' => $order_statuses_object->id,
                'name' => $order_statuses_object->name,
                'icon' => $order_statuses_object->icon,
                'color' => $order_statuses_object->color
            ];
        }

        return json_encode($order_statuses, true);
    }

    public function actionGetMainOrderStatus($main_status_id)
    {

        Yii::info('actionGetMainOrderStatus', 'test');
        $main_status = OrderStatus::find()->where(['id' => $main_status_id])->one();
        Yii::info($main_status, 'test');
        $main_status_val[] = [
            'id' => $main_status->id,
            'name' => $main_status->name,
            'icon' => $main_status->icon,
            'color' => $main_status->color
        ];
        return json_encode($main_status_val, true);
    }

    /**
     * @param $model_name
     * @param $id
     * @return bool
     * @throws Exception
     */
    public function actionFileUpload($model_name, $id)
    {

        Yii::info('_upload', 'file');
        Yii::info($_POST, 'file');
        if (!empty($_FILES['file'])) {

            //загрузка дополнительных файлов.
            if ($model_name == 'request') {
                $request_model = Request::find()->where(['id' => $id])->one();
                $request_model->file = UploadedFile::getInstanceByName('file');
//                Yii::info('UploadedFile #'. $_POST['file_id'], 'file');
//                Yii::info($request_model -> file, 'file');

                $path = "files/request/";
//                Yii::info('request_model saved.', 'file');
                if (!empty($request_model->file)) {
//                    Yii::info('order_part -> file #' . $_POST['file_id'], 'file');
//                    Yii::info($request_model -> file, 'request');
//                    Yii::info($request_model->file->extension, 'request');
                    $name = [];
                    if (!file_exists(($path))) {
                        mkdir($path, 0777, true);
                    }
                    $current_name = $path . time() . '_' . Functions::generateRandomString(10) . '.' . $request_model->file->extension;
//                    Yii::info('current_name #'. $_POST['file_id'], 'file');
//                    Yii::info($current_name, 'file');
                    $request_model->file->saveAs($current_name);
                    ImageHelper::optimize($current_name);
                    $name[] = "/" . $current_name;
                    //additional_request_photos_from_db
                    $photos_from_db = json_decode($request_model->additional_request_photos, true);
//                    Yii::info('$photos_from_db', 'file');
//                    Yii::info($photos_from_db, 'file');
                    if (!empty($photos_from_db)) {
                        $update_additional_request_photos = array_merge($name, $photos_from_db);
                    } else {
                        $update_additional_request_photos = $name;
                    }
                    //
                    Yii::$app->db->createCommand()->update('request',
                        ['additional_request_photos' => json_encode($update_additional_request_photos, true)],
                        ['id' => $request_model->id]
                    )->execute();
                    $request_model->file = [];
                } else {
                    Yii::info('there is no uploaded files', 'file');
                }

            } else if ($model_name == 'order_part') {

                $order_part_model = OrderPart::find()->where(['id' => $id])->one();
                $order_part_model->file = UploadedFile::getInstanceByName('file');
//                Yii::info('UploadedFile #'. $_POST['file_id'], 'file');
//                Yii::info($order_part_model -> file, 'file');

                $path = "files/request/";
//                Yii::info('order_part_model saved.', 'file');
                if (!empty($order_part_model->file)) {
//                    Yii::info('order_part -> file #' . $_POST['file_id'], 'file');
//                    Yii::info($order_part_model -> file, 'file');
//                    Yii::info($order_part_model->file->extension, 'file');
                    $name = [];
                    if (!file_exists(($path))) {
                        mkdir($path, 0777, true);
                    }
                    $current_name = $path . time() . '_' . Functions::generateRandomString(10) . '.' . $order_part_model->file->extension;
//                    Yii::info('current_name #'. $_POST['file_id'], 'file');
//                    Yii::info($current_name, 'file');
                    $order_part_model->file->saveAs($current_name);
                    $name[] = "/" . $current_name;
                    //additional_request_photos_from_db
                    $photos_from_db = json_decode($order_part_model->files_location, true);
//                    Yii::info('$photos_from_db', 'file');
//                    Yii::info($photos_from_db, 'file');
                    if (!empty($photos_from_db)) {
                        $files_location = array_merge($name, $photos_from_db);
                    } else {
                        $files_location = $name;
                    }
                    //
                    Yii::$app->db->createCommand()->update('order_part',
                        ['files_location' => json_encode($files_location, true)],
                        ['id' => $order_part_model->id]
                    )->execute();
//                    Yii::info('$update', 'file');
//                    Yii::info($update, 'file');

                    $order_part_model->file = [];
                } else {
                    Yii::info('there is no uploaded files', 'file');
                }
            }

        }

        return true;
    }

    /**
     * @param $model_id
     * @param $model_name
     * @param $removed_image
     * @return string
     * @throws Exception
     */
    public function actionFileRemove($model_id, $model_name, $removed_image)
    {

//        $removed_image = preg_replace('/\bweb/\b/i', '', $removed_image);
//        $removed_image = substr($removed_image, 1, strlen($removed_image));
//        $removed_image = substr($removed_image, 5, strlen($removed_image));
//        $removed_image = substr($removed_image, 1, strlen($removed_image));

        Yii::info('Удаляемая картинка: ' . $removed_image, 'file');
        $update = false;

        if ($model_name == "request") {

            $request_model = Request::find()->where(['id' => $model_id])->one();
            $additional_request_photos = $request_model->additional_request_photos;
            $additional_request_photos = json_decode($additional_request_photos, true);
            Yii::info($additional_request_photos, 'file');
//            Yii::info($additional_request_photos, 'file');
            $updated_additional_photos = [];

            if (!empty($additional_request_photos)) {
                foreach ($additional_request_photos as $additional_request_photo) {

                    Yii::info('Текущее фото: ' . $additional_request_photo, 'test');
                    Yii::info('Удаляемое фото: ' . $removed_image, 'test');

                    if ($additional_request_photo != $removed_image) {
                        $updated_additional_photos[] = $additional_request_photo;
                    } else {
                        Yii::info('this image was removed #request', 'file');
                        Yii::info($additional_request_photo, 'file');
                    }
                }
            }

            if (count($updated_additional_photos) > 0) {

                $update = Yii::$app->db->createCommand()->update('request',
                    ['additional_request_photos' => json_encode($updated_additional_photos, true)],
                    ['id' => $request_model->id]
                )->execute();
            } else {
                $update = Yii::$app->db->createCommand()->update('request',
                    ['additional_request_photos' => null],
                    ['id' => $request_model->id]
                )->execute();
            }

        } else if ($model_name == "order_part") {

            $order_part_model = OrderPart::find()->where(['id' => $model_id])->one();
            $files_locations = $order_part_model->files_location;
            $files_locations = json_decode($files_locations, true);

            Yii::info('files_locations', 'file');
            Yii::info($files_locations, 'file');

            $updated_files_location = [];
            if (!empty($files_locations)) {
                foreach ($files_locations as $file_location) {
                    Yii::info($file_location . ' == ' . $removed_image, 'test');
                    if ($file_location != $removed_image) {
                        $updated_additional_photos[] = $file_location;
                    } else {
                        Yii::info('this image was removed #order_part', 'file');
                        Yii::info($file_location, 'file');
                    }
                }
            }
            if (count($updated_files_location) > 0) {
//                $update = Yii::$app->db->createCommand()->update('order_part',
//                    ['files_location' => json_encode($updated_files_location, true)],
//                    ['id' => $model_id]
//                )->execute();
                $order_part_model->files_location = json_encode($updated_files_location, true);
            } else {
//                $update = Yii::$app->db->createCommand()->update('order_part',
//                    ['files_location' => null],
//                    ['id' => $model_id]
//                )->execute();
                $order_part_model->files_location = null;
            }
            $update = $order_part_model->save();
        }
        if ($update) {
            $removed_image = substr($removed_image, 1, strlen($removed_image));
            if ($removed_image != null && file_exists($removed_image)) {
                $result = Functions::deleteFile($removed_image);
                Yii::info('Результат: ' . $result, 'test');
            }
            return "updated successfully.";
        } else {
            Yii::error($order_part_model->errors, __METHOD__);
            return "Something went wrong! " . "model_id = " . $model_id . " model_name=" . $model_name;
        }
    }

    /**
     * Получаем меру или валюту
     *
     * @return string
     */
    public function actionGetMeasures()
    {
        $select = '<select style=" width: 100%;height: 32px;" name="measure">'; //
        $options = "";
//        $measures = Functions::getMeasureList();
        $measures = Functions::getCurrency();
//        $measures = Functions::getMeasureList();

        foreach ($measures as $key => $measure) {
            $options .= '<option value="' . $measure . '">' . $measure . '</option>';
        }
        $select .= $options;
        $select .= '</select>';

        return $select;
    }

    public function actionChangeChinaStatus()
    {

        $id = Yii::$app->request->get('id');

        $model = OrderPart::findOne($id);

        Yii::info('$id: ' . $id, 'test');

        if ($model->in_china != 1) {
            $model->in_china = 1;
        } else {
            $model->in_china = 0;
        }

        Yii::info('in_china: ' . $model->in_china, 'test');

        if (!$model->save()) {
            Yii::error($model->errors, 'error');
        } else {
            Yii::info('in_china success', 'test');
            return $model->in_china;
        }

        return true;
    }

    /**
     * Пишет новый основной статус и возвращает его цвет
     * @throws NotFoundHttpException
     */
    public function actionChangeMainOrderStatus()
    {
        $request = Yii::$app->request;
        $status_id = $request->post('status_id');
        $model_id = $request->post('model_id');

        $request_model = $this->findModel($model_id);

        if ($request_model) {
            $request_model->main_status_id = $status_id;
            if (!$request_model->save()) {
                Yii::error($request_model->errors, 'error');
                return '';
            }
        }
        $new_color = OrderStatus::findOne($status_id)->color;
        return $new_color;
    }

    /**
     * Пишет дополнительный статус и возвращает его цвет
     * @throws NotFoundHttpException
     */
    public function actionChangeAdditionalOrderStatus()
    {
        $request = Yii::$app->request;
        $status_id = $request->post('status_id');
        $model_id = $request->post('model_id');

        $request_model = $this->findModel($model_id);

        if ($request_model) {
            $request_model->additional_status_id = $status_id;
            if (!$request_model->save()) {
                Yii::error($request_model->errors);
            }
        }

        $new_color = AdditionalOrderStatus::findOne($status_id)->color;
        return $new_color;

    }

    public function actionGetStatusesColor()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $request = Yii::$app->request;
        $model_id = $request->post('model_id');

        $model = Request::findOne($model_id);

        if ($model->main_status_id){
            $main_color = OrderStatus::findOne($model->main_status_id)->color;
        } else {
            $main_color = '#fff';
        }

        if ($model->additional_status_id){
            $adv_color = AdditionalOrderStatus::findOne($model->additional_status_id)->color;
        } else {
            $adv_color = '#fff';
        }

        Yii::info('main_color = ' . $main_color, 'test');
        Yii::info('adv_color = ' . $adv_color, 'test');

        return [$main_color, $adv_color];
    }

    /**
     * Генерация кода для вставки фоток на страницу заявки
     *
     * @array $data Массив значений атрибута 'data-fancybox'
     * @int $id ID детали из номенклатуры
     *
     * @return null|string
     */
    public function actionGetAdditionalPhoto()
    {
        $request = Yii::$app->request;

        if ($request->isAjax) {
            $data = $request->post('data');

            Yii::info($data, 'test');

            $out = '';

            if (!$data) return null;

            foreach ($data as $value) {
                $id = explode('-', $value);
                $id = $id[1];

                //Получаем дополнительные фотки для детали из номенклатуры
                if (is_numeric($id) && $model = Nomenclature::findOne($id)) {
                    if ($model->additional_photos && $additional_photos = Json::decode($model->additional_photos, true)){
                        foreach ($additional_photos as $url_photo){
                            Yii::info($url_photo, 'test');
                            if (!$url_photo) continue;
                            //Из ссылок к картинкам удалить '/web'. т.к. на серваке не бычит
                            $out .= '<a data-fancybox="gallery-' . $model->id . '" href="' . $url_photo . '">
                                    <img class="request_image" src="' . $url_photo . '" alt="image">
                                </a>';
                        }
                    }

                }
            }

            return $out;
        }

        return null;
    }
}
