<?php

namespace app\controllers;

use app\components\AccessController;
use app\models\Currency;
use app\models\Functions;
use app\models\Nomenclature;
use app\models\Reserve;
use Yii;
use app\models\OrderPart;
use app\models\OrderPartSearch;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * OrderPartController implements the CRUD actions for OrderPart model.
 */
//class OrderPartController extends Controller
class OrderPartController extends AccessController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all OrderPart models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderPartSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 40;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Request models.
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function actionRequestDetails()
    {
        $request = Yii::$app->request;
        $has_editable = $request->post('hasEditable');
        if (isset($has_editable)) {
            $editable_attribute = $request->post('editableAttribute');
            $editable_key = $request->post('editableKey');
            $editable_index = $request->post('editableIndex');

            if ($editable_attribute == 'sell_price') {
                $measure = $request->post('measure');
                $currency_icon = Currency::find()->where(['name' => $measure])->one()->icon;
                $currency = '<i class = "fa ' . $currency_icon . '"></i>';
//                $update_model = OrderPart::find()->where(['id' => $editable_key])->one();
                $editable_model = $request->post('OrderPart');
                Yii::info('$editable_model', 'request');
                Yii::info($editable_model, 'request');
                $new_value = $editable_model[$editable_index]['sell_price'];
                Yii::info('new_value', 'request');
                Yii::info($new_value, 'request');
                if (!empty($new_value)) {
                    $sell_price = $new_value . ' ' . $currency . PHP_EOL . '(' . date('d.m.Y', time()) . ')';
                    try {
                        $update = Yii::$app->db->createCommand()->update('order_part',
                            ['sell_price' => $sell_price],
                            ['id' => $editable_key]
                        )->execute();
                        if ($update) {
                            Yii::$app->response->format = Response::FORMAT_JSON;
                            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
//                                $this -> refresh();
                        }
                    } catch (\Exception $e) {
                        Yii::info('Exception', 'request');
                        Yii::info($e->getMessage() . PHP_EOL . $e->getTraceAsString(), 'request');
                    } catch (\Error $e) {
                        Yii::info('Exception', 'request');
                        Yii::info($e->getMessage() . PHP_EOL . $e->getTraceAsString(), 'request');
                    }

                } else {
                    Yii::$app->db->createCommand()->update('order_part',
                        ['sell_price' => null],
                        ['id' => $editable_key]
                    )->execute();
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
                }
                return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
            } else {
                if ($editable_attribute == 'buy_price') {
                    $measure = $request->post('measure');
                    $currency_icon = Currency::find()->where(['name' => $measure])->one()->icon;
                    $currency = '<i class = "fa ' . $currency_icon . '"></i>';
//                $update_model = OrderPart::find() -> where(['id' => $editable_key]) -> one();
                    $editable_model = $request->post('OrderPart');
                    $new_value = $editable_model[$editable_index]['buy_price'];

                    if (!empty($new_value)) {
                        $buy_price = $new_value . ' ' . $currency . PHP_EOL . '(' . date('d.m.Y') . ')';
                        try {
                            $update = Yii::$app->db->createCommand()->update('order_part',
                                ['buy_price' => $buy_price],
                                ['id' => $editable_key]
                            )->execute();
                            if ($update) {
                                Yii::info('model updated!', 'request');
                                Yii::$app->response->format = Response::FORMAT_JSON;
                                return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
                            }
                        } catch (\Exception $e) {
                            Yii::info('Exception', 'request');
                            Yii::info($e->getMessage() . PHP_EOL . $e->getTraceAsString(), 'request');
                        } catch (\Error $e) {
                            Yii::info('Exception', 'request');
                            Yii::info($e->getMessage() . PHP_EOL . $e->getTraceAsString(), 'request');
                        }
                    } else {
                        Yii::$app->db->createCommand()->update('order_part',
                            ['buy_price' => null],
                            ['id' => $editable_key]
                        )->execute();
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
                    }

                }
            }
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        }

        $searchModel = new OrderPartSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 40;

        return $this->render('request_details', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OrderPart model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Деталь #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Закрыть',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Редактировать', ['update', 'id' => $id],
                        ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new OrderPart model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new OrderPart();
        $request_id = Yii::$app->session->get('request_id');

//        if (isset($_GET['request_id'])){
//            $request_id = $_GET['request_id'];
//            Yii::$app->session->set('request_id', $request_id);
//        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;

            Yii::info('is Ajax', 'test');
            if ($request->isGet) {
                Yii::info('is Get', 'test');

                $model->request_id = $request_id;

                return [
                    'title' => "Новая позиция к заявке № " . $request_id,
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post())) {

                    Yii::info('is Post', 'test');

//                if (!empty($request_id)) {
                    $model->request_id = $request_id;
//                } else {
//                    $model->request_id = Yii::$app->session->get('request_id');
//                }
                    $model->created_by = Yii::$app->user->identity->id;

                    $save = null;

                    if ($model->save()) {
                        Yii::info('model_saved', 'test');
                        Yii::info($model, 'request');
                        $model->files = UploadedFile::getInstances($model, 'files');
                        Yii::info('UploadedFile', 'test');
                        Yii::info($model->files, 'test');

                        $path = "files/request/";
                        if (!empty($model->files)) {
                            Yii::info('order_part -> files', 'test');
                            Yii::info($model->files, 'test');
                            $name = [];
                            if (!file_exists(($path))) {
                                mkdir($path, 0777, true);
                            }
                            foreach ($model->files as $uploadedFile) {
                                $current_name = $path . time() . '_' . Functions::generateRandomString(10) . '.' . $uploadedFile->extension;
                                $uploadedFile->saveAs($current_name);
                                $name[] = "/" . $current_name;
                            }
                            Yii::$app->db->createCommand()->update('order_part',
                                ['files_location' => json_encode($name, true)],
                                ['id' => $model->id]
                            )->execute();
                            $model->files = [];
                        } else {
                            Yii::info('there is no uploaded files', 'test');
                        }

                        Yii::$app->session->setFlash('success', "Деталь успешно добавлена.");
                        Yii::$app->response->format = Response::FORMAT_JSON;
//                    return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
                        return [
                            'forceReload' => '#crud-datatable-pjax',
                            'title' => "Добавление позиции к заявке №" . $request_id,
                            'content' => '<span class="text-success">Позиция создана.</span>',
                            'footer' =>
                                Html::button('Закрыть',
                                    ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                                . Html::a('Создать еще', ['create', 'request_id'],
                                    ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                        ];
                    } else {
                        Yii::error('Номер заявки: ' . $request_id, 'error');
                        Yii::error($model->errors, 'error');
                        return [
                            'title' => "Новая позиция к заявке №" . $request_id,
                            'content' => $this->renderAjax('create', [
                                'model' => $model,
                                'request_id' => $request_id
                            ]),
                            'footer' => Html::button('Закрыть',
                                    ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                        ];
                    }

                } else {
                    return [
                        'title' => "Новая Деталь",
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing OrderPart model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        Yii::info('actionUpdate', 'test');
        Yii::info($request->get(), 'request-get');
        Yii::info($request->post(), 'request-post');

        if ($request->isGet || $request->isPost) {
            $only_detail_get = $request->get('only_detail');
            $td_get = $request->get('td');
            $only_detail_post = $request->post('only_detail');

            if (isset($only_detail_get) || isset($only_detail_post)) {
                if ($request->isAjax) {
                    /*
                    *   Process for ajax request
                    */
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    if ($request->isGet) {
                        return [
                            'title' => "",
//                            'title'=> "Редактировать Деталь1 #".$id,
                            'content' => $this->renderAjax('edit_detail_form', [
                                'model' => $model,
                                'td' => $td_get,
                            ]),
                            'footer' => Html::button('Закрыть',
                                    ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                        ];
                    } else {
                        if ($model->load($request->post())) {

//                        $model->updated_date = Functions::rusDate(time(), '%DAYWEEK%, j %MONTH% Y, G:i');
                            $update = null;
                            $order_part = $request->post('OrderPart');
                            Yii::info('order_part', 'test');
                            Yii::info($order_part, 'test');

                            if (isset($order_part['nomenclature'])) {

                                Yii::info('nomenclature', 'test');
                                Yii::info($order_part['nomenclature'], 'test');

                                $check_nomenclature = strlen(preg_replace('/[0-9]+/', '', $order_part['nomenclature']));

                                Yii::info('check_nomenclature', 'test');
                                Yii::info($check_nomenclature, 'test');

                                if ($check_nomenclature == 0) {

                                    $check_nomenclature_from_db = Nomenclature::find()->where(['id' => $order_part['nomenclature']])->one();
                                    if ($check_nomenclature_from_db != null) {

                                        Yii::info('nomenclature exist1', 'test');
                                        $model->nomenclature_id = $check_nomenclature_from_db->id;

                                    } else {
                                        if ($check_nomenclature_from_db == null) {

                                            $new_nomenclature = new Nomenclature();
                                            $new_nomenclature->name = $order_part['nomenclature'];
                                            $new_nomenclature->nomenclature_type = 'manager';
                                            $create_new_nomenclature = $new_nomenclature->save();

                                            Yii::info('create_new_nomenclature1', 'test');
                                            Yii::info($create_new_nomenclature, 'test');

                                            if ($create_new_nomenclature) {
                                                $model->nomenclature_id = $new_nomenclature->id;
                                            }

                                        }
                                    }
                                } else {
                                    if ($check_nomenclature > 0) {

                                        $check_nomenclature_name_db = Nomenclature::find()->where(['name' => $order_part['nomenclature']])->one();

                                        if ($check_nomenclature_name_db != null) {

                                            Yii::info('nomenclature exist2', 'test');
                                            $model->nomenclature_id = $check_nomenclature_name_db->id;

                                        } else {
                                            if ($check_nomenclature_name_db == null) {

                                                $new_nomenclature = new Nomenclature();
                                                $new_nomenclature->name = $order_part['nomenclature'];
                                                $new_nomenclature->nomenclature_type = 'manager';
                                                $create_new_nomenclature = $new_nomenclature->save();

                                                Yii::info('create_new_nomenclature2', 'test');
                                                Yii::info($create_new_nomenclature, 'test');

                                                if ($create_new_nomenclature) {

                                                    $model->nomenclature_id = $new_nomenclature->id;
                                                }

                                            }
                                        }
                                    }
                                }
                            } else {
                                if (isset($order_part['detail'])) {

                                    Yii::info('detail', 'test');
                                    Yii::info($order_part['detail'], 'test');

                                    $check_nomenclature = strlen(preg_replace('/[0-9]+/', '', $order_part['detail']));

                                    Yii::info('check_nomenclature', 'test');
                                    Yii::info($check_nomenclature, 'test');

                                    if ($check_nomenclature == 0) {

                                        $check_nomenclature_from_db = Nomenclature::find()->where(['id' => $order_part['detail']])->one();

                                        if ($check_nomenclature_from_db != null) {

                                            Yii::info('nomenclature exist1', 'test');
                                            $model->detail_id = $check_nomenclature_from_db->id;

                                        } else {
                                            if ($check_nomenclature_from_db == null) {

                                                $new_nomenclature = new Nomenclature();
                                                $new_nomenclature->name = $order_part['detail'];
                                                $new_nomenclature->nomenclature_type = 'tech';
                                                $create_new_nomenclature = $new_nomenclature->save();

                                                Yii::info('create_new_nomenclature1', 'test');
                                                Yii::info($create_new_nomenclature, 'test');
                                                if ($create_new_nomenclature) {
                                                    $model->detail_id = $new_nomenclature->id;
                                                }

                                            }
                                        }
                                    } else {
                                        if ($check_nomenclature > 0) {

                                            $check_nomenclature_name_db = Nomenclature::find()->where(['name' => $order_part['detail']])->one();

                                            if ($check_nomenclature_name_db != null) {

                                                Yii::info('nomenclature exist2', 'test');
                                                $model->detail_id = $check_nomenclature_name_db->id;

                                            } else {
                                                if ($check_nomenclature_name_db == null) {

                                                    $new_nomenclature = new Nomenclature();
                                                    $new_nomenclature->name = $order_part['detail'];
                                                    $new_nomenclature->nomenclature_type = 'tech';
                                                    $create_new_nomenclature = $new_nomenclature->save();

                                                    Yii::info('create_new_nomenclature2', 'test');
                                                    Yii::info($create_new_nomenclature, 'test');

                                                    if ($create_new_nomenclature) {

                                                        $model->detail_id = $new_nomenclature->id;
                                                    }

                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if (isset($order_part['delivery_time_id'])) {
                                        $model->delivery_time_id = $order_part['delivery_time_id'];
                                    } else {
                                        if (isset($order_part['suppliers_id'])) {
                                            $model->suppliers_id = $order_part['suppliers_id'];
                                        } else {
                                            if (isset($order_part['in_china'])) {
                                                $model->in_china = $order_part['in_china'];

                                                Yii::info('$model -> in_china', 'test');
                                                Yii::info($model->in_china, 'test');
                                            }
                                        }
                                    }
                                }
                            }

                            Yii::info('actionUpdate post', 'test');
                            Yii::info($request->post(), 'test');

                            if ($model->validate()) {
                                Yii::info('validated', 'test');
                            } else {
                                Yii::info('has some errors', 'test');
                                Yii::info($model->errors, 'test');
                            }

                            $update = $model->save();

                            if ($update) {
                                Yii::info('saved', 'test');

                                Yii::$app->response->format = Response::FORMAT_JSON;
                                return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
                            } else {
                                return [
                                    'title' => "Редактировать позицию #" . $id . ' в заявке №' . $model->request_id,
                                    'content' => $this->renderAjax('edit_detail_form', [
                                        'model' => $model,
                                        'td' => $td_get,
                                    ]),
                                    'footer' => Html::button('Закрыть',
                                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                                ];
                            }

                        } else {
                            return [
                                'title' => "Редактировть Деталь #" . $id,
                                'content' => $this->renderAjax('edit_detail_form', [
                                    'model' => $model,
                                ]),
                                'footer' => Html::button('Закрыть',
                                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                    Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                            ];
                        }
                    }
                }
//                $this -> redirect('/order-part/update-detail?id=' . $id);
            } else {
                if ($request->isAjax) {
                    /*
                    *   Process for ajax request
                    */
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    if ($request->isGet) {
                        return [
                            'title' => "Редактирование позиции в заявке #" . $model->request_id,
                            'content' => $this->renderAjax('update', [
                                'model' => $model,
                            ]),
                            'footer' => Html::button('Закрыть',
                                    ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                        ];
                    } else {
                        if ($model->load($request->post())) {
//                    $model->updated_date = Functions::rusDate(time(), '%DAYWEEK%, j %MONTH% Y, G:i');
                            $update = null;
                            Yii::info('model validate', 'file');


                            $model->files = UploadedFile::getInstances($model, 'files');

                            Yii::info($model->validate('files'), 'file');

//                if ($model -> validate()) {
//                    Yii::info('validated', 'file');
//                } else {
//                    Yii::info('has some errors', 'file');
//                    Yii::info($model->errors, 'file');
//                }
//                try {
//                    $update = $model -> save();
//
//                } catch (\Exception $e) {
//                    Yii::info('Exception', 'file');
//                    Yii::info($e->getMessage() . PHP_EOL . $e->getTraceAsString(), 'file');
//                } catch (\Error $e) {
//                    Yii::info('Exception', 'file');
//                    Yii::info($e->getMessage() . PHP_EOL . $e->getTraceAsString(), 'file');
//                }

                            $update = $model->save();

                            if ($update) {
                                Yii::info('saved', 'file');

//                        $files_location = $this->actionFilesUpload($model);
//                    Yii::info('return files_location', 'file');
//                    Yii::info($files_location, 'file');

//                    $model -> files_location = $files_location;

                                Yii::$app->response->format = Response::FORMAT_JSON;
                                return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
                            } else {
                                return [
                                    'title' => "Редактировать Деталь #" . $id,
                                    'content' => $this->renderAjax('update', [
                                        'model' => $model,
                                    ]),
                                    'footer' => Html::button('Закрыть',
                                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                                ];
                            }
//                return [
//                    'forceReload'=>'#crud-datatable-pjax',
//                    'title'=> "Деталь #".$id,
//                    'content'=>$this->renderAjax('view', [
//                        'model' => $model,
//                    ]),
//                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
//                            Html::a('Редактировать',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
//                ];
                        } else {
                            return [
                                'title' => "Редактировть Деталь #" . $id,
                                'content' => $this->renderAjax('update', [
                                    'model' => $model,
                                ]),
                                'footer' => Html::button('Закрыть',
                                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                    Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                            ];
                        }
                    }
                } else {
                    /*
                    *   Process for non-ajax request
                    */
                    if ($model->load($request->post()) && $model->save()) {
                        return $this->redirect(['view', 'id' => $model->id]);
                    } else {
                        return $this->render('update', [
                            'model' => $model,
                        ]);
                    }
                }
            }
        }
        return false;
    }

    /**
     * Delete an existing OrderPart model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        //
        $order_part = $this->findModel($id);

        $files_locations = $order_part->files_location;
        $files_locations = json_decode($files_locations, true);

        Yii::info('_delete order-part', 'test');
        Yii::info('files_locations', 'test');
        Yii::info($files_locations, 'test');

        if (isset($files_locations) && count($files_locations) > 0) {
            foreach ($files_locations as $files_location) {
                $files_location = substr($files_location, 1, strlen($files_location));
                if ($files_location != null && file_exists($files_location)) {
                    Functions::deleteFile($files_location);
                }
            }
        }
        //
        $order_part->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing OrderPart model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBulkdelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the OrderPart model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OrderPart the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OrderPart::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }

    /**
     * @throws \yii\db\Exception
     */
    public static function actionAdditionalDelete()
    {

        $request = Yii::$app->request;
        $deleted_item = $request->post('deleted_item');
        Yii::info('$deleted_item', 'test');
        Yii::info($deleted_item, 'test');
        //substr
//        $deleted_item = substr($deleted_item, 4, strlen($deleted_item));
        //
        $ok = false;
        $files_location = $request->post('files_location');
        if (!empty($files_location)) {
            $ok = true;
        }
        $files_location = explode(", ", $files_location);

        Yii::info('$files_location', 'test');
        Yii::info($files_location, 'test');

        $model_id = $request->post('model_id');
        $new_files_location = [];
        if ($ok) {
            foreach ($files_location as $file_location) {

                if ($file_location != $deleted_item) {
                    $new_files_location[] = $file_location;
                }
            }
        }

        if (!empty($deleted_item)) {

            $deleted_element = substr($deleted_item, 1, strlen($deleted_item));
            if ($deleted_element != null && file_exists($deleted_element)) {
                Yii::info('$deleted_element removed', 'test');
                Yii::info($deleted_element, 'test');
                Functions::deleteFile($deleted_element);
            }
        }
        $update = Yii::$app->db->createCommand()->update(
            'order_part',
            ['files_location' => json_encode($new_files_location, true)],
            ['id' => $model_id]
        )->execute();

        Yii::info('$update result', 'test');
        Yii::info($update, 'test');
    }

    /**
     * @param $model
     * @return int|null
     * @throws \yii\db\Exception
     */
    public function actionFilesUpload($model)
    {

//        $model->files = UploadedFile::getInstances($model, 'files');

        if (!empty($model->files)) {

            $files_location_from_db = $model->files_location;
            $files_location_from_db = json_decode($files_location_from_db, true);

            Yii::info('$files_location_from_db', 'file');
            Yii::info($files_location_from_db, 'file');

            $path = 'files/request/';
            $new_files_location = [];

            foreach ($model->files as $file) {
                $current_name = $path . time() . '_' . Functions::generateRandomString(5) . '.' . $file->extension;
                if ($file->saveAs($current_name)) {
                    $new_files_location[] = "/" . $current_name;
                }
            }
            Yii::info('$new_files_location', 'file');
            Yii::info($new_files_location, 'file');

            if (!empty($files_location_from_db)) {
//                Yii::info('$files_location_from_db is not empty', 'file');
//                Yii::info($files_location_from_db, 'file');
                $new_files_location = array_merge($new_files_location, $files_location_from_db);
//                Yii::info('after merge', 'file');
//                Yii::info($new_files_location, 'file');
            }

            $update = Yii::$app->db->createCommand()->update(
                'order_part',
                ['files_location' => json_encode($new_files_location, true)],
                ['id' => $model->id]
            )->execute();
//            $model -> files_location = json_encode($new_files_location, true);

//            Yii::info('return $update', 'file');
//            Yii::info($update, 'file');

//            return $model->save();
            return $update;
        }
        return null;
    }

    /**
     * @param $id
     * @return array|string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdateDetail($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Редактировать Деталь #" . $id,
                    'content' => $this->renderAjax('edit_detail_form', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else {
                if ($model->load($request->post())) {
//                $model->updated_date = Functions::rusDate(time(), '%DAYWEEK%, j %MONTH% Y, G:i');
                    $update = null;


//                if ($model -> validate()) {
//                    Yii::info('validated', 'file');
//                } else {
//                    Yii::info('has some errors', 'file');
//                    Yii::info($model->errors, 'file');
//                }


                    $update = $model->save();

                    if ($update) {
                        Yii::info('saved', 'file');

                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
                    } else {
                        return [
                            'title' => "Редактировать Деталь #" . $id,
                            'content' => $this->renderAjax('edit_detail_form', [
                                'model' => $model,
                            ]),
                            'footer' => Html::button('Закрыть',
                                    ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                        ];
                    }

                } else {
                    return [
                        'title' => "Редактировть Деталь #" . $id,
                        'content' => $this->renderAjax('edit_detail_form', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('edit_detail_form', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Обнуляем деталь тех специалиста в позиции к заявке
     * @param int $id ID order_part
     * @return bool
     */
    public function actionClearDetail($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = OrderPart::findOne($id);
        $model->detail_id = null;
        if (!$model->save()) {
            Yii::error($model->errors, 'error');
        }
        return true;
    }

    /**
     * Получает файктический остаток для детали
     * @param int $id Идентификатор детали
     * @return array
     */
    public function actionGetRemains($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $detail = Nomenclature::findOne($id);

        if ($detail){
            return [
                'success' => true,
                'data' => $detail->fact_balance,
            ];
        }

        return [
            'success' => false,
            'error' => 'Деталь не найдена',
        ];


    }

    /**
     * Удаление или создание резерва для детали заявки
     * @param int $id Идентификатор Детали в заявке
     * @return array
     * @throws \yii\db\StaleObjectException
     */
    public function actionSwitchReserveForOrderPart(int $id): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $part = OrderPart::findOne($id);
        if (!$part) return [
            'success' => false,
            'error' => 'Деталь не найдена'
        ];

        if ($part->reserve){
            //Деталь в резерве, удаляем
            $part->reserve->delete();
            $data = 0;
        } else {
            //Детали нет в резерве, добавляем
            $reserve = new Reserve([
                'order_part_id' => $part->id,
                'nomenclature_id' => $part->detail_id,
                'count' => $part->quantity,
                'created_by' => Yii::$app->user->id,
            ]);
            if (!$reserve->save()){
                Yii::error($reserve->errors, '_error');
                return [
                    'success' => false,
                    'error' => 'Ошибка постановки в резерв'
                ];
            }
            $data = 1;
        }
        return [
            'success' => true,
            'data' => $data
        ];
    }
}
