<?php

namespace app\controllers;

use app\components\AccessController;
use app\models\Accounting;
use app\models\Arriving;
use app\models\DetailToArriving;
use app\models\Functions;
use app\models\Nomenclature;
use app\models\Realization;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Response;

class AccountingController extends AccessController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['set-balances'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionArriving()
    {
        return $this->render('arriving', [
            'test_data' => '',
            'path_file' => '',
        ]);
    }

    /**
     * Отчет за период. Остаток на начало периода, приход, расход, остаток на конец периода.
     * В каждом поле отображается кол-во и общая сумма
     */
    public function actionReportNew()
    {
        set_time_limit(600);

        $acc_model = new Accounting();

//        if ($request->isPost) {
//            if (!$acc_model->load($request->post())) {
//                Yii::$app->session->setFlash('error', 'Ошибка получения данных');
//            } else {
//                $acc_model->report_start_date = date('Y-m-d 00:00:00', strtotime($acc_model->report_start_date));
//                if (!$acc_model->report_end_date) {
//                    $acc_model->report_end_date = date('Y-m-d 23:59:59', time());
//                } else {
//                    $acc_model->report_end_date = date('Y-m-d 23:59:59', strtotime($acc_model->report_end_date));
//                }
//
//                $query = Nomenclature::find()
//                    ->joinWith(['arriving arr'])
//                    ->joinWith(['sales sal'])
//                    ->select([
//                        'nomenclature.id',
//                        'nomenclature.name',
//                        'nomenclature.measure_id',
//                        'nomenclature.own_vendor_code'
//                    ])
//                    ->andWhere(['nomenclature.nomenclature_type' => Users::USER_ROLE_TECH_SPECIALIST])
//                    ->andWhere(['OR',
//                            ['detail_to_arriving.warehouse' => DetailToArriving::WAREHOUSE_BASIS],
//                            ['IS', 'detail_to_arriving.warehouse', null]
//                        ]
//                    )
//                    ->distinct();
//
//                if ($acc_model->detail_id) {
//                    $query->andWhere(['nomenclature.id' => $acc_model->detail_id]);
//                }
//                if ($acc_model->own_vendor_code) {
//                    $query->andWhere(['nomenclature.id' => $acc_model->own_vendor_code]);
//                }
//
//                if ($acc_model->detail_id || $acc_model->own_vendor_code) {
//                    $det_query = clone $query;
//                    $details = $det_query
//                            ->andWhere([
//                                'OR',
//                                [
//                                    'BETWEEN',
//                                    'sal.date',
//                                    $acc_model->report_start_date,
//                                    $acc_model->report_end_date
//                                ],
//                                [
//                                    'BETWEEN',
//                                    'arr.created_at',
//                                    $acc_model->report_start_date,
//                                    $acc_model->report_end_date
//                                ]
//                            ])
//                            ->all() ?? null;
//
//                    if (!$details) {
//                        $details = $query
//                                ->andWhere([
//                                    'OR',
//                                    [
//                                        'BETWEEN',
//                                        'sal.date',
//                                        '2000-01-01 00:00:00',
//                                        date('Y-m-d H:i:s', time()),
//                                    ],
//                                    [
//                                        'BETWEEN',
//                                        'arr.created_at',
//                                        '2000-01-01 00:00:00',
//                                        date('Y-m-d H:i:s', time()),
//                                    ]
//                                ])
//                                ->all() ?? null;
//                    }
//                } else {
////                    $details = Nomenclature::find()->andWhere(['nomenclature_type' => Users::USER_ROLE_TECH_SPECIALIST])->all();
//                    $details = $query->all();
//                }
////                Yii::warning('Использовано памяти: ' . (memory_get_usage(true) / 1048576) . 'М', 'test');
//
//                return $this->render('report_doc', [
//                    'acc_model' => $acc_model,
//                    'details' => $details,
//
//                ]);
//
//            }
//        }
        $details = null;
        return $this->render('report_doc', [
            'acc_model' => $acc_model,
            'details' => $details,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionReport()
    {
        $request = Yii::$app->request;
        $model = new Accounting();
        $profit_contractors = '';
        $expense_contractors = '';

        if ($request->isPost) {
            if (!$model->load($request->post())) {
                Yii::$app->session->setFlash('error', 'Ошибка получения данных');
            } else {
                $model->report_start_date = $model->report_start_date . ' 00:00:00';
                if (!$model->report_end_date) {
                    $model->report_end_date = date('Y-m-d 23:59:59', time());
                } else {
                    $model->report_end_date = $model->report_end_date . ' 23:59:59';
                }

                $start = $model->report_start_date;
                $end = $model->report_end_date;

                $profit_contractors = Arriving::getContractors($start, $end);
                $expense_contractors = Realization::getContractors($start, $end);
//                $arriving = Arriving::getProfit($start,$end);
//                $realization = Realization::getExpenses($start, $end);

//            VarDumper::dump($model, 10, true);

//                if (!$arriving) {
//                    Yii::$app->session->setFlash('error', 'Ничего не найдено');
//                }
            }
        }
        return $this->render('report', [
            'acc_model' => $model,
            'profit_contractors' => $profit_contractors,
            'expense_contractors' => $expense_contractors,
//            'arriving_model' => $arriving,
//            'realizations_model' => $realization,
        ]);

    }

    /**
     * Экспорт заявок в XML файл
     * @return array|string
     */
    public function actionExport()
    {
        $request = Yii::$app->request;
        $model = new Accounting();
        $result = '';

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => 'Экспорт заявок',
                    'size' => 'md',
                    'content' => $this->renderAjax('export', ['model' => $model]),
                    'footer' => Html::submitButton('Экспортировать', [
                        'class' => 'btn btn-primary',
                        'data-pjax' => false,
                    ]),
                ];
            } else {
                $model->load($request->post());
                if (!$model->export_start_date) {
                    //Если не указана начальная дата
                    $model->addError('export_start_date', 'Не задана начальная дата');
                    return [
                        'title' => 'Экспорт заявок',
                        'size' => 'md',
                        'content' => $this->renderAjax('export', ['model' => $model]),
                        'footer' => Html::submitButton('Экспортировать', ['class' => 'btn btn-primary']),
                    ];
                }


                //Подчищаем папку с файлами 'экспорта'
                Functions::cleanDirectory(Url::to('@webroot/uploads/export'));


                $result = Accounting::export($model->export_start_date, $model->export_end_date,
                    $model->export_request_status);
                Yii::info($result, 'test');

//                if ($result['error'] != '') {
//                    Yii::$app->session->setFlash('error', 'Ошибки экспорта. ' . $result['error']);
//                }
//
//                if ($result['warning']) {
//                    Yii::$app->session->setFlash('warning', 'Предупреждения экспорта. ' . $result['warning']);
//                }
            }

        }

        $error_block = '';
        if (isset($result['warning']) && $result['warning'] != '') {
            $error_block = Html::tag('div', $result['warning'], ['class' => 'alert alert-warning']);
        }

        return [
            'title' => 'Результат экспорта',
            'content' => $result['data']
                . '<br><br>'
                . Html::a('<span class="glyphicon glyphicon-download-alt"></span> Скачать сформированный файл.',
                    ['download-file', 'path' => $result['path_file'] ?? ''])
                . '<br><br>'
                . $error_block
        ];
    }

    /**
     * Отдет файл на скачивание
     * @param string $path Путь к файлу
     */
    public function actionDownloadFile($path)
    {
        Yii::$app->response->sendFile($path);
    }

    /**
     * Расчитывает остатки сумм и количества товаров (деталей) на текущий момент времени. Для Cron
     * @return array
     */
    public function actionSetBalances()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return (new Accounting)->setBalances();
    }

    /**
     * Отображает приходы детали
     */
    public function actionArrivingByDetail(int $warehouse): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request->post();

        $id = $request['id'] ?? null; //ID детали
        $start = $request['start'] ?? null;
        $end = $request['end'] ?? null;

        if (!$end) {
            $end = date('Y-m-d', time());
        }

        if (!$id || !$start || !$end) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['success' => 0, 'error' => 'Не переданы необходимые параметры'];
        }

//        return $this->redirect(['/arriving/index', 'id' => $id, 'start' => $start, 'end' => $end]);
        $params = [
            'id' => $id,
            'start' => $start,
            'end' => $end,
            'warehouse' => $warehouse
        ];

        return [
            'success' => 1,
            'url' => '/arriving/index?' . http_build_query($params),
        ];
    }

    /**
     * Отображает реализации детали
     */
    public function actionRealizationByDetail()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $request = Yii::$app->request->post();

        $id = $request['id'] ?? null;
        $start = $request['start'] ?? null;
        $end = $request['end'] ?? null;

        if (!$end) {
            $end = date('Y-m-d', time());
        }

        if (!$id || !$start || !$end) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['success' => 0, 'data' => 'Не переданы необходимые параметры'];
        }
//        return $this->redirect(['/realization/index', 'id' => $id, 'start' => $start, 'end' => $end]);
        $params = [
            'id' => $id,
            'start' => $start,
            'end' => $end
        ];

        return [
            'success' => 1,
            'url' => '/realization/index?' . http_build_query($params),
        ];
    }

    /**
     * Экспорт отчета в Excel
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function actionReportExport()
    {
//        Yii::$app->response->format = Response::FORMAT_JSON;
        set_time_limit(300);
        $request = Yii::$app->request;

        if (!$request->isPost || !$request->isAjax) {
            return ['success' => 0, 'error' => 'Неподдерживаемый тип запроса'];
        }

        $acc_model = new Accounting();
//
//        foreach ($request->post()['Accounting'] as $item) {
//            if ($item['name'] != '_csrf') {
//                preg_match('/\[(.*?)\]/', $item['name'], $matches);
//                $field_name = $matches[1];
//                Yii::info('Field name: ' . $field_name, 'test');
//                $acc_model->$field_name = $item['value'];
//            }
//        }

        $acc_model->load($request->post());

        Yii::info($acc_model->attributes, 'test');

        $file = (new Accounting())->reportExport($acc_model);

        return $this->redirect(['download-report', 'file' => $file]);
    }

    public function actionDownloadReport($file)
    {
        if (file_exists($file)) {
            return Yii::$app->response->sendFile($file);
        }

        Yii::$app->session->setFlash('error', 'Ошибка формирования файла');
        return $this->redirect('report-new');
    }

    /**
     * Получает таблицу с позициями номенлкатуры для выбранных условий
     */
    public function actionGetReport(): array
    {
        set_time_limit(600);

        $request = Yii::$app->request;
        $acc_model = new Accounting();

        $table = null;

        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($request->post()) {
            $acc_model->load($request->post());
            Yii::debug($acc_model->attributes, 'Параметры построения отчета');
            if (!$acc_model->report_start_date) {
                return ['success' => false, 'error' => 'Не выбран период.'];
            }

            if (!$acc_model->report_end_date) {
                $acc_model->report_end_date = date('Y-m-d 23:59:59', time());
            } else {
                $acc_model->report_end_date = date('Y-m-d 23:59:59', strtotime($acc_model->report_end_date));
            }

            $query = $acc_model->buildQuery();

            //Разбиваем детали на склады
            $detail_by_warehouse = [];
            /** @var Nomenclature $detail */
            $basis_w_query = clone $query;
            $basis_w_result = $basis_w_query->andWhere([
                'OR',
                ['detail_to_arriving.warehouse' => DetailToArriving::WAREHOUSE_BASIS],
                ['IS', 'detail_to_arriving.warehouse', null],
            ])
                ->all();
            if ($basis_w_result) {
                $detail_by_warehouse[DetailToArriving::WAREHOUSE_BASIS] = $basis_w_result;
            }

            $way_w_query = clone $query;
            $way_w_result = $way_w_query
                ->andWhere(['detail_to_arriving.warehouse' => DetailToArriving::WAREHOUSE_ON_THE_WAY])
                ->all();
            if ($way_w_result) {
                Yii::debug(count($way_w_result), '$way_w_result count');
                $detail_by_warehouse[DetailToArriving::WAREHOUSE_ON_THE_WAY] = $way_w_result;
            }

            $way_w_w_query = clone $query;
            $way_w_w_result = $way_w_w_query
                ->andWhere(['detail_to_arriving.warehouse' => DetailToArriving::WAREHOUSE_ON_THE_WAY_TO_WAREHOUSE])
                ->all();
            if ($way_w_w_result) {
                $detail_by_warehouse[DetailToArriving::WAREHOUSE_ON_THE_WAY_TO_WAREHOUSE] = $way_w_w_result;
            }

            Yii::debug(count($detail_by_warehouse), '$detail_by_warehouse count');
//            Yii::debug($detail_by_warehouse, '$detail_by_warehouse count');

            $table = $this->renderAjax('_report_table', [
                'acc_model' => $acc_model,
//                'details' => $details,
                'details' => $detail_by_warehouse,
                'remains_type' => $acc_model->remains_type
            ]);
        }

        if (!$table) {
            $table = $this->renderAjax('_report_table', [
                'acc_model' => $acc_model,
            ]);
        }


        return [
            'success' => true,
            'data' => $table
        ];
    }
}
