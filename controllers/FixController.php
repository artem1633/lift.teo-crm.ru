<?php


namespace app\controllers;


use app\models\DetailToArriving;
use app\models\MovementPart;
use app\models\Users;
use Yii;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

class FixController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * 18.05.2022
     * Перенос информации по деталям на складах в новую таблицу
     * !!!Внимание метод работает только с пустой принимающей таблицей
     */
    public function actionReplaceWarehouseInformation(): array
    {

        Yii::$app->response->format = Response::FORMAT_JSON;
        if (!Users::isAdmin()) return ['success' => false, 'error' => 'Доступ запрещен'];
        if (MovementPart::find()->count() > 0) return ['success' => false, 'error' => 'Принимающая таблица уже содержит данные! Перенос не возможен.'];

        $counter = 0;

        $current_dta = DetailToArriving::find()
            ->andWhere(['IS NOT', 'warehouse', null]);

        $columns = [
            'nomenclature_id',
            'arriving_id',
            'warehouse_to',
            'date',
            'num',
        ];
        Yii::debug($columns, 'columns replace test');
        $rows = [];

        /** @var DetailToArriving $dta */
        foreach ($current_dta->each() as $dta){
            array_push($rows, [
                $dta->detail_id,
                $dta->arriving_id,
                $dta->warehouse,
                date('Y-m-d', strtotime($dta->arriving->created_at)),
                $dta->number
            ]);
            $counter++;
        }

        Yii::debug($rows, 'rows replace test');

        try {
            Yii::$app->db->createCommand()->batchInsert(MovementPart::tableName(), $columns, $rows)->execute();
        } catch (Exception $e) {
            Yii::error($e->getTraceAsString(), '_error');
            return [
                'success' => false,
                'error' => $e->getMessage(),
            ];
        }

        return [
            'success' => true,
            'message' => 'Перенос выполнен успешно. Обработано записей: ' . $counter
        ];
    }
}