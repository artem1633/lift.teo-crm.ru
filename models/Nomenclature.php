<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "nomenclature".
 *
 * @property int $id
 * @property string $name Название
 * @property string $vendor_code Артикул
 * @property string $own_vendor_code Собственный артикул
 * @property int $brand_id Бренды
 * @property int $group_id Группы
 * @property int $weight Вес
 * @property int $type_of_parts_id Тип запчасти
 * @property int $type_of_mechanism_id Тип механизма
 * @property int $description Описание
 * @property string $main_photo Основное изображение
 * @property string $additional_photos Дополнительное изображение
 * @property string $created_date Дата создания и время создания
 * @property string $updated_date Дата и время редактирования
 * @property int $created_by Кто создал
 * @property string $nomenclature_type Тип номенклатуры (manager/tech_specialist)
 * @property int $request_id ID заявки
 * @property int $cost_price Себестоимость
 * @property int $measure_id Единица измерения
 * @property double $avg_purchase_price Средняя цена закупки
 * @property double $fact_balance Фактический баланс
 * @property int $reserve Зарезервировано
 * @property int $on_the_way Кол-во деталей в пути (Склад/Поступления)
 * @property int $warehouse Склад
 *
 * @property Brand $brand
 * @property NomenclatureGroup $group
 * @property TypeOfMechanism $typeOfMechanism
 * @property TypeOfParts $typeOfParts
 * @property Measure $measure
 * @property Arriving[] $arriving
 * @property float $totalReservesCount Общее кол-во в резерве
 * @property \app\models\Reserve[] $reserves Резервы для детали
 * @property \app\models\LogisticRequestPart[] $logisticRequestPart
 * @property \app\models\LogisticRequest[] $logisticRequest Логистические счета для детали
 * @property \app\models\MovementPart[] $movementParts Движения по складам для детали
 *
 */
class Nomenclature extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $file;
    public $files;
    public $request_id; //Для организации возврата к заявке после редактирования детали

    public $codename; //Наименование + внутренний артикул

    public $on_the_way;
    public $warehouse;

//    public $nomenclature_type; //Тех спец или менеджер

    public static function tableName()
    {
        return 'nomenclature';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'brand_id',
                    'group_id',
                    'type_of_parts_id',
                    'type_of_mechanism_id',
                    'created_by',
                    'measure_id',
                    'reserve',
                    'warehouse',
                ],
                'integer'
            ],
            [
                [
                    'nomenclature_type',
                    'name',
                    'vendor_code',
                    'own_vendor_code',
                    'main_photo',
                    'created_date',
                    'updated_date'
                ],
                'string',
                'max' => 255
            ],
            [['description', 'additional_photos'], 'string'],
            [
                ['brand_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Brand::className(),
                'targetAttribute' => ['brand_id' => 'id']
            ],
            [
                ['group_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => NomenclatureGroup::className(),
                'targetAttribute' => ['group_id' => 'id']
            ],
            [
                ['type_of_mechanism_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => TypeOfMechanism::className(),
                'targetAttribute' => ['type_of_mechanism_id' => 'id']
            ],
            [
                ['type_of_parts_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => TypeOfParts::className(),
                'targetAttribute' => ['type_of_parts_id' => 'id']
            ],
            [['file'], 'file', 'maxFiles' => 1, 'extensions' => 'jpeg, jpg, png'],
            [
                ['files'],
                'file',
                'maxFiles' => 1e9, /*'skipOnEmpty' => true,*/  /*'maxSize' => 1024 * 5120, */
                'extensions' => 'jpeg, jpg, png'
            ],
            [['weight', 'cost_price', 'avg_purchase_price'], 'number'],
            [['request_id'], 'integer'],
            [['fact_balance'], 'number']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'vendor_code' => 'Артикул',
            'own_vendor_code' => 'Собственный артикул',
            'brand_id' => 'Бренды',
            'group_id' => 'Группы',
            'weight' => 'Вес',
            'type_of_parts_id' => 'Тип запчасти',
            'type_of_mechanism_id' => 'Тип механизма',
            'description' => 'Описание',
            'main_photo' => 'Основное изображение',
            'additional_photos' => 'Дополнительные изображения',
            'created_date' => 'Дата создания и время создания',
            'updated_date' => 'Дата и время редактирования',
            'created_by' => 'Кто создал',
            'file' => 'Основное изображение',
            'files' => 'Дополнительные изображения',
            'nomenclature_type' => 'Тип номенклатуры',
            'cost_price' => 'Себестоимость',
            'measure_id' => 'Единица измерения',
            'avg_purchase_price' => 'Средняя цена закупки',
            'fact_balance' => 'Фактические остатки',
            'reserve' => 'Резерв',
            'on_the_way' => 'В пути',
            'warehouse' => 'Склад',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord && $this->nomenclature_type == Users::USER_ROLE_TECH_SPECIALIST && !$this->weight) {
            //Если при добавлении детали в базу тех специалиста не указан вес
            $this->addError('weight', 'Не указан вес детали');
            return false;
        }

        if ($this->nomenclature_type == Users::USER_ROLE_TECH_SPECIALIST && Users::isManager()) {
            //Если менеджер пытается отредактировать запись тех специалиста
            $this->addError('name', 'Не достаточно прав для внесения изменений');
            return false;
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert && $this->nomenclature_type == Users::USER_ROLE_TECH_SPECIALIST) {
            //Если новая запись и она добавляется в номенклатуру специалиста
            $this->own_vendor_code = $this->getOwnVendorCode($this->id);
            if (!$this->save()) {
                Yii::$app->session->addFlash('error', $this->errors);
                Yii::error($this->errors, __METHOD__);
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brand::className(), ['id' => 'brand_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(NomenclatureGroup::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypeOfMechanism()
    {
        return $this->hasOne(TypeOfMechanism::className(), ['id' => 'type_of_mechanism_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypeOfParts()
    {
        return $this->hasOne(TypeOfParts::className(), ['id' => 'type_of_parts_id']);
    }

    /**
     * Генерит собственный артикул
     * @param int $id ID зписи в номенклатуре
     * @return string
     */
    public function getOwnVendorCode($id)
    {
        $own_vendor_code = 'L' . str_pad($id, '8', '0', STR_PAD_LEFT);
        Yii::info('Собственный артикул: ' . $own_vendor_code, 'test');
        return $own_vendor_code;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderParts()
    {
        return $this->hasMany(OrderPart::className(), ['nomenclature_id' => 'id']);

    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getRequest()
    {
        return $this->hasMany(Request::className(), ['id' => 'request_id'])
            ->viaTable('order_part', ['detail_id' => 'id']);
    }

    /**
     * @param int $id ID позиции в заявке
     * @return mixed|null
     */
    public static function getOwnerVendorCodeByOrderPartId($id)
    {
        return self::find()
                ->joinWith(['orderParts op'])
                ->andWhere(['op.id' => $id])
                ->one()
                ->own_vendor_code ?? null;
    }

    /**
     * Получает собственный артикул
     * @param int $id ID Детали в номенклатуре
     * @return null|string
     */
    public static function getOwnerVendorCodeByDetailId($id)
    {
        return self::findOne($id)->own_vendor_code ?? null;
    }

    /**
     * Получает список деталей специалиста
     * @return array
     */
    public static function getList()
    {
        return ArrayHelper::map(Nomenclature::find()->andWhere(['nomenclature_type' => Users::USER_ROLE_TECH_SPECIALIST])->all(),
            'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeasure()
    {
        return $this->hasOne(Measure::class, ['id' => 'measure_id']);
    }

    /**
     * Получает все закупки для детали
     * @return \yii\db\ActiveQuery
     */
    public function getPurchases()
    {
        return $this->hasMany(DetailToArriving::class, ['detail_id' => 'id']);
    }

    /**
     * Получает все продажи для детали
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getSales()
    {
        return $this->hasMany(Realization::class, ['id' => 'realization_id'])
            ->viaTable('detail_to_realization', ['detail_id' => 'id']);
    }

    /**
     * Получает все Поступления для детали
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getArriving()
    {
        return $this->hasMany(Arriving::class, ['id' => 'arriving_id'])
            ->viaTable('detail_to_arriving', ['detail_id' => 'id']);
    }

    public function getArrivings()
    {
        return $this->hasMany(Arriving::class, ['id' => 'arriving_id'])
            ->via('purchases');
    }

    /**
     * @param string $code Артикул
     * @return array
     */
    public static function getIdByVendorCode($code)
    {
        /** @var Nomenclature $detail */
        $detail = Nomenclature::find()
                ->andWhere(['nomenclature_type' => Users::USER_ROLE_TECH_SPECIALIST])
                ->andWhere(['own_vendor_code' => $code])
                ->one() ?? null;
        if (!$detail) {
            return ['error' => 1, 'data' => 'Деталь не найдена'];
        }

        return [
            'error' => 0,
            'data' => [
                'id' => $detail->id,
                'name' => $detail->name
            ]
        ];/**/
    }

    public static function getOwnVendorList()
    {
        return ArrayHelper::map(Nomenclature::find()->andWhere(['nomenclature_type' => Users::USER_ROLE_TECH_SPECIALIST])->all(),
            'own_vendor_code', 'own_vendor_code');
    }

    public static function getListWithOwnVendorCode()
    {
        $result = self::find()
            ->select(['id', 'CONCAT(name, " (", own_vendor_code, ")") AS codename'])
            ->all();

        return ArrayHelper::map($result, 'id', 'codename');
    }

    public static function getInfo($id)
    {
        $result = self::find()->andWhere(['id' => $id])->asArray()->one();

        $photo = Nomenclature::findOne($id)->main_photo;
        $thumbs = Functions::getThumbnailPath($photo);

        Yii::info($photo, 'test');

        if (is_file(Url::to('@webroot' . $photo))) {
            $result['thumbs'] = $thumbs;
            $result['photo'] = $photo;
        } else {
            $result['thumbs'] = '/images/site/no-image.png';
        }


        Yii::info($result, 'test');

        return $result;
    }

    /**
     * Все связи детали с поступлением (Склад/Поступления)
     * @return \yii\db\ActiveQuery
     */
    public function getDetailToArriving(): ActiveQuery
    {
        return $this->hasMany(DetailToArriving::class, ['detail_id' => 'id']);
    }

    /**
     * Все резервы. Счета из логистики. Заявки
     * @return float|int
     */
    public function getTotalReservesCount()
    {
        return Reserve::getTotalReservesCountByDetail($this->id);
    }

    /**
     * Резервы детали
     * @return \yii\db\ActiveQuery
     */
    public function getReserves(): ActiveQuery
    {
        return $this->hasMany(Reserve::class, ['nomenclature_id' => 'id']);
    }

    public function getLogisticRequestPart(): ActiveQuery
    {
        return $this->hasMany(LogisticRequestPart::class, ['nomenclature_id' => 'id']);
    }

    /**
     * Счета логистики для детали
     * @return \yii\db\ActiveQuery
     */
    public function getLogisticRequest(): ActiveQuery
    {
        return $this->hasMany(LogisticRequest::class, ['id' => 'logistic_request_id'])
            ->via('logisticRequestPart');
    }

    /**
     * Движение по складам для детали
     * @return \yii\db\ActiveQuery
     */
    public function getMovementParts(): ActiveQuery
    {
        return $this->hasMany(MovementPart::class, ['nomenclature_id' => 'id']);
    }

//    /**
//     * Получает наименование еденицы измерения для детали
//     * @param $id
//     */
//    public static function getMeasureNameByDetail($id)
//    {
//        return self::getMeasure()->asArray()
//    }
//    public function beforeDelete()
//    {
//        $monologComponent = Yii::$app->monolog;
//        $logger = $monologComponent->getLogger("delete");
//        if ($this -> main_photo != null && file_exists($this->main_photo)) {
////            \Yii::info('beforeDelete', 'upload');
////            \Yii::info('images/nomenclature/main_photo/' . $this->main_photo, 'upload');
////            unlink(Yii::getAlias($this->main_photo));
//            $logger->log('info', "beforeDelete");
//            $logger->log('info', var_export(substr($this->main_photo, 1, strlen($this->main_photo)), true));
//            unlink(Yii::getAlias(substr($this->main_photo, 1, strlen($this->main_photo))));
//        }
//        return parent::beforeDelete();
//    }
}
