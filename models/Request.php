<?php

namespace app\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "request".
 *
 * @property int $id
 * @property int $contractor_id Контрагент
 * @property string $description Комментарии
 * @property int $main_status_id Статус основной
 * @property int $additional_status_id Статус дополнительный
 * @property string $additional_request_photos Дополнительные изображения
 * @property string $created_date Дата создания и время создания
 * @property string $updated_date Дата и время редактирования
 * @property int $created_by Кто создал
 * @property int $in_china Есть в китае
 * @property int $is_reserve В резерве или нет
 *
 * @property AdditionalOrderStatus $additionalStatus
 * @property Contractor $contractor
 * @property OrderStatus $mainStatus
 */
class Request extends ActiveRecord
{
    public $in_china;
    public $file;
    public $files;
    public $contractor;
    public $email;
    public $is_reserve;

    public $account_number;
    public $count;

    /**
     * {@inheritdoc}
     */

    public static function tableName()
    {
        return 'request';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['contractor_id', 'main_status_id', 'additional_status_id', 'in_china', 'created_by', 'is_reserve'], 'integer'],
            [['description', 'additional_request_photos'], 'string'],
            [['created_date', 'updated_date'], 'string', 'max' => 255],
            [['file'], 'file', 'extensions' => 'jpeg, jpg, png'],
            [['files'], 'file', 'maxFiles' => 1e9, 'extensions' => 'jpeg, jpg, png' /*'skipOnEmpty' => true,*/  /*'maxSize' => 1024 * 5120, */],
            [['additional_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => AdditionalOrderStatus::className(), 'targetAttribute' => ['additional_status_id' => 'id']],
            [['contractor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contractor::className(), 'targetAttribute' => ['contractor_id' => 'id']],
            [['main_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderStatus::className(), 'targetAttribute' => ['main_status_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contractor_id' => 'Контрагент',
            'contractor' => 'Контрагент1',
            'description' => 'Комментарии',
            'main_status_id' => 'Статус',
            'additional_status_id' => 'Статус Менеджера',
            'additional_request_photos' => 'Дополнительные изображения',
            'created_date' => 'Дата создания',
            'updated_date' => 'Дата редактирования',
            'created_by' => 'Кто создал',
//            'request_detail' => 'Деталь',
            'in_china' => 'Есть в китае',
            'contractorName' => 'Контрагент',
            'createdDate' => 'Дата создания',
            'updatedDate' => 'Дата обновления',
            'is_reserve' => 'Резерв',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdditionalStatus()
    {
        return $this->hasOne(AdditionalOrderStatus::className(), ['id' => 'additional_status_id']);
    }

    /**
     * Связь с моделью Контрагента
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'contractor_id']);
    }

    /**
     * Геттер для имени контрагента
     * @return string
     */
    public function getContractorName()
    {
        return $this->contractor->name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainStatus()
    {
        return $this->hasOne(OrderStatus::className(), ['id' => 'main_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getDetails()
    {
        return $this->hasMany(Nomenclature::className(), ['id' => 'detail_id'])
            ->viaTable('order_part', ['request_id' => 'id']);
    }

    /**
     * Генерирует строку с суммой и денежным знаком
     * @param int $order_part_id ИД order_part
     * @param string $type Тип sell/buy покупка или продажа
     * @return string
     * @property mixed $model Модель OrderPart
     */
    public function getPriceAndCurrency($order_part_id, $type = 'sell')
    {
        $model = OrderPart::findOne($order_part_id);

        if ($type == 'sell') {
            return $model->sell_price . ' <i class="fa ' . Currency::findOne($model->sell_currency)->icon . '"></i>';
        } elseif ($type == 'buy') {
            return $model->buy_price . ' <i class="fa ' . Currency::findOne($model->buy_currency)->icon . '"></i>';
        }

        return null;
    }

    /**
     * Связь с деталями заявки
     * @return \yii\db\ActiveQuery
     */
    public function getOrderParts()
    {
        return $this->hasMany(OrderPart::class, ['request_id' => 'id']);
    }

    /**
     * Получает все детали тех специалиста для заказа
     * @param int $id ID заказа
     * @return array|ActiveRecord[]
     */
    public static function getDetailsFromRequest($id)
    {
        $result = Nomenclature::find()
            ->joinWith(['request r'])
            ->andWhere(['r.id' => $id])
            ->andWhere(['nomenclature_type' => Users::USER_ROLE_TECH_SPECIALIST])
            ->all();

        return $result;

    }

    /**
     * Резервы для заявки
     * @return ActiveQuery
     */
    public function getReserves(): ActiveQuery
    {
        return $this->hasMany(Reserve::class, ['order_part_id' => 'id'])
            ->via('orderParts');
    }

    /**
     * @param int $id Идентификатор детали из номенклатуры
     * @return array
     */
    public static function getReserveForNomenclature(int $id): array
    {
        $part = Nomenclature::findOne($id);

        if (!$part) return [];

        return self::find()
            ->joinWith(['reserves'])
            ->select(['request.id', 'request.id AS account_number' , 'reserve.count'])
            ->andWhere(['reserve.nomenclature_id' => $part->id])
            ->andWhere(['IS NOT', 'reserve.order_part_id', null])
            ->all();
    }
}
