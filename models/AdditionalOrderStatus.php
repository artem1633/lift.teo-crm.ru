<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "additional_order_status".
 *
 * @property int $id
 * @property string $name Наименование
 * @property string $icon Иконка
 * @property string $color Цвет
 *
 */
class AdditionalOrderStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'additional_order_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            [['icon', 'color'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'icon' => 'Иконка',
            'color' => 'Цвет',
        ];
    }
}
