<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $file;

    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'XML'],
        ];
    }

    public function upload($tmp_name)
    {
        if ($this->validate()) {
            $target_dir = 'uploads/import/';
            if (!is_dir($target_dir)){
                mkdir($target_dir, 0777, true);
            }
            $file_path = $target_dir . $tmp_name . '.' . $this->file->extension;
            $this->file->saveAs($file_path);
            return $file_path;
        } else {
            Yii::error($this->errors, '_error');
            return false;
        }
    }
}