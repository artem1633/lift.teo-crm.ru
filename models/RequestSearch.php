<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * RequestSearch represents the model behind the search form about `app\models\Request`.
 */
class RequestSearch extends Request
{
    public $contractorName;
    public $createdDate;
    public $updatedDate;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'contractor_id', 'main_status_id', 'additional_status_id', 'created_by'], 'integer'],
            [['contractorName', 'description', 'additional_request_photos', 'created_date', 'updated_date'], 'safe'],
            [['createdDate', 'updatedDate'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Request::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['created_date' => SORT_DESC],
            ]
        ]);

        $dataProvider->sort->attributes['contractorName'] = [
            'asc' => [Contractor::tableName() . '.name' => SORT_ASC],
            'desc' => [Contractor::tableName() . '.name' => SORT_DESC],
            'label' => 'Контрагент'
        ];

//        $this->load($params);
//
//        if (!$this->validate()) {
//            // uncomment the following line if you do not want to return any records when validation fails
//            // $query->where('0=1');
//            return $dataProvider;
//        }

        if (!($this->load($params) && $this->validate())) {
            /**
             * Жадная загрузка данных модели Контрагента
             * для работы сортировки.
             */
            $query->joinWith(['contractor']);
            return $dataProvider;
        }

        $query->andFilterWhere([
            'request.id' => $this->id,
            'contractor_id' => $this->contractor_id,
            'main_status_id' => $this->main_status_id,
            'additional_status_id' => $this->additional_status_id,
            'request.created_by' => $this->created_by,
        ]);

        \Yii::info('id = ' . $this->id, 'test');
        \Yii::info('createdDate = ' . $this->createdDate, 'test');
        \Yii::info('created_date = ' . $this->created_date, 'test');
        \Yii::info('contractor_id = ' . $this->contractor_id, 'test');
        \Yii::info('contractorName = ' . $this->contractorName, 'test');

        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'additional_request_photos', $this->additional_request_photos]);
//            ->andFilterWhere(['like', Request::tableName() . '.created_date', $this->created_date])
//            ->andFilterWhere(['like', Request::tableName() . '.updated_date', $this->updated_date]);

//        if ($this->created_date) {
//            $query->andFilterWhere(['between', 'request.created_date', date('Y-m-d 00:00:00', strtotime($this->created_date)), date('Y-m-d 23:59:59', strtotime($this->created_date))]);
//        }
//        if ($this->updated_date) {
//            $query->andFilterWhere(['between', 'request.updated_date', date('Y-m-d 00:00:00', strtotime($this->updated_date)), date('Y-m-d 23:59:59', strtotime($this->updated_date))]);
//        }
//         Фильтр по Контрагенту
        $query->joinWith(['contractor' => function ($q) {
            $q->where(Contractor::tableName() . '.name LIKE "%' . $this->contractorName . '%"');
        }]);


        if($this->created_date){
//            $date = explode(' - ', $this->created_date);
            $query->andWhere(['between', Request::tableName() .'.created_date', $this->created_date.' 00:00:00', $this->created_date.' 23:59:59']);
        }

        if($this->updated_date){
//            $date = explode(' - ', $this->updated_date);
            $query->andWhere(['between', Request::tableName() .'.updated_date', $this->updated_date.' 00:00:00', $this->updated_date.' 23:59:59']);
        }

        return $dataProvider;
    }
}
