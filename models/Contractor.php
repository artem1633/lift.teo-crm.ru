<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "contractor".
 *
 * @property int $id
 * @property string $name Название
 * @property string $full_name Полное название
 * @property int $city_id Город
 * @property string $address Адрес
 * @property string $phone Телефон
 * @property string $contact_person Контактное лицо
 * @property string $email Емайл
 * @property string $requisites Реквизиты
 * @property string $comment Комментарии
 * @property int $manager_id Менеджер
 * @property int $do_not_touch Не трогать
 * @property string $created_date Дата создания и время создания
 * @property string $updated_date Дата и время редактирования
 * @property int $created_by Кто создал
 * @property string $link_1c Ссылка в 1С
 * @property string $inn ИНН контрагента
 * @property string $adv_name Наименование контрагента + ИНН
 * @property int $is_active Активен/Деактивирован
 *
 * @property Cities $city
 * @property Users $manager
 * @property JuridicalPerson $juridicalPerson
 */
class Contractor extends ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_DEACTIVATE = 2;

    public $adv_name;

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'contractor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['city_id', 'manager_id', 'do_not_touch', 'created_by', 'is_active'], 'integer'],
//            [['inn'], 'required'],
            [['requisites', 'comment', 'address', 'inn', 'link_1c'], 'string'],
            [
                ['name', 'full_name', 'phone', 'contact_person', 'email', 'created_date', 'updated_date'],
                'string',
                'max' => 255
            ],
            [
                ['city_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Cities::class,
                'targetAttribute' => ['city_id' => 'id']
            ],
            [
                ['manager_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Users::class,
                'targetAttribute' => ['manager_id' => 'id']
            ],
            [['adv_name'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'full_name' => 'Полное наименование',
            'city_id' => 'Город',
            'address' => 'Адрес',
            'phone' => 'Телефон',
            'contact_person' => 'Контактное лицо',
            'email' => 'Email',
            'requisites' => 'Реквизиты',
            'comment' => 'Комментарии',
            'manager_id' => 'Менеджер',
            'do_not_touch' => 'Не трогать',
            'created_date' => 'Дата создания и время создания',
            'updated_date' => 'Время последнего действия',
            'created_by' => 'Кто создал',
            'link_1c' => 'Ссылка в 1С',
            'inn' => 'ИНН',
            'is_active' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(Users::className(), ['id' => 'manager_id']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            if (!$this->created_by) {
                $this->created_by = Yii::$app->user->id;
            }
        }
        $this->updated_date = date('Y-m-d H:i:s', time());
        return parent::beforeSave($insert);
    }

    public static function getEmailList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'email');
    }

    public static function getList()
    {
        return ArrayHelper::map(self::find()->orderBy('name')->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealizations()
    {
        return $this->hasMany(Realization::class, ['contractor_id' => 'id']);
    }

    public function getListWithInn()
    {
        //Контрагенты с ИНН
        $result1 = self::find()
            ->select('`id`, CONCAT(`name`, " (ИНН: ", `inn`, ")") AS `adv_name`')
            ->andWhere(['OR', ['IS NOT', 'inn', null], ['<>', 'inn', ""]])
            ->all();

        //Контрагенты без ИНН
        $result2 = self::find()
            ->select('`id`, `name` AS `adv_name`')
            ->andWhere(['OR', ['IS', 'inn', null], ['=', 'inn', ""]])
            ->all();

        $arr1 = ArrayHelper::map($result1, 'id', 'adv_name');
        $arr2 = ArrayHelper::map($result2, 'id', 'adv_name');

//        $result_arr = array_merge($arr1,$arr2);
        $result_arr = $arr1 + $arr2;

//        Yii::info($result_arr, 'test');

        return $result_arr;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJuridicalPerson()
    {
        return $this->hasMany(JuridicalPerson::class, ['contractor_id' => 'id']);
    }

    /**
     * Статусы контрагента
     * @return string[]
     */
    public static function getStatusList(): array
    {
        return [
            self::STATUS_ACTIVE => 'Активен',
            self::STATUS_DEACTIVATE => 'Не активен',
        ];
    }
}
