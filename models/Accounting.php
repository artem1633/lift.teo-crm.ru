<?php

namespace app\models;

use bupy7\xml\constructor\XmlConstructor;
use Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Yii;
use yii\base\Model;
use yii\db\ActiveQuery;

/**
 * Класс модели для складского учета.
 *
 * @property string $export_start_date Начало периода для экспорта
 * @property string $export_end_date Конец периода для экспорта
 * @property string $report_start_date НАчало периода для отчета
 * @property string $report_end_date Конец периода для отчета
 * @property string $export_request_status Статус заявки для выгрузки
 * @property int $detail_id Деталь
 * @property string $own_vendor_code Собственный артикул
 * @property int $brand_id Бренд
 * @property int $type_of_parts_id Тип запчасти
 * @property int $nomenclature_group_id Группа запчасти
 * @property int $remains_type Тип отчета. 1 - вся номенклатура, 2 - без нулевых остатков, только то что есть
 * @property int $warehouse Склад
 *
 * @property Accounting $accounting
 * @property Request $request
 */
class Accounting extends Model
{
    const REMAINS_TYPE_ALL = 1;
    const REMAINS_TYPE_NOT_ZERO = 2;

    public $export_start_date;
    public $export_end_date;
    public $report_start_date;
    public $report_end_date;
    public $export_request_status;
    public $detail_id;
    public $own_vendor_code;
    public $brand_id;
    public $type_of_parts_id;
    public $nomenclature_group_id;
    public $remains_type;
    public $warehouse;

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [
                ['export_start_date', 'export_end_date', 'report_start_date', 'report_end_date', 'own_vendor_code'],
                'safe'
            ],
            [['export_start_date', 'report_start_date'], 'required', 'message' => 'Укажите начало периода'],
            [[
                'export_request_status',
                'detail_id', 'brand_id',
                'type_of_parts_id',
                'nomenclature_group_id',
                'remains_type',
                'warehouse',
            ], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'export_start_date' => 'Начало периода',
            'export_end_date' => 'Конец периода',
            'export_request_status' => 'Экспортировать заявки со статусом:',
            'brand_id' => 'Бренд',
            'type_of_parts_id' => 'Тип',
            'nomenclature_group_id' => 'Группа',
            'remains_type' => 'Остатки',
            'warehouse' => 'Склад',
        ];
    }

    /**
     * Експорт заявок в файл.
     * @param string $start_date Начальная дата
     * @param string $end_date Конечная дата
     * @param string $export_request_status Статусы экспортируемых заявок
     * @return array
     */
    public static function Export($start_date, $end_date = '', $export_request_status = '')
    {
        $order_counter = 0;
        $xml_elements = [];
        $errors = [];
        $warnings = [];
        $product_count = 0;

        $query = Request::find()
            ->andWhere(['>=', 'created_date', $start_date . ' 00:00:00']);

        if ($end_date) {
            $query->andWhere(['<=', 'created_date', $end_date . ' 23:59:59']);
        }

        if ($export_request_status && is_array($export_request_status) && count($export_request_status) > 0) {
            $query->andWhere(['IN', 'main_status_id', $export_request_status]);
        }

        /** @var Request $order */
        foreach ($query->each() as $order) {

            $contractor_model = Contractor::findOne($order->contractor_id) ?? null;

            $contractor_name = $contractor_model->name ?? null;
            $contractor_full_name = $contractor_model->full_name ?? null;
            $contractor_inn = $contractor_model->inn ?? null;
            $contractor_link_1c = $contractor_model->link_1c ?? null;

            array_push($xml_elements, [
                'tag' => 'Документ.РеализацияТоваровУслуг',
                'elements' => [
                    //Добавляем инфу о контрагенте
                    [
                        'tag' => 'Контрагент',
                        'elements' => [
                            ['tag' => 'Ссылка', 'content' => $contractor_link_1c],
                            [
                                'tag' => 'Наименование',
                                'content' => strip_tags($contractor_name),
                                'cdata' => true,
                            ],
                            [
                                'tag' => 'НаименованиеПолное',
                                'content' => strip_tags($contractor_full_name),
                                'cdata' => true,
                            ],
                            ['tag' => 'ИНН', 'content' => $contractor_inn],
                        ]
                    ],
                    //Добавляем заявку
                    [
                        'tag' => 'Заказ',
                        'elements' => [
                            [
                                'tag' => 'Дата',
                                'content' => self::convertTo1cDate($order->created_date)
                            ],
                            [
                                'tag' => 'Номер',
                                'content' => 'CRM-' . str_pad($order->id, 6, '0', STR_PAD_LEFT),
                            ]
                        ],
                    ],
                    //Тэг товары, для добавления товаров
                    ['tag' => 'Товары']
                ]
            ]);

            Yii::info('Заказ: ' . $order->id, 'test');

            //Добавляем детали
//            $xml_elements[$order_counter]['elements'][2]['elements']; //Тэг 'Товары'
            $elements = [];

            /** @var OrderPart $part */
            foreach ($order->getOrderParts()->each() as $part) {
                Yii::info($part->toArray(), 'test');

                $detail = $part->detail ?? null;
                if (!$detail) {
                    array_push($warnings, 'Заявка №' . $order->id . ' пропущена, т.к. в заявке отсутствуют детали');
                    continue 2;
                }

                Yii::info($detail->toArray(), 'test');

                $sell_price = $part->sell_price ?? 0;
                $quantity = $part->quantity ?? 0;
                $sum = (string)((double)$sell_price * (double)$quantity);
                if (!$detail->own_vendor_code) {
                    $detail->own_vendor_code = ' ';
                }


                array_push($elements, [
                        'tag' => 'Строка',
                        'elements' => [
                            [
                                'tag' => 'ДанныеНоменклатуры',
                                'elements' => [
                                    [
                                        'tag' => 'Номенклатура',
                                        'elements' => [
                                            [
                                                'tag' => 'Артикул',
                                                'content' => $detail->own_vendor_code,
                                            ],
                                            [
                                                'tag' => 'НаименованиеПолное',
                                                'content' => $detail->name,
                                                'cdata' => true,
                                            ],
                                            [
                                                'tag' => 'Наименование',
                                                'content' => $detail->name,
                                                'cdata' => true,
                                            ],
                                        ]
                                    ]
                                ]
                            ],
                            [
                                'tag' => 'ЕдиницаИзмерения',
                                'elements' => [
                                    ['tag' => 'Наименование', 'content' => $part->measure->name ?? 'Не указано']
                                ]
                            ],
                            ['tag' => 'Количество', 'content' => str_replace(',', '.', $quantity)],
                            ['tag' => 'Цена', 'content' => $sell_price],
                            ['tag' => 'Сумма', 'content' => $sum]
                        ]
                    ]
                );

                $xml_elements[$order_counter]['elements'][2]['elements'] = $elements;
                $product_count += 1;
            }
            $order_counter += 1;
        }

        $xml_data = [
            [
                'tag' => 'Body',
                'attributes' => [
                    'xmlns' => 'http://v8.1c.ru/edi/edi_stnd/EnterpriseData/1.6'
                ],
                'elements' => $xml_elements,
            ]
        ];
//        Yii::info($xml_data, 'test');

        $xml = new XmlConstructor();
        $result = $xml->fromArray($xml_data)->toOutput();

        Yii::info($result, 'test');

        $path_dir = 'uploads/export';
        $file_name = 'export_' . date('d.m.Y H_i', time()) . '.xml';

        if (!is_dir($path_dir)) {
            mkdir($path_dir, 0777, true);
        }

        $path_file = $path_dir . '/' . $file_name;

        if (file_put_contents($path_file, $result)) {

            if (count($errors) > 0) {
                Yii::error($errors, 'error');
                $error = implode(' ', $errors);
            } else {
                $error = '';
            }

            if (count($warnings) > 0) {
                Yii::warning($warnings, 'warning');
                $warning = implode(',<br>', $warnings);
            } else {
                $warning = '';
            }

            return [
                'success' => 'true',
                'data' => 'Операция экспорта завершена'
                    . '<br> Заказов: ' . ($order_counter)
                    . '<br> Экспортировано товаров: ' . $product_count,
                'warning' => $warning,
                'error' => $error,
                'order_count' => $order_counter,
                'product_count' => $product_count,
                'path_file' => $path_file,
            ];
        } else {
            return [
                'success' => 'false',
                'error' => 'Ошибка сохранения информации в файл',
            ];
        }
    }

    private static function convertTo1cDate($date)
    {
        if (!$date) {
            $date = '2000-01-01 00:00:00';
        }

        return str_replace(' ', 'T', $date);
    }

    /**
     * Расчитывает остатки для каждой детали на текущий момент времени.
     * @return array
     */
    public static function setBalances()
    {
        $errors = [];

        //Получаем все детали из списка тех специалиста
        $all_components = Nomenclature::find()
                ->andWhere(['nomenclature_type' => Users::USER_ROLE_TECH_SPECIALIST])
                ->all() ?? null;

        if (!$all_components) {
            return ['error' => 1, 'data' => 'Детали не найдены'];
        }

        /** @var Nomenclature $component Позиция номенклатуры (деталь) */
        foreach ($all_components as $component) {
            //Получаем последний остаток для детали
            $balance_id = Balance::find()->andWhere(['detail_id' => $component->id])->max('id') ?? null;

            if ($balance_id) {
                //Если баланс уже расчитывался

                //Расчитываем остатки на текущую дату
                $balance_model = Balance::findOne($balance_id);
                //Получаем информацию о реализациях за период (от даты последнего расчета остатков до текущего момента)
                $realizations = self::getRealizationInfo($component->id, $balance_model->date);
                $arrival = self::getArrivingInfo($component->id, $balance_model->date);

//                Yii::info($realizations, 'test');
//                Yii::info($arrival, 'test');

                $new_balance = new Balance();
                $new_balance->detail_id = $component->id;
                $new_balance->date = date('Y-m-d H:i:s', time());
                $new_balance->number = $balance_model->number - $realizations['num'] + $arrival['num'];
                $new_balance->total_amount = $balance_model->total_amount + $realizations['amount'] - $arrival['amount'];

//                Yii::info($new_balance->toArray(), 'test');

                if (!$new_balance->save()) {
                    Yii::error($new_balance->errors, '_error');
                    array_push($errors, $new_balance->errors);
                }
            } else {
                //Если баланс еще не расчитывался (например новая позиция в номенклатуре)

                //Получаем сумму всех закупукок для детали
                $total_purchase_amount = DetailToArriving::find()
                        ->andWhere(['detail_id' => $component->id])
                        ->sum('total_amount') ?? 0;

                //Получаем общее кол-во закупленной детали
                $total_purchase_number = DetailToArriving::find()
                        ->andWhere(['detail_id' => $component->id])
                        ->sum('number') ?? 0;

                //Получаем сумму всех продаж детали
                $total_sale_amount = Realization::find()
                        ->andWhere(['product_id' => $component->id])
                        ->sum('sum * amount') ?? 0;

                //Получаем количество проданных деталей
                $total_sale_number = Realization::find()
                        ->andWhere(['product_id' => $component->id])
                        ->sum('sum') ?? 0;

                //Сумма остатков. Сумма всех продаж минус сумма закупок
                $balance_amount = $total_sale_amount - $total_purchase_amount;
                //Кол-во остатков. Общее количество купленного минус количество проданного
                $balance_number = $total_purchase_number - $total_sale_number;

                $model = new Balance([
                    'detail_id' => $component->id,
                    'number' => $balance_number,
                    'total_amount' => $balance_amount,
                ]);

                if (!$model->save()) {
                    Yii::error($model->errors, '_error');
                    $errors[$model->detail_id] = $model->errors;
                }
            }
        }

        if (count($errors) > 0) {
            return ['error' => 1, 'data' => $errors];
        }
        return ['error' => 0, 'data' => 'success'];
    }

//    /**
//     * Получает кол-во проданных деталей за период
//     * @param Nomenclature $model
//     * @param string $start Начальная дата
//     * @param string $end Конечная дата
//     */
//    private static function getSalesProductNumber($model, $start, $end = null)
//    {
//        Yii::info('Start date: ' . $start, 'test');
//
//        if (!$end) {
//            $end = date('Y-m-d H:i:s', time());
//        }
//
//        //Получаем максимально близкую дату рачета остатков для детали к начальной дате отчета.
//        $near_date = Balance::find()
//                ->andWhere(['detail_id' => $model->id])
//                ->andWhere(['<', 'date', $start])
//                ->max('date') ?? null;
//
//        Yii::info('Near date: ' . $near_date, 'test');
//
//        /** @var double $last_number Количество деталей в последнем остатке */
//        $last_number = Balance::find()
//                ->andWhere(['date' => $near_date])
//                ->andWhere(['detail_id' => $model->id])
//                ->one()
//                ->number ?? 0;
//
//        //Расчитываем остатки количества на начало отчетного периода
////        $intermediate_balance_number =
//
//
//        Yii::info($last_number, 'test');
//
//        //Кол-во деталей за период
//        $detail_number = Realization::find()
//                ->andWhere(['product_id' => $model->id])
//                ->andWhere(['BETWEEN', 'date', $start, $end])
//                ->sum('sum') ?? 0;
//
//        Yii::info($detail_number, 'test');
//
//        $balance = round((double)$detail_number + (double)$last_number, 2);
//
//        self::getRealizationInfo($model->id, $start, $end);
//    }
//
//    /**
//     * Получает модели всех реализаций за период
//     * @param int $id ID детали/продукта
//     * @param string $start НАчальная дата
//     * @param string $end Конечная дата
//     * @return array|\yii\db\ActiveRecord[]
//     */
//    public static function getRealizationsProduct($id, $start, $end = null)
//    {
//        if (!$end) {
//            $end = date('Y-m-d H:i:s', time());
//        }
//        return Realization::find()
//            ->andWhere(['product_id' => $id])
//            ->andWhere(['BETWEEN', 'date', $start, $end])
//            ->all();
//    }

    /**
     * Получает кол-во и сумму всех реализаций для детали за период
     * @param int $id ID детали/продукта
     * @param string $start НАчальная дата
     * @param string $end Конечная дата
     * @return array ['num' => количество, 'amount' => сумма]
     */
    public static function getRealizationInfo($id, $start, $end = null)
    {
//        Yii::info('getRealizationInfo function', 'test');

        if (!$end) {
            $end = date('Y-m-d H:i:s', time());
        }

        $query = Realization::find()
            ->joinWith(['details det'])
            ->andWhere(['det.id' => $id])
            ->andWhere(['BETWEEN', 'date', $start, $end]);

        $query_num = clone $query;
        $num = $query_num->sum('number');

        $query_amount = clone $query;
        $amount = $query_amount->sum('total_amount');
//        Yii::debug('Реализация за период ' . $start . ' - ' . $end, 'test');
//        Yii::debug('Кол-во: ' . $num . '. Сумма: ' . $amount, 'test');

        return ['num' => round($num, 2), 'amount' => $amount];
    }

    /**
     * Получает кол-во и сумму всех поступлений для детали за период
     * @param int $id ID детали/продукта
     * @param string $start НАчальная дата
     * @param string $end Конечная дата
     * @return array ['num' => количество, 'amount' => сумма]
     */
    public static function getArrivingInfo($id, $start, $end = null)
    {
//        Yii::info('Рсчет поступления для ' . $id . ' за период с ' . $start . ' по ' . $end, 'test');

        if (!$end) {
            $end = date('Y-m-d H:i:s', time());
        }

        $query = DetailToArriving::find()
            ->joinWith(['arriving arr'])
            ->andWhere(['detail_to_arriving.detail_id' => $id])
            ->andWhere(['BETWEEN', 'arr.created_at', $start, $end])
            ->andWhere(['arr.status' => Arriving::STATUS_ACCEPTED])
            ->andWhere(['OR',
                ['detail_to_arriving.warehouse' => DetailToArriving::WAREHOUSE_BASIS],
                ['IS', 'detail_to_arriving.warehouse', null],
            ]);

        $query_num = clone $query;
        $num = $query_num->sum('detail_to_arriving.number');

        $query_amount = clone $query;
        $amount = $query_amount->sum('total_amount_rub');
//        Yii::debug('Поступления за период ' . $start . ' - ' . $end, 'test');
//        Yii::debug('Кол-во: ' . $num . ' Сумма: ' . $amount, 'test');

        return ['num' => round($num, 2), 'amount' => $amount];
    }

    /**
     * Получает остатки на определенную дату
     * @param int $id ID детали
     * @param string $date Дата
     * @return array
     */
    public static function getBalanceForDate($id, $date)
    {
        $date = date('Y-m-d 23:59:59', strtotime($date));
//        Yii::info('Расчет остатка на ' . $date, 'test');

        //Получаем максимально близкую дату рачета остатков для детали.
        $near_date = Balance::find()
                ->andWhere(['detail_id' => $id])
                ->andWhere(['<', 'date', $date])
                ->max('date') ?? null;

//        Yii::info('Ближайшая дата расчета остатков: ' . $near_date, 'test');

        if (!$near_date) {
//            Yii::debug('Остатки не расчитывались.', 'test');

            //Если расчет остатков не производился, считаем все раходы и доходы до указанной даты
            $arriving_info = Accounting::getArrivingInfo($id, '2000-01-01 00:00:00', $date);
            $realization_info = Accounting::getRealizationInfo($id, '2000-01-01 00:00:00', $date);

            return self::calculateBalance($arriving_info, $realization_info);
        } else {
            //Если расчет остатков найден в базе
            Yii::info('Остатки расчитывались.', 'test');

            //Берем остатки из базы
            /** @var Balance $balance */
            $balance = Balance::find()
                    ->andWhere(['detail_id' => $id])
                    ->andWhere(['date' => $near_date])
                    ->one() ?? null;

            //Расчитываем остатки за период (от даты последних остатков до указанной даты)
            $arriving_info = Accounting::getArrivingInfo($id, $balance->date, $date);
            $realization_info = Accounting::getRealizationInfo($id, $balance->date, $date);

            return self::calculateBalance($arriving_info, $realization_info);
        }


    }

    /**
     * Расчитывает  остатки из переданных значений
     * @param array $arriving Поступление ['num' => Количество, 'amount' => Сумма]
     * @param array $realization Реализация ['num' => Количество, 'amount' => Сумма]
     * @return array ['num' => Количество, 'amount' => Сумма]
     */
    private static function calculateBalance($arriving, $realization)
    {
//        Yii::info('calculateBalance function', 'test');
//        Yii::info($arriving, 'test');
//        Yii::info($realization, 'test');

        $num = $arriving['num'] - $realization['num'];
        $amount = $realization['amount'] - $arriving['amount'];

//        Yii::debug('Остаток: ' . $num . ' На сумму: ' . $amount, 'test');
//        Yii::debug('Amount: ' . $amount, 'test');

        return ['num' => $num, 'amount' => $amount];
    }

    /**
     * Экспорт отчета
     * @param Accounting $acc_model
     * @return string
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function reportExport($acc_model)
    {
        $acc_model->report_start_date = date('Y-m-d 00:00:00', strtotime($acc_model->report_start_date));

        if (!$acc_model->report_end_date) {
            $acc_model->report_end_date = date('Y-m-d 23:59:59', time());
        } else {
            $acc_model->report_end_date = date('Y-m-d 23:59:59', strtotime($acc_model->report_end_date));
        }

        $query = Nomenclature::find()
            ->joinWith(['arriving arr'])
            ->joinWith(['sales sal'])
            ->select([
                'nomenclature.id',
                'nomenclature.name',
                'nomenclature.measure_id',
                'nomenclature.own_vendor_code'
            ])
            ->andWhere(['nomenclature.nomenclature_type' => Users::USER_ROLE_TECH_SPECIALIST])
            ->distinct();

        if ($acc_model->detail_id) {
            $query->andWhere(['nomenclature.id' => $acc_model->detail_id]);
        }
        if ($acc_model->own_vendor_code) {
            $query->andWhere(['nomenclature.id' => $acc_model->own_vendor_code]);
        }
        $query->andFilterWhere(['nomenclature.brand_id' => $acc_model->brand_id]);
        $query->andFilterWhere(['nomenclature.type_of_parts_id' => $acc_model->type_of_parts_id]);
        $query->andFilterWhere(['nomenclature.group_id' => $acc_model->nomenclature_group_id]);
        if ($acc_model->detail_id || $acc_model->own_vendor_code) {
            $details = $query
                ->andWhere([
                    'OR',
                    [
                        'BETWEEN',
                        'sal.date',
                        $acc_model->report_start_date,
                        $acc_model->report_end_date
                    ],
                    [
                        'BETWEEN',
                        'arr.created_at',
                        $acc_model->report_start_date,
                        $acc_model->report_end_date
                    ]
                ]);
        } else {
            $details = $query;
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $row_counter = 1;
        $detail_counter = 1;

        $headers = [
            'A' => '#',
            'B' => 'Артикул',
            'C' => 'Наименование детали',
            'D' => 'Начальный остаток',
            'E' => 'Приход',
            'F' => 'Расход',
            'G' => 'Конечный остаток'
        ];

        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ],
        ];


        foreach ($headers as $column => $header) {
            $sheet->getStyle($column . $row_counter)->applyFromArray($styleArray);
            $sheet->getStyle($column . $row_counter)->getAlignment()->setWrapText(true);
            $sheet->getColumnDimension($column)->setAutoSize(true);
            $sheet->setCellValue($column . $row_counter, $header);
        }

        $row_counter += 1;

        //проходисмся по всем деталям
        /** @var Nomenclature $detail */
        foreach ($details->each() as $detail) {
            $measure = $detail->measure ?? null;
            if ($measure) {
                $detail_measure = ' ' . $detail->measure->name ?? null;
            } else {
                $detail_measure = '';
            }
            $start_balance = Accounting::getBalanceForDate($detail->id,
                date('Y-m-d 23:59:59',
                    strtotime($acc_model->report_start_date) - (24 * 60 * 60)));
            $dta = DetailToArriving::getSum($detail->id, $acc_model->report_start_date,
                $acc_model->report_end_date);
            $realization = Realization::getRealization($detail->id,
                $acc_model->report_start_date,
                $acc_model->report_end_date);
//            $end_balance = Accounting::getBalanceForDate($detail->id,
//                $acc_model->report_end_date);

            $amounts = '';

            if (isset($dta['by_currency'])) {
                /** @var DetailToArriving $currency */
                foreach ($dta['by_currency'] as $cur_key => $amount) {
                    $amounts .= $amount . PHP_EOL;
                }
            }

            $calculated_end_balance['num'] = $start_balance['num'] + $dta['sum'] - $realization['num'];
            if ($calculated_end_balance['num'] == 0 && $acc_model->remains_type == $acc_model::REMAINS_TYPE_NOT_ZERO) {
                //Если баланс нулевой и тип отчета без нулевых остатков
                continue;
            }
            $calculated_end_balance['amount'] = $start_balance['amount'] + $dta['total_amount'] - $realization['amount'];

            $sheet->getStyle('D' . $row_counter)->getAlignment()->setWrapText(true);
            $sheet->getStyle('E' . $row_counter)->getAlignment()->setWrapText(true);
            $sheet->getStyle('F' . $row_counter)->getAlignment()->setWrapText(true);
            $sheet->getStyle('G' . $row_counter)->getAlignment()->setWrapText(true);

            $sheet->setCellValue('A' . $row_counter, $detail_counter);
            $sheet->setCellValue('B' . $row_counter, $detail->own_vendor_code);
            $sheet->setCellValue('C' . $row_counter, $detail->name);
            $sheet->setCellValue('D' . $row_counter,
                $start_balance['num'] . $detail_measure);
            $sheet->setCellValue('E' . $row_counter,
                $dta['sum'] . $detail_measure . PHP_EOL . $amounts);
            $sheet->setCellValue('F' . $row_counter,
                $realization['num'] . $detail_measure);
            $sheet->setCellValue('G' . $row_counter,
                $calculated_end_balance['num'] . $detail_measure);

            $detail_counter += 1;
            $row_counter += 1;
        }

        $writer = new Xlsx($spreadsheet);
        $dir = 'uploads/export/report';
        $file = $dir . '/Отчет_' . date('d.m.Y_H_i', time()) . '.xlsx';

        Yii::info($file, 'test');

        if (!is_dir($dir)) {
            try {
                mkdir($dir);
            } catch (Exception $e) {
                Yii::error($e->getMessage(), '_error');
            }
        }

        $writer->save($file);

        return $file;
    }

    /**
     * Построение запроса на выборку в зависимости от параметров модели
     * @return \yii\db\ActiveQuery
     */
    public function buildQuery(): ActiveQuery
    {
        $query = Nomenclature::find()
            ->joinWith(['arriving arr', 'sales sal'])
            ->select([
                'nomenclature.id',
                'nomenclature.name',
                'nomenclature.measure_id',
                'nomenclature.own_vendor_code',
                'detail_to_arriving.warehouse'
            ])
            ->andWhere(['nomenclature.nomenclature_type' => Users::USER_ROLE_TECH_SPECIALIST])
            ->distinct();

        $query->andFilterWhere(['nomenclature.id' => $this->detail_id]);
        $query->andFilterWhere(['nomenclature.own_vendor_code' => $this->own_vendor_code]);
        $query->andFilterWhere(['nomenclature.brand_id' => $this->brand_id]);
        $query->andFilterWhere(['nomenclature.type_of_parts_id' => $this->type_of_parts_id]);
        $query->andFilterWhere(['nomenclature.group_id' => $this->nomenclature_group_id]);

        switch ($this->warehouse) {
            case DetailToArriving::WAREHOUSE_ON_THE_WAY_TO_WAREHOUSE:
            case DetailToArriving::WAREHOUSE_ON_THE_WAY:
                $query->andWhere(['detail_to_arriving.warehouse' => $this->warehouse]);
                break;
            case DetailToArriving::WAREHOUSE_BASIS:
                $query->andWhere([
                    'OR',
                    ['detail_to_arriving.warehouse' => $this->warehouse],
                    ['IS', 'detail_to_arriving.warehouse', null],
                ]);
                break;
            default:
                //Если склад не указан
                $query->orderBy(['detail_to_arriving.warehouse' => SORT_ASC]);
                break;
        }

        $query->andWhere(['<=', 'arr.created_at', $this->report_end_date]);

        return $query;
    }
}
