<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;

/**
 * ArrivingSearch represents the model behind the search form about `app\models\Arriving`.
 */
class ArrivingSearch extends Arriving
{
    public $contractor_name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'contractor_id', 'status'], 'integer'],
            [['created_at', 'contractor_name', 'number'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Arriving::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'contractor_id' => $this->contractor_id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'number', $this->number]);

        $query->joinWith(['contractor' => function (Query $q) {
            $q->where(Contractor::tableName() . '.name LIKE "%' . $this->contractor_name . '%" OR ' . Contractor::tableName() . '.full_name LIKE "%' . $this->contractor_name . '%"');
        }]);

        return $dataProvider;
    }
}
