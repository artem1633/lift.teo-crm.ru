<?php

namespace app\models;

use app\models\query\MovementPartQuery;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "movement_part".
 *
 * @property int $id
 * @property int|null $nomenclature_id
 * @property int|null $arriving_id
 * @property int|null $warehouse_from Со склада
 * @property int|null $warehouse_to На склад
 * @property string|null $date
 * @property float|null $num Кол-во
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Arriving $arriving
 * @property Nomenclature $nomenclature
 */
class MovementPart extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'movement_part';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['nomenclature_id', 'arriving_id', 'warehouse_from', 'warehouse_to'], 'integer'],
            [['date', 'created_at', 'updated_at'], 'safe'],
            [['num'], 'number'],
            [['arriving_id'], 'exist', 'skipOnError' => true, 'targetClass' => Arriving::className(), 'targetAttribute' => ['arriving_id' => 'id']],
            [['nomenclature_id'], 'exist', 'skipOnError' => true, 'targetClass' => Nomenclature::className(), 'targetAttribute' => ['nomenclature_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'nomenclature_id' => 'Nomenclature ID',
            'arriving_id' => 'Arriving ID',
            'warehouse_from' => 'Со склада',
            'warehouse_to' => 'На склад',
            'date' => 'Date',
            'num' => 'Кол-во',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Arriving]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getArriving(): ActiveQuery
    {
        return $this->hasOne(Arriving::class, ['id' => 'arriving_id']);
    }

    /**
     * Gets query for [[Nomenclature]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNomenclature(): ActiveQuery
    {
        return $this->hasOne(Nomenclature::class, ['id' => 'nomenclature_id']);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\MovementPartQuery the active query used by this AR class.
     */
    public static function find(): query\MovementPartQuery
    {
        return new MovementPartQuery(get_called_class());
    }

    /**
     * Остаток на дату (на начало дня) для склада
     * @param int $detail_id
     * @param int $warehouse
     * @param string $date
     * @return float
     */
    public static function getBalanceByWarehouseOnDate(int $detail_id, int $warehouse, string $date): float
    {
        $from = MovementPart::find()
            ->fromWarehouse($warehouse)
            ->andWhere(['<', 'date', $date])
            ->andWhere(['nomenclature_id' => $detail_id])
            ->sum('num');
        $to = MovementPart::find()
            ->toWarehouse($warehouse)
            ->andWhere(['<', 'date', $date])
            ->andWhere(['nomenclature_id' => $detail_id])
            ->sum('num');

            Yii::debug($from, 'from getBalanceByWarehouseOnDate');
            Yii::debug($to, 'to getBalanceByWarehouseOnDate');

        return round($to - $from, 2);
    }

    /**
     * Приход на склад за период
     * @param int $detail_id
     * @param int $warehouse
     * @param string $start
     * @param string $end
     * @return float
     */
    public static function getArrivingForPeriod(int $detail_id, int $warehouse, string $start, string $end): float
    {
        return self::find()
            ->andWhere(['warehouse_to' => $warehouse, 'nomenclature_id' => $detail_id])
            ->between($start, $end)
            ->sum('num') ?? 0;
    }

    /**
     * Списание со склада за период
     * @param int $detail_id
     * @param int $warehouse
     * @param string $start
     * @param string $end
     * @return float
     */
    public static function getWriteOffForPeriod(int $detail_id, int $warehouse, string $start, string $end): float
    {
        return self::find()
            ->andWhere(['warehouse_from' => $warehouse, 'nomenclature_id' => $detail_id])
            ->between($start, $end)
            ->sum('num') ?? 0;
    }
}
