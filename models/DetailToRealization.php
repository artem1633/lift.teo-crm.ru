<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "detail_to_realization".
 *
 * @property int $id
 * @property int $realization_id Реализация
 * @property int $detail_id Деталь
 * @property double $number Количество
 * @property double $price Цена за единицу товара
 * @property double $total_amount Общая сумма (включая НДС)
 *
 * @property Nomenclature $detail
 * @property Realization $realization
 */
class DetailToRealization extends ActiveRecord
{
    public $vendor_code; //Артикул

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'detail_to_realization';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['realization_id', 'detail_id'], 'integer'],
            [['number', 'price', 'total_amount'], 'number'],
            [
                ['detail_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Nomenclature::className(),
                'targetAttribute' => ['detail_id' => 'id']
            ],
            [
                ['realization_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Realization::className(),
                'targetAttribute' => ['realization_id' => 'id']
            ],
            ['vendor_code', 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'realization_id' => 'Реализация',
            'detail_id' => 'Деталь',
            'number' => 'Количество',
            'price' => 'Цена',
            'total_amount' => 'Общая сумма (включая НДС)',
            'vendor_code' => 'Артикул',
        ];
    }

    public function beforeSave($insert)
    {
        $this->price = round($this->total_amount / $this->number, 2);
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        /** @var Nomenclature $detail */
        $detail = Nomenclature::findOne($this->detail_id);

        if ($insert) {
            $detail->fact_balance -= $this->number;
        } else {
            //Прибавляем старое значение расхода и вычитаем новое
            $detail->fact_balance = $detail->fact_balance + $changedAttributes['number'] - $this->number;
        }
        if (!$detail->save()) {
            \Yii::error($detail->errors);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetail()
    {
        return $this->hasOne(Nomenclature::className(), ['id' => 'detail_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealization()
    {
        return $this->hasOne(Realization::className(), ['id' => 'realization_id']);
    }
}
