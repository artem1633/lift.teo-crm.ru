<?php

namespace app\models;

use app\models\query\ReserveQuery;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "reserve".
 *
 * @property int $id
 * @property int|null $order_part_id
 * @property int|null $logistic_request_part_id
 * @property int|null $nomenclature_id
 * @property int|null $count Количество
 * @property string|null $created_at
 * @property int|null $created_by
 *
 * @property Users $createdBy
 * @property LogisticRequest $logisticRequest
 * @property \app\models\LogisticRequestPart $logisticRequestPart
 * @property Nomenclature $nomenclature
 * @property \app\models\OrderPart $orderPart
 * @property Request $request
 */
class Reserve extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'reserve';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['order_part_id', 'logistic_request_part_id', 'nomenclature_id', 'count', 'created_by'], 'integer'],
            [['created_at'], 'safe'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['created_by' => 'id']],
            [['logistic_request_part_id'], 'exist', 'skipOnError' => true, 'targetClass' => LogisticRequestPart::class, 'targetAttribute' => ['logistic_request_part_id' => 'id']],
            [['nomenclature_id'], 'exist', 'skipOnError' => true, 'targetClass' => Nomenclature::class, 'targetAttribute' => ['nomenclature_id' => 'id']],
            [['order_part_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderPart::class, 'targetAttribute' => ['order_part_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'order_part_id' => 'Деталь заявки',
            'logistic_request_part_id' => 'Деталь счета',
            'nomenclature_id' => 'Nomenclature ID',
            'count' => 'Количество',
            'created_at' => 'Создан',
            'created_by' => 'Кем создан',
        ];
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy(): ActiveQuery
    {
        return $this->hasOne(Users::class, ['id' => 'created_by']);
    }

    /**
     * Gets query for [[LogisticRequest]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLogisticRequestPart(): ActiveQuery
    {
        return $this->hasOne(LogisticRequestPart::class, ['id' => 'logistic_request_part_id']);
    }

    /**
     * Gets query for [[LogisticRequest]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLogisticRequest(): ActiveQuery
    {
        return $this->hasOne(LogisticRequest::class, ['id' => 'logistic_request_id'])
            ->via('logisticRequestPart');
    }

    /**
     * Gets query for [[Nomenclature]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNomenclature(): ActiveQuery
    {
        return $this->hasOne(Nomenclature::class, ['id' => 'nomenclature_id']);
    }

    /**
     * Gets query for [[Request]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrderPart(): ActiveQuery
    {
        return $this->hasOne(OrderPart::class, ['id' => 'order_part_id']);
    }

    /**
     * Заявка для резерва
     * @return \yii\db\ActiveQuery
     */
    public function getRequest(): ActiveQuery
    {
        return $this->hasOne(Request::class, ['id' => 'request_id'])
            ->via('orderPart');
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\ReserveQuery the active query used by this AR class.
     */
    public static function find(): query\ReserveQuery
    {
        return new ReserveQuery(get_called_class());
    }

    /**
     * Проверка резервов менеджеров на истечение срока в три дня (ТЗ от марта 2022г.)
     * @return string
     */
    public static function check(): string
    {
        $min_date = date('Y-m-d H:i:s', time() - (3600 * 24 * 3));
        $overdue_reserve_ids = Reserve::find()
            ->select(['id'])
            ->andWhere(['IS NOT', 'order_part_id', null])
            ->andWhere(['<=', 'created_at', $min_date])
            ->column();
        Yii::debug($overdue_reserve_ids, 'checkReserve');
        //Удаляем найденное
        Reserve::deleteAll(['IN', 'id', $overdue_reserve_ids]);

        return 'Проверка завершена. Удалено из резерва: ' . count($overdue_reserve_ids);
    }

    /**
     * Возвращает все резервы для детали
     * @param int|null $id
     * @return int
     */
    public static function getTotalReservesCountByDetail($id = null): int
    {
        if (!$id) return 0;

        $reserve = Reserve::find()
            ->select(['SUM(count)'])
            ->andWhere(['nomenclature_id' => $id])
            ->scalar();
        if (!$reserve) return 0;
        return $reserve;
    }

    public static function getReservesByDetail(int $id): array
    {
        return Reserve::findAll(['nomenclature_id' => $id]);
    }
}
