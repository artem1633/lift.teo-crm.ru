<?php

namespace app\models\query;

/**
 * This is the ActiveQuery class for [[\app\models\Reserve]].
 *
 * @see \app\models\Reserve
 */
class ReserveQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * Резерв для счета
     * @param int $id Идентификатор счета
     * @return \app\models\query\ReserveQuery
     */
    public function forLogisticRequestPart(int $id): ReserveQuery
    {
        return $this->andWhere(['logistic_request_part_id' => $id]);
    }

    /**
     * Резерв для детали
     * @param int $id Идентификатор детали
     * @return \app\models\query\ReserveQuery
     */
    public function forNomenclature(int $id): ReserveQuery
    {
        return $this->andWhere(['nomenclature_id' => $id]);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\Reserve[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return array|\yii\db\ActiveRecord|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
