<?php

namespace app\models\query;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\app\models\LogisticRequest]].
 *
 * @see \app\models\LogisticRequest
 */
class LogisticRequestQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \app\models\LogisticRequest[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\LogisticRequest|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
