<?php

namespace app\models\query;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\app\models\MovementPart]].
 *
 * @see \app\models\MovementPart
 */
class MovementPartQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/
    /**
     * Поступления на склад
     * @param integer $warehouse Склад
     * @return \app\models\query\MovementPartQuery
     */
    public function toWarehouse(int $warehouse): MovementPartQuery
    {
        return $this->andWhere(['warehouse_to' => $warehouse]);
    }

    /**
     * Списания со склада
     * @param integer $warehouse Склад
     * @return \app\models\query\MovementPartQuery
     */
    public function fromWarehouse(int $warehouse): MovementPartQuery
    {
        return $this->andWhere(['warehouse_from' => $warehouse]);
    }

    /**
     * Движение деталей за период
     * @param string $start
     * @param string $end
     * @return \app\models\query\MovementPartQuery
     */
    public function between(string $start, string  $end): MovementPartQuery
    {
        $start = date('Y-m-d', strtotime($start));
        $end = date('Y-m-d', strtotime($end));

        return $this->andWhere(['BETWEEN', 'date', $start, $end]);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\MovementPart[]|array
     */
    public function all($db = null): array
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return array|\yii\db\ActiveRecord|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
