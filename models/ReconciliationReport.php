<?php

namespace app\models;


use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Формирование данных для Акта сверки
 * @property integer $contractor_id Идентификатор контрагента
 * @property string $report_start_date Идентификатор контрагента
 * @property string $report_end_date Идентификатор контрагента
 * @property int $juridical_person_id Идентификатор юр лица
 */
class ReconciliationReport extends Model
{
    public $contractor_id;
    public $report_start_date;
    public $report_end_date;
    public $juridical_person_id;

    public function rules()
    {
        return [
            [['contractor_id', 'juridical_person_id'], 'integer'],
            [['report_start_date', 'report_start_date'], 'safe'],
        ];
    }

    /**
     * Данные для акта сверки
     * @return array
     */
    public function getDataReport()
    {
        $this->report_start_date = date('Y-m-d 00:00:00', strtotime($this->report_start_date));
        $this->report_end_date = date('Y-m-d 23:59:59', strtotime($this->report_end_date));

        $arriving = $this->getArriving();
        $payments = $this->getPayments();

        $result = $this->sortByDate($arriving, $payments);

        $start_balance = $this->getStartBalance();

        Yii::info($result, 'test');

        return [
            'start_balance' => $start_balance,
            'data' => $result,
        ];
    }

    /**
     * Объединение массивов и сортировка по дате
     * @param $arr1
     * @param $arr2
     * @return array
     */
    private function sortByDate($arr1, $arr2)
    {
        $arr = ArrayHelper::merge($arr1, $arr2);
        $prev_item = null;
        $arr1 = $arr;
        foreach ($arr1 as $key => $item) {
            if ($prev_item) {
                $prev_date = strtotime($prev_item['date']);
                $cur_date = strtotime($item['date']);

                if ($prev_date > $cur_date) {
                    $arr[$key - 1] = $item;
                    $arr[$key] = $prev_item;
                } else {
                    $prev_item = $item;
                }
            } else {
                $prev_item = $item;
            }
            array_values($arr);
        }

        return $arr;
    }

    /**
     * Получает оплаты за период
     * @param null $start
     * @param null $end
     * @return array|\yii\db\ActiveRecord[]
     */
    private function getArriving($start = null, $end = null)
    {
        if (!$start) {
            $start = $this->report_start_date;
        }

        if (!$end) {
            $end = $this->report_end_date;
        }

        $contractors = null;
        if (!$this->contractor_id && $this->juridical_person_id) {
            $contractors = Contractor::find()
                ->joinWith(['juridicalPerson'])
                ->select(['contractor.id'])
                ->andWhere(['juridical_person.id' => $this->juridical_person_id])
                ->asArray()
                ->all();
        }

        $arriving = Arriving::find()
            ->joinWith(['details'])
            ->joinWith(['juridicalPerson'])
            ->select([
                'DATE_FORMAT(arriving.created_at, "%d.%m.%Y") as date',
                'SUM(detail_to_arriving.total_amount_currency) as sum',
                'detail_to_arriving.currency_id',
                'arriving.id',
                'arriving.number'
            ])
            ->andWhere(['BETWEEN', 'arriving.created_at', $start, $end])
            ->andWhere(['arriving.contractor_id' => $this->contractor_id])
            ->andFilterWhere(['IN', 'arriving.contractor_id', $contractors])
            ->groupBy('arriving_id')
            ->orderBy('arriving.created_at')
            ->asArray()
            ->all();

        $currencies = ArrayHelper::map(Currency::find()->all(), 'id', 'icon');

        for ($i = 0; $i < count($arriving); $i++) {
            $new_key = 'icon';
            $new_val = '';
            $cur_id = $arriving[$i]['currency_id'];
            if ($cur_id) {
                $new_val = $currencies[$cur_id];
                unset ($arriving[$i]['currency_id']);
            }
            $arriving[$i][$new_key] = $new_val;
            $arriving[$i]['type'] = 'arriving';
            unset($arriving[$i]['details']);
        }

        return $arriving;
    }

    private function getPayments($start = null, $end = null)
    {
        if (!$start) {
            $start = $this->report_start_date;
        }

        if (!$end) {
            $end = $this->report_end_date;
        }
//        $payments = Payment::find()
//            ->joinWith(['currency'])
//            ->select(['DATE_FORMAT(payment.pay_date, "%d.%m.%Y") as date', 'sum', '_currency.icon', 'payment.id'])
//            ->andWhere(['payment.contractor_id' => $this->contractor_id])
//            ->andWhere(['BETWEEN', 'payment.pay_date', $start, $end])
//            ->orderBy('payment.pay_date')
//            ->asArray()
//            ->all();

        $payments_query = Payment::find()
            ->andWhere(['contractor_id' => $this->contractor_id])
            ->andWhere(['BETWEEN', 'pay_date', $start, $end])
            ->orderBy('pay_date');

        $currencies = ArrayHelper::map(Currency::find()->all(), 'id', 'icon');

        $payments = [];
        /** @var Payment $payment */
        foreach ($payments_query->each() as $payment) {
            $payments[] = [
                'id' => $payment->id,
                'date' => date('d.m.Y', strtotime($payment->pay_date)),
                'sum' => $payment->sum,
                'number' => $payment->id,
                'icon' => $currencies[$payment->currency_id],
            ];
            Yii::info($payment->attributes, 'test');
        }

        Yii::info($payments, 'test');
        return $payments;
    }

    private function getStartBalance()
    {
        $start_date = '2000-01-01 00:00:00';
        $end_date = $this->report_start_date;

        $arriving = $this->getArriving($start_date, $end_date);
        $payments = $this->getPayments($start_date, $end_date);

        $total_sum = 0;

        foreach ($arriving as $item) {
            $total_sum += $item['sum'];
        }

        foreach ($payments as $item) {
            $total_sum -= $item['sum'];
        }
        return round($total_sum, 2);
    }
}