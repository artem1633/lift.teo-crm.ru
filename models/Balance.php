<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "balance".
 *
 * @property int $id
 * @property int $detail_id Деталь
 * @property string $date Дата на которую расчитываются остатки
 * @property double $number Количество
 * @property double $total_amount Общая суммма
 *
 * @property Nomenclature $detail
 */
class Balance extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'balance';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['detail_id'], 'integer'],
            [['date'], 'safe'],
            [['number', 'total_amount'], 'number'],
            [['detail_id'], 'exist', 'skipOnError' => true, 'targetClass' => Nomenclature::className(), 'targetAttribute' => ['detail_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'detail_id' => 'Деталь',
            'date' => 'Дата на которую расчитываются остатки',
            'number' => 'Количество',
            'total_amount' => 'Общая суммма',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetail()
    {
        return $this->hasOne(Nomenclature::className(), ['id' => 'detail_id']);
    }
}
