<?php

namespace app\models;

use app\models\query\LogisticRequestQuery;
use kartik\select2\Select2;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "logistic_request".
 *
 * @property int $id
 * @property int $payee_id Получатель платежа
 * @property string $account_number Номер счета
 * @property int $contractor_id Контрагент
 * @property int $responsible_id Ответственный
 * @property int $logistic_master_status_id Статус
 * @property int $logistic_status_id Статус логиста
 * @property string $invoice_date Дата счета
 * @property string $payed_date Дата оплаты
 * @property string $created_by Создатель счета
 * @property string $created_at Дата создания
 * @property integer $juridical_person_id Идентификатор юр. лица
 * @property string $invoices Инвойсы из позиций счета
 * @property string $requests_numbers Номера заявок в счете
 *
 * @property Contractor $contractor
 * @property LogisticMasterStatus $logisticMasterStatus
 * @property LogisticStatus $logisticStatus
 * @property Payee $payee
 * @property Users $responsible
 * @property LogisticRequestPart[] $logisticRequestParts
 * @property Users $creator
 * @property Payee $payee_name
 * @property Contractor $contractor_name
 * @property Users $responsible_name
 * @property JuridicalPerson $juridicalPerson
 * @property \app\models\Nomenclature[] $details
 */
class LogisticRequest extends ActiveRecord
{
    /** @var string */
    public $payee_name;

    /** @var string */
    public $contractor_name;

    /** @var string */
    public $responsible_name;

    /** @var string */
    public $juridical_person_name;

    /** @var string */
    public $invoices;

    /** @var string */
    public $requests_numbers;

    public $client_ship_date;

    public $count;

    public $number;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'logistic_request';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'payee_id',
                    'contractor_id',
                    'responsible_id',
                    'logistic_master_status_id',
                    'logistic_status_id',
                    'created_by',
                    'juridical_person_id',
                ],
                'integer'
            ],
            [['invoice_date', 'payed_date'], 'safe'],
            [['account_number'], 'string', 'max' => 255],
            [
                ['contractor_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Contractor::className(),
                'targetAttribute' => ['contractor_id' => 'id']
            ],
            [
                ['logistic_master_status_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => LogisticMasterStatus::className(),
                'targetAttribute' => ['logistic_master_status_id' => 'id']
            ],
            [
                ['logistic_status_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => LogisticStatus::className(),
                'targetAttribute' => ['logistic_status_id' => 'id']
            ],
            [
                ['payee_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Payee::className(),
                'targetAttribute' => ['payee_id' => 'id']
            ],
            [
                ['responsible_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Users::className(),
                'targetAttribute' => ['responsible_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'payee_id' => 'Получатель платежа',
            'account_number' => 'Номер счета',
            'contractor_id' => 'Контрагент',
            'responsible_id' => 'Ответственный',
            'logistic_master_status_id' => 'Статус',
            'logistic_status_id' => 'Статус логиста',
            'invoice_date' => 'Дата счета',
            'payed_date' => 'Дата оплаты',
            'created_by' => 'Счет создал',
            'payee_name' => 'Получатель платежа',
            'contractor_name' => 'Контрагент',
            'responsible_name' => 'Ответственный',
            'juridical_person_id' => 'Юр. лицо',
            'juridical_person_name' => 'Юр. лицо',
            'requests_numbers' => 'Заявки',
            'client_ship_date' => 'Дата отгрузки',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->invoice_date) {
            $this->invoice_date = date('Y-m-d 00:00:00', strtotime($this->invoice_date));
        }
        if ($this->payed_date) {
            $this->payed_date = date('Y-m-d 00:00:00', strtotime($this->payed_date));
        }

        if ($insert){
            $this->created_by = Yii::$app->user->id;
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'contractor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(Users::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogisticMasterStatus()
    {
        return $this->hasOne(LogisticMasterStatus::className(), ['id' => 'logistic_master_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogisticStatus()
    {
        return $this->hasOne(LogisticStatus::className(), ['id' => 'logistic_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayee()
    {
        return $this->hasOne(Payee::className(), ['id' => 'payee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResponsible()
    {
        return $this->hasOne(Users::className(), ['id' => 'responsible_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogisticRequestParts()
    {
        return $this->hasMany(LogisticRequestPart::className(), ['logistic_request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJuridicalPerson()
    {
        return $this->hasOne(JuridicalPerson::className(), ['id' => 'juridical_person_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getArriving()
    {
        return $this->hasOne(Arriving::className(), ['id' => 'arriving_id'])
            ->viaTable('logistic_request_part', ['logistic_request_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return LogisticRequestQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LogisticRequestQuery(get_called_class());
    }

    /**
     * Формирует строку с доп информацией по счету (в шапке таблицы)
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getAdditionalInfo()
    {
        return '<p style="font-size: 1rem;">Контрагент: '
            . Functions::getContractor($this->contractor_id) . ' | '
            . Users::getRoleDescriptionByRoleName($this->creator->permission ?? null) . ': '
            . Functions::getCreatedBy($this->created_by) . ' | '
            . 'Дата создания: ' . Yii::$app->formatter->asDatetime($this->created_at)
//            . ' | '
//            . 'Дата изменения: ' . Yii::$app->formatter->asDatetime($this->updated_date)// date('d.m.Y H:i', strtotime($model->updated_date))
            . '</p>';
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getHeaderTableButtons()
    {
        return
            '<div class="btn-group" style="display: flex;">' .
            Html::a('<i class="fa fa-chevron-circle-left">&nbsp;Назад</i>',
                ['/logistic-request'],
                [
                    'title' => 'Назад',
                    'class' => 'btn btn-default',
                    'data-pjax' => 0,
                    'style' => 'background-color: #3c8dbc; color: white;'
                ]) .
            Html::a('<i class="fa fa-plus">&nbsp;Добавить позицию</i>',
                ['/logistic-request-part/create', 'logistic_request_id' => $this->id],
                [
                    'role' => 'modal-remote',
                    'title' => 'Добавить Деталь',
                    'class' => 'btn btn-default',
                    'style' => 'background-color:#00a65a;color:white'
                ]) .

            Html::a('<i class="fa fa-edit">&nbsp;Редактировать Счет</i>',
                ['/logistic-request/update', 'id' => $this->id],
                [
                    'role' => 'modal-remote',
                    'title' => 'Редактировать Счет',
                    'class' => 'btn btn-default',
                    'style' => 'background-color:#337ab7;color:white'
                ]) .
            Html::a('<i class="fa fa-remove">&nbsp;Удалить Счет</i>', ['/logistic-request/delete?id=' . $this->id],
                [
                    'data-pjax' => 0,
                    'data-method' => 'post',
                    'title' => 'Удалить Счет',
                    'class' => 'btn btn-default',
                    'style' => 'background-color: #dd4b39; color: white;',
                ]) .
            Select2::widget([
                'name' => 'main_status_dropdown',
                'data' => (new LogisticMasterStatus())->getList(),
                'value' => $this->logistic_master_status_id,
                'theme' => Select2::THEME_BOOTSTRAP,
                'hideSearch' => true,
                'size' => 'md',
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'options' => [
                    'placeholder' => 'Выберите статус',
                    'multiple' => false,
                    'onchange' => '$.post(
                         "' . Url::toRoute('/logistic-request/change-logistic-master-status') . '", 
                            {
                                status_id: $(this).val(),
                                model_id: ' . $this->id . ',
                            },
                            )
                            .done(function( color ) {
                                //console.log( "Status Data Loaded: " + color );
                                 var selector = $("#select2-w0-container");
                                 var parent_selector = $("#select2-w0-container").parent(".select2-selection");
                                 parent_selector.css("background-color", color);
                                  if (color === ""){
                                     selector.css("color", "black");
                                } else {
                                     selector.css("color", "white");
                                }
                              });'
                ],
            ]) .
            Select2::widget([
                'name' => 'additional_status_dropdown',
                'data' => (new LogisticStatus())->getList(),
                'value' => $this->logistic_status_id,
                'theme' => Select2::THEME_BOOTSTRAP,
                'hideSearch' => true,
                'size' => 'md',
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'options' => [
                    'placeholder' => 'Выберите статус',
                    'multiple' => false,
                    'onchange' => '$.post(
                                     "' . Url::toRoute('/logistic-request/change-logistic-status') . '", 
                                        {
                                            status_id: $(this).val(),
                                            model_id: ' . $this->id . '
    
                                        },
                                        ).done(function( color ) {
                                             var selector = $("#select2-w1-container");
                                             var parent_selector = $("#select2-w1-container").parent(".select2-selection");
                                             parent_selector.css("background-color", color);
                                              if (color === ""){
                                                 selector.css("color", "black");
                                            } else {
                                                 selector.css("color", "white");
                                            }
                                          });'
                ],
            ]) .
            '</div>' .
            '{toggleData}';
    }

    /**
     * Получает список инвойсов из позиций счета
     */
    public function getInvoices()
    {
        $positions = LogisticRequestPart::find()
            ->andWhere(['logistic_request_id' => $this->id])
            ->all();
        $invoices = [];
        /** @var LogisticRequestPart $position */
        foreach ($positions as $position){
            $invoice_number  = $position->arriving->number ?? null;
            if ($invoice_number){
                array_push($invoices, $invoice_number);
            }
        }

        return implode(PHP_EOL, $invoices);
    }

    /**
     * Резервы для счета
     * @return \yii\db\ActiveQuery
     */
    public function getReserves(): ActiveQuery
    {
        return $this->hasMany(Reserve::class, ['logistic_request_part_id' => 'id'])
            ->via('logisticRequestParts');
    }

    /**
     * @param int $id Идентификатор детали из номенклатуры
     * @return array
     */
    public static function getReserveForNomenclature(int $id): array
    {
        $part = Nomenclature::findOne($id);

        if (!$part) return [];

        return self::find()
            ->joinWith(['reserves'])
            ->select(['logistic_request.id', 'logistic_request.account_number', 'reserve.count'])
            ->andWhere(['reserve.nomenclature_id' => $part->id])
            ->andWhere(['IS NOT', 'reserve.logistic_request_part_id', null])
            ->all();
    }

    /**
     * Детали для Логистического счета
     * @return \yii\db\ActiveQuery
     */
    public function getDetails(): ActiveQuery
    {
        return $this->hasMany(Nomenclature::class, ['id' => 'nomenclature_id'])
            ->via('logisticRequestParts');
    }
}
