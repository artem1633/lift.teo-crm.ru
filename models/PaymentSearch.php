<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Payment;
use yii\db\Query;

/**
 * PaymentSearch represents the model behind the search form about `app\models\Payment`.
 */
class PaymentSearch extends Payment
{
    public $contractor_name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'contractor_id', 'currency_id'], 'integer'],
            [['pay_date', 'created_at'], 'safe'],
            [['sum'], 'number'],
            [['contractor_name'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Payment::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'contractor_id' => $this->contractor_id,
            'pay_date' => $this->pay_date,
            'sum' => $this->sum,
            'currency_id' => $this->currency_id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'pay_date', $this->pay_date]);

        if ($this->contractor_name){
            $query->joinWith(['contractor' => function (Query $q) {
                $q->where(Contractor::tableName() . '.name LIKE "%' . $this->contractor_name . '%" OR ' . Contractor::tableName() . '.full_name LIKE "%' . $this->contractor_name . '%"');
            }]);
        }

        return $dataProvider;
    }
}
