<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "detail_to_arriving".
 *
 * @property int $id
 * @property int $arriving_id Поступление
 * @property int $detail_id Деталь
 * @property double $number Количество
 * @property double $price Цена за штуку
 * @property double $nds НДС %
 * @property double $vendor_code Артикул
 * @property double $nds_amount НДС от цены одной детали
 * @property double $exchange_rate Курс валюты к рублю
 * @property int $currency_id ID Валюты
 * @property int $total_amount_rub Общая сумма покупки в рублях
 * @property double $total_amount_currency Общая сумма в валюте закпки детали
 * @property double $total_cur Общая сумма для валюты
 * @property int $warehouse Склад
 *
 * @property Arriving $arriving
 * @property Nomenclature $detail
 * @property Currency $currency
 * @property string warehouseName Название склада
 */
class DetailToArriving extends ActiveRecord
{
    const WAREHOUSE_ON_THE_WAY = 1;
    const WAREHOUSE_BASIS = 2;
//    const WAREHOUSE_ON_THE_WAY_TO_WAREHOUSE = 3;
    const WAREHOUSE_ON_THE_WAY_TO_WAREHOUSE = 3;

    const STATUS_SAVED = 1;
    const STATUS_ACCEPTED = 2;

    public $vendor_code; //Артикул
    public $nds_amount; //НДС от цены одной детали
    public $total_cur; //Общая сумма для валюты
    public $icon;
    public $count;

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'detail_to_arriving';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['arriving_id', 'detail_id', 'currency_id', 'warehouse', 'status'], 'integer'],
            [['number', 'price', 'total_amount_rub', 'total_amount_currency', 'exchange_rate'], 'number'],
            [
                ['arriving_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Arriving::class,
                'targetAttribute' => ['arriving_id' => 'id']
            ],
            [
                ['detail_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Nomenclature::class,
                'targetAttribute' => ['detail_id' => 'id']
            ],
            ['vendor_code', 'string'],
            [['detail_id', 'currency_id', 'number', 'total_amount_currency', 'exchange_rate'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'arriving_id' => 'Поступление',
            'detail_id' => 'Деталь',
            'number' => 'Количество',
            'price' => 'Цена за штуку',
            'total_amount_currency' => 'Сумма',
            'total_amount_rub' => 'Сумма в рублях',
            'vendor_code' => 'Артикул',
            'nds_amount' => 'НДС',
            'exchange_rate' => 'Курс',
            'currency_id' => 'Валюта',
            'warehouse' => 'Склад',
            'status' => 'Статус',
        ];
    }

    public function beforeSave($insert): bool
    {
        //Рассчитываем сумму в рублях
        $this->total_amount_rub = round($this->total_amount_currency * $this->exchange_rate);

        if ($this->total_amount_currency && $this->number) {
            $this->price = round($this->total_amount_currency / $this->number, 2); //Цена товара за штуку
        }

        //Добавляем на склад
        $move = new MovementPart([
            'nomenclature_id' => $this->detail_id,
            'arriving_id' => $this->arriving_id,
            'warehouse_from' => $this->oldAttributes['warehouse'] ?? null,
            'warehouse_to' => $this->warehouse,
            'date' => date('Y-m-d', time()),
            'num' => $this->number,
        ]);

        if (!$move->save()){
            Yii::error($move->errors, '_error');
            $this->addError('warehouse', 'Ошибка при перемещении детали на склад.');
            return false;
        }

        //Проверяем изменение склада
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        //Пересчитываем среднюю сумму закупки для детали
        $avg_result = self::find()
                ->andWhere((['detail_id' => $this->detail_id]))
                ->average('price') ?? 0;

        $avg_price = round($avg_result, 2);

        $nom_model = Nomenclature::findOne($this->detail_id) ?? null;
        $nom_model->avg_purchase_price = $avg_price;
        if ($insert) {
            $nom_model->fact_balance += $this->number;
        } else {
            //Вычитаем старое значение прихода и прибавляем новое
            \Yii::warning($changedAttributes, 'heere');
            if(isset($changedAttributes['number']) == false){
                $changedAttributes['number'] = 0;
            }
            $nom_model->fact_balance = $nom_model->fact_balance - $changedAttributes['number'] + $this->number;
        }
        if (!$nom_model->save()) {
            Yii::error($nom_model->errors, '_error');
            Yii::$app->session->setFlash('warning', 'Ошибка сохранения средней цены закупки');
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArriving()
    {
        return $this->hasOne(Arriving::className(), ['id' => 'arriving_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::class, ['id' => 'contractor_id'])
            ->viaTable('arriving', ['id' => 'arriving_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetail()
    {
        return $this->hasOne(Nomenclature::className(), ['id' => 'detail_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::class, ['id' => 'currency_id']);
    }

    /**
     * Возвращает среднюю цену детали в Поступлении за период
     * @param int $detail_id
     * @param string $start Начальная дата
     * @param string $end Конечная дата
     * @return int|mixed
     */
    public static function getAveragePrice($detail_id, $start, $end)
    {
//        \Yii::info('Деталь: ' . $detail_id, 'test');
//        \Yii::info('Старт: ' . $start, 'test');
//        \Yii::info('Конец: ' . $end, 'test');
        $result = self::find()
                ->joinWith(['arriving arr'])
                ->andWhere(['BETWEEN', 'arr.created_at', $start, $end])
                ->andWhere(['detail_id' => $detail_id])
                ->average('price') ?? 0;
        return round($result, 2);
    }

    /**
     * Возвращает количество деталей в Поступлении за период
     * @param int $detail_id
     * @param string $start Начальная дата
     * @param string $end Конечная дата
     * @return array
     */
    public static function getSum(int $detail_id, string $start, string $end, $warehouseArray = [self::WAREHOUSE_BASIS, self::WAREHOUSE_ON_THE_WAY_TO_WAREHOUSE], $includeWarehouseNull = true): array
    {
//        Yii::debug('getSum function для ' . $detail_id, 'test');
//        Yii::debug('С ' . $start . ' по ' . $end, 'test');

        $result = [];
        $query = self::find()
            ->joinWith(['arriving arr'])
            ->andWhere(['BETWEEN', 'arr.created_at', $start, $end])
            ->andWhere(['or', ['detail_id' => $detail_id, 'arr.status' => Arriving::STATUS_ACCEPTED], ['detail_id' => $detail_id, 'detail_to_arriving.status' => Arriving::STATUS_ACCEPTED]]);
//            ->andWhere()

        if($includeWarehouseNull){
            $query->andWhere(['OR',
                ['warehouse' => $warehouseArray],
                ['IS', 'warehouse', null],
            ]);
        } else {
            $query->andWhere(['warehouse' => $warehouseArray]);
        }

        //Получаем поступления детали в разрезе валют
        $query_by_currency = clone $query;
        $query_result = $query_by_currency
                ->joinWith(['currency curr'])
                ->select(['currency_id', 'curr.icon as icon', 'SUM(total_amount_currency) as total_cur'])
                ->groupBy(['currency_id'])
                ->all() ?? null;

        /** @var DetailToArriving $qr */
        foreach ($query_result as $qr) {
            $result['by_currency'][$qr->icon] = $qr->total_cur;
        }

        //Общее количество
        $query_sum = clone $query;
        $result['sum'] = round($query_sum
                ->sum('detail_to_arriving.number') ?? 0, 2);

//        Yii::info('Общее кол-во деталей: ' . $result['sum'], 'test');

        $query_amount = clone $query;
        $result['total_amount'] = round($query_amount
            ->sum('total_amount_rub'), 2);

//        Yii::debug('Поступило деталей за период ' . $start . ' - ' . $end , 'test');
//        Yii::debug($result, 'test');

        return $result;
    }

    /**
     * Возвращает количество деталей в Поступлении за период
     * @param int $detail_id
     * @param string $start Начальная дата
     * @param string $end Конечная дата
     * @return int|mixed
     */
    public static function getTotal($detail_id, $start, $end)
    {
        $result = self::find()
                ->joinWith(['arriving arr'])
                ->andWhere(['BETWEEN', 'arr.created_at', $start, $end])
                ->andWhere(['detail_id' => $detail_id])
                ->sum('total_amount') ?? 0;
        return round($result, 2);
    }

    /**
     * Получает все детали из поступлений для контрагента за период
     * @param int $contractor_id ID Контрагента
     * @param string $start Начальная дата
     * @param string $end Конечная дата
     * @return array|null|ActiveRecord[]
     */
    public static function getFromContractor($contractor_id, $start, $end)
    {

        $sql = /** @lang MySQL */
            <<<SQL
                SELECT DISTINCT `detail_id` 
                FROM `detail_to_arriving`
                LEFT JOIN `arriving` 
                ON `detail_to_arriving`.`arriving_id` = `arriving`.`id` 
                LEFT JOIN `contractor` `c` 
                ON `arriving`.`contractor_id` = `c`.`id` 
                WHERE (`arriving`.`created_at` 
                BETWEEN :start_date AND :end_date) 
                AND (`c`.`id`=:contractor_id)
SQL;

        $models_id = self::findBySql($sql, [
                ':contractor_id' => $contractor_id,
                ':start_date' => $start,
                ':end_date' => $end
            ])
                ->all() ?? null;
        $ids = [];
        /** @var DetailToArriving $model */
        foreach ($models_id as $model) {
            array_push($ids, $model->detail_id);
        }

        Yii::info($ids, 'test');

        return self::find()
                ->andWhere(['IN', 'detail_id', $ids])
                ->groupBy('detail_id')
                ->all() ?? null;


    }

    /**
     * Получает последнюю величину НДС %
     * @return float|int
     */
    public static function getLastNds()
    {
        $last_record_id = self::find()
            ->max('id');
        return self::findOne($last_record_id)->nds ?? 20;
    }

    public static function getWarehouses(): array
    {
        return [
            self::WAREHOUSE_ON_THE_WAY => 'В пути',
            self::WAREHOUSE_ON_THE_WAY_TO_WAREHOUSE => 'В пути на склад',
            self::WAREHOUSE_BASIS => 'Основной',
        ];
    }

    /**
     * Имя склада
     * @return string
     */
    public function getWarehouseName(): string
    {
        return self::getWarehouses()[$this->warehouse] ?? '';
    }

    /**
     * Количество деталей на складе "В пути"
     * @param int $id Идентификатор детали
     * @return int|string
     */
    public static function getOnTheWayCount(int $id)
    {
        return DetailToArriving::find()
                ->select(['SUM(number)'])
                ->andWhere(['detail_id' => $id, 'warehouse' => self::WAREHOUSE_ON_THE_WAY])
                ->scalar() ?? '';
    }

    /**
     * Количество деталей на складе "Основной"
     * @param int $id Идентификатор детали
     * @return float
     */
    public static function getBasisCount(int $id): float
    {
        //Кол-во деталей в поступлениях
//        $arriving = DetailToArriving::find()
//                ->select(['SUM(number)'])
//                ->andWhere(['detail_id' => $id])
//                ->andWhere(['OR',
//                    ['warehouse' => self::WAREHOUSE_BASIS],
//                    ['IS', 'warehouse', null],
//                ])
//                ->scalar() ?? 0;
        $arriving = DetailToArriving::getSum($id, '2000-01-01', date('Y-m-d', time()), [self::WAREHOUSE_BASIS])['sum'];
        //Кол-во деталей в реализациях
        $realization = DetailToRealization::find()
                ->joinWith(['realization'])
                ->select(['SUM(detail_to_realization.number)'])
                ->andWhere(['detail_to_realization.detail_id' => $id, 'realization.warehouse_id' => 2])
                ->scalar() ?? 0;

        Yii::debug($arriving, 'поступления getBasisCount');
        Yii::debug($realization, 'реализации getBasisCount');
        return round($arriving - $realization, 2);
    }


    /**
     * Количество деталей на складе "Основной"
     * @param int $id Идентификатор детали
     * @return float
     */
    public static function getOnWayCount(int $id): float
    {
        //Кол-во деталей в поступлениях
//        $arriving = DetailToArriving::find()
//                ->select(['SUM(number)'])
//                ->andWhere(['detail_id' => $id])
//                ->andWhere(['OR',
//                    ['warehouse' => self::WAREHOUSE_BASIS],
//                    ['IS', 'warehouse', null],
//                ])
//                ->scalar() ?? 0;
        $arriving = DetailToArriving::getSum($id, '2000-01-01', date('Y-m-d', time()), [self::WAREHOUSE_ON_THE_WAY_TO_WAREHOUSE], false)['sum'];
        //Кол-во деталей в реализациях
        $realization = DetailToRealization::find()
                ->joinWith(['realization'])
                ->select(['SUM(detail_to_realization.number)'])
                ->andWhere(['detail_to_realization.detail_id' => $id, 'realization.warehouse_id' => 3])
                ->scalar() ?? 0;
        Yii::debug($arriving, 'поступления getBasisCount');
        Yii::debug($realization, 'реализации getBasisCount');
//        return round($arriving - $realization, 2);
        return round($arriving, 2);
    }

    /**
     * Количество деталей на складе "Основной"
     * @param int $id Идентификатор детали
     * @return float
     */
    public static function getOnWayCountLiftgou(int $id): float
    {
        //Кол-во деталей в поступлениях
//        $arriving = DetailToArriving::find()
//                ->select(['SUM(number)'])
//                ->andWhere(['detail_id' => $id])
//                ->andWhere(['OR',
//                    ['warehouse' => self::WAREHOUSE_BASIS],
//                    ['IS', 'warehouse', null],
//                ])
//                ->scalar() ?? 0;
        $arriving = DetailToArriving::getSum($id, '2000-01-01', date('Y-m-d', time()), [self::WAREHOUSE_ON_THE_WAY_TO_WAREHOUSE], false)['sum'];
        //Кол-во деталей в реализациях
        $realization = DetailToRealization::find()
                ->joinWith(['realization'])
                ->select(['SUM(detail_to_realization.number)'])
                ->andWhere(['detail_to_realization.detail_id' => $id, 'realization.warehouse_id' => 3])
                ->scalar() ?? 0;
        Yii::debug($arriving, 'поступления getBasisCount');
        Yii::debug($realization, 'реализации getBasisCount');
//        return round($arriving - $realization, 2);
        return round($arriving - $realization, 2);
    }
}
