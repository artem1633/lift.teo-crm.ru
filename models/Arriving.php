<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "arriving".
 *
 * @property int $id
 * @property string $created_at
 * @property int $contractor_id Контрагент
 * @property string $total_amount Общая сумма поступления
 * @property string $number Номер
 * @property string $arriving_info инфа о поступлении (Номер + дата)
 * @property string $status Статус поступления (Сохранено/Проведено)
 *
 * @property Contractor $contractor
 * @property DetailToArriving[] $details
 * @property Currency $currency
 * @property string $statusLabel
 */
class Arriving extends ActiveRecord
{
    const STATUS_SAVED = 1;
    const STATUS_ACCEPTED = 2;

    public $total_amount;
    public $count;

    /** @var string */
    public $arriving_info;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'arriving';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'total_amount'], 'safe'],
            [['contractor_id', 'status'], 'integer'],
            [
                ['contractor_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Contractor::class,
                'targetAttribute' => ['contractor_id' => 'id']
            ],
            [['number', 'arriving_info'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Дата',
            'contractor_id' => 'Контрагент',
            'total_amount' => 'Сумма',
            'number' => 'Номер',
            'status' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'contractor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetails()
    {
        return $this->hasMany(DetailToArriving::className(), ['arriving_id' => 'id']);
    }

    public function totalAmountCurrency()
    {
        $result = DetailToArriving::find()
                ->select(['currency_id', 'SUM(total_amount_currency) as total_cur'])
                ->andWhere(['arriving_id' => $this->id])
                ->groupBy(['currency_id'])->all() ?? 0;

        foreach ($result as $one) {
            Yii::info($one->toArray(), 'test');
        }

        return $result;
    }

    /**
     * Получает все проданные детали за выданный период
     * @param $start
     * @param $end
     * @return null
     */
    public static function getProfit($start, $end = null)
    {
        $start = $start . ' 00:00:00';
        if (!$end) {
            $end = date('Y-m-d 23:59:59', time());
        } else {
            $end = $end . ' 23:59:59';
        }

        $contractors = Arriving::find()
                ->select('contractor_id')
                ->andWhere(['BETWEEN', 'created_at', $start, $end])
                ->distinct()
                ->asArray()
                ->all() ?? null;

        \Yii::info($contractors, 'test');

        $arriving = Arriving::find()
                ->joinWith(['details d'])
//                ->select(['arriving.id', 'created_at'])
                ->andWhere(['BETWEEN', 'created_at', $start, $end])
                ->andWhere(['IN', 'contractor_id', $contractors])
                ->all() ?? null;

//        \Yii::info($arriving, 'test');

        return $arriving;


    }

    /**
     * @param string $start Начало периода
     * @param string $end Конец периода
     * @return array|null|ActiveRecord[]
     */
    public static function getContractors($start, $end)
    {
        $start = $start . ' 00:00:00';
        if (!$end) {
            $end = date('Y-m-d 23:59:59', time());
        } else {
            $end = $end . ' 23:59:59';
        }

        $contractors_id = Arriving::find()
                ->select('contractor_id')
                ->andWhere(['BETWEEN', 'created_at', $start, $end])
                ->distinct()
                ->asArray()
                ->all() ?? null;
        \Yii::info($contractors_id, 'test');

        $ids = [];
        foreach ($contractors_id as $value) {

            array_push($ids, $value['contractor_id']);
        }
        \Yii::info($ids, 'test');

        if ($contractors_id) {
            return Contractor::find()
                ->andWhere(['IN', 'id', $ids])
                ->all();
        }
        return null;
    }

    public function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'number');
    }

    public function getListInfo()
    {

        $sql = <<<SQL
                SELECT `arriving`.`id`, 
                CONCAT(`arriving`.`number`, " от ", DATE_FORMAT(`arriving`.`created_at`, "%d.%m.%Y"), " ", (SELECT `contractor`.`name` FROM `contractor` WHERE `contractor`.`id` = `arriving`.`contractor_id`)) AS `arriving_info`
                FROM `arriving`
                LEFT JOIN `contractor`
                ON `arriving`.`contractor_id` = `contractor`.`id`
                WHERE `arriving`.`id` IN (SELECT `arriving`.`id` FROM `arriving` WHERE `arriving`.`number` IS NOT NULL)
SQL;
        return ArrayHelper::map(self::findBySql($sql)->all(), 'id', 'arriving_info');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJuridicalPerson()
    {
        return $this->hasMany(JuridicalPerson::class, ['contractor_id' => 'id']);
    }

    /**
     * Список статусов
     * @return array
     */
    public static function getStatusList()
    {
        return [
            self::STATUS_SAVED => 'Сохранено',
            self::STATUS_ACCEPTED => 'Проведено',
        ];
    }

    /**
     * Статус поступления
     */
    public function getStatusLabel()
    {
        return self::getStatusList()[$this->status];
    }
}
