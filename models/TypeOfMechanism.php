<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "type_of_mechanism".
 *
 * @property int $id
 * @property string $name Наименование
 *
 * @property Nomenclature[] $nomenclatures
 */
class TypeOfMechanism extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'type_of_mechanism';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNomenclatures()
    {
        return $this->hasMany(Nomenclature::className(), ['type_of_mechanism_id' => 'id']);
    }
}
