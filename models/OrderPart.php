<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "order_part".
 *
 * @property int $id
 * @property int $request_id Заявка
 * @property int $nomenclature_id Запрос
 * @property string $comment Комментарий
 * @property int $detail_id Код детали
 * @property int $in_china Есть в китае
 * @property int $delivery_time_id
 * @property int $suppliers_id Код поставщика
 * @property int $buy_price Цена ппокупки
 * @property int $sell_price Цена продажи
 * @property int $quantity Количество
 * @property int $measure_id Единица измерения
 * @property string $files_location Файлы
 * @property string $created_date Дата создания и время создания
 * @property string $updated_date Дата и время редактирования
 * @property int $created_by Кто создал
 * @property int $vendor_code Артикул
 * @property int $buy_currency Валюта покупки
 * @property int $sell_currency Валюта продажи
 *
 * @property Suppliers $suppliers Поставщики
 * @property Measure $measure Мера веса
 * @property Nomenclature $nomenclature Номенклатура
 * @property Nomenclature $detail Деталь
 * @property Request $request Заявка
 * @property DeliveryTime $deliveryTime Время доставки
 * @property string $updated_buy_price Дата обновления закупочной цены
 * @property string $updated_sell_price Дата обновления цены продажи
 * @property \app\models\Reserve $reserve Резерв для детали в заявке
 */
class OrderPart extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $file;
    public $files;
    public $files_location_hidden;
    public $model_id;
    public $updateRecord;
//    public $detail;
//    public $nomenclature;

    public static function tableName()
    {
        return 'order_part';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'request_id',
                    'in_china',
                    'delivery_time_id',
                    'suppliers_id',
                    'measure_id',
                    'created_by',
                    'detail_id',
                    'nomenclature_id',
                    'buy_currency',
                    'sell_currency'
                ],
                'integer'
            ],
            [['comment', 'files_location'], 'string'],
            [['files_location_hidden'], 'string'],
            [['model_id', 'updateRecord'], 'integer'],
            [['buy_price', 'sell_price', 'quantity'], 'number'],
            [['created_date', 'updated_date', 'vendor_code', 'updated_buy_price'], 'string', 'max' => 255],
            [['file'], 'file', 'extensions' => 'jpeg, jpg, png'],
            [
                ['files'],
                'file',
                'maxFiles' => 1e9, /*'skipOnEmpty' => true,*/  /*'maxSize' => 1024 * 5120, */
                'extensions' => 'jpeg, jpg, png'
            ],
            [
                ['suppliers_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Suppliers::className(),
                'targetAttribute' => ['suppliers_id' => 'id']
            ],
            [
                ['measure_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Measure::className(),
                'targetAttribute' => ['measure_id' => 'id']
            ],
            [
                ['nomenclature_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Nomenclature::className(),
                'targetAttribute' => ['nomenclature_id' => 'id']
            ],
            [
                ['request_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Request::className(),
                'targetAttribute' => ['request_id' => 'id']
            ],
            [
                ['delivery_time_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => DeliveryTime::className(),
                'targetAttribute' => ['delivery_time_id' => 'id']
            ],
            [
                ['suppliers_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Suppliers::className(),
                'targetAttribute' => ['suppliers_id' => 'id']
            ],
            [
                ['detail_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Nomenclature::className(),
                'targetAttribute' => ['detail_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'request_id' => 'Номер Заявки',
            'nomenclature_id' => 'Запрос',
            'nomenclature' => 'Запрос',
            'detail_id' => 'Деталь',
            'detail' => 'Деталь',
            'delivery_time_id' => 'Срок доставки',
            'suppliers_id' => 'Поставщик',
            'in_china' => 'Есть в китае',
            'buy_price' => 'Цена покупки',
            'sell_price' => 'Цена продажи',
            'vendor_code' => 'Артикул',
            'comment' => 'Комментарий',
            'quantity' => 'Количество',
            'measure_id' => 'Единица измерения',
            'files_location' => 'Изображения',
            'created_date' => 'Дата создания',
//            'created_date' => 'Дата создания и время создания',
            'updated_date' => 'Дата редактирования',
//            'updated_date' => 'Дата и время редактирования',
            'created_by' => 'Кто создал',
//            'sell_price_currency' => 'Валюта продажи',
//            'buy_price_currency' => 'Валюта покупки',
            'buy_currency' => 'Валюта покупки',
            'sell_currency' => 'Валюта продажи',
            'updated_buy_price' => 'Дата обновления закупочной цены',
            'updated_sell_price' => 'Дата обновления цены продажи',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuppliers()
    {
        return $this->hasOne(Suppliers::className(), ['id' => 'suppliers_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeasure()
    {
        return $this->hasOne(Measure::className(), ['id' => 'measure_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNomenclature()
    {
        return $this->hasOne(Nomenclature::className(), ['id' => 'nomenclature_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetail()
    {
        return $this->hasOne(Nomenclature::className(), ['id' => 'detail_id']);
    }

    public function getOwnerVendorCode()
    {
        $this->detail->own_vendor_code;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(Request::className(), ['id' => 'request_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryTime()
    {
        return $this->hasOne(DeliveryTime::className(), ['id' => 'delivery_time_id']);
    }

    private function changeUpdatedDate($request_id, $date)
    {
        //Обновляем updated_date заявки в таблице заявок
        $request_model = Request::findOne($request_id);
        $request_model->updated_date = $date;
        if (!$request_model->save()) {
            Yii::error('Ошибка сохранения даты обновления заявки №' . $request_model->id, 'error');
            Yii::error($request_model->errors, 'error');
            return false;
        }
        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            //Дата редактирования заявки = дате создания позиции в заявке
            $date = date('Y-m-d H:i:s', time());
        } else {
            //Дата редактирования заявки = дате редактирования позиции в заявке
            $date = $this->updated_date;
        }

        $this->changeUpdatedDate($this->request_id, $date);

    }

    public function afterDelete()
    {
        parent::afterDelete();
        //Меняем дату обновления в заявке
        $this->changeUpdatedDate($this->request_id, date('Y-m-d H:i:s', time()));
    }

    /**
     * Получаем ID позиции в заявке
     * @param $detail_name
     * @return bool
     */
    public static function getOrderPartId($detail_name)
    {
        $request_id = Yii::$app->session->get('request_id');

        $detail = Nomenclature::find()
            ->where(['name' => $detail_name])
            ->one();
        if (isset($detail->id)) {
            $order_part = OrderPart::find()
                ->where(['request_id' => $request_id])
                ->andWhere(['detail_id' => $detail->id])
                ->one();
        }

        if (isset($order_part->id)) {
            return $order_part->id;
        }

        return false;
    }

    /**
     * Получает общую сумму проданных деталей
     * @param $detail_id
     * @param $start
     * @param $end
     * @return int|mixed
     */
    public static function getExpense($detail_id, $start, $end)
    {
        $expense_total = OrderPart::find()
                ->andWhere(['detail_id' => $detail_id])
                ->andWhere(['BETWEEN', 'created_date', $start, $end])
                ->sum('sell_price') ?? 0;

        return round($expense_total, 2);
    }

    /**
     * Резервы для детали
     * @return \yii\db\ActiveQuery
     */
    public function getReserve(): ActiveQuery
    {
        return $this->hasOne(Reserve::class, ['order_part_id' => 'id']);
    }

}
