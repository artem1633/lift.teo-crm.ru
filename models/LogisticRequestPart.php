<?php

namespace app\models;

use app\models\query\LogisticRequestPartQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "logistic_request_part".
 *
 * @property int $id
 * @property int $logistic_request_id Счет
 * @property int $nomenclature_id Деталь
 * @property double $num Количество
 * @property string $comment Комментарий
 * @property string $request_number Номер заявки
 * @property string $arriving_id Поступление, т.н. Инвойс
 * @property int $supplier_id Поставщик
 * @property int $logistic_status_id Статус логиста
 * @property string $contract_date Дата по договору
 * @property string $order_date Дата заказа
 * @property string $ship_date Дата отгрузки поставщика
 * @property string $receipt_date_manch Дата получения (склад Маньчжурия)
 * @property string $receipt_date_novosib Дата получения (склад Нвосибирск)
 * @property string $client_ship_date Дата отгрузки клиенту
 * @property string $tn_tk_rus Номер ТН ТК в России
 * @property integer $measure_id Идентификатор меры
 * @property string $vendor_code Артикул
 * @property string $img_src Путь к изображению детали
 * @property string $nomenclature_name
 * @property string $supplier_name
 * @property string $arriving_info Составная инфа о поступлении
 *
 * @property LogisticRequest $logisticRequest
 * @property LogisticStatus $logisticStatus
 * @property Nomenclature $nomenclature
 * @property Suppliers $supplier
 * @property Measure $measure
 * @property Arriving $arriving
 */
class LogisticRequestPart extends ActiveRecord
{

    /** @var string */
    public $vendor_code;

    /** @var string */
    public $img_src;

    /** @var string */
    public $nomenclature_name;

    /** @var string */
    public $supplier_name;

    /** @var string */
    public $arriving_info;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'logistic_request_part';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'logistic_request_id',
                    'nomenclature_id',
                    'supplier_id',
                    'logistic_status_id',
                    'measure_id',
                    'arriving_id'
                ],
                'integer'
            ],
            [['num'], 'number'],
            [['comment'], 'string'],
            [['ship_date', 'receipt_date_manch', 'receipt_date_novosib', 'client_ship_date'], 'safe'],
            [['request_number', 'order_date', 'tn_tk_rus', 'contract_date'], 'string', 'max' => 255],
            [
                ['logistic_request_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => LogisticRequest::className(),
                'targetAttribute' => ['logistic_request_id' => 'id']
            ],
            [
                ['logistic_status_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => LogisticStatus::className(),
                'targetAttribute' => ['logistic_status_id' => 'id']
            ],
            [
                ['nomenclature_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Nomenclature::className(),
                'targetAttribute' => ['nomenclature_id' => 'id']
            ],
            [
                ['supplier_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Suppliers::className(),
                'targetAttribute' => ['supplier_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'logistic_request_id' => 'Счет',
            'nomenclature_id' => 'Деталь',
            'num' => 'Кол-во',
            'comment' => 'Комментарий',
            'request_number' => 'Номер заявки',
            'arriving_id' => 'Инвойс',
            'arriving_info' => 'Инвойс',
            'supplier_id' => 'Поставщик',
            'logistic_status_id' => 'Статус логиста',
            'contract_date' => 'Дата по договру',
            'order_date' => 'Дата заказа',
            'ship_date' => 'Отгрузка поставщика',
            'receipt_date_manch' => 'Таможня',
            'receipt_date_novosib' => 'склад Нвосибирск',
            'client_ship_date' => 'Отгрузка клиенту',
            'tn_tk_rus' => 'ТН ТК в России',
            'vendor_code' => 'Артикул',
            'nomenclature_name' => 'Деталь',
            'supplier_name' => 'Поставщик',
            'measure_id' => 'Мера',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->ship_date) {
            $this->ship_date = date('Y-m-d', strtotime($this->ship_date));
        }
        if ($this->receipt_date_manch) {
            $this->receipt_date_manch = date('Y-m-d', strtotime($this->receipt_date_manch));
        }
        if ($this->receipt_date_novosib) {
            $this->receipt_date_novosib = date('Y-m-d', strtotime($this->receipt_date_novosib));
        }
        if ($this->client_ship_date) {
            $this->client_ship_date = date('Y-m-d', strtotime($this->client_ship_date));
        }

        return parent::beforeSave($insert);
    }

    /**
     * @throws \yii\db\Exception
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        //Проверяем смену статуса
        $old_status_id = $changedAttributes['logistic_status_id'] ?? null;
        $new_status_id = $this->logistic_status_id;
        Yii::debug($changedAttributes, 'test');
        Yii::debug($new_status_id, 'test');
        if ($old_status_id != $new_status_id) {
            $transaction = Yii::$app->db->beginTransaction();
            Yii::debug('Статус изменился', 'test');
            $detail = Nomenclature::findOne($this->nomenclature_id);

            if ($new_status_id == 6) {
                Yii::debug('Статус "В наличии"', 'test');
                //Если статус изменился и выбран статус "В наличии" добавляем в резерв
                $model = Reserve::find()->forLogisticRequestPart($this->id)->one();
                if (!$model) {
                    $model = new Reserve();
                    $model->logistic_request_part_id = $this->id;
                    $model->nomenclature_id = $this->nomenclature_id;
                    $model->count = 0;
                }
                if (!$model->created_by) {
                    $model->created_by = Yii::$app->user->id;
                }
                $model->count = $this->num;
                Yii::debug($model->attributes, 'test');
                if (!$model->save()) {
                    Yii::error($model->errors, '_error');
                } else {
                    $transaction->commit();
                }
            } elseif ($new_status_id == 5) {
                Yii::debug('Статус "Отгружен заказчику"', 'test');
                //Если статус "Отгружен заказчику"

                //Вычитаем кол-во в резерве в таблице номенклатуры
                $model = Reserve::find()->forLogisticRequestPart($this->id)->one();
                Yii::debug($model->attributes, 'test');
                $count = $model->count ?? 0;
                $detail->reserve = $detail->reserve - $count;
                if (!$detail->save()) {
                    Yii::error($detail->errors, '_error');
                    $transaction->rollBack();
                } else {
                    $transaction->commit();
                }
                //Удаляем из резерва
                Reserve::deleteAll([
                    'logistic_request_part_id' => $this->id,
                ]);
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogisticRequest()
    {
        return $this->hasOne(LogisticRequest::className(), ['id' => 'logistic_request_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogisticStatus()
    {
        return $this->hasOne(LogisticStatus::className(), ['id' => 'logistic_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNomenclature()
    {
        return $this->hasOne(Nomenclature::className(), ['id' => 'nomenclature_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArriving()
    {
        return $this->hasOne(Arriving::className(), ['id' => 'arriving_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplier()
    {
        return $this->hasOne(Suppliers::className(), ['id' => 'supplier_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeasure()
    {
        return $this->hasOne(Measure::className(), ['id' => 'measure_id']);
    }

    /**
     * {@inheritdoc}
     * @return LogisticRequestPartQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LogisticRequestPartQuery(get_called_class());
    }


}
