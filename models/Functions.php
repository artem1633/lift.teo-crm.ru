<?php

namespace app\models;

use johnitvn\ajaxcrud\BulkButtonWidget;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "functions".
 */
class Functions extends ActiveRecord
{

    const NO_IMAGE = '/images/site/no-image.png';

    public static function getCitiesList()
    {
        $cities = Cities::find()->all();
        if (!empty($cities)) {
            return ArrayHelper::map($cities, 'id', 'name');
        }
        return [];
    }

    public static function getNomenclatureList($role = '')
    {
        Yii::info('Роль: ' . $role, __METHOD__);
        switch ($role) {
            case Users::USER_ROLE_MANAGER:
                $nomenclatures = Nomenclature::find()->where(['nomenclature_type' => Users::USER_ROLE_MANAGER])->all();
                break;
            case Users::USER_ROLE_TECH_SPECIALIST:
                $nomenclatures = Nomenclature::find()->where(['nomenclature_type' => Users::USER_ROLE_TECH_SPECIALIST])->all();
                break;
            default:
                $nomenclatures = Nomenclature::find()->all();
        }

//        $nomenclature = Nomenclature::find()-> andFilterWhere(['like', 'name', $name]) -> all();
        if (!empty($nomenclatures)) {
            return ArrayHelper::map($nomenclatures, 'id', 'name');
        }
        return [];
    }

    public static function getNomenclature($nomenclature_id)
    {
        $nomenclature = Nomenclature::find()->where(['id' => $nomenclature_id])->one();
        if (!empty($nomenclature)) {
            return $nomenclature->name;
        }
        return "";
//        return ArrayHelper::map($nomenclature, 'id', 'name');
    }

    public static function getNomenclatureWithImage($nomenclature_id, $request_id = null) //,$detail_id=null
    {
        Yii::info('1. $nomenclature_id = ' . $nomenclature_id, 'test');

        $nomenclature = Nomenclature::find()->where(['id' => $nomenclature_id])->one();

        if (Users::isTech()) { // Неактивно
            $result_witch_image = '<div style="display: flex; align-items: center;">' . Html::a(Html::img(self::getThumbnailPath($nomenclature->main_photo),
                    ['style' => 'max-width: 50px; margin: 0 0.5rem;']), [$nomenclature->main_photo],
                    ['data-fancybox' => 'gallery-' . $nomenclature_id]) .
                $nomenclature->name . '</div>';
            $result_no_image = '<div class="row">
                                    <div class="col-xs-12 text-center">' .
                $nomenclature->name .
                '</div>
                                </div>';
        } else {
            $result_witch_image = '<div style="display: flex; align-items: center;">' . Html::a(Html::img(self::getThumbnailPath($nomenclature->main_photo),
                    ['style' => 'max-width: 50px; margin: 0 0.5rem;']), [$nomenclature->main_photo],
                    ['data-fancybox' => 'gallery-' . $nomenclature_id]) .
                Html::a($nomenclature->name,
                    [
                        '/nomenclature/view',
                        'id' => $nomenclature->id,
                        'no_ajax' => 1,
                        'request_id' => $request_id
                    ]) . '</div>';
            $result_no_image = '<div class="row">
                                    <div class="col-xs-12 text-center">' .
                Html::a($nomenclature->name,
                    ['/nomenclature/view', 'id' => $nomenclature->id, 'no_ajax' => 1, 'request_id' => $request_id]) .
                '</div>
                                </div>';
        }

        if (!empty($nomenclature)) {
            if (!empty($nomenclature->main_photo)) {
                return $result_witch_image;
            } else { //Если нет фотки
                return $result_no_image;
            }
        } else {
            Yii::info('2. $nomenclature_id = ' . $nomenclature_id, 'test');
            return Html::a('Подобрать деталь', ['request/view', 'id' => $nomenclature->id]);
        }
    }

    public static function getNomenclatureWithImage2($nomenclature_id)
    {
        $nomenclature = Nomenclature::find()->where(['id' => $nomenclature_id])->one();
        if (!empty($nomenclature)) {
            if (!empty($nomenclature->main_photo)) {
                return Html::a(Html::img($nomenclature->main_photo, ['style' => 'max-width: 50px; margin: 0 0.5rem;']),
                        [$nomenclature->main_photo], ['data-fancybox' => 'gallery-' . $nomenclature_id]) .
                    $nomenclature->name;
            } else { //Если нет фотки
                return '<div class="row">
                        <div class="col-xs-12 text-center">' . $nomenclature->name . '
                        </div>
                    </div>';
            }
        } else {
            return 'Подобрать деталь';
        }
    }

    public static function getNomenclatureWithImageNoEdit($nomenclature_id)
    {
        $nomenclature = Nomenclature::find()->where(['id' => $nomenclature_id])->one();
        if (!empty($nomenclature)) {
            if (!empty($nomenclature->main_photo)) {

                return '<div class="row">
                            <div class="col-xs-12">
                                <a class="fancy_box" href="' . $nomenclature->main_photo . '"><img style="width: 100px" src="' . $nomenclature->main_photo . '"></a>
                            </div>
                            <div class="col-xs-12">' .
                    Html::a($nomenclature->name, ['/nomenclature/view', 'id' => $nomenclature->id, 'no_ajax' => 1]) .
                    '</div>
                        </div>';
            } else {
                return '<div class="row">
                        <div class="col-xs-12">' .
                    Html::a($nomenclature->name, ['/nomenclature/view', 'id' => $nomenclature->id, 'no_ajax' => 1]) .
                    '</div>
                    </div>';
            }
        } else {
            return '';
        }
    }

    public static function getNomenclatureNameList()
    {
        $nomenclature = Nomenclature::find()
            ->select('name')
//            ->indexBy('id')
            ->asArray()
            ->column();
        if (!empty($nomenclature)) {
            return $nomenclature;
        }
        return [];
    }

    public static function getMeasureList()
    {
        $measures = Measure::find()->all();
        if (!empty($measures)) {
            return ArrayHelper::map($measures, 'id', 'name');
        }
        return [];
    }

    public static function getMeasure($measure_id)
    {
        $measure = Measure::find()->where(['id' => $measure_id])->one();
        if (!empty($measure)) {
            return $measure->name;
        }
        return [];
//        return ArrayHelper::map($measure, 'id', 'name');
    }

    public static function getCity($city_id)
    {
        $city = Cities::find()->where(['id' => $city_id])->one();
        if (!empty($city)) {
            return $city->name;
        }
        return [];
    }

    public static function getManagersList()
    {
        $managers = Users::find()->where(['permission' => 'manager'])->all();
        if (!empty($managers)) {
            return ArrayHelper::map($managers, 'id', 'fio');
        }
        return [];
    }

    public static function getManager($manager_id)
    {
        $manager = Users::find()->where(['id' => $manager_id])->one();
        if (!empty($manager)) {
            return Html::a($manager->fio, ['/users/manager-view', 'id' => $manager_id]);
        } // '<a href="/users/manager-view?id=' . $manager_id . '">' . $manager -> fio . '</a>';
        return "";
    }

    public static function getBrandsList()
    {
        $brands = Brand::find()->all();
        if (!empty($brands)) {
            return ArrayHelper::map($brands, 'id', 'name');
        }
        return [];
    }

    public static function getBrand($brand_id)
    {
        $brand = Brand::find()->where(['id' => $brand_id])->one();
        if (!empty($brand)) {
            return $brand->name;
        }
        return "";
    }

    public static function getContractorsList()
    {
        $contractors = Contractor::find()->orderBy('name')->all();
        if (!empty($contractors)) {
            return ArrayHelper::map($contractors, 'id', 'name');
        }
        return [];
    }

    public static function getContractor($contractor_id)
    {
        $contractor = Contractor::find()->where(['id' => $contractor_id])->one();
        if (!empty($contractor)) {
            if (Users::isTech()) {
                return $contractor->name;
            } else {
                return Html::a($contractor->name, ['/contractor/view', 'id' => $contractor->id],
                    ['role' => 'modal-remote']);
            }
        }
        return "";
    }

    public static function getMainStatusesList()
    {
        $main_statuses = OrderStatus::find()->all();
        if (!empty($main_statuses)) {
            return ArrayHelper::map($main_statuses, 'id', 'name');
        }
        return [];
    }

    public static function getMainStatus($main_status_id)
    {
        $main_status = OrderStatus::find()->where(['id' => $main_status_id])->one();
        if (!empty($main_status)) {
            return '<span style="padding-top: 3px; padding-bottom: 2px; background-color:' . $main_status->color . '" class="btn btn-sm btn-block color_shadow fa ' . $main_status->icon . '">&nbsp; ' . $main_status->name . '</span>';
        } else {
            return "";
        }

    }

    public static function getAdditionalStatusesList()
    {
        $additional_statuses = AdditionalOrderStatus::find()->all();
        if (!empty($additional_statuses)) {
            return ArrayHelper::map($additional_statuses, 'id', 'name');
        }
        return [];
    }

    public static function getAdditionalStatus($additional_status_id)
    {
        $additional_status = AdditionalOrderStatus::find()->where(['id' => $additional_status_id])->one();
        if (!empty($additional_status)) {
//            return $additional_status->name;
            return '<span style="padding-top: 3px; padding-bottom: 2px; background-color:' . $additional_status->color . '" class="btn btn-sm btn-block color_shadow fa ' . $additional_status->icon . '">&nbsp; ' . $additional_status->name . '</span>';

        }
        return "";
    }

    public static function getGroupList()
    {
        $groups = NomenclatureGroup::find()->all();
        if (!empty($groups)) {
            return ArrayHelper::map($groups, 'id', 'name');
        }
        return [];
    }

    public static function getGroup($group_id)
    {
        $group = NomenclatureGroup::find()->where(['id' => $group_id])->one();
        if (!empty($group)) {
            return $group->name;
        }
        return "";
    }

    public static function getCreatedBy($id)
    {
        $user = Users::findOne($id);

        if (isset($user->id)) {
            if (Users::isManager() || Users::isTech()) {
                return $user->fio;
            } else {
                return Html::a($user->fio, [
                    '/users/creator-view',
                    'id' => $user->id
                ]);//'<a href="/users/creator-view?id=' . $user -> id . '">' . $user -> fio . '</a>';
            }
        }

        return null;
    }

    public static function getUserRole($id)
    {
        $user = Users::find()->where(['id' => $id])->one();
        if ($user) {
            if ($user->permission == 'administrator') {
                return 'Администратор';
            }
            if ($user->permission == 'manager') {
                return 'Менеджер';
            }
            if ($user->permission == 'user') {
                return 'Пользователь';
            }
            if ($user->permission == 'tech_specialist') {
                return 'Тех.Специалист';
            }
        }
        return null;
    }

    public static function getTypeOfPartsList()
    {
        $type_of_parts = TypeOfParts::find()->all();
        if (!empty($type_of_parts)) {
            return ArrayHelper::map($type_of_parts, 'id', 'name');
        }
        return [];
    }

    public static function getTypeOfPart($type_of_parts_id)
    {
        $type_of_part = TypeOfParts::find()->where(['id' => $type_of_parts_id])->one();
        if (!empty($type_of_part)) {
            return $type_of_part->name;
        }
        return "";
    }

    public static function getTypeOfMechanismList()
    {
        $type_of_mechanisms = TypeOfMechanism::find()->all();
        if (!empty($type_of_mechanisms)) {
            return ArrayHelper::map($type_of_mechanisms, 'id', 'name');
        }
        return [];
    }

    public static function getTypeOfMechanism($type_of_mechanism_id)
    {
        $type_of_mechanism = TypeOfMechanism::find()->where(['id' => $type_of_mechanism_id])->one();
        if (!empty($type_of_mechanism)) {
            return $type_of_mechanism->name;
        }
        return "";
    }

    public static function additionalFields($model)
    {
        $model->created_date = date('l jS \of F Y h:i:s A');
        // выведет примерно следующее: 21th of October 2018 10:31 PM
//        $model->updated_date = date('l jS \of F Y h:i:s A');
        //Кто создал? пользователь который залогинился.
        $model->created_by = Yii::$app->user->identity->id;

        if (!empty($model)) {
            return $model;
        }
        return [];
    }

    public static function generateRandomString($length = 40)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function stringToArray($additional_photos)
    {
        $convert_to_array = explode('^ ', $additional_photos);
//        \Yii::info('convert_to_array', 'upload');
//        \Yii::info($convert_to_array, 'upload');
        return $convert_to_array;
    }

    public static function arrayToString($additional_photos)
    {
        $convert_to_sting = implode('^ ', $additional_photos);
//        \Yii::info('convert_to_string', 'upload');
//        \Yii::info($convert_to_sting, 'upload');
        return $convert_to_sting;
    }

    public static function additionalPhotosHtml($additional_photos)
    {
        $html = '';
//        Functions::stringToArray($additional_photos);
        foreach ($additional_photos as $additional_photo) {
            $html .= "<img src='" . $additional_photo . "' style='width: 400px'>";
        }
//        \Yii::info('html', 'upload');
//        \Yii::info($html, 'upload');
        return $html;
    }

    public static function deleteImage($image_name)
    {
        if ($image_name != null && file_exists($image_name)) {
            unlink(Yii::getAlias($image_name));
//            \Yii::info('deleteImage', 'delete');
//            \Yii::info($image_name);
        }
    }

    public static function additionalPhotosHtmlEdit($additional_photos)
    {
        $html_array = [];
        if (!empty($additional_photos)) {
            $additional_photos = json_decode($additional_photos, true);
            foreach ($additional_photos as $additional_photo) {
                $html_array[] = "<img src='" . $additional_photo . "' class='kv-file-content additional_photos file-preview-image'>";
            }
//        $html = implode(', ', $html_array);
//            \Yii::info('additionalPhotosHtmlEdit', 'test');
//            \Yii::info($html_array, 'test');
        }
        return $html_array;
    }

    public static function photosHtml($additional_photos)
    {
//        $html= '';
        $html_array = '';
//        $fancy = 'data-fancybox="gallery"';

        if (!empty($additional_photos)) {
            $additional_photos = json_decode($additional_photos, true);
            foreach ($additional_photos as $additional_photo) {
                $html = '<img src="' . $additional_photo . '" class="file-preview-image additional_photos" style="max-height: 100px">';
                $html_array .= '<a class="fancy_box" href="' . $additional_photo . '">' . $html . '</a>`';
            }

//        $html = implode(', ', $html_array);
            \Yii::info('additionalPhotosHtmlEdit', 'test');
            \Yii::info($html_array, 'test');
        }
        return $html_array;
    }

    public static function initialPreviewConfig($additional_photos)
    {
        $html_array = [];

        foreach ($additional_photos as $additional_photo) {
            $html_array[] = [
                'caption' => $additional_photo,
                'size' => '873727',
                'url' => "/delete-image",
                'key' => $additional_photo,
            ];
        }
//        $html = implode(', ', $html_array);
//        \Yii::info('initialPreviewConfig', 'upload');
//        \Yii::info($html, 'upload');

        return $html_array;
    }

    public static function getRequestsList()
    {
        $requests = Request::find()->all();
        if (!empty($requests)) {
            return ArrayHelper::map($requests, 'id', 'description');
        }
        return [];
    }

    public static function getRequestIdsList()
    {
        $requests = Request::find()->all();
        if (!empty($requests)) {
            return ArrayHelper::map($requests, 'id', 'id');
        }
        return [];
    }

    public static function getRequest($request_id)
    {
        $request = Request::find()->where(['id' => $request_id])->one();
        if (!empty($request)) {
            return Html::a('Заявка № ' . $request_id, ['/request/view', 'id' => $request_id]);
        }//'<a href="/request/view?id=' . $request_id . '">Заявка №' . $request_id . '</a>';
        return "";
    }

    public static function getTechNomenclatureList()
    {
        $nomenclatures = Nomenclature::find()->where(['nomenclature_type' => Users::USER_ROLE_TECH_SPECIALIST])->all();
        if (!empty($nomenclatures)) {
            return ArrayHelper::map($nomenclatures, 'id', 'name');
        }
        return [];
    }

    public static function getTechNomenclature($nomenclature_id)
    {
        $nomenclature = Nomenclature::find()->where(['id' => $nomenclature_id])->one();
        if (!empty($nomenclature)) {
            return $nomenclature->name;
        }
//        return ArrayHelper::map($nomenclature, 'id', 'name');
        return "";
    }

    public static function getDeliveryTimeList()
    {
        $delivery_times = DeliveryTime::find()->all();
        if (!empty($delivery_times)) {
            return ArrayHelper::map($delivery_times, 'id', 'name');
        }
        return [];
    }

    public static function getDeliveryTime($delivery_time_id)
    {
        $delivery_time = DeliveryTime::find()->where(['id' => $delivery_time_id])->one();
        if (!empty($delivery_time)) {

            return '<div class="row">
                        <div class="col-xs-12 text-center">
                            <a href="/delivery-time/view?id=' . $delivery_time->id . '&no_ajax=1">' . $delivery_time->name . '</a>
                        </div>
                        <div class="col-xs-12 text-right">
                            <span class="btn fa fa-edit only_detail" >&nbsp;
                            </span>
                        </div>
                    </div>';
        }

        return '<div class="col-xs-12 text-right">
                    <span class="btn fa fa-edit only_detail" >&nbsp;
                    </span>
                </div>';
    }

    public static function getSuppliersList()
    {
        $suppliers = Suppliers::find()->all();
        if (!empty($suppliers)) {
            return ArrayHelper::map($suppliers, 'id', 'name');
        }
        return [];
    }

    public static function getSuppliers($suppliers_id)
    {
        $suppliers = Suppliers::find()->where(['id' => $suppliers_id])->one();
        if (!empty($suppliers)) {

            return '<div class="row">
                        <div class="col-xs-12 text-center">
                            <a href="/suppliers/view?id=' . $suppliers->id . '&no_ajax=1">' . $suppliers->name . '</a>
                        </div>
                        <div class="col-xs-12 text-right">
                            <span class="btn fa fa-edit only_detail" >&nbsp;
                            </span>
                        </div>
                    </div>';
        }

        return '<div class="col-xs-12 text-right">
                    <span class="btn fa fa-edit only_detail" >&nbsp;
                    </span>
                </div>';

    }

    public static function getSuppliersNoEdit($suppliers_id)
    {

        $suppliers = Suppliers::find()->where(['id' => $suppliers_id])->one();
        if (!empty($suppliers)) {

            return '<div class="row">
                        <div class="col-xs-12">
                            <a href="/suppliers/view?id=' . $suppliers->id . '&no_ajax=1">' . $suppliers->name . '</a>
                        </div>
                    </div>';
        }

        return '';

    }

    public static function getOrderParts($request_id)
    {
        $order_parts = OrderPart::find()->where(['request_id' => $request_id])->all();
        if (!empty($order_parts)) {
            return $order_parts;
        }
        return [];
    }

    public static function getInChina($request_id)
    {
        $in_china = OrderPart::find()->where(['request_id' => $request_id])->one();
//        return $in_china -> in_china == 1 ? '<span style="padding-top: 1px; padding-bottom: 2px;" class="btn btn-sm btn-success color_shadow">Да</span>' : '<span style="padding-top: 1px; padding-bottom: 2px;" class="btn btn-sm btn-danger color_shadow">Нет</span>';
        if (!empty($in_china)) {
            return $in_china->in_china;
        }
        return "";
    }

    public static function getInChina2($in_china)
    {

        $status = '';
        if ($in_china == 1) {

            $status = '<span style="padding-top: 1px; padding-bottom: 2px;" class="btn btn-sm btn-success color_shadow is-china-btn">Да</span>';

//            return '<div class="row">
//                        <div class="col-xs-12 text-center">'
//                . $status .
//                '</div>
//                        <div class="col-xs-12 text-right">
//                            <span class="btn fa fa-edit only_detail" >&nbsp;
//                            </span>
//                        </div>
//                    </div>';

        } else {
            if ($in_china == 0) {

                $status = '<span style="padding-top: 1px; padding-bottom: 2px;" class="btn btn-sm btn-danger color_shadow is-china-btn">Нет</span>';

//            return '<div class="row">
//                        <div class="col-xs-12 text-center">'
//                . $status .
//                '</div>
//                        <div class="col-xs-12 text-right">
//                            <span class="btn fa fa-edit only_detail" >&nbsp;
//                            </span>
//                        </div>
//                    </div>';
            }
        }
//        } else {
//
//            return '<div class="col-xs-12 text-right">
//                    <span class="btn fa fa-edit only_detail" >&nbsp;
//                    </span>
//                </div>';
//        }
        return $status;
    }

    public static function getInChina3($id)
    {
        Yii::info('$id: ' . $id, 'test');
        $model = OrderPart::findOne($id);

        if ($model->in_china == 1) {
            return Html::button('Да', [
                'id' => 'in-china-' . $model->id,
                'class' => 'btn btn-block btn-xs btn-success in-china-btn'
            ]);
        } else {
            return Html::button('Нет', [
                'id' => 'in-china-' . $model->id,
                'class' => 'btn btn-block btn-xs btn-danger in-china-btn'
            ]);
        }
    }

    public static function getCurrency()
    {
        $currency = Currency::find()->all();
        if (!empty($currency)) {
            return ArrayHelper::map($currency, 'id', 'name');
        }
        return [];
    }

    public static function russianDate()
    {
        //список месяцев с названиями для замены
        $_monthsList = array(
            ".01." => "января",
            ".02." => "февраля",
            ".03." => "марта",
            ".04." => "апреля",
            ".05." => "мая",
            ".06." => "июня",
            ".07." => "июля",
            ".08." => "августа",
            ".09." => "сентября",
            ".10." => "октября",
            ".11." => "ноября",
            ".12." => "декабря"
        );
        //текущая дата
        $currentDate = date("d.m.Y");
        //переменная $currentDate теперь хранит текущую дату в формате 08.07.2018
        //но так как наша задача - вывод русской даты,
        //заменяем число месяца на название:
        $_mD = date(".m."); //для замены
        $currentDate = str_replace($_mD, " " . $_monthsList[$_mD] . " ", $currentDate);
        //теперь в переменной $currentDate хранится дата в формате 08 ноября 2018
        return $currentDate;
    }

    public static function rusDate($d, $format = 'j %MONTH% Y', $offset = 0)
    {
        $montharr = array(
            'января',
            'февраля',
            'марта',
            'апреля',
            'мая',
            'июня',
            'июля',
            'августа',
            'сентября',
            'октября',
            'ноября',
            'декабря'
        );
        $dayarr = array('понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье');

        $d += 3600 * $offset;

        $sarr = array('/%MONTH%/i', '/%DAYWEEK%/i');
        $rarr = array($montharr[date("m", $d) - 1], $dayarr[date("N", $d) - 1]);

        $format = preg_replace($sarr, $rarr, $format);
        return date($format, $d);
    }

    public static function checkManager()
    {
        if (empty(Yii::$app->user->identity->id)) {
            return "-";
        } else {
            if (Functions::getUserRole(Yii::$app->user->identity->id) == "Менеджер") {
                return "-";
            }
        }
        return "+";
    }

    /**
     * В зависимости от роли возвращает или нет кнопку таблицы "Удалить все"
     * @return string
     * @throws \Exception
     */
    public static function getBulkButtonWidget()
    {
        $bulk_button_widget = BulkButtonWidget::widget([
            'buttons' => Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить Все',
                ["bulkdelete"],
                [
                    "class" => "btn btn-danger btn-xs",
                    'role' => 'modal-remote-bulk',
                    'data-confirm' => false,
                    'data-method' => false,// for overide yii data api
                    'data-request-method' => 'post',
                    'data-confirm-title' => 'Вы уверены?',
                    'data-confirm-message' => 'Вы уверены что хотите удалить все эти элементы?'
                ]),
        ]);

        if (!Users::isAdmin()) {
            $bulk_button_widget = '';
        }

        return $bulk_button_widget;
    }

    /**
     * Добавляет к пути картинки имя директории "thumbs"
     * @param $path_image
     * @return string
     */
    public static function getThumbnailPath($path_image)
    {
        $route = explode('/', $path_image);
        $name_img = end($route);
        $num = strrpos($path_image, '/');
        $dir_thumbs = substr($path_image, 0, $num) . '/' . Yii::$app->params['thumbnail_dir'] . '/';
        return $dir_thumbs . $name_img;
    }

    /** Удаляет файл-картинку и превьюху к нему
     * @param string $file Путь к файлу
     * @return bool
     */
    public static function deleteFile($file)
    {
        unlink(Yii::getAlias($file));
        $thumb_file = Functions::getThumbnailPath($file);
        if (is_file(Yii::getAlias($thumb_file))) {
            unlink(Yii::getAlias($thumb_file));
        }
        return true;
    }

    /**
     * Преобразует одинэсную дату (2019-06-07T11:49:55) в Y-m-d H:i:s
     * @param $date_1c
     * @return string
     */
    public static function convert1CtoTimestampDate($date_1c)
    {
//        return str_replace('Т', ' ', $date_1c);

        Yii::info($date_1c, 'test');

        $part_date_time = explode('T', $date_1c);
        Yii::info($part_date_time, 'test');

        if (count($part_date_time) != 2) {
            return '2000-01-01 00:00:00';
        }

        return $part_date_time[0] . ' ' . $part_date_time[1];


    }

    /**
     * Удаляет старые бэкапы, оставляет только те у которых дата изменения меньше указанного кол-ва дней
     * @param int $max_days Кол-во дней
     */
    public static function cleanBackups($max_days = 20)
    {
        $back_path = Url::to('@app/backups');
        $files = array_diff(scandir($back_path), ['.', '..', '.gitignore']);

        Yii::info($files, 'test');

//        $date = date_create(time());
//        $calc_date = date_sub($date, date_interval_create_from_date_string($max_days . ' days'));
//        $target_date = date_format($calc_date, 'Y-m-d');

        $target_date = date('Y-m-d', (time() - ($max_days * 24 *60 *60)));
        Yii::info('Минимальная дата: ' . $target_date, 'test');
        foreach ($files as $key => $file) {
            if (count($files) > 20) { //Запускаем чистку - если файлов больше 20
                $path = $back_path . '/' . $file;
                $file_time = date('Y-m-d', filectime($path));
                Yii::info('Дата бэкапа: ' . $file_time, 'test');

                if ($file_time < $target_date) {
                    try {
                        unlink($path);
                    } catch (\Exception $e) {
                        Yii::error('Ошибка удаления бэкапа. ' . $e->getMessage(), '_error');
                    }
                    Yii::info('Бэкап удален', 'test');
                    unset($files[$key]);
                }
            }
        }
    }

    /**
     * Возвращает фотку детали
     * @param Nomenclature $model
     * @return string
     */
    public static function getMainPhoto($model)
    {
        if (!$model) {
            return '<div>' . '<img class="main_photo" src="' . self::NO_IMAGE . '">' . '</div>';
        };
        $full_path = Url::to('@webroot' . $model->main_photo);
        Yii::info($full_path, 'test');
        if ($model->main_photo && is_file($full_path)) {
            return '<div>' . '<img class="main_photo" src="' . $model->main_photo . '">' . '</div>';
        } else {
            return '<div>' . '<img class="main_photo" src="' . self::NO_IMAGE . '">' . '</div>';
        }
    }

    /**
     * Очищает папку от старых файлов. Оставляет заданное кол-во новых файлов, всё что больше и старее удаляет.
     * @param string $path_dir Путь к директории
     * @param int $num Максимальное кол-во оставляемых файлов
     * @param array $excludes Массив с названиями файлов, не подлежащих удалению
     */
    public static function cleanDirectory($path_dir, $num = 10, $excludes = [])
    {
        Yii::info('Путь к очищаемой директории: ' . $path_dir, 'test');

        $files = array_diff(scandir($path_dir), array('..', '.', '.gitignore'));

        $clean_files = array_diff($files, $excludes);

        $prepared_files = [];

        if (count($clean_files) > $num) {
            foreach ($clean_files as $file_name) {
                $path_file = $path_dir . '/' . $file_name;
                $prepared_files[$file_name] = filemtime($path_file);
            }
            asort($prepared_files);
            $prepared_files = array_keys($prepared_files);

            //Сортируем по убыванию (новые файлы в начале)
            $prepared_files = array_reverse($prepared_files);

            Yii::info($prepared_files, 'test');
            //Проходимся по лишним файлам
            $counter = 0;

            foreach ($prepared_files as $key => $path_file) {
                if ($counter > $num) {
                    unlink($path_dir . '/' . $path_file);
                }
                $counter++;
            }
        }
        Yii::info($files, 'test');
    }
}
