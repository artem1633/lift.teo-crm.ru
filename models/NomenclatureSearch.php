<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * NomenclatureSearch represents the model behind the search form about `app\models\Nomenclature`.
 */
class NomenclatureSearch extends Nomenclature
{
    public $sibir;

    public $liftgou;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'brand_id',
                    'group_id',
                    'type_of_parts_id',
                    'type_of_mechanism_id',
                    'created_by',
                    'reserve',
                    'on_the_way',
                ],
                'integer'
            ],
            [
                [
                    'name',
                    'vendor_code',
                    'own_vendor_code',
                    'main_photo',
                    'additional_photos',
                    'created_date',
                    'updated_date',
                    'description',
                    'cost_price',
                    'weight',
                    'avg_purchase_price',
                    'fact_balance',
                    'sibir',
                    'liftgou',
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Nomenclature::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'brand_id' => $this->brand_id,
            'group_id' => $this->group_id,
            'type_of_parts_id' => $this->type_of_parts_id,
            'type_of_mechanism_id' => $this->type_of_mechanism_id,
            'description' => $this->description,
            'created_by' => $this->created_by,
            'fact_balance' => $this->fact_balance,
            'reserve' => $this->reserve,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'vendor_code', $this->vendor_code])
            ->andFilterWhere(['like', 'own_vendor_code', $this->own_vendor_code])
            ->andFilterWhere(['like', 'main_photo', $this->main_photo])
            ->andFilterWhere(['like', 'cost_price', $this->cost_price])
            ->andFilterWhere(['like', 'weight', $this->weight])
//            ->andFilterWhere(['like', 'additional_photos', $this->additional_photos])
            ->andFilterWhere(['like', 'created_date', $this->created_date])
            ->andFilterWhere(['like', 'updated_date', $this->updated_date])
            ->andFilterWhere(['like', 'avg_purchase_price', $this->avg_purchase_price]);

        return $dataProvider;
    }

    public function managerSearch($params)
    {
        $query = Nomenclature::find()->where(['nomenclature_type' => 'manager']);

//        $query = Nomenclature::find()
//            -> select('n.*')
//            -> from(Nomenclature::tableName() . " n")
//            -> innerJoin(Users::tableName() . " u", "u.permission = '" . "manager" . "' and u.id = n.created_by");
//            -> all();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'brand_id' => $this->brand_id,
            'group_id' => $this->group_id,
            'type_of_parts_id' => $this->type_of_parts_id,
            'type_of_mechanism_id' => $this->type_of_mechanism_id,
            'description' => $this->description,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'vendor_code', $this->vendor_code])
            ->andFilterWhere(['like', 'own_vendor_code', $this->own_vendor_code])
            ->andFilterWhere(['like', 'main_photo', $this->main_photo])
//            ->andFilterWhere(['like', 'additional_photos', $this->additional_photos])
            ->andFilterWhere(['like', 'created_date', $this->created_date])
            ->andFilterWhere(['like', 'cost_price', $this->cost_price])
            ->andFilterWhere(['like', 'weight', $this->weight])
            ->andFilterWhere(['like', 'updated_date', $this->updated_date]);

//        Yii::debug('managers nomenclature', 'test');
//        Yii::debug($dataProvider, 'test');

        return $dataProvider;
    }

    public function techSearch($params): ActiveDataProvider
    {
        $query = Nomenclature::find()->where(['nomenclature_type' => Users::USER_ROLE_TECH_SPECIALIST]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }



        $query->andFilterWhere([
            'id' => $this->id,
            'brand_id' => $this->brand_id,
            'group_id' => $this->group_id,
            'type_of_parts_id' => $this->type_of_parts_id,
            'type_of_mechanism_id' => $this->type_of_mechanism_id,
            'description' => $this->description,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'vendor_code', $this->vendor_code])
            ->andFilterWhere(['like', 'own_vendor_code', $this->own_vendor_code])
            ->andFilterWhere(['like', 'main_photo', $this->main_photo])
            ->andFilterWhere(['like', 'created_date', $this->created_date])
            ->andFilterWhere(['like', 'cost_price', $this->cost_price])
            ->andFilterWhere(['like', 'weight', $this->weight])
            ->andFilterWhere(['like', 'updated_date', $this->updated_date]);

        if ($this->reserve){
            $r_query = clone $query;
            $ids = $r_query
                ->joinWith(['reserves'])
                ->select(['SUM(reserve.count)', 'nomenclature.id'])
                ->andWhere(['>', 'count', 0])
                ->groupBy(['id'])
                ->all();
            $query->andWhere(['IN', 'id', ArrayHelper::getColumn($ids, 'id')]);
        }

        if ($this->on_the_way || $this->fact_balance) {
            $query->joinWith(['detailToArriving dta']);
            if ($this->on_the_way) {
                $query->andWhere(['dta.warehouse' => DetailToArriving::WAREHOUSE_ON_THE_WAY]);
            }
            if ($this->fact_balance) {
                $query->andWhere(['dta.warehouse' => DetailToArriving::WAREHOUSE_BASIS]);
                $query->andWhere(['dta.number' => $this->fact_balance]);
            }
        }

        if($this->sibir == 2){
            $clonedDataProvider = clone $dataProvider;
            $clonedDataProvider->pagination = false;
            $models = $clonedDataProvider->models;
            $pks = [];
            foreach ($models as $model)
            {
                $count = DetailToArriving::getBasisCount($model->id);

                if($count > 0){
                    $pks[] = $model->id;
                }
            }
            if($pks){
                $query->andWhere(['id' => $pks]);
            }
        }

        if($this->liftgou == 2){
            $clonedDataProvider = clone $dataProvider;
            $clonedDataProvider->pagination = false;
            $models = $clonedDataProvider->models;
            $pks = [];
            foreach ($models as $model)
            {
                $count = DetailToArriving::getOnWayCount($model->id);

                if($count > 0){
                    $pks[] = $model->id;
                }
            }
            if($pks){
                $query->andWhere(['id' => $pks]);
            }
        }

        return $dataProvider;
    }
}
