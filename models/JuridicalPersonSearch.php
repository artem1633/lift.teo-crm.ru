<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * JuridicalPersonSearch represents the model behind the search form about `app\models\JuridicalPerson`.
 */
class JuridicalPersonSearch extends JuridicalPerson
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'contractor_id'], 'integer'],
            [['name', 'full_name', 'inn', 'kpp', 'bik', 'pay_off_account', 'corr_account', 'contractor_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = JuridicalPerson::find();

        $query->joinWith(['contractor']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'contractor_id' => $this->contractor_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'inn', $this->inn])
            ->andFilterWhere(['like', 'kpp', $this->kpp])
            ->andFilterWhere(['like', 'bik', $this->bik])
            ->andFilterWhere(['like', 'pay_off_account', $this->pay_off_account])
            ->andFilterWhere([
                'OR',
                ['like', 'contractor.name', $this->contractor_name],
                ['like', 'contractor.full_name', $this->contractor_name]
            ])
            ->andFilterWhere(['like', 'corr_account', $this->corr_account]);

        return $dataProvider;
    }
}
