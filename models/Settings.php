<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "settings".
 *
 * @property int $id
 * @property string $key
 * @property string $name
 * @property string $value
 * @property string $note
 */
class Settings extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['note'], 'string'],
            [['key', 'name', 'value'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Key',
            'name' => 'Name',
            'value' => 'Value',
            'note' => 'Note',
        ];
    }

    /**
     * @param $key
     * @return mixed|null
     */
    public static function getValueByKey($key)
    {
        if (!$key) return null;
        return self::find()->where(['key' => $key])->one()->value ?? null;
    }

    /**
     * Изменение настройки
     * @param string $key
     * @param string $value
     * @return bool
     */
    public static function setValueByKey(string $key, string $value): bool
    {
        if (!$key) return false;
        $item = Settings::findOne(['key' => $key]);
        if (!$item) return false;
        $item->value = $value;
        if (!$item->save()){
            Yii::error($item->errors, '_error');
            return false;
        }
        return true;
    }

    public static function getRefreshToken()
    {
        return self::find()->where(['key' => 'google_refresh_token'])->one()->value ?? null;
    }

    public static function getBackupGoogleFolder()
    {
        return self::find()->where(['key' => 'backup_folder_in_google_drive'])->one()->value ?? null;
    }
}
