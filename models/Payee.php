<?php

namespace app\models;

use app\models\query\PayeeQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "payee".
 *
 * @property int $id
 * @property string $name Наименование
 * @property string $full_name Полное наименование
 * @property int $city_id Город
 * @property string $address Адрес
 * @property string $phone Телефон
 * @property string $contact_person Контактное лицо
 * @property string $email Email
 * @property string $inn ИНН
 * @property string $requisites Реквизиты
 * @property string $comment Комментарии
 * @property string $created_date Дата создания и время создания
 * @property string $updated_date Дата и время редактирования
 *
 * @property Cities $city
 */
class Payee extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payee';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city_id'], 'integer'],
            [['requisites', 'comment', 'inn'], 'string'],
            [
                ['name', 'full_name', 'address', 'phone', 'contact_person', 'email'],
                'string',
                'max' => 255
            ],
            [
                ['city_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Cities::className(),
                'targetAttribute' => ['city_id' => 'id']
            ],
            [['inn', 'name'], 'required'],
            [['inn'], 'unique', 'message' => 'ИНН уже используется одним из получателей']
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'full_name' => 'Полное наименование',
            'city_id' => 'Город',
            'address' => 'Адрес',
            'phone' => 'Телефон',
            'contact_person' => 'Контактное лицо',
            'email' => 'Email',
            'inn' => 'ИНН',
            'requisites' => 'Реквизиты',
            'comment' => 'Комментарии',
            'created_date' => 'Дата создания и время создания',
            'updated_date' => 'Дата и время редактирования',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id']);
    }

    /**
     * {@inheritdoc}
     * @return PayeeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PayeeQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        if ($insert){
            $this->created_date = date('Y-m-d H:i:s', time());
        }
        $this->updated_date = date('Y-m-d H:i:s', time());

        $this->phone = str_replace(' ', '', $this->phone);

        return parent::beforeSave($insert);
    }

    public function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }
}
