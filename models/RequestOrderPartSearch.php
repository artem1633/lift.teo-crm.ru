<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\OrderPart;

/**
 * OrderPartSearch represents the model behind the search form about `app\models\OrderPart`.
 */
class RequestOrderPartSearch extends OrderPart
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'request_id', 'nomenclature_id', 'detail_id', 'delivery_time_id', 'suppliers_id', 'buy_price', 'sell_price', 'quantity', 'measure_id', 'created_by'], 'integer'],
            [['comment', 'in_china', 'files_location', 'created_date', 'updated_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($id)
    {
        $query = OrderPart::find() -> where(['request_id' => $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($id);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}
