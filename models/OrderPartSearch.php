<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\OrderPart;
use yii\db\ActiveQuery;
use yii\helpers\VarDumper;

/**
 * OrderPartSearch represents the model behind the search form about `app\models\OrderPart`.
 */
class OrderPartSearch extends OrderPart
{
    public $detailName;
    public $nomenclatureName;
    public $ownVendorCode;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'request_id', 'nomenclature_id', 'detail_id', 'delivery_time_id', 'suppliers_id', 'buy_price', 'sell_price', 'quantity', 'measure_id', 'created_by'], 'integer'],
            [['comment', 'in_china', 'files_location', 'created_date', 'updated_date', 'detailName', 'nomenclatureName', 'ownVendorCode'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrderPart::find();

//        $query->joinWith(['detail d' => function (ActiveQuery $q) {
//            VarDumper::dump($this->detailName, 10, true); die;
//
//            $q->andWhere('d.name LIKE "%' . $this->detailName . '%"');
//        }]);
//        $query->joinWith(['nomenclature n' => function (ActiveQuery $q) {
//            $q->andWhere('n.name LIKE "%' . $this->nomenclatureName . '%"')
//            ->andWhere('n.own_vendor_code LIKE "%' . $this->ownVendorCode . '%"');
//        }]);
//        $query->joinWith(['nomenclature'])->alias('n');

        $query
            ->joinWith(['nomenclature n'])
            ->joinWith(['detail d'])
            ->andWhere('d.name LIKE "%' . $this->detailName . '%"')
            ->andWhere('n.name LIKE "%' . $this->nomenclatureName . '%"')
            ->andWhere('d.own_vendor_code LIKE "%' . $this->ownVendorCode . '%"');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['detail'] = [
            'asc' => [Nomenclature::tableName() . '.name' => SORT_ASC],
            'desc' => [Nomenclature::tableName() . '.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['nomenclature'] = [
            'asc' => [Nomenclature::tableName() . '.name' => SORT_ASC, Nomenclature::tableName() . '.own_vendor_code' => SORT_ASC],
            'desc' => [Nomenclature::tableName() . '.name' => SORT_DESC, Nomenclature::tableName() . '.own_vendor_code' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'request_id' => $this->request_id,
            'nomenclature_id' => $this->nomenclature_id,
            'detail_id' => $this->detail_id,
            'delivery_time_id' => $this->delivery_time_id,
            'suppliers_id' => $this->suppliers_id,
            'buy_price' => $this->buy_price,
            'sell_price' => $this->sell_price,
            'quantity' => $this->quantity,
            'measure_id' => $this->measure_id,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'in_china', $this->in_china])
            ->andFilterWhere(['like', 'files_location', $this->files_location])
            ->andFilterWhere(['like', 'created_date', $this->created_date])
            ->andFilterWhere(['like', 'updated_date', $this->updated_date])
            ->andFilterWhere(['like', 'd.name', $this->detailName])
            ->andFilterWhere(['like', 'n.name', $this->nomenclatureName])
            ->andFilterWhere(['like', 'd.own_vendor_code', $this->ownVendorCode]);

        return $dataProvider;
    }
}
