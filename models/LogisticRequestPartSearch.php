<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * LogisticRequestPartSearch represents the model behind the search form about `app\models\LogisticRequestPart`.
 */
class LogisticRequestPartSearch extends LogisticRequestPart
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['id', 'logistic_request_id', 'nomenclature_id', 'supplier_id', 'logistic_status_id', 'measure_id', 'arriving_id'],
                'integer'
            ],
            [['num'], 'number'],
            [
                [
                    'comment',
                    'request_number',
                    'arriving_id',
                    'contract_date',
                    'order_date',
                    'ship_date',
                    'receipt_date_manch',
                    'receipt_date_novosib',
                    'client_ship_date',
                    'tn_tk_rus',
                    'vendor_code',
                    'nomenclature_name',
                    'supplier_name',
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LogisticRequestPart::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query
                ->joinWith(['nomenclature'])
                ->joinWith(['supplier sup']),
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
//                'measure_id' => [
//                    'asc' => ['measure_id' => SORT_ASC],
//                    'desc' => ['measure_id' => SORT_DESC],
//                    'label' => 'Мера',
//                    'default' => SORT_ASC
//                ],
                'vendor_code' => [
                    'asc' => ['nomenclature.own_vendor_code' => SORT_ASC],
                    'desc' => ['nomenclature.own_vendor_code' => SORT_DESC],
//                    'label' => 'Артикул',
//                    'default' => SORT_ASC
                ],
                'nomenclature_name' => [
                    'asc' => ['nomenclature.name' => SORT_ASC],
                    'desc' => ['nomenclature.name' => SORT_DESC],
                ],
                'supplier_name' => [
                    'asc' => ['sup.name' => SORT_ASC, 'sup.full_name' => SORT_ASC],
                    'desc' => ['sup.name' => SORT_DESC, 'sup.full_name' => SORT_DESC],
                ],
                'nomenclature_id',
                'num',
                'comment',
                'request_number',
                'arriving_id',
                'arriving_info',
                'supplier_id',
                'logistic_status_id',
                'contract_date',
                'order_date',
                'ship_date',
                'receipt_date_manch',
                'receipt_date_novosib',
                'client_ship_date',
                'tn_tk_rus',
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'logistic_request_id' => $this->logistic_request_id,
            'nomenclature_id' => $this->nomenclature_id,
            'num' => $this->num,
            'supplier_id' => $this->supplier_id,
            'logistic_status_id' => $this->logistic_status_id,
            'ship_date' => $this->ship_date,
            'receipt_date_manch' => $this->receipt_date_manch,
            'receipt_date_novosib' => $this->receipt_date_novosib,
            'client_ship_date' => $this->client_ship_date,
            'arriving_id' => $this->arriving_id,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'request_number', $this->request_number])
            ->andFilterWhere(['like', 'contract_date', $this->contract_date])
            ->andFilterWhere(['like', 'order_date', $this->order_date])
            ->andFilterWhere(['like', 'tn_tk_rus', $this->tn_tk_rus])
            ->andFilterWhere(['like', 'nomenclature.own_vendor_code', $this->vendor_code])
            ->andFilterWhere(['like', 'nomenclature.name', $this->nomenclature_name])
            ->andFilterWhere(['like', 'sup.name', $this->supplier_name]);

        return $dataProvider;
    }
}
