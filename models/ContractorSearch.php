<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ContractorSearch represents the model behind the search form about `app\models\Contractor`.
 */
class ContractorSearch extends Contractor
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id', 'city_id', 'manager_id', 'created_by'], 'integer'],
            [
                [
                    'name',
                    'full_name',
                    'address',
                    'phone',
                    'contact_person',
                    'email',
                    'requisites',
                    'comment',
                    'do_not_touch',
                    'created_date',
                    'updated_date',
                    'link_1c',
                    'inn'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Contractor::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'city_id' => $this->city_id,
            'manager_id' => $this->manager_id,
            'created_date' => $this->created_date,
//            'updated_date' => $this->updated_date,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'contact_person', $this->contact_person])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'requisites', $this->requisites])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'inn', $this->inn])
            ->andFilterWhere(['like', 'link_1c', $this->link_1c])
            ->andFilterWhere(['like', 'do_not_touch', $this->do_not_touch]);

        if($this->updated_date){
//            $date = explode(' - ', $this->updated_date);
            $query->andWhere(['between', 'updated_date', $this->updated_date.' 00:00:00', $this->updated_date.' 23:59:59']);
        }

        return $dataProvider;
    }

}
