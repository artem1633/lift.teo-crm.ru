<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "actions".
 *
 * @property int $id
 * @property int $contractor_id Контрагент
 * @property string $description Описание
 * @property int $request_id Заявка
 * @property int $manager_id Менеджер
 * @property string $created_date Дата создания и время создания
 * @property string $updated_date Дата и время редактирования
 * @property int $created_by Кто создал
 *
 * @property Contractor $contractor
 * @property Request $request
 */
class Actions extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'actions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['contractor_id', 'description'], 'required'],
            [['contractor_id', 'request_id', 'manager_id', 'created_by'], 'integer'],
            [['description'], 'string'],
            [['created_date', 'updated_date'], 'string', 'max' => 255],
            [['contractor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contractor::className(), 'targetAttribute' => ['contractor_id' => 'id']],
            [['request_id'], 'exist', 'skipOnError' => true, 'targetClass' => Request::className(), 'targetAttribute' => ['request_id' => 'id']],
            [['manager_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['manager_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contractor_id' => 'Контрагент',
            'description' => 'Описание',
            'request_id' => 'Заявка',
            'manager_id' => 'Менеджер',
            'created_date' => 'Дата Создания',
            'updated_date' => 'Дата Редактирования',
            'created_by' => 'Создал',
            'contractorName' => 'Контрагент',
            'managerName' => 'Менеджер',
        ];
    }

    public function beforeSave($insert)
    {
        if ($insert){
            $contractor_model = Contractor::findOne($this->contractor_id) ?? null;
            if ($contractor_model){
                $contractor_model->updated_date = date('Y-m-d H:i', time());
                if (!$contractor_model->save()){
                    Yii::$app->session->setFlash('warning', 'Не удалось обновить дату последнего действия контрагента');
                    Yii::error($contractor_model->errors, '_error');
                }
            }
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'contractor_id']);
    }

    /**
     * Геттер для получения имени контрагента
     * @return string
     */
    public function getContractorName()
    {
        return $this->contractor->name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(Request::className(), ['id' => 'request_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(Users::className(), ['id' => 'manager_id']);
    }

    /**
     * Геттер получения имени менеджера
     * @return mixed
     */
    public function getManagerName()
    {
        return $this->getManager()->fio;
    }
}
