<?php

namespace app\models;

use app\models\query\PaymentQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "payment".
 *
 * @property int $id
 * @property int $contractor_id Контрагент
 * @property string $pay_date Дата
 * @property double $sum Сумма
 * @property int $currency_id Валюта
 * @property string $created_at
 * @property int $exchange_rate Курс
 *
 * @property Contractor $contractor
 * @property Currency $currency
 */
class Payment extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['contractor_id', 'currency_id', 'exchange_rate'], 'integer'],
            [['pay_date', 'created_at'], 'safe'],
            [['sum'], 'number'],
            [['contractor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contractor::class, 'targetAttribute' => ['contractor_id' => 'id']],
            [['currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::class, 'targetAttribute' => ['currency_id' => 'id']],
            [['contractor_id', 'currency_id', 'pay_date', 'sum'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contractor_id' => 'Контрагент',
            'pay_date' => 'Дата',
            'sum' => 'Сумма',
            'currency_id' => 'Валюта',
            'created_at' => 'Created At',
            'exchange_rate' => 'Курс',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->pay_date){
            $this->pay_date = date('Y-m-d H:i:s', strtotime($this->pay_date));
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'contractor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * {@inheritdoc}
     * @return PaymentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PaymentQuery(get_called_class());
    }
}
