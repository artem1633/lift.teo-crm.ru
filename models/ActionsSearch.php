<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Actions;

/**
 * ActionsSearch represents the model behind the search form about `app\models\Actions`.
 */
class ActionsSearch extends Actions
{
    public $contractorName;
    public $managerName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'contractor_id', 'request_id', 'manager_id', 'created_by'], 'integer'],
            [['description', 'created_date', 'updated_date'], 'safe'],
            [['contractorName', 'managerName'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Actions::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['contractorName'] = [
            'asc' => [Contractor::tableName() . '.name' => SORT_ASC],
            'desc' => [Contractor::tableName() . '.name' => SORT_DESC],
            'label' => 'Контрагент'
        ];
        $dataProvider->sort->attributes['managerName'] = [
            'asc' => [Users::tableName() . '.fio' => SORT_ASC],
            'desc' => [Users::tableName() . '.fio' => SORT_DESC],
            'label' => 'Менеджер'
        ];

        \Yii::info('contractorName = ' . $this->contractorName, 'test');
        \Yii::info('managerName = ' . $this->managerName, 'test');

        if (!($this->load($params) && $this->validate())) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            $query->joinWith(['contractor']);
//            $query->joinWith(['manager']);
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'contractor_id' => $this->contractor_id,
            'request_id' => $this->request_id,
            'manager_id' => $this->manager_id,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description]);
//            ->andFilterWhere(['like', 'created_date', $this->created_date])
//            ->andFilterWhere(['like', 'updated_date', $this->updated_date]);

//        Фильтр по Контрагенту
        $query->joinWith(['contractor' => function ($q) {
            $q->where(Contractor::tableName() . '.name LIKE "%' . $this->contractorName . '%"');
        }]);
//         Фильтр по Менеджеру
        $query->joinWith(['manager' => function ($q) {
            $q->where(Users::tableName() . '.fio LIKE "%' . $this->managerName . '%"');
        }]);

        if($this->created_date){
//            $date = explode(' - ', $this->created_date);
            $query->andWhere(['between',  'actions.created_date', $this->created_date.' 00:00:00', $this->created_date.' 23:59:59']);
        }

        if($this->updated_date){
//            $date = explode(' - ', $this->updated_date);
            $query->andWhere(['between', 'actions.updated_date', $this->updated_date.' 00:00:00', $this->updated_date.' 23:59:59']);
        }

        return $dataProvider;
    }

    public function searchByContractor($id)
    {
        $query = Actions::find()->where(['contractor_id' => $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($id);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }

    public function searchByRequest($id)
    {
        $query = Actions::find()->where(['request_id' => $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($id);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}
