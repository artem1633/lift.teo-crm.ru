<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * DetailToRealizationSearch represents the model behind the search form about `app\models\DetailToRealization`.
 */
class DetailToRealizationSearch extends DetailToRealization
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'realization_id', 'detail_id'], 'integer'],
            [['number', 'price', 'total_amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DetailToRealization::find();

        Yii::info($params, 'test');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'realization_id' => $this->realization_id,
            'detail_id' => $this->detail_id,
            'number' => $this->number,
            'price' => $this->price,
            'total_amount' => $this->total_amount,
        ]);

        return $dataProvider;
    }
}
