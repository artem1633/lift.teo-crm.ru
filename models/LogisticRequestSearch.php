<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * LogisticRequestSearch represents the model behind the search form about `app\models\LogisticRequest`.
 */
class LogisticRequestSearch extends LogisticRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'payee_id',
                    'contractor_id',
                    'responsible_id',
                    'logistic_master_status_id',
                    'logistic_status_id',
                    'juridical_person_id',
                ],
                'integer'
            ],
            [
                [
                    'account_number',
                    'invoice_date',
                    'payed_date',
                    'payee_name',
                    'contractor_name',
                    'responsible_name',
                    'juridical_person_name',
                    'invoices',
                    'requests_numbers',
                    'client_ship_date',
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LogisticRequest::find()->orderBy(['created_at' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query
                ->joinWith(['payee'])
                ->joinWith(['contractor'])
                ->joinWith(['responsible'])
                ->joinWith(['juridicalPerson'])
//                ->joinWith(['logisticRequestParts'])
//                ->joinWith(['arriving'])
            ,
        ]);

        Yii::info($params, 'test');

        if (isset($params['LogisticRequestSearch'])){
            if ($params['LogisticRequestSearch']['invoices'] || $params['LogisticRequestSearch']['requests_numbers']){
                //ON включает в себя связи с logistic_request_parts и arriving
                $dataProvider->query->joinWith(['arriving']);
            }

        }

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'payee_name' => [
                    'asc' => ['payee.name' => SORT_ASC],
                    'desc' => ['payee.name' => SORT_DESC],
//                    'label' => 'Артикул',
//                    'default' => SORT_ASC
                ],
                'invoices',
                'contractor_name' => [
                    'asc' => ['contractor.name' => SORT_ASC],
                    'desc' => ['contractor.name' => SORT_DESC],
                ],
                'responsible_name' => [
                    'asc' => ['users.name' => SORT_ASC],
                    'desc' => ['users.name' => SORT_DESC],
                ],
                'logistic_status_id',
                'logistic_master_status_id',
                'invoice_date',
                'payed_date',
                'account_number',
                'juridical_person_id',
                'requests_numbers',
                'juridical_person_name' => [
                    'asc' => ['juridical_person.name' => SORT_ASC, 'juridical_person.full_name' => SORT_ASC],
                    'desc' => ['juridical_person.name' => SORT_DESC, 'juridical_person.full_name' => SORT_DESC],
                ],
                'client_ship_date'
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'logistic_request.id' => $this->id,
            'logistic_request.payee_id' => $this->payee_id,
            'logistic_request.contractor_id' => $this->contractor_id,
            'logistic_request.responsible_id' => $this->responsible_id,
            'logistic_request.logistic_master_status_id' => $this->logistic_master_status_id,
            'logistic_request.logistic_status_id' => $this->logistic_status_id,
//            'invoice_date' => $this->invoice_date,
//            'payed_date' => $this->payed_date,
            'logistic_request.juridical_person_id' => $this->juridical_person_id,
        ]);
        \Yii::info('Invoice date: ' . date('Y-m-d', strtotime($this->invoice_date)), 'test');

//        if ($this->invoice_date) {
//            $this->invoice_date = $this->prepareDate($this->invoice_date);
//        }

//        if ($this->payed_date) {
//            $this->payed_date = $this->prepareDate($this->payed_date);
//        }

        $query->andFilterWhere(['like', 'logistic_request.account_number', $this->account_number]);
        $query->andFilterWhere(['like', 'users.fio', $this->responsible_name]);
//        $query->andFilterWhere(['like', 'invoice_date', $this->invoice_date]);
//        $query->andFilterWhere(['like', 'payed_date', $this->payed_date]);

        $query->andFilterWhere([
            'OR',
            ['like', 'contractor.name', $this->contractor_name],
            ['like', 'contractor.full_name', $this->contractor_name]
        ]);

        $query->andFilterWhere([
            'OR',
            ['like', 'payee.name', $this->payee_name],
            ['like', 'payee.full_name', $this->payee_name]
        ]);

        $query->andFilterWhere([
            'OR',
            ['like', 'juridical_person.name', $this->juridical_person_name],
            ['like', 'juridical_person.full_name', $this->juridical_person_name]
        ]);

//        $query->andFilterWhere(['like', 'lrp.invoice_number', $this->invoices]);
        $query->andFilterWhere(['like', 'arriving.number', $this->invoices]);

        $query->andFilterWhere(['like', 'logistic_request_part.request_number', $this->requests_numbers]);

        if($this->invoice_date){
//            $date = explode(' - ', $this->invoice_date);
            $query->andWhere(['between', 'invoice_date', $this->invoice_date.' 00:00:00', $this->invoice_date.' 23:59:59']);
        }

        if($this->payed_date){
//            $date = explode(' - ', $this->payed_date);
            $query->andWhere(['between', 'payed_date', $this->payed_date.' 00:00:00', $this->payed_date.' 23:59:59']);
        }

        if($this->client_ship_date){
//            $date = explode(' - ', $this->client_ship_date);
            $query->andWhere(['between', 'client_ship_date', $this->client_ship_date.' 00:00:00', $this->client_ship_date.' 23:59:59']);
        }

        return $dataProvider;
    }

    private function prepareDate($date)
    {
        if (strpos($date, '.') || strpos($date, ',')) {
            $date = str_replace(',', '.', $date);

            $date_parts = explode('.', $date);

            if (count($date_parts) > 2) {
                $date = date('Y-m-d', strtotime($date));
            } else {
                $date = $date_parts[1] . '-' . $date_parts[0];
            }
        }

        return $date;
    }
}
