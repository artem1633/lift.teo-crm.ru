<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $fio ФИО
 * @property string $telephone Телефон
 * @property string $permission Должность
 * @property string $login Логин
 * @property string $password Пароль
 * @property string $email Имайл
 * @property string $access Доступ
 */
class Users extends ActiveRecord
{
    const USER_ROLE_ADMIN = 'administrator';
    const USER_ROLE_MANAGER = 'manager';
    const USER_ROLE_USER = 'user';
    const USER_ROLE_TECH_SPECIALIST = 'tech_specialist';
    const USER_ROLE_LOGIST = 'logist';

    public $new_password;
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['fio', 'telephone', 'permission', 'login', 'password', 'new_password', 'email', 'access'],
                'string',
                'max' => 255
            ],
            [['login', 'email'], 'unique'],
            [['fio', 'password', 'login', 'email'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'ФИО',
            'telephone' => 'Телефон',
            'permission' => 'Должность',
            'login' => 'Логин',
            'email' => 'Email',
            'access' => 'Доступ',
            'password' => 'Пароль',
            'new_password' => 'Новый пароль',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->password = md5($this->password);
        }

        if ($this->new_password != null) {
            $this->password = md5($this->new_password);
        }
        return parent::beforeSave($insert);
    }

    public function getRoleList()
    {
        return ArrayHelper::map([
            ['id' => self::USER_ROLE_ADMIN, 'name' => 'Администратор',],
            ['id' => self::USER_ROLE_MANAGER, 'name' => 'Менеджер',],
            ['id' => self::USER_ROLE_USER, 'name' => 'Пользователь',],
            ['id' => self::USER_ROLE_TECH_SPECIALIST, 'name' => 'Тех.Специалист',],
            ['id' => self::USER_ROLE_LOGIST, 'name' => 'Логист',],
        ], 'id', 'name');
    }

    /**
     * Получает описание роли
     * @return bool|string
     */
    public function getRoleDescription()
    {
        if (self::USER_ROLE_ADMIN == $this->permission) {
            return 'Администратор';
        }
        if (self::USER_ROLE_MANAGER == $this->permission) {
            return 'Менеджер';
        }
        if (self::USER_ROLE_USER == $this->permission) {
            return 'Пользователь';
        }
        if (self::USER_ROLE_TECH_SPECIALIST == $this->permission) {
            return 'Тех.Специалист';
        }
        if (self::USER_ROLE_LOGIST == $this->permission) {
            return 'Логист';
        }
        return false;
    }

    /**
     * Получает описание роли по наименованию роли
     * @param string $role_name Имя роли
     * @return string
     */
    public static function getRoleDescriptionByRoleName($role_name)
    {
        switch ($role_name) {
            case self::USER_ROLE_ADMIN:
                $description = 'Администратор';
                break;
            case self::USER_ROLE_MANAGER:
                $description = 'Менеджер';
                break;
            case self::USER_ROLE_TECH_SPECIALIST:
                $description = 'Тех.Специалист';
                break;
            case self::USER_ROLE_LOGIST:
                $description = 'Логист';
                break;
            default:
                $description = 'Пользователь';
        }
        return $description;
    }

    /**
     * Проверят, является ли пользователь админом
     * @return bool
     */
    public static function isAdmin()
    {
        $permission = self::getPermission();
        if ($permission == self::USER_ROLE_ADMIN) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Проверят, является ли пользователь менеджером
     * @return bool
     */
    public static function isManager()
    {
        $premission = self::getPermission();
        if ($premission == self::USER_ROLE_MANAGER) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return bool
     */
    public static function isTech()
    {
        $premission = self::getPermission();
        if ($premission == self::USER_ROLE_TECH_SPECIALIST) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Проверят, является ли текущий пользователь "Пользователем (user)"
     * @return bool
     */
    public static function isUser()
    {
        $permission = self::getPermission();
        if ($permission == self::USER_ROLE_USER) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Проверят, является ли текущий пользователь "Логистом (logist)"
     * @return bool
     */
    public static function isLogist()
    {
        $permission = self::getPermission();
        if ($permission == self::USER_ROLE_LOGIST) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Получает текущие права (permission)
     * @return mixed
     */
    public static function getPermission()
    {
        $model = Users::find()->where(['id' => Yii::$app->user->identity->id])->one();

        if (isset($model->permission)) {
            return $model->permission;
        }

        return false;
    }

    public function getActiveManagers()
    {
        return ArrayHelper::map(self::find()
            ->andWhere(['permission' => self::USER_ROLE_MANAGER])
            ->andWhere(['access' => 'Вкл'])
            ->all(), 'id', 'fio');
    }
}
