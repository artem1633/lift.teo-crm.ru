<?php

namespace app\models;

use app\models\query\LogisticMasterStatusQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "logistic_master_status".
 *
 * @property int $id
 * @property string $name Наименование
 * @property string $description Описание
 * @property string $icon Иконка
 * @property string $color Цвет
 *
 *
 * @property LogisticRequest[] $logisticRequests
 */
class LogisticMasterStatus extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'logistic_master_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description'], 'string', 'max' => 255],
            [['icon', 'color'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'icon' => 'Иконка',
            'color' => 'Цвет',
            'description' => 'Описание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogisticRequests()
    {
        return $this->hasMany(LogisticRequest::className(), ['logistic_master_status_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return LogisticMasterStatusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LogisticMasterStatusQuery(get_called_class());
    }

    public function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }
}
