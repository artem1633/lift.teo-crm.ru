<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;

/**
 * DetailToArrivingSearch represents the model behind the search form about `app\models\DetailToArriving`.
 */
class DetailToArrivingSearch extends DetailToArriving
{
    public $detail_name;
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id', 'arriving_id', 'detail_id', 'currency_id', 'warehouse'], 'integer'],
            [['number', 'price', 'total_amount_rub', 'total_amount_currency', 'nds_amount'], 'number'],
            ['detail_name', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios(): array
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = DetailToArriving::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'arriving_id' => $this->arriving_id,
            'detail_id' => $this->detail_id,
            'number' => $this->number,
            'price' => $this->price,
            'total_amount_rub' => $this->total_amount_rub,
            'total_amount_currency' => $this->total_amount_currency,
            'nds_amount' => $this->nds_amount,
            'currency_id' => $this->currency_id,
            'warehouse' => $this->warehouse,
        ]);

        $query->joinWith(['detail' => function (Query $q) {
            $q->where(Nomenclature::tableName() . '.name LIKE "%' . $this->detail_name . '%"');
        }]);

        return $dataProvider;
    }
}
