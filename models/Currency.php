<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "_currency".
 *
 * @property int $id
 * @property string $name Наименование
 * @property string $icon
 * @property double $exchange_rate Курс
 */
class Currency extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '_currency';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['id', 'integer'],
            [['name'], 'string', 'max' => 255],
            [['icon'], 'string', 'max' => 10],
            ['exchange_rate', 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'icon' => 'Иконка',
            'exchange_rate' => 'Курс',
        ];
    }

    public static function getCurrencyName($id)
    {
       return self::findOne($id)->name;
    }

    public static function getCurrencyIcon($id)
    {
        return self::findOne($id)->icon;
    }

    public static function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }

    /**
     * ЗАписывает курс валюты
     * @param int $currency ID валюты
     * @param double $exchange_rate Курс
     * @return bool
     */
    public static function setExchange($currency, $exchange_rate)
    {
        $model = self::findOne($currency);
        $model->exchange_rate = $exchange_rate;

        if (!$model->save()){
            \Yii::error($model->errors, '_error');
            return false;
        }
        return true;
    }

    public static function getConvertedAmount($amount, $currency)
    {
        $exchange_rate = self::findOne($currency)->exchange_rate ?? 1;
        return round($amount * $exchange_rate, 2);
    }
}
