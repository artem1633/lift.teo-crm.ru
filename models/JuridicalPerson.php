<?php

namespace app\models;

use app\models\query\JuridicalPersonQuery;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "juridical_person".
 *
 * @property int $id
 * @property string $name Наименование
 * @property string $full_name Полное наименование
 * @property string $inn ИНН
 * @property string $kpp КПП
 * @property string $bik БИК
 * @property string $pay_off_account Расчетный счет
 * @property string $corr_account Кор. счет
 * @property int $contractor_id Контрагент
 * @property int $contractor_name Наименование контрагента
 * @property string $link_1c Ссылка из 1С
 *
 * @property Contractor $contractor
 */
class JuridicalPerson extends ActiveRecord
{
    public $contractor_name;

    public $adv_name; //Наименование + ИНН

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'juridical_person';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['contractor_id'], 'integer'],
            [
                ['name', 'full_name', 'inn', 'kpp', 'bik', 'pay_off_account', 'corr_account', 'link_1c'],
                'string',
                'max' => 255
            ],
            [
                ['contractor_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Contractor::className(),
                'targetAttribute' => ['contractor_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'full_name' => 'Полное наименование',
            'inn' => 'ИНН',
            'kpp' => 'КПП',
            'bik' => 'БИК',
            'pay_off_account' => 'Расчетный счет',
            'corr_account' => 'Кор. счет',
            'contractor_id' => 'Контрагент',
            'contractor_name' => 'Контрагент',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'contractor_id']);
    }

    /**
     * {@inheritdoc}
     * @return JuridicalPersonQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new JuridicalPersonQuery(get_called_class());
    }

    public function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }

    public function getListWithInn()
    {
        //Юр. лица с ИНН
        $result1 = self::find()
            ->select('`id`, CONCAT(`name`, " (ИНН: ", `inn`, ")") AS `adv_name`')
            ->andWhere(['OR', ['IS NOT', 'inn', null], ['<>', 'inn', ""]])
            ->all();

//        Yii::info($result1, 'test');

        //Юрлица без ИНН
        $result2 = self::find()
            ->select('`id`, `name` AS `adv_name`')
            ->andWhere(['OR', ['IS', 'inn', null], ['=', 'inn', ""]])
            ->all();

//        Yii::info($result2, 'test');


        $arr1 = ArrayHelper::map($result1, 'id', 'adv_name');
        $arr2 = ArrayHelper::map($result2, 'id', 'adv_name');

//        $result_arr = array_merge($arr1,$arr2);
        $result_arr = $arr1 + $arr2;

//        Yii::info($result_arr, 'test');

        return $result_arr;
    }

    /**
     * Импорт файла
     * @param string $file Путь к файлу
     * @return array
     */
    public function importFile($file)
    {
        $xml_string = file_get_contents($file);
        $xml = simplexml_load_string($xml_string);
        $json = json_encode($xml);
        $data_array = json_decode($json, true);
        $errors = [];
        $warnings = [];
        $count = 0;
        $person_count = 0;
        $person_missing = 0;

//        Yii::info($data_array, 'test');

        $persons = $data_array['Body']['Справочник.Контрагенты'] ?? null;

        if (!$persons) {
            return ['success' => 'false', 'error' => 'Не найдена информация о юридических лицах'];
        }

        /** @var JuridicalPerson $person */
        foreach ($persons as $person) {
//            Yii::info($person, 'test');

            $count += 1;

            /** @var string $sale_date Дата заказа в формате 2019-02-15T16:25:00 */
            $info = $person['КлючевыеСвойства'] ?? null;

            $person_link1c = $info['Ссылка'] ?? null;
            $name = $info['Наименование'] ?? null;
            $full_name = $info['НаименованиеПолное'] ?? null;
            $inn = $info['ИНН'] ?? null;
            $kpp = $info['КПП'] ?? null;
            $bik = $info['БИК'] ?? null;

            //Проверяем лицо на существование
            $isExist = $this->existsPerson($person_link1c, $inn);
            if ($person_link1c && $isExist !== false) {
                array_push($warnings, $isExist);
                $person_missing += 1;
                continue;
            }

//            Yii::info('Импортируемое лицо: ' . $name, 'test');
//            Yii::info('ИНН импортируемого лица' . $inn, 'test');

            //Добавляем юр лицо
            $juridical_person_model = new JuridicalPerson([
                'name' => $name,
                'full_name' => $full_name,
                'inn' => $inn,
                'kpp' => $kpp,
                'bik' => $bik,
                'link_1c' => $person_link1c
            ]);

            if (!$juridical_person_model->save()) {
                Yii::error($juridical_person_model->errors, '_error');
                array_push($errors, json_encode($juridical_person_model->errors));
                $person_missing += 1;
                continue;
            }
            $person_count += 1;
        }

        if (count($errors) > 0) {
            $error = implode(', <br>', $errors);
        } else {
            $error = '';
        }

        if (count($warnings) > 0) {
            $warning = implode(', <br>', $warnings);
        } else {
            $warning = '';
        }

        return [
            'success' => 'true',
            'errors' => $error,
            'warnings' => $warning,
            'count' => $count,
            'person_count' => $person_count,
            'person_missing' => $person_missing,
        ];
    }

    /**
     * Ищет юр лицо по 1С-ной сслыке
     * @param string $link 1C ссылка
     * @param string $inn ИНН
     * @return bool
     */
    private function existsPerson($link, $inn = null)
    {
        $message = '';

        $link_exist =  self::find()
            ->andWhere(['link_1c' => $link])->exists();

        if ($inn){
            $inn_exist = self::find()
                ->andWhere(['inn' => $inn])->exists();
        } else {
            $inn_exist = false;
        }

        if ($link_exist){
            Yii::warning('Совпадение по ссылке', 'test');
            $message = 'Ссылка 1C: ' . $link . ' - Совпадение по ссылке из 1С. Пропущено';
        }
        if ($inn_exist){
            Yii::warning('Совпадение по ИНН', 'test');
            $message = 'ИНН: ' . $inn . ' - Совпадение по ИНН. Пропущено';
        }

        if ($link_exist || $inn_exist){
            return $message;
        }

        return false;
    }

}
