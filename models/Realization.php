<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "realization".
 *
 * @property int $id
 * @property string $date Дата реализации
 * @property int $contractor_id Контрагент (юр. лицо)
 * @property string $note Примечание
 * @property string $link_1c Примечание
 *
 * @property JuridicalPerson $contractor
 * @property Nomenclature $product
 */
class Realization extends ActiveRecord
{
    public $total_amount; //Общая сумма по документу релаизации

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'realization';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['contractor_id', 'warehouse_id'], 'integer'],
            [['note', 'link_1c', 'date'], 'string'],
            [
                ['contractor_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => JuridicalPerson::className(),
                'targetAttribute' => ['contractor_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Дата',
            'contractor_id' => 'Контрагент',
            'note' => 'Примечание',
            'link_1c' => 'Ссылка в 1С',
            'warehouse_id' => 'Склад',
        ];
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            if (!$this->date) {
                $this->date = date('Y-m-d H:i:s', time());
            }
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(JuridicalPerson::className(), ['id' => 'contractor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getDetails()
    {
        return $this->hasMany(Nomenclature::class, ['id' => 'detail_id'])
            ->viaTable('detail_to_realization', ['realization_id' => 'id']);
    }

    /**
     * Получает все реализации для контрагента за период
     * @param $contractor_id
     * @param $start
     * @param $end
     * @return array|null|ActiveRecord[]
     */
    public static function getFromContractor($contractor_id, $start, $end)
    {
        return self::find()
                ->select(['product_id'])
                ->andWhere(['contractor_id' => $contractor_id])
                ->andWhere(['BETWEEN', 'date', $start, $end])
                ->distinct()
                ->all() ?? null;
    }

    /**
     * Получает контрагентов за выбранный период
     * @param $start
     * @param null $end
     * @return array|null|ActiveRecord[]
     */
    public static function getContractors($start, $end = null)
    {
        return JuridicalPerson::find()
                ->joinWith(['realizations rel'])
                ->andWhere(['BETWEEN', 'rel.date', $start, $end])
                ->all() ?? null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouse()
    {
        return $this->hasOne(Warehouse::className(), ['id' => 'warehouse_id']);
    }

    /**
     * Получает среднюю цену продажи детали
     * @param int $product_id
     * @param string $start
     * @param string $end
     * @return float
     */
    public static function getAveragePrice($product_id, $start, $end)
    {

        $avg_cost = Realization::find()
                ->andWhere(['product_id' => $product_id])
                ->andWhere(['BETWEEN', 'date', $start, $end])
                ->average('amount') ?? 0;
        return round($avg_cost, 2);
    }

    public static function getSum($product_id, $start, $end)
    {
        return self::find()
                ->andWhere(['product_id' => $product_id])
                ->andWhere(['BETWEEN', 'date', $start, $end])
                ->sum('sum') ?? 0;
    }

    public static function getTotal($product_id, $start, $end)
    {
        return self::find()
                ->andWhere(['product_id' => $product_id])
                ->andWhere(['BETWEEN', 'date', $start, $end])
                ->sum('amount') ?? 0;
    }

    /**
     * Получает общую сумму проданных деталей
     * @param $detail_id
     * @param $start
     * @param $end
     * @return int|mixed
     */
    public static function getRealization($detail_id, $start, $end)
    {
        $query = self::find()
            ->joinWith(['details'])
            ->andWhere(['nomenclature.id' => $detail_id])
            ->andWhere(['BETWEEN', 'date', $start, $end]);

        $query_amount = clone $query;
        $amount = $query_amount->sum('detail_to_realization.total_amount') ?? 0;

        $query_num = clone $query;
        $num = $query_num->sum('number') ?? 0;

        return ['num' => round($num, 2), 'amount' => round($amount, 2)];
    }

    public function getTotalAmount()
    {
        return DetailToRealization::find()
                ->andWhere(['realization_id' => $this->id])
                ->sum('total_amount') ?? 0;
    }


    /**
     * Импорт файла
     * @param string $file Путь к файлу
     * @return array
     * @throws \yii\db\Exception
     */
    public static function importFile($file)
    {
        $xmlstring = file_get_contents($file);
        $xml = simplexml_load_string($xmlstring);
        $json = json_encode($xml);
        $data_array = json_decode($json, true);
        $errors = [];
        $warnings = [];
        $order_count = 0;
        $order_missing = 0;
        $product_count = 0;
        $not_vendor_code_count = 0;
        $contractors_added = [];

        $realizations = $data_array['Body']['Документ.РеализацияТоваровУслуг'] ?? null;

        if (!$realizations) {
            return ['success' => 'false', 'error' => 'Не найдена информация о реализации товаров'];
        }
        foreach ($realizations as $sale) {
            /** @var string $sale_date Дата заказа в формате 2019-02-15T16:25:00 */
            $sale_date = $sale['КлючевыеСвойства']['Дата'] ?? null;
            $sale_link1c = $sale['КлючевыеСвойства']['Ссылка'] ?? null;

            //Проверяем реализацию на существование
            if (isset($sale_link1c) && self::existsRealization($sale_link1c)) {
                Yii::info('Реализация уже существует, пропускаем.', 'test');
                continue;
            }

            $contractor_info = $sale['Контрагент'];
            $contractor_name = $contractor_info['Наименование'] ?? null;
            $contractor_name_full = $contractor_info['НаименованиеПолное'] ?? null;
            $contractor_link = $contractor_info['Ссылка'] ?? null;
            $contractor_inn = $contractor_info['ИНН'] ?? null;

            Yii::info('Импортируемый контрагент: ' . $contractor_name, 'test');
            Yii::info('ИНН импортируемого контрагента' . $contractor_inn, 'test');

            //Ищем контрагента
            /** @var JuridicalPerson $contractor */
            $contractor = JuridicalPerson::find()
                    ->andWhere(['inn' => $contractor_inn])
                    ->one() ?? null;

            if (!$contractor && $contractor_inn) {
                //Добавляем контрагента
                $contractor = new JuridicalPerson();
                $contractor->name = $contractor_name;
                $contractor->full_name = $contractor_name_full;
                $contractor->inn = $contractor_inn;
                $contractor->link_1c = $contractor_link;

                if (!$contractor->save()) {
                    Yii::error($contractor->errors, '_error');
                    array_push($errors, '<br>Ошибка добавления контрагента ' . $contractor_name_full);
                    //Если контрагент не добавлен то пропускаем заказ данного контрагента
                    continue;
                } else {
                    array_push($contractors_added, 'Контрагент ' . $contractor_name_full . ' добавлен в базу');
                }
            }

            Yii::info($contractor->toArray(), 'test');


            //Добавляем реализацию
            /** @var Realization $realization_model */
            $realization_model = new Realization();
            $realization_model->date = Functions::convert1CtoTimestampDate($sale_date);
            $realization_model->contractor_id = $contractor->id;
            $realization_model->link_1c = $sale_link1c;

            $transaction = Yii::$app->db->beginTransaction();

            if (!$realization_model->save()) {
                Yii::error($realization_model->errors, '_error');
                continue;
            }

            Yii::info('Общая инфа о реализации добавлена успешно', 'test');

            $goods = $sale['Товары'];
            $success_detail = 0; //Количесвто позиций в реализации прошедших проверки и попавших в базу

            //Перебираем товары в заказе
            foreach ($goods as $prod) {
                if (isset($prod['ДанныеНоменклатуры'])) {
                    //Если один товар в заказе

                    Yii::info('В заказе только одна деталь', 'test');
                    Yii::info($prod, 'test');

                    $full_name = $prod['ДанныеНоменклатуры']['Номенклатура']['НаименованиеПолное'] ?? null;
                    $name = $prod['ДанныеНоменклатуры']['Номенклатура']['Наименование'] ?? null;
                    if (isset($prod['ДанныеНоменклатуры']['Номенклатура']['Артикул'])) {
                        $vendor_code = $prod['ДанныеНоменклатуры']['Номенклатура']['Артикул'];
                    } else {
                        Yii::info('Отсутствует артикул, деталь пропущена', 'test');
                        $not_vendor_code_count += 1;
                        continue;
                    }
//                    $measure = $prod['ЕдиницаИзмерения']['Наименование'];
                    $count = $prod['Количество'];
                    $price = $prod['Цена'];

                    //Ищем товар в базе
                    /** @var Nomenclature $product_model */
                    $product_model = Nomenclature::find()
                            ->andWhere(['own_vendor_code' => $vendor_code])
                            ->one() ?? null;
                    if (!$product_model) {
                        if ($full_name) {
                            $product_name = $full_name;
                        } elseif ($name) {
                            $product_name = $name;
                        } else {
                            $product_name = 'Наименование не определено';
                        }
                        Yii::info('деталь не найдена в базе', 'test');
                        array_push($warnings,
                            ' <br>Товар ' . $product_name . ' (' . $vendor_code . ') не найден в базе');
                    } else {
                        //Добавляем инфу о релизации в detail_to_realization
                        $dtr_model = new DetailToRealization();
                        $dtr_model->realization_id = $realization_model->id;
                        $dtr_model->detail_id = $product_model->id;
                        $dtr_model->number = $count;
                        $dtr_model->price = $price;
                        $dtr_model->total_amount = round($price * $count, 2);

                        Yii::info('Реализация: ' . $dtr_model->realization_id, 'test');

                        if (!$dtr_model->save()) {
                            Yii::error($realization_model->errors, '_error');
                            continue;
                        }

                        Yii::info('Сохранение позиции ' . $dtr_model->id
                            . ' в реализации ' . $realization_model->id
                            . ' прошло успешно',
                            'test');

                        $product_count += 1;
                        $success_detail += 1;
                    }
                } else {
                    //Если в заказе больше одного товара
                    Yii::info('В заказе несколько деталей', 'test');

                    foreach ($prod as $product) {
                        Yii::info($product, 'test');
                        $full_name = $product['ДанныеНоменклатуры']['Номенклатура']['НаименованиеПолное'] ?? null;
                        $name = $product['ДанныеНоменклатуры']['Номенклатура']['Наименование'] ?? null;
                        if (isset($product['ДанныеНоменклатуры']['Номенклатура']['Артикул'])) {
                            $vendor_code = $product['ДанныеНоменклатуры']['Номенклатура']['Артикул'];
                        } else {
                            Yii::info('Отсутствует артикул, деталь пропущена', 'test');
                            $not_vendor_code_count += 1;
                            continue;
                        }
//                        $link_1c = $product['ДанныеНоменклатуры']['Номенклатура']['Ссылка'] ?? null;
//                        $measure = $product['ЕдиницаИзмерения']['Наименование'];
                        $count = $product['Количество'];
                        $price = $product['Цена'];


                        //Ищем товар в базе
                        /** @var Nomenclature $product_model */
                        $product_model = Nomenclature::find()
                                ->andWhere(['own_vendor_code' => $vendor_code])
                                ->one() ?? null;
                        if (!$product_model) {
                            Yii::info('Деталь не найдена в базе', 'test');
                            if ($full_name) {
                                $product_name = $full_name;
                            } elseif ($name) {
                                $product_name = $name;
                            } else {
                                $product_name = 'Наименование не определено';
                            }
                            array_push($warnings,
                                'Деталь: ' . $product_name . ' (' . $vendor_code . ') не найдена в базе');
                        } else {
                            //Добавляем инфу о релизации в detail_to_realization
                            $dtr_model = new DetailToRealization();
                            $dtr_model->realization_id = $realization_model->id;
                            $dtr_model->detail_id = $product_model->id;
                            $dtr_model->number = $count;
                            $dtr_model->price = $price;
                            $dtr_model->total_amount = round($price * $count, 2);

                            if (!$dtr_model->save()) {
                                Yii::error($realization_model->errors, '_error');
                                continue;
                            }
                            Yii::info('Сохранение позиции ' . $dtr_model->id . ' в реализации ' . $realization_model->id . ' прошло успешно',
                                'test');

                            $product_count += 1;
                            $success_detail += 1;
                        }
                    };
                }

            }
            if ($success_detail == 0){
                Yii::info('Сохранение реализации отменено. Отсутствуют позиции', 'test');
                $transaction->rollBack(); //Отменям сохранение общей инфы о реализации
                $order_missing += 1;
                continue;
            } else {
                Yii::info('Реализация ' . $realization_model->id . ' сохранена', 'test');
                $transaction->commit();
            }
            $order_count += 1;
        }

        if ($not_vendor_code_count){
            array_push($warnings,'Пропущено деталей (отсутствует Артикул): ' . $not_vendor_code_count);
        }

        if ($order_missing){
            array_push($warnings,'Пропущено реализаций (отсутствуют позиции): ' . $order_missing);
        }

        if (count($errors) > 0) {
            Yii::error($errors, 'error');
            $error = implode(', <br>', $errors);
        } else {
            $error = '';
        }

        if (count($warnings) > 0) {
            Yii::warning($warnings, 'warning');
            $warning = implode(', <br>', $warnings);
        } else {
            $warning = '';
        }

        return [
            'success' => 'true',
            'errors' => $error,
            'warnings' => $warning,
            'order_count' => $order_count,
            'product_count' => $product_count,
        ];
    }

    /**
     * Проверка реализации на наличие в базе
     * @param string $link Ссылка из 1С
     * @return bool
     */
    public static function existsRealization($link)
    {
        return Realization::find()
            ->andWhere(['link_1c' => $link])->exists();
    }

}
